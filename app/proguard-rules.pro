#-------------------------------------------定制化区域---------------------------------------
#把类归到一个目录下,最好是你的JAVA代码的包名(并非APP包名)
-repackageclasses com.app.openmtademo
# 指定外部模糊字典 proguard-chinese.txt 改为混淆文件名，下同
-obfuscationdictionary proguard-o0O.txt
# 指定class模糊字典
-classobfuscationdictionary proguard-o0O.txt
# 指定package模糊字典
-packageobfuscationdictionary proguard-o0O.txt

# 不做预校验，preverify是proguard的四个步骤之一，Android不需要preverify，去掉这一步能够加快混淆速度。
-dontpreverify

#GDT的aar混淆打包报错加此选项：
-dontoptimize

# 抛出异常时保留代码行号
-keepattributes SourceFile,LineNumberTable

# 抑制警告
-ignorewarnings
#---------------------------------1.实体类(model)---------------------------------
#-------------------------------------------------------------------------
#---------------------------------2.第三方包-------------------------------
#【fastjson】
-dontwarn com.alibaba.fastjson.**
-keep class com.alibaba.fastjson.**{*; }
#【okhttp3】
#okhttputils
-dontwarn com.zhy.http.**
-keep class com.zhy.http.**{*;}
#okhttp
-dontwarn okhttp3.**
-keep class okhttp3.**{*;}
#okio
-dontwarn okio.**
-keep class okio.**{*;}

#【XMB需要Keep的】
-dontwarn org.apache.**
-keep class org.apache.** {*;}
-dontwarn android.net.**
-keep class android.net.**{*;}

-dontwarn org.conscrypt.**
-keep class org.conscrypt.**{*;}

-dontwarn com.cazaea.*
-keep class com.cazaea.**{*;}

-dontwarn com.umeng.*
-keep class com.umeng.** {*;}

-dontwarn com.tencent.stat.*
-keep class com.tencent.stat.*{*;}
 -dontwarn com.tencent.mid.*
-keep class com.tencent.mid.*{*;}

-dontwarn com.iflytek.sunflower.*
-keep class com.iflytek.sunflower.**{*;}

#【反馈相关实体类-MTA9.6.3】
-dontwarn com.nil.sdk.ui.vo.*
-keep class com.nil.sdk.ui.vo.**{*;}
-dontwarn android.app.ui.FeedbackUI$FeedbackVo
-keep class android.app.ui.FeedbackUI$FeedbackVo{*;}
#【AliPay】
-dontwarn com.xmb.mta.util.*
-keep class com.xmb.mta.util.**{*;}
#【GDT】
-keep class com.qq.e.** {
    public protected *;
}
-keep class android.support.v4.**{
    public *;
}
-keep class android.support.v7.**{
    public *;
}
-dontwarn com.android.vy.*
-keep class com.android.vy.**{*;}
#【OpenPaySDK】
-dontwarn com.xvx.sdk.payment.*
-keep class com.xvx.sdk.payment.**{*;}

#【Butterknife】
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keepclasseswithmembernames class * {
   @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
   @butterknife.* <methods>;
}

#【Umeng】开发者中心  https://developer.umeng.com/docs/119267/detail/118584
-keep class com.umeng.** {*;}
-keep class com.uc.** {*;}
-keep class com.efs.** { *; }
-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep class com.zui.** {*;}
-keep class com.miui.** {*;}
-keep class com.heytap.** {*;}
-keep class a.** {*;}
-keep class com.vivo.** {*;}
#-keep public class [您的应用包名].R$*{
#public static final int *;
#}

#【支付宝】混淆：https://opendocs.alipay.com/open/54/104509
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.app.AuthTask{ public *;}
-keep class com.alipay.sdk.app.H5PayCallback {
    <fields>;
    <methods>;
}
-keep class com.alipay.android.phone.mrpc.core.** { *; }
-keep class com.alipay.apmobilesecuritysdk.** { *; }
-keep class com.alipay.mobile.framework.service.annotation.** { *; }
-keep class com.alipay.mobilesecuritysdk.face.** { *; }
-keep class com.alipay.tscenter.biz.rpc.** { *; }
-keep class org.json.alipay.** { *; }
-keep class com.alipay.tscenter.** { *; }
-keep class com.ta.utdid2.** { *;}
-keep class com.ut.device.** { *;}
-dontwarn com.ta.utdid2.**
-dontwarn com.ut.device.**
-dontwarn com.alipay.mobilesecuritysdk.**
-dontwarn com.alipay.security.**
-dontwarn android.net.SSLCertificateSocketFactory

# 【AndroidX】
-keep class com.google.android.material.** {*;}
-keep class androidx.** {*;}
-keep public class * extends androidx.**
-keep interface androidx.** {*;}
-keep @androidx.annotation.Keep class *
-keepclassmembers class * {
    @androidx.annotation.Keep *;
}
-dontwarn com.google.android.material.**
-dontnote com.google.android.material.**
-dontwarn androidx.**

#【微信支付】混淆：https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Access_Guide/Android.html
-keep class com.tencent.mm.opensdk.** { *;}
-keep class com.tencent.wxop.** { *;}
-keep class com.tencent.mm.sdk.** { *;}

# 【GreenDao】
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties { *; }
# If you DO use SQLCipher:
-keep class org.greenrobot.greendao.database.SqlCipherEncryptedHelper { *; }
# If you do NOT use SQLCipher:
-dontwarn net.sqlcipher.database.**
# If you do NOT use RxJava:
-dontwarn rx.**

# 不混淆Parcelable
-keep class * implements android.os.Parcelable {
public static final android.os.Parcelable$Creator *;
}
# 不混淆Serializable
-keep class * implements java.io.Serializable {*;}
-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {*;}

# 不混淆Csj穿山甲广告类：
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
-keep class com.bytedance.sdk.openadsdk.** {*;}
-keep public interface com.bytedance.sdk.openadsdk.downloadnew.** {*;}
-keep class com.pgl.sys.ces.* {*;}

# 不混淆Agentweb
-keep class com.just.agentweb.** {*;}
-dontwarn com.just.agentweb.**
-keepclassmembers class com.just.agentweb.sample.common.AndroidInterface{ *; }

# 不混淆ViewBinding
-keep class * implements androidx.viewbinding.ViewBinding {*;}
-keepclassmembers public class * extends androidx.lifecycle.ViewModel {
    public <init>(...);
}

# 不混淆UltimateBarX
-keep class com.zackratos.ultimatebarx.ultimatebarx.** { *; }
-keep public class * extends androidx.fragment.app.Fragment { *; }

# 不混淆Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep class * extends com.bumptech.glide.module.AppGlideModule {
 <init>(...);
}
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keep class com.bumptech.glide.load.data.ParcelFileDescriptorRewinder$InternalRewinder {
  *** rewind();
}

# 不混淆PictureSelector
-keep class com.luck.picture.lib.** { *; }
# use Camerax
-keep class com.luck.lib.camerax.** { *; }
# use uCrop
-dontwarn com.yalantis.ucrop**
-keep class com.yalantis.ucrop** { *; }
-keep interface com.yalantis.ucrop** { *; }

# 不混淆实体类所在的包
-keep class **.bean.**{ *; }
-keep class **.entity.**{ *; }
-keep class **.net.**{*;}
-keep public class * extends **.mvp.**

#不混淆ShapeBlurView高斯模糊库
-keep class android.support.v8.renderscript.** { *; }
-keep class androidx.renderscript.** { *; }

#不混淆JunkCode垃圾代码插件（下面配置为androidJunkCode里面的packageBase）：
-keep class com.androidx.MtaDemo.** {*;}

#不混淆DialogX库：
-keep class com.kongzue.dialogx.** { *; }
-dontwarn com.kongzue.dialogx.**
# 额外的，建议将 android.view 也列入 keep 范围：
-keep class android.view.** { *; }
# 若启用模糊效果，请增加如下配置：
-dontwarn androidx.renderscript.**
-keep public class androidx.renderscript.** { *; }

#-------------------------------------------------------------------------
#---------------------------------3.与 js 互相调用的类------------------------
#-------------------------------------------------------------------------
#---------------------------------4.反射相关的类和方法-----------------------
#----------------------------------------------------------------------------
#---------------------------------以下基本不用动---------------------------------------------
#---------------------------------基本指令区----------------------------------
# 混淆的压缩比例，0-7
-optimizationpasses 5
# 指定不去忽略非公共的库的类的成员
-dontskipnonpubliclibraryclassmembers
# 指定混淆是采用的算法
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*

#----------------------------------------------------------------------------
#---------------------------------默认保留区---------------------------------
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends android.view.View
-keep public class com.android.vending.licensing.ILicensingService
-keep class android.support.** {*;}
-keepclasseswithmembernames class * {
native <methods>;
}
-keepclassmembers class * extends android.app.Activity{
public void *(android.view.View);
}
-keepclassmembers enum * {
public static **[] values();
public static ** valueOf(java.lang.String);
}
-keep public class * extends android.view.View{
*** get*();
void set*(***);
public <init>(android.content.Context);
public <init>(android.content.Context, android.util.AttributeSet);
public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclasseswithmembers class * {
public <init>(android.content.Context, android.util.AttributeSet);
public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keep class * implements android.os.Parcelable {
public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class * implements java.io.Serializable {
static final long serialVersionUID;
private static final java.io.ObjectStreamField[] serialPersistentFields;
private void writeObject(java.io.ObjectOutputStream);
private void readObject(java.io.ObjectInputStream);
java.lang.Object writeReplace();
java.lang.Object readResolve();
}
-keep class **.R$* {
*;
}
-keepclassmembers class * {
void *(**On*Event);
}
# 保留注解中的参数
-keepattributes *Annotation*
##################不混淆Android Keep##################
-keep class android.support.annotation.Keep
# 不混淆使用了注解的类及类成员
-keep @android.support.annotation.Keep class * {*;}
# 如果类中有使用了注解的方法，则不混淆类和类成员
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}
# 如果类中有使用了注解的字段，则不混淆类和类成员
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}
# 如果类中有使用了注解的构造函数，则不混淆类和类成员
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}

##################不混淆Android X Keep#################
-keep class androidx.annotation.Keep
# 不混淆使用了注解的类及类成员
-keep @androidx.annotation.Keep class * {*;}
# 如果类中有使用了注解的方法，则不混淆类和类成员
-keepclasseswithmembers class * {
    @androidx.annotation.Keep <methods>;
}
# 如果类中有使用了注解的字段，则不混淆类和类成员
-keepclasseswithmembers class * {
    @androidx.annotation.Keep <fields>;
}
# 如果类中有使用了注解的构造函数，则不混淆类和类成员
-keepclasseswithmembers class * {
    @androidx.annotation.Keep <init>(...);
}
##################不混淆GreenDao Keep#################
-keep class org.greenrobot.greendao.annotation.Keep
# 不混淆使用了注解的类及类成员
-keep @org.greenrobot.greendao.annotation.Keep class * {*;}
# 如果类中有使用了注解的方法，则不混淆类和类成员
-keepclasseswithmembers class * {
    @org.greenrobot.greendao.annotation.Keep <methods>;
}
# 如果类中有使用了注解的字段，则不混淆类和类成员
-keepclasseswithmembers class * {
    @org.greenrobot.greendao.annotation.Keep <fields>;
}
# 如果类中有使用了注解的构造函数，则不混淆类和类成员
-keepclasseswithmembers class * {
    @org.greenrobot.greendao.annotation.Keep <init>(...);
}

#----------------------------------------------------------------------------
#---------------------------------webview------------------------------------
-keepclassmembers class fqcn.of.javascript.interface.for.Webview {
public *;
}
-keepclassmembers class * extends android.webkit.WebViewClient {
public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
public boolean *(android.webkit.WebView, java.lang.String);
}
-keepclassmembers class * extends android.webkit.WebViewClient {
public void *(android.webkit.WebView, jav.lang.String);
}
#----------------------------------------------------------------------------
