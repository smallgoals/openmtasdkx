package com.android.MtaDemo;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.vy.SplashUI;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AutoUpdateUtil;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.DensityUtils;
import com.nil.sdk.utils.ShareAPP2FriendUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.ZzAdUtils;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XMBApiCallback;
import com.xmb.mta.util.XmbOnlineConfigAgent;
import com.xmb.mta.vo.OnlineConfigBean;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import androidx.annotation.RequiresApi;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 加密/解密工具测试（Java、C、服务器）<br>
 */
@Deprecated
@SuppressLint({"SetTextI18n","DefaultLocale"})
public class MtaMainActivity extends BaseAppCompatActivity {
    private static final String TAG = MtaMainActivity.class.getSimpleName();
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etAppKey)
    EditText etAppKey;

    @BindView(R.id.btnClear)
    Button btnClear;
    @BindView(R.id.btnCopy)
    Button btnCopy;
    @BindView(R.id.btnKpAd)
    Button btnKpAd;
    @BindView(R.id.btnClearCache)
    Button btnClearCache;

    @BindView(R.id.tvResult)
    TextView tvResult;

    String result = "";
    List<String> mResultAry = new ArrayList<>();
    int curLength = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setFullScreen();
        hideActionBar();
        setContentView(R.layout.activity_main_mta);
        ButterKnife.bind(this);
        initData();
        tvResult.setMovementMethod(ScrollingMovementMethod.getInstance());
        btnClear.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                initData();
                return true;
            }
        });
        btnCopy.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                MyMessageDialog.popDialog(getActivity(), "确定清除全部的输出结果数据？", "确认", "取消", new MyMessageDialog.DialogMethod() {
                    public void sure() {
                        initData();
                    }
                });
            return true;
            }
        });
        btnKpAd.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                BaseUtils.gotoActivity(getActivity(), MainActivity.class.getName());
                return true;
            }
        });
        btnClearCache.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                BaseUtils.clearAllCache();
                return true;
            }
        });

        BaseUtils.openLogOut(this); //开启调试模式

        //自定义参数正常支持
        BaseUtils.popMyToast(getActivity(), "自定义参数：dead_time="+ AdSwitchUtils.getInstance(getActivity()).getOnlineValue("dead_time"));
    }

    public void initData() {
        etAppKey.setText(XmbOnlineConfigAgent.getAppKey());

        tvResult.setText("");
        result = "";
        mResultAry = new ArrayList<>();

        preLineNums = 0;
        curLength = 0;
        tvResult.scrollTo(0,0);
    }
    public void clearData(){
        initData();

        etAppKey.setText("");
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // 有焦点时，视图大小才能正常获取
        if(hasFocus) {
            updateResultInfo();
        }
    }

    long beginTime,endTime;
    @RequiresApi(api = Build.VERSION_CODES.Q)
    @OnClick({R.id.btnQzUpdate, R.id.btnHcUpdate, R.id.btnClear, R.id.btnCopy,
            R.id.btnKpAd, R.id.btnCpAd, R.id.btnClearCache, R.id.btnShowDebugDlg,
            R.id.btnPay, R.id.btnAppUpdate, R.id.btnAppShare, R.id.btnT4
    })
    public void onViewClicked(View view) {
        beginTime = System.currentTimeMillis();
        String eKey = etAppKey.getText().toString().trim();
        final String bName = ((Button)view).getText().toString().trim();
        if(TextUtils.isEmpty(eKey)){
            eKey = XmbOnlineConfigAgent.getAppKey();
            etAppKey.setText(eKey);
        }

        //更新App_key
        XmbOnlineConfigAgent.setAppKey(eKey);

        final String app_key = eKey;
        int i = view.getId();
        if (i == R.id.btnQzUpdate) {//aes
            XMBApi.updateOnlineConfig(new XMBApiCallback<ArrayList<OnlineConfigBean>>() {
                @Override
                public void onFailure(Call call, Exception e) {
                    endTime = System.currentTimeMillis();
                    result = StringUtils.getExcetionInfo(e);
                    addResult(app_key, bName, result);
                }
                public void onResponse(ArrayList<OnlineConfigBean> obj, Call call, Response response) {
                    endTime = System.currentTimeMillis();
                    result = getMapString(XmbOnlineConfigAgent.ary2Map(obj));
                    addResult(app_key, bName, result);
                }
            }, app_key);
        } else if (i == R.id.btnHcUpdate) {
            if(XmbOnlineConfigAgent.isExpire(app_key)){//已到期
                XMBApi.updateOnlineConfig(new XMBApiCallback<ArrayList<OnlineConfigBean>>() {
                    @Override
                    public void onFailure(Call call, Exception e) {
                        endTime = System.currentTimeMillis();
                        result = StringUtils.getExcetionInfo(e);
                        addResult(app_key, bName, result);
                    }
                    public void onResponse(ArrayList<OnlineConfigBean> obj, Call call, Response response) {
                        endTime = System.currentTimeMillis();
                        result = getMapString(XmbOnlineConfigAgent.ary2Map(obj));
                        addResult(app_key, bName, result);
                    }
                }, app_key);
            }else{
                endTime = System.currentTimeMillis();
                result = getMapString(XmbOnlineConfigAgent.ary2Map(app_key));
                addResult(app_key, bName, result);
            }
        } else if (i == R.id.btnClear) {//clear
            clearData();
            return;
        } else if (i == R.id.btnCopy) {
            BaseUtils.openCopy(getActivity(), result, true);
            return;
        }else if (i == R.id.btnKpAd) {//广告相关
            BaseUtils.gotoActivity(getActivity(), SplashUI.class.getName());
            return;
        }else if (i == R.id.btnCpAd) {
            ZzAdUtils.openCpAd(this);
            return;
        }else if (i == R.id.btnClearCache) {
            //BaseUtils.clearAllCache();
            BaseUtils.clearXmbCache();
            clearData();
            return;
        }else if (i == R.id.btnShowDebugDlg) {
            BaseUtils.showInnerLog(this);
            return;
        }else if (i == R.id.btnPay) {
            BaseUtils.startActivity(PayMainActivity.class);
            return;
        }else if (i == R.id.btnAppUpdate) {
            //手动更新
            AutoUpdateUtil.checkUpdate(getActivity(), false);
            return;
        }else if (i == R.id.btnAppShare) {
            if(ShareAPP2FriendUtil.isSetUpOnlineConfig(getActivity())) {
                ShareAPP2FriendUtil.onClickShare(getActivity());
            }else{
                BaseUtils.popMyToast(getActivity(), "当前分享内容为空，请去在线参数中配置!");
            }
            return;
        }else if(i == R.id.btnT4){
            String msg = String.format(
                    "Legacy=%b,"+
                    "orders=%s,imei=%s,cacheDid=%s,myPath=%s",
                    Environment.isExternalStorageLegacy(),
                    OrderBeanV2.getOrderBeans(), ACacheUtils.getMyIMEI(),
                    Utils.getApp().getCacheDir().getAbsoluteFile(),
                    ACacheUtils.getCachePath()
            );
            BaseUtils.popMyToast(getActivity(), msg);
            System.out.println("msg-->"+msg);
            return;
        }
    }
    public String getMapString(Map<String, String> map){
        StringBuffer buf = new StringBuffer();
        for(String key : map.keySet()){
            buf.append("::").append(key).append("=").append(map.get(key)).append("\n");
        }
        String rr = buf.toString();
        if(StringUtils.isNullStr(rr)){
            rr = "数据为空\n";
        }
        return rr;
    }

    public void addResult(String app_key, String bName, String result){
        String curTime = DateUtils.getStringByFormat(new Date(), DateUtils.dateFormatYMDHMSS);

        long time = XmbOnlineConfigAgent.getLastUpdateTime(app_key, XmbOnlineConfigAgent.LAST_UPDATE_KEY);
        int cacheTime = XmbOnlineConfigAgent.getXmbCacheTime(app_key);
        if(time < 1){
            time = XmbOnlineConfigAgent.getLastUpdateTime(app_key, XmbOnlineConfigAgent.LAST_UPDATE_ERROR_KEY);
            cacheTime = XmbOnlineConfigAgent.XMB_ERROR_CACHE_TIME;
        }
        String udpateTime = DateUtils.getStringByFormat(new Date(time),DateUtils.dateFormatYMDHMS);

         String data = String.format("【%s】\n最近更新：%s/缓存时间：%d秒\nAPP_KEY：%s\n执行操作：%s/耗时：%d毫秒\n操作结果：\n%s\n",
                curTime,
                udpateTime, cacheTime,
                app_key,
                bName,(endTime-beginTime),
                result
        );

        curLength = data.length();
        mResultAry.add(data);
        updateResult(mResultAry);
    }

    int rLineH,rMaxLineNums,rH,preLineNums=0;
    public void updateResultInfo(){
        try {
            rH = tvResult.getHeight();
            rLineH = tvResult.getLineHeight();
            rMaxLineNums = rH/rLineH;
            String info = StringUtils.concat(",",rMaxLineNums,rLineH,rH,preLineNums);
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,"updateResultInfo-->"+info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void printResultInfo(TextView tv){
        try {
            String msg = String.format("updateResult-->LineCount=%d,LineH=%d,H=%d,W=%d,5dp=%d,12sp=%d\n" +
                            "Point(%.2f,%.2f)-Scale(%.2f,%.2f)\n" +
                            "Pivot(%.2f,%.2f)-Scroll(%d,%d)\n" +
                            "Rotation(%.2f,%.2f)-Translation(%.2f,%.2f)\n" +
                            "Measured(%d,%d)-MeasuredAndState(%d,%d)\n" +
                            "Point:t/b/l/r(%d,%d,%d,%d)",
                    tv.getLineCount(),tv.getLineHeight(),tv.getHeight(),tv.getWidth(),
                    DensityUtils.dp2px(this,5f),DensityUtils.sp2px(this,12f),
                    tv.getX(),tv.getY(),tv.getScaleX(),tv.getScaleY(),
                    tv.getPivotX(),tv.getPivotY(),tv.getScrollX(),tv.getScrollY(),
                    tv.getRotationX(),tv.getRotationY(),tv.getTranslationX(),tv.getTranslationY(),
                    tv.getMeasuredWidth(),tv.getMeasuredHeight(),tv.getMeasuredWidthAndState(),tv.getMeasuredHeightAndState(),
                    tv.getTop(),tv.getBottom(),tv.getLeft(),tv.getRight()
            );
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateResult(final List<String> r){
        CrashApp.mainThread(new Runnable() {
            public void run() {
                int size = r.size();
                StringBuilder buf = new StringBuilder();
                for (int i = size - 1; i >= 0; i--) {
                    buf.append(r.get(i));
                }

                //tvResult.setText(buf.toString());

                // 创建一个 SpannableString对象
                SpannableString sp = new SpannableString(buf.toString());
                //设置背景颜色
                sp.setSpan(new ForegroundColorSpan(Color.RED), 0 ,curLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                //sp.setSpan(new BackgroundColorSpan(Color.YELLOW), 0 ,curLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvResult.setText(sp);
                tvResult.scrollTo(0,0);
            }
        });
    }

    /*long exitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == 4 && event.getAction() == 0) {
            if(System.currentTimeMillis() - exitTime > 2000L) {
                NbToast.showToast("再按一次返回退出程序");
                exitTime = System.currentTimeMillis();
            } else {
                BaseUtils.onAutoExit(getActivity());
            }

            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }*/

    public String getSubString(String msg){
        return getSubString(msg, 100);
    }
    public String getSubString(String msg, int subLen){
        return getSubString(msg, subLen, "...", 1);
    }
    public String getSubString(String msg, int subLen, String padStr, int type){
        String r = msg;
        if(msg != null && msg.length() > subLen){
            int strLen = msg.length();
            int padLen = padStr.length();
            int curLen = subLen - padLen;
            if (type == 0) {//left
                r = padStr + msg.substring(strLen - curLen, strLen);
            } else if (type == 1) {//mid
                int mid = subLen / 2 + 1;
                int padMid = padLen / 2 + 1;
                int left = mid - padMid;
                int right = subLen - left - padLen;
                r = msg.substring(0, left) + padStr + msg.substring(strLen - right, strLen);
            } else if (type == 2) {//right
                r = msg.substring(0, curLen) + padStr;
            }
        }
        return r;
    }
}
