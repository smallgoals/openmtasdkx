package com.android.MtaDemo;

import android.app.Activity;
import android.app.Application;

import com.android.vy.SplashUI;
import com.nil.crash.utils.ActivityMonitor;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.utils.AppUtils;

public class MyApp extends CrashApp {
    @Override
    public void onCreate() {
        super.onCreate();

        /*
        // 手动配置开关信息
        String ssp = AdSwitchUtils.getInstance(this).getVersionSpValue();
        if(StringUtils.isNullStr(ssp)) {
            AdSwitchUtils.getInstance(this).setCurChannelValue(
                    Ads.Qq.open(), Sws.Kp.open(),
                    Sws.Hf.open(), Sws.Cp.open());
        }*/

        ActivityMonitor.getInstance(this).addActivityMonitor(new ActivityMonitor.DefActivityMonitor() {
            @Override
            public void bootSplash(Application app, Activity activity) {
                SplashUI.start(activity,true);
            }
        });

        if(AppUtils.hasAgreePrivacyPolicy()){
            SplashUI.preInitGdt(this);
        }
    }
}
