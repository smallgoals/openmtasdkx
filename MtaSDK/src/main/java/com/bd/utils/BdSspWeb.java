package com.bd.utils;

import com.android.core.XSEUtils;

/**
 * Created by Administrator on 2018/06/07.
 */
public class BdSspWeb {
    public static final String bdpd	=	XSEUtils.decode("c4LydGIXxXn4bhf9foxN91fItc2iMnMKJ8SoNeqVcqbs%2BvVJpMnuiOy3EQqOfPAmwftqmVT");	//本地频道
    public static final String mmdsppd	=	XSEUtils.decode("ywR0gCI4spGzrIERqc%2F3ALEjhJZ1Wqo7MGQtcL5ra7UWBzyVjmGQvR4zFj8C8RrK4xI2kgO");	//萌萌哒-视频频道
    public static final String tjsppd	=	XSEUtils.decode("bQCQ3zO8o%2B1Y1vHCHg5D48%2Fyk1HHFxIFIfd%2F3tR7a8R21xZeQNf52yzcSsO2cqW1R9xNjfw");	//推荐-视频频道
    public static final String dmpd	=	XSEUtils.decode("Zb6hD3HTjE6RU%2F0UFK%2BWmAZQGz1Sgmzw%2BWX2HtyuuUZezMxAeiTp2Sg0rE5duCHMOUK440G");	//动漫频道
    public static final String jkpd	=	XSEUtils.decode("rjgNXZhhVgKU7uW5WKXXf2IO9XSvNJtuHK9cJ%2FYWKBi2wJCzc40zNFQczNlcDkT0x6fFdeT");	//健康频道
    public static final String mypd	=	XSEUtils.decode("pfZGVWNhyzUtGXk99rau3IGtGiZ%2BkwKmMIQA5TH%2BbB0KqizWE%2FGoR%2FZY6SIg%2F1GNIcE1Kfq");	//母婴频道
    public static final String yxpd	=	XSEUtils.decode("bOY37MqzzKYmI86L3530gnP%2B5BRNGgI6u6lSFyvOUM%2Be0sSJI0C6jkhG9VXciHkmwqSwWLk");	//游戏频道
    public static final String whpd	=	XSEUtils.decode("u4vbjA5vdHHVdvHYDQSOnRcUED5UWnjL%2FXeOFAfaxUqkVULU8HrIXFYn%2FO12%2FqLUJEHBgKP");	//文化频道
    public static final String shpd	=	XSEUtils.decode("zgjGox4y9h1xMGf6VI2e9uIgokCaNsXGgITtq2V6KhnxU894gMVjTkCLJJTufpKnt3vZjsa");	//生活频道
    public static final String nrpd	=	XSEUtils.decode("M26gdlqSBjWXEq3cM3eoIxMhOXPdwukR6VMAFWZr7U%2FZXQYlXyERn%2FyMXd3OgvxnROISV%2FC");	//女人频道
    public static final String sppd	=	XSEUtils.decode("XL42kkWHEktiV3v0X8KNnHfKMtoN8Dg%2FEwScQAqeCEqCb8FSXrt9JYPM5bP7UbeQumfBEfw");	//视频频道
    public static final String gxpd	=	XSEUtils.decode("IxoZkhn0wwTxLftXVsUGWA816LSXS42rUnpjvX7PkN7jYbNXM1%2F4wDSiikv4AR%2BFN0Dxqdi");	//搞笑频道
    public static final String mnpd	=	XSEUtils.decode("0TczL67doTa3dWrjd2YiRXO%2FoSKTnCj8%2BhnTz6WQGAewB9hPRJTCc818dSeGnFOwgUN89rJ");	//美女频道
    public static final String tjpd	=	XSEUtils.decode("KzCVmbjmba3CSQZueLbg7py1SaJI%2Fo8nSUhTDiqrcZ9ahHgGX%2BBSQy4mVmGbevI5%2FbdwSEs");	//推荐频道
    public static final String rdpd	=	XSEUtils.decode("sOJP7YR%2BjZ2KnWnoaC0CWJ7KdoRY06vcguS3LKvvpMibEssDE5UBiKo8Dmf%2BCOnDVx%2BuMTn");	//热点频道
    public static final String kjpd	=	XSEUtils.decode("KghSzUfd8ypyhjJFBlR6a9D6p9GTJa8XtDYoIZTE6v50sh5D%2FMqLhsP0Lf0rSb5mikuGXnj");	//科技频道
    public static final String jspd	=	XSEUtils.decode("zwlkCYyF0CJrVH3S0njjCAp2vLOoWLmY4NUlrgZWvJTlcgIPOKJoE509HUyOUDBf6QtsXo4");	//军事频道
    public static final String sspd	=	XSEUtils.decode("Rggj3KaZQKEc%2B%2F8hlZONYIB1f8%2BOtD4lCxajUZiOxpVNIUzsScZlysGQMpv8iKRqCqg6D%2BG");	//时尚频道
    public static final String fcpd	=	XSEUtils.decode("OTOhscrBf5%2BwvJ9Mqj08fmx9zGuVffIGNeDpDHYBOh5%2B%2FdClq5gQMMi4lLU1QiBd2EhiVAn");	//房产频道
    public static final String qcpd	=	XSEUtils.decode("tBNZ4OysXkOn91n%2Fj9U3GGl9bfopCbeA6wtZlDsKBYQicGabj%2BTsZ6pE3%2B2O7XnzELoTw%2BV");	//汽车频道
    public static final String cjpd	=	XSEUtils.decode("6pBxAcrRm0588odkYkLkrJd01zp1J%2BrsVIqGeHaVTb1yXCNGRNDFtDzgNlFkq6diV32N1nr");	//财经频道
    public static final String sjpd	=	XSEUtils.decode("9Kbym8m8hyY0SLbR6FlaTMky4vo2qx7ch2VspUgT2jQrtSqmja9PpiDX5sZ81uH9qjRbTP%2F");	//手机频道
    public static final String typd	=	XSEUtils.decode("czjcdXDQei%2FgTED1UE6f0KOOcDPTbsPDjM64jojA4lZr8pshuHqmNLGwxuFdYPRS5hKmBRh");	//体育频道
    public static final String ylpd	=	XSEUtils.decode("jSBs7J232a30DRgX%2FlyOFxPzXRygzgRzu%2BPoIqWRLWVZCQtV3WGe2xKEo2kep4c23ZB7WVv");	//娱乐频道
    public static final String jhpd	=	XSEUtils.decode("ltHGOIViJJTWR5aE8%2FeMRkt5yFL6DVtJodfbhxVvuuVx3X%2FPa6NK8fudzb9IZUXz9wI0HpX");	//聚合频道
}
