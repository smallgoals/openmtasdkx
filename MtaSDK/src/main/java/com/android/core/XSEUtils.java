package com.android.core;

import android.content.Context;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.Utils;

public class XSEUtils {
    // Java上层对外接口：
    public static boolean init(Context context, String k1, String k2){
        return XSE.IAgent(context, k1, k2);
    }
    public static boolean init(Context context){
        return init(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign));
    }
    public static boolean free(){
        return XSE.FAgent();
    }

    public static String decode(Context context, String str) {
        return XSE.DAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    // Java上层对外接口（迷惑用的假接口）：
    public static String encode(Context context, String str) {
        return XSE.EAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    public static String encode(String str) {
        return encode(Utils.getApp(), str);
    }

    public static String mtaDecode(Context context, String str) {
        return XSE.MtaDAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    public static String mtaEncode(Context context, String str) {
        return XSE.MtaEAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    public static String dataDecode(Context context, String str) {
        return XSE.DataDAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    public static String dataEncode(Context context, String str) {
        return XSE.DataEAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    public static String mixDecode(Context context, String str) {
        return XSE.MixDAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }
    public static String mixEncode(Context context, String str) {
        return XSE.MixEAgent(context, getS(context, R.string.jsk_sign), getS(context, R.string.pkg_sign), str);
    }

    public static String decode(String str) {
        return decode(Utils.getApp(), str);
    }
    public static String mtaDecode(String str) {
        return mtaDecode(Utils.getApp(), str);
    }
    public static String mtaEncode(String str) {
        return mtaEncode(Utils.getApp(), str);
    }
    public static String dataDecode(String str) {
        return dataDecode(Utils.getApp(), str);
    }
    public static String dataEncode(String str) {
        return dataEncode(Utils.getApp(), str);
    }
    public static String mixDecode(String str) {
        return mixDecode(Utils.getApp(), str);
    }
    public static String mixEncode(String str) {
        return mixEncode(Utils.getApp(), str);
    }

    private static String getS(Context context, Object obj) {
        String result = null;
        if (obj instanceof String) {
            result = (String) obj;
        } else if (obj instanceof Integer) {
            try {
                result = context.getResources().getString((Integer) obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
