package com.android.core;

import android.content.Context;
import androidx.annotation.Keep;

@Keep
public class XSE {
    //加载底层库：
    static {
        System.loadLibrary("xo");
    }

    //C底层通用接口：
    public static native boolean IAgent(Context context, String jsk_sign, String pkg_sign);
    public static native boolean FAgent();

    //字符解密（核心）
    public static native String DAgent(Context context, String jsk_sign, String pkg_sign, String data);

    //MTA加解密
    public static native String MtaDAgent(Context context, String jsk_sign, String pkg_sign, String data);
    public static native String MtaEAgent(Context context, String jsk_sign, String pkg_sign, String data);

    //数据加解密
    public static native String DataDAgent(Context context, String jsk_sign, String pkg_sign, String data);
    public static native String DataEAgent(Context context, String jsk_sign, String pkg_sign, String data);

    //综合加解密
    public static native String MixDAgent(Context context, String jsk_sign, String pkg_sign, String data);
    public static native String MixEAgent(Context context, String jsk_sign, String pkg_sign, String data);

    //C底层算法混淆接口：
    public static native String EAgent(Context context, String jsk_sign, String pkg_sign, String data);
    public static native String GetKeyAgent(Context context, String jsk_sign, String pkg_sign);
    public static native boolean ConfigAgent(Context context, String jsk_sign, String pkg_sign);
    public static native String DecodeAgent(Context context, String jsk_sign, String pkg_sign, String data);
    public static native String EncodeAgent(Context context, String jsk_sign, String pkg_sign, String data);

    //C底层功能混淆接口：
    public static native String SignAgent(Context context, String jsk_sign, String pkg_sign);
    public static native boolean LoginAgent(Context context, String jsk_sign, String pkg_sign, String user, String pwd);
    public static native boolean LogoutAgent(Context context, String jsk_sign, String pkg_sign);
    public static native String GetWebApiAgent(Context context, String jsk_sign, String pkg_sign, String data);
}
