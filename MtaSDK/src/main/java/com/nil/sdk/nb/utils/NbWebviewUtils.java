package com.nil.sdk.nb.utils;

import android.webkit.WebSettings;
import android.webkit.WebView;

import com.just.agentweb.AgentWeb;

/**
 * Webview工具类<br>
 */
public class NbWebviewUtils {
    /**
     * 关闭Webview漏洞<br>
     * @param webSettings
     */
    public static void closeWebviewBug(WebSettings webSettings){
        try {
            //你不知道的 Android WebView 使用漏洞 - 简书  https://www.jianshu.com/p/3a345d27cd42
            if (webSettings != null) {
                //Webview明文存储密码风险；--禁用
                webSettings.setSavePassword(false);

                //Webview Fi1e同源策略绕过漏洞;--禁用 file 协议；
                webSettings.setAllowFileAccess(false);

                /*
                // 需要使用 file 协议
                webSettings.setAllowFileAccessFromFileURLs(false);
                webSettings.setAllowUniversalAccessFromFileURLs(false);
                webSettings.setAllowFileAccess(true);
                // 禁止 file 协议加载 JavaScript
                if (url.startsWith("file://")){
                    webSettings.setJavaScriptEnabled(false);
                } else {
                    webSettings.setJavaScriptEnabled(true);
                }
                */
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void closeWebviewBug(AgentWeb agentWeb){
        try {
            if(agentWeb != null){
                WebSettings webSettings = agentWeb.getAgentWebSettings().getWebSettings();
                closeWebviewBug(webSettings);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void removeJavascriptInterface(WebView webView){
        if(webView != null){
            try {
                //移除有风险的Webview系统隐藏接口：
                webView.removeJavascriptInterface("searchBoxJavaBridge_");
                webView.removeJavascriptInterface("accessibility");
                webView.removeJavascriptInterface("accessibilityTraversal");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 移除Webview风险代码和禁用WebSettings漏洞<br>
     */
    public static void removeRiskCode(WebView webView){
        if(webView != null){
            try {
                //移除有风险的Webview系统隐藏接口：
                removeJavascriptInterface(webView);

                //Webview明文存储密码风险；--禁用
                //Webview Fi1e同源策略绕过漏洞;--禁用 file 协议；
                closeWebviewBug(webView.getSettings());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
