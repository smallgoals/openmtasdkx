package com.nil.sdk.nb.utils;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.utils.Spu;

/**
 * @declaration Log调试日志<br>
 * @author ksA2y511xh@163.com<br>
 *         2016-7-7 下午7:14:29<br>
 */
public class NbLog {
	public static final String TAG = NbLog.class.getSimpleName();
	public static final android.content.Context mContext = Utils.getApp();
	private static boolean isDebug = /*BuildConfig.DEBUG && */com.nil.sdk.ui.BaseUtils.getIsDebug(mContext);

	public static void i(String msg, Object... args) {
		try {
			if (isDebug)
				if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, String.format(msg, args));
		} catch (java.util.MissingFormatArgumentException e) {
			LogUtils.eTag(TAG, "Nb.Log", e);
			if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, msg);
		}
	}

	public static void d(String msg, Object... args) {
		try {
			if (isDebug)
				if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, String.format(msg, args));
		} catch (java.util.MissingFormatArgumentException e) {
			LogUtils.eTag(TAG, "Nb.Log", e);
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, msg);
		}
	}
	
	public static void w(String msg, Object... args) {
		try {
			if (isDebug)
				LogUtils.wTag(TAG, String.format(msg, args));
		} catch (java.util.MissingFormatArgumentException e) {
			LogUtils.eTag(TAG, "Nb.Log", e);
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, msg);
		}
	}

	public static void e(String msg, Object... args) {
		try {
			if (isDebug)
				LogUtils.eTag(TAG, String.format(msg, args));
		} catch (java.util.MissingFormatArgumentException e) {
			LogUtils.eTag(TAG, "Nb.Log", e);
			LogUtils.eTag(TAG, msg);
		}
	}

	public static void e(String msg, Throwable t) {
		if (isDebug) {
			LogUtils.eTag(TAG, msg, t);
		}
	}

	public static void SysO(String msg) {
		if (isDebug) {
			System.out.println(TAG + ":" + msg);
		}
	}

	public static void SysE(String msg) {
		if (isDebug) {
			System.err.println(TAG + ":" + msg);
		}
	}

	public static void showToastInThread(Object msg) {
		if (isDebug) {
			NbToast.showToastInThread(msg);
		}
	}
}
