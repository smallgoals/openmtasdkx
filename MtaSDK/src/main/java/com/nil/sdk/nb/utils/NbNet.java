package com.nil.sdk.nb.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.TrafficStats;

public class NbNet {
	private static long lastTotalRxBytes = 0;
	private static long lastTimeStamp = 0;
	private static long getTotalRxBytes(Context ctx) {
		long rxByte = 0;
		if(TrafficStats.getUidRxBytes(ctx.getApplicationInfo().uid) != TrafficStats.UNSUPPORTED){
			rxByte = TrafficStats.getTotalRxBytes();
		}
		return rxByte;
	}
	public static long getSpeedV(Context ctx){
		long nowTotalRxBytes = getTotalRxBytes(ctx);
		long nowTimeStamp = System.currentTimeMillis();
		long speed = 0;
		try{
			speed = ((nowTotalRxBytes - lastTotalRxBytes) * 1000 / (nowTimeStamp - lastTimeStamp));// 毫秒转换
		}catch(Exception e){e.printStackTrace();};
		
		lastTimeStamp = nowTimeStamp;
		lastTotalRxBytes = nowTotalRxBytes;
		return speed;
	}
	private static final double KB = 1024.0;
	private static final double MB = KB * KB;
	private static final double GB = KB * KB * KB;
	@SuppressLint("DefaultLocale")
	private static String getFileSize(long size) {
		String fileSize;
		if (size < KB)
			fileSize = String.format("%.02f", size / 1.0) + "B";
		else if (size < MB)
			fileSize = String.format("%.02f", size / KB) + "K";
		else if (size < GB)
			fileSize = String.format("%.02f", size / MB) + "M";
		else
			fileSize = String.format("%.02f", size / GB) + "G";
		return fileSize/*fileSize.replaceAll(".0", "")*/;
	}
	public static String getSpeed(Context ctx){
		return getFileSize(getSpeedV(ctx))+"/S";
	}
	
	/*private Handler mHandler;
	TimerTask task = new TimerTask() {
		public void run() {
			showNetSpeed();
		}
	};
	private void showNetSpeed() {
		long nowTotalRxBytes = getTotalRxBytes();
		long nowTimeStamp = System.currentTimeMillis();
		long speed = ((nowTotalRxBytes - lastTotalRxBytes) * 1000 / (nowTimeStamp - lastTimeStamp));// 毫秒转换

		lastTimeStamp = nowTimeStamp;
		lastTotalRxBytes = nowTotalRxBytes;

		Message msg = mHandler.obtainMessage();
		msg.what = 100;
		msg.obj = String.valueOf(speed) + " KB/s";

		mHandler.sendMessage(msg);// 更新界面
	}
	public void main(String[] args) {
		lastTotalRxBytes = getTotalRxBytes();
		lastTimeStamp = System.currentTimeMillis();
		new Timer().schedule(task, 1000, 2000); // 1s后启动任务，每2s执行一次
	}*/
}
