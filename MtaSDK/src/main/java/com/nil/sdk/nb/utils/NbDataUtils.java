package com.nil.sdk.nb.utils;

import com.google.gson.Gson;
import com.nil.sdk.utils.ACacheUtils;

import java.lang.reflect.Type;

public class NbDataUtils {
    public static <T> T getMyAppFileCacheObject(String key, Class<T> cls){
        return getMyType(ACacheUtils.getAppFileCacheObject(key), cls);
    }
    public static <T> T getMyAppCacheObject(String key, Class<T> cls){
        return getMyType(ACacheUtils.getAppCacheObject(key), cls);
    }
    public static <T> T getMyCacheObject(String key, Class<T> cls){
        return getMyType(ACacheUtils.getCacheObject(key), cls);
    }
    private static <T> T getMyTypeT(Object o, Class<T> cls) throws Exception {
        Gson gson = new Gson();
        return gson.fromJson(gson.toJson(o), cls);
    }
    public static <T> T getMyType(Object o, Class<T> cls){
        try{
            return getMyTypeT(o, cls);
        } catch (Exception e){
            return null;
        }
    }

    public static <T> T getMyAppFileCacheObject(String key, Type type){
        return getMyType(ACacheUtils.getAppFileCacheObject(key), type);
    }
    public static <T> T getMyAppCacheObject(String key, Type type){
        return getMyType(ACacheUtils.getAppCacheObject(key), type);
    }
    public static <T> T getMyCacheObject(String key, Type type){
        return getMyType(ACacheUtils.getCacheObject(key), type);
    }
    private static <T> T getMyTypeT(Object o, Type type) throws Exception {
        Gson gson = new Gson();
        return gson.fromJson(gson.toJson(o), type);
    }
    public static <T> T getMyType(Object o, Type type){
        try{
            return getMyTypeT(o, type);
        } catch (Exception e){
            return null;
        }
    }
}
