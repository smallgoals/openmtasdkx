package com.nil.sdk.nb.utils;

import android.util.SparseArray;
import android.view.View;

/**
 * 比较规范独立的的ViewHolder.<br>
 * @see "http://www.cnblogs.com/tianzhijiexian/p/4157889.html"<br>
 * @date 2015/4/28<br>
 */
public class NbViewHolder {
	@SuppressWarnings("unchecked")
	/** I added a generic return type to reduce the casting noise in client code*/
	public static <T extends View> T get(View view, int id) {
		SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
		if (viewHolder == null) {
			viewHolder = new SparseArray<View>();
			view.setTag(viewHolder);
		}
		View childView = viewHolder.get(id);
		if (childView == null) {
			childView = view.findViewById(id);
			viewHolder.put(id, childView);
		}
		return (T) childView;
	}
	
    /*public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // init convertView by layout
            convertView = LayoutInflater.from(context.inflate(R.layout.item, parent, false));
        }
        ImageView imageView = ViewHolder.get(convertView, R.id.imageView_id); //call
        imageView.setImageResource(R.drawable.ic_launcher);
        return convertView;
    }*/
}
