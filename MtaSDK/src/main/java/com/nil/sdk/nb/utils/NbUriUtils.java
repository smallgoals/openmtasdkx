package com.nil.sdk.nb.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.FileProvider;

import com.blankj.utilcode.util.UriUtils;
import com.blankj.utilcode.util.Utils;

import java.io.File;

/**
 * 统一资源标识符工具类<br>
 * 用于兼容blankj的FileProvider被禁用，导致UriUtils.file2Uri失效的bug；<br>
 */
@SuppressLint("ObsoleteSdkInt")
public class NbUriUtils {
    /**
     *  文件转换为Uri对象<br>
     *  【fix】所有用到的Uri.fromFile-->NbUriUtils.fromFile<br>
     */
    public static Uri fromFile(File file){
        return fromFile(file, false);
    }
    public static Uri fromFile(File file, boolean isExternalApp){
        return file2Uri(file, isExternalApp);
    }

    /**
     *  文件转换为Uri对象<br>
     *  【fix】所有用到的UriUtils.file2Uri-->NbUriUtils.file2Uri<br>
     */
    public static Uri file2Uri(File file){
        return file2Uri(Utils.getApp(), file, false);
    }
    public static Uri file2Uri(File file, boolean isExternalApp){
        return file2Uri(Utils.getApp(), file, isExternalApp);
    }

    /**
     * @param isExternalApp 是否为外部APP，外部使用则用FileProvider路径，内部使用getAbsolutePath路径；<br>
     *                      外部场景：安装、卸载、调用其它APP打开<br>
     */
    public static Uri file2Uri(Context context, File file, boolean isExternalApp) {
        /*Uri fileUri = null;
        try {
            if (Build.VERSION.SDK_INT >= 24 && isExternalApp) {//N:7.0、24
                fileUri = getUriForFile24(context, file);
            } else {
                fileUri = Uri.parse("file://" + file.getAbsolutePath());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return fileUri;*/
        return UriUtils.file2Uri(file);
    }

    private static Uri getUriForFile24(Context context, File file) {
        //return FileProvider.getUriForFile(context,context.getPackageName()+".fileprovider", file);
        String authority = Utils.getApp().getPackageName() + ".utilcode.fileprovider";
        return FileProvider.getUriForFile(context, authority, file);
    }

    public static void grantPermissions(Context context, Intent intent, Uri uri, boolean writeAble) {
        int flag = Intent.FLAG_GRANT_READ_URI_PERMISSION;
        if (writeAble) {
            flag |= Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        }
        intent.addFlags(flag);
        java.util.List<ResolveInfo> resInfoList = context.getPackageManager()
                .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, flag);
        }
    }

    public static void setIntentData(Context context, Intent intent, File file, boolean writeAble) {
        if (Build.VERSION.SDK_INT >= 24) {
            intent.setData(file2Uri(context, file, true));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (writeAble) {
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        } else {
            intent.setData(NbUriUtils.fromFile(file));
        }
    }

    public static void setIntentDataAndType(Context context, Intent intent, String type, File file, boolean writeAble) {
        if (Build.VERSION.SDK_INT >= 24) {
            intent.setDataAndType(file2Uri(context, file, true), type);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (writeAble) {
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        } else {
            intent.setDataAndType(NbUriUtils.fromFile(file), type);
        }
    }
}
