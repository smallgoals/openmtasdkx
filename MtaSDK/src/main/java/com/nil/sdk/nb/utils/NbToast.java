package com.nil.sdk.nb.utils;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.blankj.utilcode.util.Utils;

/**
 * @declaration Toast工具类<br>
 * @author ksA2y511xh@163.com<br>
 *2016-7-7 下午1:36:17<br>
 */
public class NbToast {
    private static final Context mContext = Utils.getApp();
    private static final int SHOW_TOAST = 110;
    private static final String DEF_TEXT = "Message is null...";
	private static Handler baseHandler = new Handler(mContext.getMainLooper()) {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case SHOW_TOAST:
					showToast(mContext, msg.getData().getString("TEXT"));
					break;
				default:
					break;
			}
		}
	};
    
	public static String getS(Object obj){
		String text = DEF_TEXT;
		if(obj instanceof Integer){
			text = mContext.getResources().getString((Integer) obj);
		}else if(obj instanceof String){
			text = (String)obj;
		}else if(obj != null){
			text = obj.toString();
		}
		return text;
	}
	public static void showToast(Object obj) {
		showToast(mContext, obj);
	}
	public static void showToastInThread(Object obj) {
		showToastInThread(mContext, obj);
	}
	
	public static void showToast(Context context, Object obj) {
		Toast.makeText(context,getS(obj), Toast.LENGTH_SHORT).show();
	}
	public static void showToastInThread(Context context, Object obj) {
		Message msg = baseHandler.obtainMessage(SHOW_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString("TEXT", getS(obj));
		msg.setData(bundle);
		baseHandler.sendMessage(msg);
	}
}
