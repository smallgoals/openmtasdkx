package com.nil.sdk.nb.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.UriUtils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

/**
 * 视频文件相关工具类<br>
 */
@SuppressLint("DefaultLocale")
public class NbFileUtils {
	private static final String TAG = NbFileUtils.class.getSimpleName();
	// http://www.fileinfo.com/filetypes/video , "dat" , "bin" , "rms"
	public static final String[] VIDEO_EXTENSIONS = {"264", "3g2", "3gp", "3gp2", "3gpp", "3gpp2", "3mm", "3p2", "60d", "aep", "ajp", "amv", "amx", "arf", "asf", "asx", "avb", "avd", "avi", "avs", "avs", "axm", "bdm", "bdmv", "bik", "bix", "bmk", "box", "bs4", "bsf", "byu", "camre", "clpi", "cpi", "cvc", "d2v", "d3v", "dav", "dce", "dck", "ddat", "dif", "dir", "divx", "dlx", "dmb", "dmsm", "dmss", "dnc", "dpg", "dream", "dsy", "dv", "dv-avi", "dv4", "dvdmedia", "dvr-ms", "dvx", "dxr", "dzm", "dzp", "dzt", "evo", "eye", "f4p", "f4v", "fbr", "fbr", "fbz", "fcp", "flc", "flh", "fli", "flv", "flx", "gl", "grasp", "gts", "gvi", "gvp", "hdmov", "hkm", "ifo", "imovi", "imovi", "iva", "ivf", "ivr", "ivs", "izz", "izzy", "jts", "lsf", "lsx", "m15", "m1pg", "m1v", "m21", "m21", "m2a", "m2p", "m2t", "m2ts", "m2v", "m4e", "m4u", "m4v", "m75", /*"meta",*/ "mgv", "mj2", "mjp", "mjpg", "mkv", "mmv", "mnv", "mod", "modd", "moff", "moi", "moov", "mov", "movie", "mp21", "mp21", "mp2v", "mp4", "mp4v", "mpe", "mpeg", "mpeg4", "mpf", "mpg", "mpg2", "mpgin", "mpl", "mpls", "mpv", "mpv2", "mqv", "msdvd", "msh", "mswmm", "mts", "mtv", "mvb", "mvc", "mvd", "mve", "mvp", "mxf", "mys", "ncor", "nsv", "nvc", "ogm", "ogv", "ogx", "osp", "par", "pds", "pgi", "piv", "playlist", "pmf", "prel", "pro", "prproj", "psh", "pva", "pvr", "pxv", "qt", "qtch", "qtl", "qtm", "qtz", "rcproject", "rdb", "rec", "rm", "rmd", "rmp", "rmvb", "roq", /*"rp",*/ "rts", "rts", "rum", "rv", "sbk", "sbt", "scm", "scm", "scn", "sec", "seq", "sfvidcap", "smil", "smk", "sml", "smv", "spl", "ssm", "str", "stx", "svi", "swf", "swi", "swt", "tda3mt", "tivo", "tix", "tod", "tp", "tp0", "tpd", "tpr", "trp", "ts", "tvs", "vc1", "vcr", "vcv", "vdo", "vdr", "veg", "vem", "vf", "vfw", "vfz", "vgz", "vid", "viewlet", "viv", /*"vivo",*/ "vlab", "vob", "vp3", "vp6", "vp7", "vpj", "vro", "vsp", "w32", "wcp", "webm", "wm", "wmd", "wmmp", "wmv", "wmx", "wp3", "wpl", "wtv", "wvx", "xfl", "xvid", "yuv", "zm1", "zm2", "zm3", "zmv"};
	// http://www.fileinfo.com/filetypes/audio , "spx" , "mid" , "sf"
	public static final String[] AUDIO_EXTENSIONS = {"4mp", "669", "6cm", "8cm", "8med", "8svx", "a2m", "aa", "aa3", "aac", "aax", "abc", "abm", "ac3", "acd", "acd-bak", "acd-zip", "acm", "act", "adg", "afc", "agm", "ahx", "aif", "aifc", "aiff", "ais", "akp", "al", "alaw", "all", "amf", "amr", "ams", "ams", "aob", "ape", "apf", "apl", "ase", "at3", "atrac", "au", "aud", "aup", "avr", "awb", "band", "bap", "bdd", "box", "bun", "bwf", "c01", "caf", "cda", "cdda", "cdr", "cel", "cfa", "cidb", "cmf", "copy", "cpr", "cpt", "csh", "cwp", "d00", "d01", "dcf", "dcm", "dct", "ddt", "dewf", "df2", "dfc", "dig", "dig", "dls", "dm", "dmf", "dmsa", "dmse", "drg", "dsf", "dsm", "dsp", "dss", "dtm", "dts", "dtshd", "dvf", "dwd", "ear", "efa", "efe", "efk", "efq", "efs", "efv", "emd", "emp", "emx", "esps", "f2r", "f32", "f3r", "f4a", "f64", "far", "fff", "flac", "flp", "fls", "frg", "fsm", "fzb", "fzf", "fzv", "g721", "g723", "g726", "gig", "gp5", "gpk", "gsm", "gsm", "h0", "hdp", "hma", "hsb", "ics", "iff", "imf", "imp", "ins", "ins", "it", "iti", "its", "jam", "k25", "k26", "kar", "kin", "kit", "kmp", "koz", "koz", "kpl", "krz", "ksc", "ksf", "kt2", "kt3", "ktp", "l", "la", "lqt", "lso", "lvp", "lwv", "m1a", "m3u", "m4a", "m4b", "m4p", "m4r", "ma1", "mdl", "med", "mgv", "midi", "miniusf", "mka", "mlp", "mmf", "mmm", "mmp", "mo3", "mod", "mp1", "mp2", "mp3", "mpa", "mpc", "mpga", "mpu", "mp_", "mscx", "mscz", "msv", "mt2", "mt9", "mte", "mti", "mtm", "mtp", "mts", "mus", "mws", "mxl", "mzp", "nap", "nki", "nra", "nrt", "nsa", "nsf", "nst", "ntn", "nvf", "nwc", "odm", "oga", "ogg", "okt", "oma", "omf", "omg", "omx", "ots", "ove", "ovw", "pac", "pat", "pbf", "pca", "pcast", "pcg", "pcm", "peak", "phy", "pk", "pla", "pls", "pna", "ppc", "ppcx", "prg", "prg", "psf", "psm", "ptf", "ptm", "pts", "pvc", "qcp", "r", "r1m", "ra", "ram", "raw", "rax", "rbs", "rcy", "rex", "rfl", "rmf", "rmi", "rmj", "rmm", "rmx", "rng", "rns", "rol", "rsn", "rso", "rti", "rtm", "rts", "rvx", "rx2", "s3i", "s3m", "s3z", "saf", "sam", "sb", "sbg", "sbi", "sbk", "sc2", "sd", "sd", "sd2", "sd2f", "sdat", "sdii", "sds", "sdt", "sdx", "seg", "seq", "ses", "sf2", "sfk", "sfl", "shn", "sib", "sid", "sid", "smf", "smp", "snd", "snd", "snd", "sng", "sng", "sou", "sppack", "sprg", "sseq", "sseq", "ssnd", "stm", "stx", "sty", "svx", "sw", "swa", "syh", "syw", "syx", "td0", "tfmx", "thx", "toc", "tsp", "txw", "u", "ub", "ulaw", "ult", "ulw", "uni", "usf", "usflib", "uw", "uwf", "vag", "val", "vc3", "vmd", "vmf", "vmf", "voc", "voi", "vox", "vpm", "vqf", "vrf", "vyf", "w01", "wav", "wav", "wave", "wax", "wfb", "wfd", "wfp", "wma", "wow", "wpk", "wproj", "wrk", "wus", "wut", "wv", "wvc", "wve", "wwu", "xa", "xa", "xfs", "xi", "xm", "xmf", "xmi", "xmz", "xp", "xrns", "xsb", "xspf", "xt", "xwb", "ym", "zvd", "zvr"};

	private static final HashSet<String> mHashVideo;
	private static final HashSet<String> mHashAudio;
	private static final double KB = 1024.0;
	private static final double MB = KB * KB;
	private static final double GB = KB * KB * KB;

	private static HashMap<String, String> mMimeType = new HashMap<String, String>();

	static {
		mHashVideo = new HashSet<String>(Arrays.asList(VIDEO_EXTENSIONS));
		mHashAudio = new HashSet<String>(Arrays.asList(AUDIO_EXTENSIONS));

		mMimeType.put("M1V", "video/mpeg");
		mMimeType.put("MP2", "video/mpeg");
		mMimeType.put("MPE", "video/mpeg");
		mMimeType.put("MPG", "video/mpeg");
		mMimeType.put("MPEG", "video/mpeg");
		mMimeType.put("MP4", "video/mp4");
		mMimeType.put("M4V", "video/mp4");
		mMimeType.put("3GP", "video/3gpp");
		mMimeType.put("3GPP", "video/3gpp");
		mMimeType.put("3G2", "video/3gpp2");
		mMimeType.put("3GPP2", "video/3gpp2");
		mMimeType.put("MKV", "video/x-matroska");
		mMimeType.put("WEBM", "video/x-matroska");
		mMimeType.put("MTS", "video/mp2ts");
		mMimeType.put("TS", "video/mp2ts");
		mMimeType.put("TP", "video/mp2ts");
		mMimeType.put("WMV", "video/x-ms-wmv");
		mMimeType.put("ASF", "video/x-ms-asf");
		mMimeType.put("ASX", "video/x-ms-asf");
		mMimeType.put("FLV", "video/x-flv");
		mMimeType.put("MOV", "video/quicktime");
		mMimeType.put("QT", "video/quicktime");
		mMimeType.put("RM", "video/x-pn-realvideo");
		mMimeType.put("RMVB", "video/x-pn-realvideo");
		mMimeType.put("VOB", "video/dvd");
		mMimeType.put("DAT", "video/dvd");
		mMimeType.put("AVI", "video/x-divx");
		mMimeType.put("OGV", "video/ogg");
		mMimeType.put("OGG", "video/ogg");
		mMimeType.put("VIV", "video/vnd.vivo");
		mMimeType.put("VIVO", "video/vnd.vivo");
		mMimeType.put("WTV", "video/wtv");
		mMimeType.put("AVS", "video/avs-video");
		mMimeType.put("SWF", "video/x-shockwave-flash");
		mMimeType.put("YUV", "video/x-raw-yuv");
	}

	/** 是否是音频或者视频 */
	public static boolean isVideoOrAudio(File f) {
		final String ext = getFileExtension(f);
		return mHashVideo.contains(ext) || mHashAudio.contains(ext);
	}

	public static boolean isVideoOrAudio(String f) {
		final String ext = getUrlExtension(f);
		return mHashVideo.contains(ext) || mHashAudio.contains(ext);
	}

	public static boolean isVideo(File f) {
		final String ext = getFileExtension(f);
		return mHashVideo.contains(ext);
	}
	public static boolean isAudio(File f) {
		final String ext = getFileExtension(f);
		return mHashAudio.contains(ext);
	}

	public static boolean isUrl(String url){
		return StringUtils.noNullStr(url) && (url.startsWith("http") || url.startsWith("https") || url.startsWith("ftp"));
	}
	public static boolean isFile(String url){
		return StringUtils.noNullStr(url) && new File(url).isFile();
	}

	/** 获取文件后缀 */
	public static String getFileExtension(File f) {
		if (f != null) {
			String filename = f.getName();
			int i = filename.lastIndexOf('.');
			if (i > 0 && i < filename.length() - 1) {
				return filename.substring(i + 1).toLowerCase();
			}
		}
		return null;
	}

	public static String getVideoName(String url){
		String name = null;
		try {
			if(isUrl(url)){
				name = getUrlFileName(url);
			}else if(isFile(url)){
				File file = new File(url);
				String suffix = getFileExtension(file);
				if(StringUtils.isNullStr(suffix)){
					String time = DateUtils.getStringByFormat(new Date(file.lastModified()), "yyyy/MM/dd HH:mm:ss");
					name = String.format("小视频-%s.mp4",time);
				}else {
					name = file.getName();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(StringUtils.isNullStr(name)){
			name = "视频"+System.currentTimeMillis();
		}
		return name;
	}
	public static String getVideoName(File file){
		return getVideoName((file == null)? null : file.getAbsolutePath());
	}

	public static String getUrlFileName(String url) {
		int slashIndex = url.lastIndexOf('/');
		int dotIndex = url.lastIndexOf('.');
		String fn;
		if (dotIndex == -1) {
			fn = url.substring(slashIndex + 1);
		} else {
			fn = url.substring(slashIndex + 1, dotIndex);
		}
		return fn;
	}

	public static String getUrlExtension(String url) {
		if (StringUtils.noNullStr(url)) {
			int i = url.lastIndexOf('.');
			if (i > 0 && i < url.length() - 1) {
				return url.substring(i + 1).toLowerCase();
			}
		}
		return "";
	}

	public static String getFileNameNoEx(String filename) {
		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot > -1) && (dot < (filename.length()))) {
				return filename.substring(0, dot);
			}
		}
		return filename;
	}

	public static String getFileSize(long size) {
		String fileSize;
		if (size < KB)
			fileSize = String.format("%.02f", size / 1.0) + "B";
		else if (size < MB)
			fileSize = String.format("%.02f", size / KB) + "KB";
		else if (size < GB)
			fileSize = String.format("%.02f", size / MB) + "MB";
		else
			fileSize = String.format("%.02f", size / GB) + "GB";
		return fileSize/*fileSize.replaceAll(".0", "")*/;
	}

	private static final double WC = 10000;
	private static final double YC = WC * WC;
	public static String showViewTime(long size, String unit) {
		String fileSize = "∞";
		if (size < WC)
			fileSize = size+"";
		else if (size < YC)
			fileSize = String.format("%.1f", size / WC) + "万";
		else if (size < YC)
			fileSize = String.format("%.1f", size / YC) + "亿";
		return (fileSize+unit).replaceAll(".0", "");
	}

	/** 显示SD卡剩余空间 */
	@SuppressWarnings("deprecation")
	public static String showFileAvailable() {
		String result = "";
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
			long blockSize = sf.getBlockSize();
			long blockCount = sf.getBlockCount();
			long availCount = sf.getAvailableBlocks();
			return "可用："+getFileSize(availCount * blockSize) + "/总共：" + getFileSize(blockSize * blockCount);
		}
		return result;
	}

	/** 如果不存在就创建 */
	public static boolean createIfNoExists(String path) {
		File file = new File(path);
		boolean mk = false;
		if (!file.exists()) {
			mk = file.mkdirs();
		}
		return mk;
	}

	/** 获取MIME */
	public static String getMimeType(String path) {
		int lastDot = path.lastIndexOf(".");
		if (lastDot < 0)
			return null;

		return mMimeType.get(path.substring(lastDot + 1).toUpperCase());
	}

	/** 多个SD卡时 取外置SD卡 */
	public static String getExternalStorageDirectory() {
		//参考文章
		//http://blog.csdn.net/bbmiku/article/details/7937745
		Map<String, String> map = System.getenv();
		String[] values = new String[map.values().size()];
		map.values().toArray(values);
		String path = values[values.length - 1];
		if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "path-->" + path);
		if (path.startsWith("/mnt/") && !Environment.getExternalStorageDirectory().getAbsolutePath().equals(path))
			return path;
		else
			return null;
	}

	public static String getFileAttribute(String path){
		StringBuffer buf = new StringBuffer();
		if(path != null && !"".equals(path.trim())){
			File file = new File(path);
			buf.append("文件名称：").append(file.getName()).append("\r\n");
			buf.append("文件大小：").append((file.isDirectory())? file.list().length+"个文件":getFileSize(file.length())).append("\r\n");
			buf.append("修改时间：").append(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.CHINA).format(new Date(file.lastModified()))).append("\r\n");
			buf.append("路径MD5：").append(EncryptUtils.encryptMD5ToString(file.getPath())).append("\r\n");
			buf.append("完整路径：").append(file.getAbsolutePath()).append("\r\n");
		}else{
			buf.append("文件路径不合法");
		}
		return buf.toString();
	}

	/**
	 * 删除指定文件夹下所有文件
	 *
	 * @param path 文件夹完整绝对路径
	 * @return
	 */
	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists() || !file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);		// 先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);		// 再删除空文件夹
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * 删除文件夹
	 *
	 * @param folderPath  文件夹完整绝对路径
	 */
	public static boolean delFolder(String folderPath) {
		boolean isOK = false;
		try {
			delAllFile(folderPath); 		// 删除完里面所有内容
			isOK = deleteFileSafely(new File(folderPath)); 		// 删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
			isOK = false;
		}
		return isOK;
	}

	/**
	 * 安全删除文件.
	 * @param file
	 * @return
	 */
	public static boolean deleteFileSafely(File file) {
		if (file != null && file.exists()) {
			if(file.isDirectory()) delAllFile(file.getAbsolutePath());
			String tmpPath = file.getParent() + File.separator + System.currentTimeMillis();
			File tmp = new File(tmpPath);
			file.renameTo(tmp);
			return tmp.delete();
		}
		return false;
	}

	/**
	 * 得到格式化的时间戳<br>
	 */
	public static String getTimeStamp(long ms) {
		String timeStr = null;
		int hour = 0;
		int minute = 0;
		int second = 0;
		int time = (int) (ms/1000);
		if (time <= 0) {
			timeStr = "00:00";
		} else {
			minute = time / 60;
			if (minute < 60) {
				second = time % 60;
				timeStr = unitFormat(minute) + ":" + unitFormat(second);
			} else {
				hour = minute / 60;
				if (hour > 99)
					return "99:59:59";
				minute = minute % 60;
				second = time - hour * 3600 - minute * 60;
				timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
			}
		}
		return timeStr;
	}
	public static String unitFormat(int i) {
		String retStr = null;
		if (i >= 0 && i < 10)
			retStr = "0" + Integer.toString(i);
		else
			retStr = "" + i;
		return retStr;
	}

	/**
	 * 通过发送广播刷新媒体库<br>
	 */
	public static void refreshSystemMedia(Context ctx, String path){
		if(StringUtils.noNullStr(path)){
			refreshSystemMedia(ctx, new File(path));
		}
	}
	public static void refreshSystemMedia(Context ctx, File file){
		if(ctx!= null && file != null && file.exists()/* && file.isFile()*/) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//19-4.4+
				Intent it = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				Uri uri = Uri.parse("file://"+file.getAbsolutePath());
				it.setData(uri);
				ctx.sendBroadcast(it);
			} else {
				ctx.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
						Uri.parse("file://"+SDCardUtils.getSDCardPathByEnvironment())));
			}
			LogUtils.d("refreshSystemMedia-->file:"+file.getAbsolutePath());
		}
	}

	//获取视频总时长
	public static int getVideoDuration(String path){
		int duration = -1;
		try {
			MediaMetadataRetriever media = new MediaMetadataRetriever();
			media.setDataSource(path);
			String d = media.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			duration = Integer.parseInt(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return duration;
	}
	//获取视频参数信息：
	public static int getMediaV(MediaMetadataRetriever media, int key){
		int ret = -1;
		if(media != null){
			try {
				String d = media.extractMetadata(key);
				ret = Integer.parseInt(d);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	public static boolean isFullScreen(String path){
		boolean isOK = false;
		try {
			if(StringUtils.noNullStr(path)) {
				MediaMetadataRetriever media = new MediaMetadataRetriever();
				media.setDataSource(path);
				int width = getMediaV(media, MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
				int height = getMediaV(media, MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
				if(width > height){
					isOK = true;
				}
				if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "MediaMetadataRetriever-->"+media);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isOK;
	}

	/**
	 * 打开文件<br>
	 */
	public static void openFile(File file, String dataType) {
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			intent.setDataAndType(UriUtils.file2Uri(file), dataType);
			BaseUtils.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 分享文件<br>
	 */
	public static void shareFile(File file, String dataType) {
		try {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.putExtra(Intent.EXTRA_STREAM, UriUtils.file2Uri(file));
			intent.setType(dataType);
			BaseUtils.startActivity(Intent.createChooser(intent, "分享"));
		} catch (Exception e) {
			ToastUtils.showLong("暂时无法分享");
			e.printStackTrace();
		}
	}
}
