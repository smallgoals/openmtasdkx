package com.nil.sdk.utils;

import java.io.File;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;

import com.nil.sdk.ui.BaseUtils;

/**
 * @declaration 本应用数据清除管理器<br>
 * Android清除本地数据缓存代码 http://www.cnblogs.com/rayray/p/3413673.html<br>
 */
@SuppressLint("SdCardPath")
public class DataCleanManager {
	private static String sPackageName;
	/**
	 * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache)
	 * 
	 * @param context
	 */
	public static void cleanInternalCache(Context context) {
		String path = "/data/data/" + sPackageName + "/cache";
		deleteFilesByDirectory(new File(path));
	}

	/**
	 * 清除本应用所有数据库(/data/data/com.xxx.xxx/databases)
	 * 
	 * @param context
	 */
	@SuppressLint("SdCardPath")
	public static void cleanDatabases(Context context) {
		String path = "/data/data/" + sPackageName + "/databases";
		deleteFilesByDirectory(new File(path));
	}

	/**
	 * 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs)
	 * 
	 * @param context
	 */
	public static void cleanSharedPreference(Context context) {
		String path = "/data/data/" + sPackageName + "/shared_prefs";
		deleteFilesByDirectory(new File(path));
	}

	/**
	 * 按名字清除本应用数据库
	 * 
	 * @param context
	 * @param dbName
	 */
	public static void cleanDatabaseByName(Context context, String dbName) {
		context.deleteDatabase(dbName);
	}

	/**
	 * 清除/data/data/com.xxx.xxx/files下的内容
	 * 
	 * @param context
	 */
	public static void cleanFiles(Context context) {
		deleteFilesByDirectory(context.getFilesDir());
	}

	/**
	 * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache)
	 * /storage/emulated/0/Android/data/packagename/cache/../../packagename
	 * @param context
	 */
	public static void cleanExternalCache(Context context) {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			/**
			 * 如果使用API级别8或更高的版本，使用getExternalCacheDir()来打开一个File对象，<br>
			 * 它代表了保存缓存文件的外部存储器目录。<br>
			 */
			//deleteFilesByDirectory(context.getExternalCacheDir());
			try{
				String path = context.getExternalCacheDir().getAbsolutePath()+"/../../"+sPackageName;
				deleteFilesByDirectory(new File(path));
			}catch(Exception e){};
		}
	}

	/**
	 * 清除自定义路径下的文件，使用需小心，请不要误删。而且只支持目录下的文件删除
	 * 
	 * @param filePath
	 */
	public static void cleanCustomCache(String filePath) {
		deleteFilesByDirectory(new File(filePath));
	}
	
	/**
	 * 清除广告相关缓存
	 * 
	 * @param context
	 */
	public static void cleanAdviewCache(Context context){
		sPackageName = context.getPackageName();
		cleanInternalCache(context);
		cleanExternalCache(context);
		cleanDatabases(context);
		cleanFiles(context);
	}
	
	/**
	 * 清除应用全部缓存数据
	 * @param context
	 */
	public static void cleanAppDatas(Context context){
		cleanAppDatas(context, false);
	}
	public static void cleanAppDatas(Context context, boolean isTip){
		cleanAppDatas(context, context.getPackageName(), isTip);
	}
	public static void cleanAppDatas(Context context, String packageName , boolean isTip){
		if(context == null) {
			if(isTip) BaseUtils.popMyToast(context, "本地数据和缓存清空失败.");
		}
		sPackageName = packageName;
		cleanInternalCache(context);
		cleanExternalCache(context);
		cleanDatabases(context);
		cleanSharedPreference(context);
		cleanFiles(context);
		if(isTip) BaseUtils.popMyToast(context, "本地数据和缓存已清空.");
	}

	/**
	 * 清除本应用所有的数据
	 * 
	 * @param context
	 * @param filepath
	 */
	public static void cleanApplicationData(Context context, String... filepath) {
		cleanInternalCache(context);
		cleanExternalCache(context);
		cleanDatabases(context);
		cleanSharedPreference(context);
		cleanFiles(context);
		for (String filePath : filepath) {
			cleanCustomCache(filePath);
		}
	}

	/**
	 * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理
	 * 
	 * @param directory
	 */
	private static void deleteFilesByDirectory(File directory) {
		if (directory != null && directory.exists() && directory.isDirectory()) {
			/*for (File item : directory.listFiles()) {
				item.delete();
			}*/
			FileUtils.delFolder(directory.getAbsolutePath());
		}
	}
	
	public static void main(String[] args) {
		Context context = null;
		DataCleanManager.cleanAdviewCache(context);
		DataCleanManager.cleanAppDatas(context);
	}
}