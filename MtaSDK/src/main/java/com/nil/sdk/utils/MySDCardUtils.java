package com.nil.sdk.utils;

import android.content.Context;
import android.os.Environment;

import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.Utils;

public class MySDCardUtils {
    //Android11后：所有文件权限访问权限-Manifest.permission.MANAGE_EXTERNAL_STORAGE;
    public static String getSDCardPathByEnvironment() {
        return getSDCardPathByEnvironment(Utils.getApp());
    }
    public static String getSDCardPathByEnvironment(Context ctx) {
        if(ctx == null) ctx = Utils.getApp();
        String path = "";

        try {
            //大于等于29[Android Q]且开启分区存储：无法访问外置SD卡，所以返回false
            if (!AppUtils.hasLegacyExternalStorage()) {
                path = ctx.getExternalFilesDir(null).getAbsolutePath();
            }else if (SDCardUtils.isSDCardEnableByEnvironment()) {
                path = Environment.getExternalStorageDirectory().getAbsolutePath();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return path;
    }
}
