package com.nil.sdk.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.XXPermissions;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;

import java.util.List;

/**
 * 权限请求工具类<br>
 */
@SuppressLint("WrongConstant")
public class AskPermissionUtil {
    public abstract static class DefFullCallback implements PermissionUtils.FullCallback {
        @Override
        public abstract void onGranted(List<String> permissionsGranted);
        public  void onDenied(List<String> permissionsDeniedForever, List<String> permissionsDenied) {}
    }
    public static final String[] storagePermissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public static final String[] macPermissions = {
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.CHANGE_WIFI_STATE
    };
    public static final String recordPermission = Manifest.permission.RECORD_AUDIO;
    public static final String phonePermission = Manifest.permission.READ_PHONE_STATE;
    public static final String cameraPermission = Manifest.permission.CAMERA;
    public static final String settingsPermission = Manifest.permission.WRITE_SETTINGS;
    public static final String alertPermission = Manifest.permission.SYSTEM_ALERT_WINDOW;

    public static boolean hasPermanentDenied(String... pers){
        boolean isOK = false;
        if(pers != null && pers.length > 0){
            for(String p : pers){
                Object tt = ACacheUtils.getAppFileCacheObject(p);
                boolean b = tt != null && (boolean) tt;
                if(b){
                    //有永久拒绝的权限，直接跳出循环
                    isOK = true;
                    break;
                }
            }
        }
        return isOK;
    }
    public static void savePermissions(Activity act, String... pers){
        if(pers != null && pers.length > 0){
            for(String p : pers){
                //配置权限状态
                ACacheUtils.setAppFileCacheObject(p, XXPermissions.isPermanentDenied(act, p));
            }
        }
    }
    public static void askPermission(final Activity act, final String[] permissions,
                                     final DefFullCallback callback, final boolean isOpenPermanentDenied) {
        XXPermissions.with(act)
                .permission(permissions)
                // 设置权限请求拦截器（局部设置）
                //.interceptor(new PermissionInterceptor())
                // 设置不触发错误检测机制（局部设置）
                //.unchecked()
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> granted, boolean isAll) {
                        if (isAll) {
                            //全部授权才执行回调
                            if (callback != null) callback.onGranted(granted);
                        } else {
                            //获取部分权限成功，但全部权限未正常授予
                            ToastUtils.showLong(R.string.ask_permission_part_lack_tip);
                        }
                    }
                    public void onDenied(List<String> denied, boolean never) {
                        savePermissions(act, permissions);
                        if(isOpenPermanentDenied && hasPermanentDenied(permissions)){
                            ToastUtils.showLong(R.string.ask_permission_permanent_denied_tip);
                            // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            XXPermissions.startPermissionActivity(act, permissions);
                        }else{
                            if (callback != null) callback.onDenied(denied, denied);
                        }
                    }
                });
    }
    public static void askPermission(final Activity act, final String[] permissions, final DefFullCallback callback) {
        askPermission(act, permissions, callback, false);
    }


    private static MessageDialog messageDialog;
    public static void popPermissionTipDlg(final Activity act, String title, final DefFullCallback callback){
        popPermissionTipDlg(act, title, Utils.getApp().getString(R.string.ask_permission_storage_tip), storagePermissions, callback);
    }
    public static void popPermissionTipDlg(final Activity act, String title, String msg,
                                           final String[] permissions, final DefFullCallback callback){
        String ok = Utils.getApp().getString(R.string.ask_permission_ok_btn_text);
        String cancel = Utils.getApp().getString(R.string.ask_permission_cancel_btn_text);
        popPermissionTipDlg(act, title, msg, ok, cancel, permissions, callback);
    }
    public static void popPermissionTipDlg(final Activity act, String title, String msg, String ok, String cancel,
                                           final String[] permissions, final DefFullCallback callback){
        if(messageDialog != null && messageDialog.isShow()){
            messageDialog.hide();
            messageDialog = null;
        }

        //有永久拒绝权限，直接跳解决永久拒绝对话框：
        if(hasPermanentDenied(permissions)){
            //有权限被永久拒绝，跳转到应用信息界面，让用户手动授权
            MessageDialog.show(Utils.getApp().getString(R.string.ask_permission_permanent_denied_title),
                            Utils.getApp().getString(R.string.ask_permission_permanent_denied_tip),
                            Utils.getApp().getString(R.string.ask_permission_permanent_denied_ok_btn_text),
                            Utils.getApp().getString(R.string.ask_permission_permanent_denied_cancel_btn_text))
                    //.setTitleIcon(android.R.drawable.sym_def_app_icon)
                    .setOkButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                        @Override
                        public boolean onClick(MessageDialog dialog, View v) {
                            askPermission(act, permissions, callback, true);
                            return false;
                        }
                    });
            return;
        }

        String _title = StringUtils.noNullStr(title) ? title : Utils.getApp().getString(R.string.ask_permission_title_text);
        String _msg = StringUtils.noNullStr(msg) ? msg : Utils.getApp().getString(R.string.ask_permission_msg_text);
        String _cancel = StringUtils.noNullStr(cancel) ? cancel : Utils.getApp().getString(R.string.ask_permission_cancel_btn_text);
        String _ok = StringUtils.noNullStr(ok) ? ok : Utils.getApp().getString(R.string.ask_permission_ok_btn_text);

        messageDialog = new MessageDialog(_title, _msg, _ok, _cancel)
                //.setTitleIcon(android.R.drawable.sym_def_app_icon)
                .setOkButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                    @Override
                    public boolean onClick(MessageDialog dialog, View v) {
                        askPermission(act, permissions, callback);
                        return false;
                    }
                });
        messageDialog.show();
    }
}
