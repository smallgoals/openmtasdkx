package com.nil.sdk.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.blankj.utilcode.util.ToastUtils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;

/**
 * 打开市场，去评五星<br>
 * @date 2013-8-1
 */
public class OpenMarket {
	public static void go(Context ctx) {
		try {
			Uri uri = Uri.parse("market://details?id=" + ctx.getPackageName());
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			BaseUtils.startActivity(ctx,  intent);
		} catch (Exception e) {
			ToastUtils.showShort("无法加载应用市场");
		}
	}

	public static void go(Context ctx, String packageName) {
		try {
			Uri uri = Uri.parse("market://details?id=" + packageName);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			BaseUtils.startActivity(ctx,  intent);
		} catch (ActivityNotFoundException e) {
			ToastUtils.showShort("无法加载应用市场");
		}
	}
	public static void go(final Context ctx, final String packageName, final String markPackageName) {
		try{
			Intent intent = new Intent();
			intent.setPackage(markPackageName);
			intent.setAction(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=" + packageName));
			BaseUtils.startActivity(ctx,  intent);
		}catch(Exception e){
			MyMessageDialog.popDialog(ctx, "【"+markPackageName+"】该市场App不存在，是否前往安装？",
					"前往安装", "取消", new MyMessageDialog.DialogMethod() {
				public void sure() {
					go(ctx, markPackageName);
				}
			});
		}
	}
}
