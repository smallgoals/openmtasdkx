package com.nil.sdk.utils;

import android.Manifest;
import android.app.Activity;
import android.view.View;

import com.blankj.utilcode.util.PermissionUtils;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.nil.sdk.ui.BaseUtils;

import java.util.List;

/**
 * 权限工具类<br>
 */
@Deprecated
public class PermissionsUtils {
    protected Activity mActivity;
    protected String[] mPermission = {
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_WIFI_STATE
    };
    protected IPermissionCallback mPermissionCallback;

    PermissionsUtils(){}
    PermissionsUtils(Activity act, String [] ary){
        mActivity = act;
        if(ary != null && ary.length > 0) {
            mPermission = ary;
        }
    }
    PermissionsUtils(Activity act, String [] ary, IPermissionCallback callback){
        this(act, ary);
        mPermissionCallback = callback;
    }
    PermissionsUtils(Activity act, IPermissionCallback callback){
        mActivity = act;
        mPermissionCallback = callback;
    }

    protected void getPermission() {
        //使用Blankj/AndroidUtilCode的权限申请框架，感觉更简单
        PermissionUtils permissionUtils = PermissionUtils.permission(mPermission);
        permissionUtils.callback(new PermissionUtils.FullCallback() {
            @Override
            public void onGranted(List<String> permissionsGranted) {
                if(mPermissionCallback != null){
                    mPermissionCallback.onSuccess(mActivity);
                }
            }
            public void onDenied(List<String> permissionsDeniedForever, List<String> permissionsDenied) {
                if(mPermissionCallback != null){
                    mPermissionCallback.onFail(mActivity);
                }
            }
        });
        permissionUtils.request();
    }

    public interface IPermissionCallback {
        void onSuccess(Activity act);
        void onFail(Activity act);
    }
    public abstract class DefPermissionCallback implements IPermissionCallback {
        @Override
        public abstract void onSuccess(Activity act);
        public void onFail(Activity act) {
            new MessageDialog("权限不足","请授权保证软件正常运行","前往授权","退出软件")
                    //.setTitleIcon(R.drawable.xpay_pay_icon_vip)
                    .setOkButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                        @Override
                        public boolean onClick(MessageDialog dialog, View v) {
                            getPermission();
                            return true;
                        }
                    }).setCancelButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                        @Override
                        public boolean onClick(MessageDialog dialog, View v) {
                            BaseUtils.onExitNormal(mActivity);
                            return true;
                        }
                    }).show();
        }
    }
}
