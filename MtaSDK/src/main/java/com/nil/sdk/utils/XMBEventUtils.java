package com.nil.sdk.utils;

import android.annotation.SuppressLint;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * 事件工具类<br>
 */
@SuppressLint("DefaultLocale")
public class XMBEventUtils {
    private final static String TAG = XMBEventUtils.class.getSimpleName();
    public static final String Install_Date_Key = "install_date_key";
    public static void setInstallDate(){
        String curDate = DateUtils.getCurDate();
        ACacheUtils.setCacheValue(Install_Date_Key, curDate);

        send(EventType.install_date, curDate);
    }
    public static String getInstallDate(){
        return ACacheUtils.getCacheValue(Install_Date_Key);
    }

    public static void send(String key, String value){
        try {
            //友盟上报事件：
            AdSwitchUtils.reportEvent(Utils.getApp(), key, value);

            //Xmb上报事件：
            BaseUtils.reportEvent(key, value);

            if (Spu.isSucK(TAG)) LogUtils.dTag(TAG, key + "-->" + value);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public enum EventType{install_date,pay_success,update_apk,share_apk,user_destroy_success,user_destroy_fail}
    public static void send(EventType e){
        send(e, null);
    }
    public static void send(EventType e, Object o){
        String curDate = DateUtils.getCurDate();
        String installDate = getInstallDate();
        String code = ACacheUtils.getCode();
        String vv = code;
        if(e == EventType.install_date){
            vv = String.format("首次安装::%s", code);
        }else if(e == EventType.pay_success){
            vv = String.format("支付成功::%s;AZ:%s;Days:%d", code, installDate,
                    DateUtils.getOffectDay(curDate, installDate));
        }else if(e == EventType.update_apk){
            vv = String.format("APP更新::%s", code);
        }else if(e == EventType.share_apk){
            vv = String.format("APP分享::%s", code);
        }else if(e == EventType.user_destroy_success){
            vv = String.format("销户成功::%s", code);
        }else if(e == EventType.user_destroy_fail){
            vv = String.format("销户失败::%s", code);
        }

        if(o != null){
            vv = String.format("%s;o:%s;", vv, o);
        }
        XMBEventUtils.send(e.name(), vv);
    }
}
