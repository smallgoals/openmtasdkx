package com.nil.sdk.utils;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Properties工具类<br>
 * 从R.raw.x文件中通过Key读取值<br>
 */
public class PropertiesUtil {
    private final static String TAG = PropertiesUtil.class.getSimpleName();
    private static Map<Integer, Properties> propertiesHashMap;
    private static PropertiesUtil sManufacture;
    private final Charset defCharsetName = StandardCharsets.UTF_8;

    public static PropertiesUtil getInstance() {
        if (sManufacture == null) {
            sManufacture = new PropertiesUtil();
            propertiesHashMap = new HashMap<>();
        }
        return sManufacture;
    }

    /**
     * 获取raw资源文件中的键值<br>
     */
    public String getValue(int rawID, String key){
        String value = null;
        try {
            if(propertiesHashMap == null) propertiesHashMap = new HashMap<>();
            Properties p = propertiesHashMap.get(rawID);
            if (p == null) {
                InputStream in = Utils.getApp().getResources().openRawResource(rawID);
                p = new Properties();
                //InputStream转换中文乱码 https://blog.csdn.net/renwendaojian/article/details/79761576
                p.load(new InputStreamReader(in, defCharsetName));

                propertiesHashMap.put(rawID, p);
            }
            value = p.getProperty(key);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.wTag(TAG, e.getMessage());
        }
        return value;
    }
}
