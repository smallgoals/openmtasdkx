package com.nil.sdk.utils;

import android.annotation.SuppressLint;
import android.content.Context;

import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.vvv.utils.Mtas;
import com.xmb.mta.util.XMBSign;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.io.File;
import java.io.Serializable;
import java.util.UUID;

/**
 * 缓存工具类：SD卡和系统缓存目录双重缓存数据的管理<br>
 */
@SuppressLint("MissingPermission")
public class ACacheUtils {
    private static final String TAG = ACacheUtils.class.getSimpleName();

    /*######################仅缓存在应用（data/包名）缓存目录....begin######################*/
    /**
     * 【不需要任何权限】存储于APP内部包名目录的文件中，卸载或清理数据[会]被清除<br>
     */
    public static ACache getAppFileACache(){
        return ACache.get(new File(Utils.getApp().getFilesDir(), "AppFileACache"));
    }
    /**
     * 【不需要任何权限】存储于APP内部包名目录的缓存中，卸载或清理数据或清除缓存[会]被清除<br>
     */
    public static ACache getAppACache(){
        return ACache.get(Utils.getApp());
    }
    /**
     * 【需要存储权限】存储于SD卡目录，卸载或清理数据[不会]被清除<br>
     */
    public static ACache getSdACache(){
        try {
            if (AppUtils.hasSDCardPermission(Utils.getApp())) {
                return ACache.get(new File(getCachePath()));
            }
        }catch (Exception e){
            e.printStackTrace();
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
        }catch (Error ee){
            ee.printStackTrace();
            LogUtils.eTag(TAG, ee.toString());
        }
        return getAppFileACache();
    }

    /*---AppFileACache...begin---*/
    public static void setAppFileCacheObject(String key, Serializable obj, int saveTime){
        try {
            if (obj != null) {
                getAppFileACache().put(key, obj, saveTime);  //File目录缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void setAppFileCacheObject(String key, Serializable obj){
        try {
            if (obj != null) {
                getAppFileACache().put(key, obj);  //File目录缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static Object getAppFileCacheObject(String key) {
        Object id = null;
        try {
            id = getAppFileACache().getAsObject(key);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            //e.printStackTrace();
        }
        return id;
    }
    public static void setAppFileCacheValue(String key, String v, int saveTime){
        try {
            if (StringUtils.noNullStr(v)) {
                getAppFileACache().put(key, v, saveTime);  //File目录缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void setAppFileCacheValue(String key, String v) {
        try {
            if (StringUtils.noNullStr(v)) {
                getAppFileACache().put(key, v);  //File目录缓存数据
            }
            //LogUtils.d(TAG, "gap="+v+",key="+key);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static String getAppFileCacheValue(String key) {
        String id = null;
        try {
            id = getAppFileACache().getAsString(key);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return id;
    }
    /*---AppFileACache...end---*/

    public static void setAppCacheObject(String key, Serializable obj, int saveTime){
        try {
            if (obj != null) {
                getAppACache().put(key, obj, saveTime);  //缓存目录缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void setAppCacheObject(String key, Serializable obj){
        try {
            if (obj != null) {
                getAppACache().put(key, obj);  //缓存目录缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static Object getAppCacheObject(String key) {
        Object id = null;
        try {
            id = getAppACache().getAsObject(key);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            //e.printStackTrace();
        }
        return id;
    }
    public static void setAppCacheValue(String key, String v, int saveTime){
        try {
            if (StringUtils.noNullStr(v)) {
                getAppACache().put(key, v, saveTime);  //缓存目录缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void setAppCacheValue(String key, String v) {
        try {
            if (StringUtils.noNullStr(v)) {
                getAppACache().put(key, v);  //缓存目录缓存数据
            }
            //LogUtils.d(TAG, "gap="+v+",key="+key);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static String getAppCacheValue(String key) {
        String id = null;
        try {
            id = getAppACache().getAsString(key);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return id;
    }
    /*######################仅缓存在应用（data/包名）缓存目录....end######################*/

    public static void setCacheValue(String key, String v, int saveTime){
        try {
            if (StringUtils.noNullStr(v)) {
                getAppFileACache().put(key, v, saveTime);  //APP内部File目录缓存数据
                getSdACache().put(key, v, saveTime);  //内置SD卡缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void setCacheValue(String key, String v) {
        try {
            if (StringUtils.noNullStr(v)) {
                getAppFileACache().put(key, v);  //APP内部File目录缓存数据
                getSdACache().put(key, v);  //内置SD卡缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }

    public static String getCacheValue(String key) {
        String id = null;
        try {
            String sdValue = getSdACache().getAsString(key);
            String cacheValue = getAppFileACache().getAsString(key);
            if (StringUtils.isNullStr(sdValue) && StringUtils.noNullStr(cacheValue)) {
                getSdACache().put(key, cacheValue);  //内置SD卡始终缓存
            }
            //优先取SD卡缓存的数据
            id = StringUtils.getValue(sdValue, cacheValue);
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return id;
    }

    public static void setCacheObject(String key, Serializable obj, int saveTime){
        try {
            if (obj != null) {
                getAppFileACache().put(key, obj, saveTime);  //APP内部File目录缓存数据
                getSdACache().put(key, obj, saveTime);  //内置SD卡缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void setCacheObject(String key, Serializable obj){
        try {
            if (obj != null) {
                getAppFileACache().put(key, obj);  //APP内部File目录缓存数据
                getSdACache().put(key, obj);  //内置SD卡缓存数据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static Object getCacheObject(String key) {
        Object id = null;
        try {
            Object sdValue = getSdACache().getAsObject(key);
            Object cacheValue = getAppFileACache().getAsObject(key);
            if (sdValue == null && cacheValue != null) {
                getSdACache().put(key, (Serializable)cacheValue);  //内置SD卡始终缓存
            }
            //优先取SD卡缓存的数据
            id = (sdValue != null) ? sdValue : cacheValue;
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return id;
    }
    /**
     * 移除键名相关的缓存数据（包括APP缓存目录和SD缓存目录中的数据）<br>
     */
    public static void removeObject(String key){
        try {
            if (StringUtils.noNullStr(key)) {
                getAppACache().remove(key);  //移除一条缓存目录缓据
                getSdACache().remove(key);  //移除一条内置SD卡缓存数据
                getAppFileACache().remove(key);//移除一条File目录缓据
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }

    /**
     * 清空全部缓存数据（包括APP缓存目录和SD缓存目录中的数据）<br>
     */
    public static void clearObject(){
        try {
            getAppACache().clear();  //清空缓存目录缓据
            getSdACache().clear();  //清空内置SD卡缓存数据
            getAppFileACache().clear();//清除File目录缓据
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }

    /**
     * 得到缓存目录，文件夹名为包名的MD5<br>
     */
    public static String getCachePath() {
        String path = null;
        try {
            path = MySDCardUtils.getSDCardPathByEnvironment(); //内置SD卡目录
            if (StringUtils.noNullStr(path)) {
                String subPath = "cache_path_xvx";
                Context ctx = Utils.getApp();
                if (ctx != null) {
                    subPath = ctx.getPackageName();
                }
                //内置SD卡目录下，创建一个包名Md5目录
                path = path + File.separator + "." + EncryptUtils.encryptMD5ToString(subPath);
                FileUtils.createOrExistsDir(path);
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return path;
    }

    public static String getCombinedID() {//MAC地址不再加入算法
        String id = getCacheCombinedID();
        try {
            if (StringUtils.isNullStr(id)) {
                /**
                 * BSQ:     imei+mac+vendor+model+androidID-->Md5<br>
                 * VEdit:   imei+vendor+model+androidID-->Md5<br>
                 * Love:    imei+vendor+model-->Md5<br>
                 */
                String series = getMyIMEI()
                        + DeviceUtils.getManufacturer()
                        + DeviceUtils.getModel();
                id = EncryptUtils.encryptMD5ToString(series);
                setCacheSeries("xcode"+series+"xbz"+System.currentTimeMillis()); //缓存CombinedID
                setCacheCombinedID(id); //缓存CombinedID
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return id;
    }
    public static String getCombinedID(String app_id) {//MAC地址不再加入算法
        String id = getCacheCombinedID();
        try {
            if (StringUtils.isNullStr(id)) {
                /**
                 * BSQ:     imei+mac+vendor+model+androidID-->Md5<br>
                 * VEdit:   imei+vendor+model+androidID-->Md5<br>
                 * Love:    imei+vendor+model-->Md5<br>
                 */
                String series = getMyIMEI()
                        + DeviceUtils.getManufacturer()
                        + DeviceUtils.getModel();
                if(StringUtils.equalsIgnoreCase(app_id, "BSQ")){
                    series = getMyIMEI()
                            + getMyMacAddress()
                            + DeviceUtils.getManufacturer()
                            + DeviceUtils.getModel()
                            + DeviceUtils.getAndroidID();
                }else if(StringUtils.equalsIgnoreCase(app_id, "VEdit")){
                    series = getMyIMEI()
                            + DeviceUtils.getManufacturer()
                            + DeviceUtils.getModel()
                            + DeviceUtils.getAndroidID();
                }

                id = EncryptUtils.encryptMD5ToString(series);
                setCacheSeries("xcode"+series+"xbz"+System.currentTimeMillis()); //缓存CombinedID
                setCacheCombinedID(id); //缓存CombinedID
            }
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return id;
    }

    public static void setCacheCombinedID(String id) {
        setCacheValue("CombinedID", id);
    }
    public static String getCacheCombinedID() {
        return getCacheValue("CombinedID");
    }

    public static void setCacheSeries(String series) {
        setCacheValue("CombinedID_Series", series);
    }
    public static String getCacheSeries() {
        if(StringUtils.isNullStr(getCacheCombinedID())){
            getCombinedID();
        }
        return getCacheValue("CombinedID_Series");
    }

    public static String getCode() {
        String code = null;
        try {
            code = com.blankj.utilcode.util.AppUtils.getAppName() + "_"
                    + com.blankj.utilcode.util.AppUtils.getAppVersionName() + ";" +
                    com.blankj.utilcode.util.AppUtils.getAppPackageName() + ";";
            code += "V:" + (OrderBeanV2.hasViP() ? "1" : "0") + ";";
            code += "IMEI:" + getMyIMEI() + ";";
            code += "MAC:" + getMyMacAddress() + ";";
            code += "CID:" + getCombinedID() + ";";
            code += "UID:" + ACacheUtils.getAppFileCacheValue(XMBSign.Cjs.user_id.name()) + ";";
            code += "QD:" + AppUtils.getMetaData(Utils.getApp(), "UMENG_CHANNEL") + ";";
            code += "K:" + Mtas.Version + ";";
        } catch (Exception e) {
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
        return code;
    }
    public static String getMyIMEI(){
        String imei = ACacheUtils.getCacheValue(XMBSign.Cjs.xmb_imei.name());
        try {
            if (StringUtils.isNullStr(imei)) {
                imei = getMyPhoneIMEI();
                // 防止Android 10.0中IMEI为空的情况：
                if (StringUtils.isNullStr(imei)) {
                    imei = UUID.randomUUID().toString();
                    ACacheUtils.setCacheValue(XMBSign.Cjs.xmb_uuid.name(), imei);
                }
            }
        } catch (Exception e) {
            imei = UUID.randomUUID().toString();
            //e.printStackTrace();
        }
        ACacheUtils.setCacheValue(XMBSign.Cjs.xmb_imei.name(), imei);
        return imei;
    }
    public static String getMyMacAddress(){
        /*String mac = "";
        try {
            // 有MAC开关、有权限且已同意隐私政策：
            if(AdSwitchUtils.Sws.cj_mac.flag
                && AppUtils.hasMacPermission(Utils.getApp())
                && AppUtils.hasAgreePrivacyPolicy()) {
                mac = DeviceUtils.getMacAddress();
            }
        }catch (Exception e){
            mac = "";
            //e.printStackTrace();
        }
        return mac;*/
        return "20:23:03:27:15:56"; //2023.3.27 15:56
    }
    public static String getMyPhoneIMEI(){
        /*String imei = UUID.randomUUID().toString();
        //部分机型imei有乱码，导致服务器解析时异常，暂时停用获取imei by 2023.3.27 15:56
        try {
            // 有权限且已同意隐私政策：
            if(AdSwitchUtils.Sws.cj_imei.flag
                && AppUtils.hasPhonePermission(Utils.getApp())
                && AppUtils.hasAgreePrivacyPolicy()) {
                imei = PhoneUtils.getIMEI();
            }
        }catch (Exception e){
            imei = UUID.randomUUID().toString();
            //e.printStackTrace();
        }*/
        return UUID.randomUUID().toString();
    }
}
