/** http://www.devstore.cn/code/info/363.html*/
package com.nil.sdk.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.hjq.permissions.XXPermissions;
import com.nil.sdk.nb.utils.NbUriUtils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.vvv.utils.AdSwitchUtils.Sws;
import com.nil.vvv.utils.AdSwitchUtils.Vs;
import com.nil.vvv.utils.Mtas;
import com.nil.vvv.utils.ZFactory;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * 跟App相关的辅助类<br>
 */
@SuppressLint("SimpleDateFormat")
public class AppUtils{
	private final static String TAG = AppUtils.class.getSimpleName();
	private AppUtils(){
		/* cannot be instantiated */
		throw new UnsupportedOperationException("cannot be instantiated");
	}

	/**
	 * 获取应用程序名称<br>
	 * @return 当前应用的名称<br>
	 */
	public static String getAppName(){
		return getAppName(Utils.getApp());
	}
	public static String getAppName(Context context){
		try {
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			int labelRes = packageInfo.applicationInfo.labelRes;
			return context.getResources().getString(labelRes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 获取应用程序版本名称信息<br>
	 * @return 当前应用的版本名称<br>
	 */
	public static String getVersionName(){
		return getVersionName(Utils.getApp());
	}
	public static String getVersionName(Context context){
		try {
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * 获取本软件的版本号<br>
	 * @return 当前应用的版本号<br>
	 */
	public static int getVersionCode(){
		return getVersionCode(Utils.getApp());
	}
	public static int getVersionCode(Context context){
		try {
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public static int getTargetSdkVersion(Context content){
		int targetSdkVersion = 0;
		try {
			final PackageInfo info = content.getPackageManager().getPackageInfo(content.getPackageName(), 0);
			targetSdkVersion = info.applicationInfo.targetSdkVersion;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return targetSdkVersion;
	}
	public static String getMetaChannelHead(Context context){
		String result = null;
		String channel = getMetaChannel(context);
		if(!StringUtils.isNullStr(channel)){
			result = channel.split("_")[0];
		}
		return result;
	}
	public static String getMetaChannel(){
		return getMetaChannel(Utils.getApp());
	}
	public static String getMetaChannel(Context context){
		return AppUtils.getMetaData(context, "UMENG_CHANNEL");
	}
	public static String getMetaData(Context context, String key){
		String channel = null;
		try{
			Context ctx = (context != null) ? context : Utils.getApp();
			ApplicationInfo appInfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
			channel = appInfo.metaData.getString(key);	// 发到哪个市场
		}catch(Exception e){
			e.printStackTrace();
		}
		return channel;
	}
	public static void setMetaData(Context context, String key, String value){
		try{
			Context ctx = (context != null) ? context : Utils.getApp();
			ApplicationInfo appInfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
			appInfo.metaData.putString(key, value);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static Drawable getAppIco(Context context, String packageName){
		Drawable dw = null;
		if(context != null && !StringUtils.isNullStr(packageName)){
			try{
		        PackageInfo pi = getPackageInfo(context, packageName);
				dw = pi.applicationInfo.loadIcon(context.getPackageManager());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return dw;
	}
	public static Drawable getAppIco(Context context, PackageInfo packageInfo){
		Drawable dw = null;
		if(context != null && packageInfo != null){
			try{
				dw = packageInfo.applicationInfo.loadIcon(context.getPackageManager());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return dw;
	}
	public static String getPackageName(){
		return getPackageName(Utils.getApp());
	}
	public static String getPackageName(Context context){
		String pn = null;
		if(context != null){
			try{
				pn = context.getPackageName();
			}catch(Exception e){e.printStackTrace();}
		}
		return pn;
	}
	public static String getAppName(Context context, PackageInfo packageInfo){
		String dw = null;
		if(context != null && packageInfo != null){
			try{
				dw = packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString();
			}catch(Exception e){e.printStackTrace();}
		}
		return dw;
	}
	public static String getSign(Context context, String packageName) {
		String kw = null;
		if(context != null && !StringUtils.isNullStr(packageName)){
			try{
		        PackageInfo pi = getPackageInfo(context, packageName);
				Signature[] sg = pi.signatures;
	        	kw = MyMD5Util.getMD5Zw(sg[0].toByteArray());
			}catch(Exception e){e.printStackTrace();}
		}
        return kw;
    }
	public static Signature[] getSigns(Context context, String packageName) {
		Signature[] sgs = null;
		if(context != null && !StringUtils.isNullStr(packageName)){
			try{
				PackageInfo pi = getPackageInfo(context, packageName);
				sgs = pi.signatures;
			}catch(Exception e){e.printStackTrace();}
		}
        return sgs;
    }
	
	public static boolean startApp(Context context, String packageName){
		boolean isOK = false;
		try{
			PackageManager pm = context.getPackageManager();
		    Intent intent = pm.getLaunchIntentForPackage(packageName);//获取启动的包名
		    BaseUtils.startActivity(context, intent);
		    isOK = true;
		}catch(Exception ignored){}
	return isOK;
	}
	public static boolean startAppInfo(Context context, String packageName) throws Exception {
		boolean isOK = false;
		Uri uri = Uri.fromParts("package", packageName, null);
		Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri);
		BaseUtils.startActivity(context, intent);
		isOK = true;
		return isOK;
	}
	public static void installApp(Context context, String apkPath){
		installApp(context, new File(apkPath));
	}
	public static void installApp(Context context, File file){
		try{
			Intent intent = new Intent(Intent.ACTION_VIEW);
			// 由于没有在Activity环境下启动Activity,设置下面的标签
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setDataAndType(NbUriUtils.fromFile(file, true), "application/vnd.android.package-archive");
			BaseUtils.startActivity(context, intent);
		}catch(Exception ignored){}
	}
	public static boolean uninstallApp(Context context, String packageName){
		boolean isOK = false;
		try{
			Uri uri = Uri.fromParts("package", packageName, null);
			Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, uri);
			BaseUtils.startActivity(context, intent);
		    isOK = true;
		}catch(Exception ignored){}
		return isOK;
	}
	public static PackageInfo getPackageInfo(Context context){
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return info;
	}
	@SuppressLint("PackageManagerGetSignatures")
	public static PackageInfo getPackageInfo(Context context, String packageName){
		PackageInfo pi = null;
		try{
			PackageManager pm = context.getPackageManager();
	        pi = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
		}catch(Exception e){e.printStackTrace();}
		return pi;
	}

	public static String getPackageInfo(Context context, PackageInfo hv){
		String dw = null;
		if(context != null && hv != null){
			try{
				String format = DateUtils.dateFormatYMDHMSS;
				String appName = hv.applicationInfo.loadLabel(context.getPackageManager()).toString();
				String packageName = hv.applicationInfo.packageName;
				String sign = getSign(context, packageName);
				String version = "V"+hv.versionName+"_"+hv.versionCode;
				String firstInstallTime = new SimpleDateFormat(format).format(new Date(hv.firstInstallTime));
				String lastUpdateTime = new SimpleDateFormat(format).format(new Date(hv.lastUpdateTime));
				String apkBuildTime = AppUtils.getAppBuildTime(context, packageName, format);
				String dataDir = hv.applicationInfo.dataDir;
				String sourceDir = hv.applicationInfo.sourceDir;
				String apkSize = getApkSize(context, packageName)+"/"+getApkRealSize(context, packageName);
				String nativeLibraryDir = hv.applicationInfo.nativeLibraryDir;
				String permission = hv.applicationInfo.permission;
				String publicSourceDir = hv.applicationInfo.publicSourceDir;
				String processName = hv.applicationInfo.processName;
				String taskAffinity = hv.applicationInfo.taskAffinity;
				String targetSdkVersion = hv.applicationInfo.targetSdkVersion+"";
				String backupAgentName = hv.applicationInfo.backupAgentName;
				String className = hv.applicationInfo.className;
				String manageSpaceActivityName = hv.applicationInfo.manageSpaceActivityName;
				String name = hv.applicationInfo.name;
				Object metaData = hv.applicationInfo.metaData;
				String sharedLibraryFiles = Arrays.toString(hv.applicationInfo.sharedLibraryFiles);
				Object nonLocalizedLabel = hv.applicationInfo.nonLocalizedLabel;
				String signs = Arrays.toString(getSigns(context, packageName));
				
				String keys = "appName##packageName##sign##version##firstInstallTime##lastUpdateTime##apkBuildTime##dataDir##sourceDir##apkSize##nativeLibraryDir##permission##publicSourceDir##processName##taskAffinity##targetSdkVersion##backupAgentName##className##manageSpaceActivityName##name##metaData##sharedLibraryFiles##nonLocalizedLabel##signs";
				Object[] values = {appName,packageName,sign,version,firstInstallTime,lastUpdateTime,apkBuildTime,dataDir,sourceDir,apkSize,nativeLibraryDir,permission,publicSourceDir,processName,taskAffinity,targetSdkVersion,backupAgentName,className,manageSpaceActivityName,name,metaData,sharedLibraryFiles,nonLocalizedLabel,signs};
				dw = StringUtils.getPrintInfo("\n", keys, values);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return dw;
	}
    /**
     * 判断一个包名是否存在<br>
     */
	public static boolean findMyPackageName(Context ctx, String packageName){
		boolean isOK = false;
		try {
			PackageManager pm = ctx.getPackageManager();
			pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			isOK = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isOK;
	}

	/**
	 * 判断清单中一个Activity是否已注册<br>
	 */
	public static boolean findMyActivityName(Context ctx, String activityName){
		boolean isOK = false;
		if(ctx != null) {
			try {
				Intent intent = new Intent();
				intent.setClassName(ctx.getPackageName(), activityName);
				ResolveInfo resolveInfo = ctx.getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
				isOK = (resolveInfo != null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return isOK;
	}

	/**
	 * 获取Apk编译发布日期<br>
	 */
	public static String getAppBuildTime(Context ctx){
		String defDate = Vs.getValueByVs(Vs.release_date);
		String releaseDate = AppUtils.getAppBuildTime(ctx, DateUtils.dateFormatY);
		try{
			int year = (TextUtils.isEmpty(releaseDate)) ? -1 : Integer.parseInt(releaseDate);
			if (isRightYear(year)){
				releaseDate = AppUtils.getAppBuildTime(ctx, DateUtils.dateFormatYMDHMS);
				if(DateUtils.compareX(releaseDate, defDate) < 0){//releaseDate < defDate
					releaseDate = defDate;
				}
			}else{
				releaseDate = defDate;
			}
		}catch(Exception e){
			releaseDate = defDate;
		}

		int year = (TextUtils.isEmpty(releaseDate)) ? -1 :
				Integer.parseInt(DateUtils.getStringByFormat(releaseDate, DateUtils.dateFormatY));
		if(!isRightYear(year)){
			releaseDate = Mtas.defReleaseDate;
		}
		return releaseDate;
	}
	private static boolean isRightYear(int year){
		boolean isOK = false;
		if(year >= 2018 && year < 5000){
			isOK = true;
		}
		return isOK;
	}
	public static String getAppBuildTime(Context ctx, String format){
		String result = "";
		try {
			result = getAppBuildTime(ctx, ctx.getPackageName(),format);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return result;
	}
	public static String getAppBuildTime(Context ctx, String packageName, String format) {
		String result = "";
		try {
			ApplicationInfo ai = ctx.getPackageManager().getApplicationInfo(packageName,0);
			ZipFile zf = new ZipFile(ai.sourceDir);
			if(zf != null) {
				ZipEntry ze = zf.getEntry("META-INF/MANIFEST.MF");	// META-INF/MANIFEST.MF--classes.dex
				if (ze != null) {
					result = new SimpleDateFormat(format).format(new Date(ze.getTime()));
				}
			}
			zf.close();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return result;
	}
	public static String getApkSize(Context ctx, String packageName){
		String result = "0.0MB";
		try {
			ApplicationInfo ai = ctx.getPackageManager().getApplicationInfo(packageName,0);
			File f = new File(ai.sourceDir);
			result = FileUtils.getFileSize(f.length());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	public static long getApkRealSize(Context ctx, String packageName){
		long result = 0;
		try {
			ApplicationInfo ai = ctx.getPackageManager().getApplicationInfo(packageName,0);
			File f = new File(ai.sourceDir);
			result = f.length();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	public static boolean isSafetyDay(Context ctx){
		int day = Mtas.defDays;
		try{
			day = Integer.parseInt(Vs.getValueByVs(Vs.gap_days));
		}catch(Exception e){
			//e.printStackTrace();
		}
		return isSafetyDay(ctx, day);
	}
	public static boolean isSafetyDay(Context ctx, int days){
		String releaseDate = getAppBuildTime(ctx);
		Date rDate = DateUtils.getDateByFormat(releaseDate, DateUtils.dateFormatYMD);
		if(rDate == null) {
			rDate = DateUtils.getDateByFormat(Mtas.defReleaseDate, DateUtils.dateFormatYMD);
		}
		return DateUtils.isSafetyDay(rDate, days);
	}

	public static boolean isForeground(Activity act){
		boolean isOK = false;
		if(act != null){
			try{
				ActivityManager am = (ActivityManager) act.getSystemService(Context.ACTIVITY_SERVICE);
				List<RunningTaskInfo> list = am.getRunningTasks(1);
				if (list != null && list.size() > 0) {
					ComponentName cpn = list.get(0).topActivity;
					String className = act.getClass().getName();
					if (className.equals(cpn.getClassName())) {
						isOK = true;
					}
				}
			}catch(Exception e){
				isOK =  false;
				e.printStackTrace();
			}
		}
		return isOK;
	}

	public static boolean hasGdtKpPermission(Context context){
		String className = ZFactory.sSp;
		return NetworkUtils.isNetworkAvailable(context)
				&& AppUtils.hasGdtPermission(context) && Sws.Kp.flag
				&& Fs.findMyClass(className) && AppUtils.findMyActivityName(context, className)
				&& hasAgreePrivacyPolicy()	//防止未同意双协议开启广告
				&& !isClosePushAd() //未关闭推送广告时才开启广告
				;
	}
	public static boolean hasGdtPermission(Context context){
		return hasPermission(context, R.array.gdt_permission_array)
				&& hasAgreePrivacyPolicy()	//防止未同意双协议开启广告
				&& !isClosePushAd() //未关闭推送广告时才开启广告
				;
	}
	public static boolean hasUmMTAPermission(Context context){
		//开发者中心  https://developer.umeng.com/docs/66632/detail/101848
		return hasPermission(context, R.array.um_permission_array);
	}
	public static boolean hasXmbMTAPermission(Context context){
		return hasPermission(context, R.array.xmb_mta_permission_array);
	}
	public static boolean hasACachePermission(Context context){
		return hasPermission(context, R.array.a_cache_permission_array);
	}
	public static boolean hasSDCardPermission(Context context){
		return hasLegacyExternalStorage() && hasACachePermission(context);
	}

	/**
	 * 有旧的外置存储(未开启分区存储模式)，返回true<br>
	 */
	public static boolean hasLegacyExternalStorage(){
		//大于等于29[Android Q]且开启分区存储：无法访问外置SD卡，所以返回false
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q &&
				!Environment.isExternalStorageLegacy()) {
			return false;
		}else{
			return true;
		}
	}
	public static boolean hasMacPermission(Context context){
		return hasPermission(context, R.array.mac_permission_array);
	}
	public static boolean hasPhonePermission(Context context){
		return hasPermission(context, R.array.phone_permission_array);
	}
	private static boolean hasPermission(Context context, int resID){
		boolean isOK = false;
		try {
			if(context == null) context = Utils.getApp();
			String[] perAry = context.getResources().getStringArray(resID);
			isOK = hasPermission(context, perAry);
		}catch (Exception e){
			e.printStackTrace();
		}
		return isOK;
	}
	public static boolean hasPermission(Context context, String... permissions){
		if(context == null) context = Utils.getApp();
		return XXPermissions.isGranted(context, permissions);
	}
	public static boolean hasPermission(String... permissions){
		return XXPermissions.isGranted(Utils.getApp(), permissions);
	}

	/**
	 * 同意隐私政策：Spu.saveV(getActivity(), Spu.VSpu.agreement.name(), "suc");
	 * 不同意隐私政策：Spu.saveV(getActivity(), Spu.VSpu.agreement.name(), "no");
	 * @return 同意隐私政策返回true，否则返回false
	 */
	public static boolean hasAgreePrivacyPolicy(){
		return Spu.isSucK(Spu.VSpu.agreement.name());
	}

	/**
	 * 关闭推送广告：Spu.saveV(getActivity(), Spu.VSpu.close_push_ad.name(), "suc");
	 * 开启推送广告：Spu.saveV(getActivity(), Spu.VSpu.close_push_ad.name(), "no");
	 * @return 关闭推送广告返回true，否则返回false
	 */
	public static boolean isClosePushAd(){
		return Spu.isSucK(Spu.VSpu.close_push_ad.name());
	}
	public static boolean checkPermission(Context context, String permission){
	    boolean result = false;
	    if (Build.VERSION.SDK_INT >= 23) {//6.0
	        try {
	            Class<?> clazz = Class.forName("android.content.Context");
	            Method method = clazz.getMethod("checkSelfPermission", String.class);
	            int rest = (Integer) method.invoke(context, permission);
	            if (rest == PackageManager.PERMISSION_GRANTED) {
	                result = true;
	            } else {
	                result = false;
	            }
	        } catch (Exception e) {
	            result = false;
	        }
	    } else {
	        PackageManager pm = context.getPackageManager();
	        if (pm.checkPermission(permission, context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
	            result = true;
	        }
	    }
	    return result;
	}

	public static String sNilApps ="NO##应用名称##应用包名##版本号##友盟ID##友盟渠道名称##应用别名##证书名称##应用市场信息";
	public static void popAddNilAppsDlg(final Context ctx){
		final EditText et = new EditText(ctx);
		et.setHint("NO##应用名称##应用包名##版本号##友盟ID##友盟渠道名称##应用别名##证书名称##应用市场信息");
		et.setMinLines(5);
		String nilAppsV = new Spu(ctx, "nil_apps").loadStringSharedPreference("nil_apps_key");
		if(nilAppsV != null && !"".equals(nilAppsV.trim())) et.setText(nilAppsV);

		new AlertDialog.Builder(ctx).setTitle("导入测试软件信息")
				.setView(et)
				.setPositiveButton("开始导入", new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String input = et.getText().toString();
						if (StringUtils.isNullStr(input)) {
							ToastUtils.showShort("测试软件信息不能为空.");
						}else if (!input.contains("##") || !input.contains(",")) {
							ToastUtils.showShort("测试软件信息不合法.");
						}else {
							new Spu(ctx, "nil_apps").saveSharedPreferences("nil_apps_key", input);
							ToastUtils.showShort("测试软件信息导入成功！");
						}
					}
				}).setNegativeButton("取消", null).show();
	}

	/**
	 * 获取当前进程名称<br>
	 */
	public static String getMyProcessName(Context context) {
		try {
			ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningAppProcessInfo> runningApps = (am == null)? null : am.getRunningAppProcesses();
			if (runningApps == null) {
				return null;
			}
			for (ActivityManager.RunningAppProcessInfo proInfo : runningApps) {
				if (proInfo.pid == android.os.Process.myPid()) {
					if (proInfo.processName != null) {
						return proInfo.processName;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 判断当前应用是否是debug状态<br>
	 */
	public static boolean isAppDebug(Context context) {
		boolean isOK = false;
		if(context == null){
			try {
				isOK = com.blankj.utilcode.util.AppUtils.isAppDebug();
			} catch (Exception e) {
				e.printStackTrace();
				LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			}
		}else{
			try {
				ApplicationInfo info = context.getApplicationInfo();
				isOK = (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
			} catch (Exception e) {
				e.printStackTrace();
				LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			}
		}
		if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "debug-->"+isOK);
		return isOK;
	}

}
