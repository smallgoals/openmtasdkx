package com.nil.sdk.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import androidx.core.content.ContextCompat;

/**
 * Android 6 完美解决 WRITE_SETTINGS 权限设置问题 - 简书<br>
 * https://www.jianshu.com/p/0b880871b887<br>
 */
public class PermissionsChecker {
    private static final String TAG = PermissionsChecker.class.getSimpleName();

    // 判断权限集合，返回true时表示缺少相应权限
    public static boolean lacksPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (lacksPermission(context, permission)) {
                //Log.d(TAG,"is checking permission: " + permission);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//23
                    if(permission.equals(Manifest.permission.WRITE_SETTINGS)
                            && Settings.System.canWrite(context)) {
                        continue;
                    }else if(permission.equals(Manifest.permission.SYSTEM_ALERT_WINDOW)
                            && Settings.canDrawOverlays(context)){
                        continue;
                    }
                }
                return true;
            }
        }
        return false;
    }

    // 判断是否缺少权限，返回true时表示缺少相应权限
    public static boolean lacksPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_DENIED;
    }
}
