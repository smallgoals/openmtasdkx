package com.nil.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.blankj.utilcode.util.Utils;
import com.nil.vvv.utils.Mtas;

import java.util.List;
import java.util.Map;


/**
 * 存储配置信息的工具类 <br>
 * 注：可读取的数据类型有-<code>boolean、int、float、long、String.</code>
 */
public class Spu {
	private final String MAK = "spu";
	private final int MODE = Context.MODE_PRIVATE;//Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE;
	private SharedPreferences sharedpreferences = null;
	private int mEncryptMode = 0; //0:none 1:aes 2:base
	private final String ENCRYPT_TYPE = "encrypt_type"; //0:none 1:aes 2:base
	
	public Spu(Context context, String fileName) {
		if(context != null && !StringUtils.isNullStr(fileName)){
			sharedpreferences = context.getSharedPreferences(fileName, MODE);
			mEncryptMode = sharedpreferences.getInt(ENCRYPT_TYPE, 0);
			saveSharedPreferences(ENCRYPT_TYPE, mEncryptMode);
		}
	}

	public boolean saveSharedPreferences(String key, String value) {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		try {
			String str =  (mEncryptMode == 1)?AESEncryptor.encrypt(MAK, value) : value;
			editor.putString(key, str);
		} catch (Exception e) {
			editor.putString(key, value);
			e.printStackTrace();
		}
		return editor.commit();
	}

	public String loadStringSharedPreference(String key) {
		String str = null;
		try {
			str = sharedpreferences.getString(key, null);
			if (str != null && !"".equals(str)){
				str = (mEncryptMode == 1)?AESEncryptor.decrypt(MAK, str) : str;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	public boolean saveSharedPreferences(String key, int value) {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putInt(key, value);
		return editor.commit();
	}

	public int loadIntSharedPreference(String key) {
		return sharedpreferences.getInt(key, 0);
	}

	public boolean saveSharedPreferences(String key, float value) {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putFloat(key, value);
		return editor.commit();
	}

	public float loadFloatSharedPreference(String key) {
		return sharedpreferences.getFloat(key, 0f);
	}

	public boolean saveSharedPreferences(String key, Long value) {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putLong(key, value);
		return editor.commit();
	}

	public Long loadLongSharedPreference(String key) {
		return sharedpreferences.getLong(key, 0l);
	}

	public boolean saveSharedPreferences(String key, Boolean value) {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putBoolean(key, value);
		return editor.commit();
	}

	public boolean loadBooleanSharedPreference(String key) {
		return sharedpreferences.getBoolean(key, false);
	}

	public boolean saveAllSharePreference(String keyName, List<?> list) {
		int size = list.size();
		if (size < 1) {
			return false;
		}
		SharedPreferences.Editor editor = sharedpreferences.edit();
		if (list.get(0) instanceof String) {
			for (int i = 0; i < size; i++) {
				editor.putString(keyName + i, (String) list.get(i));
			}
		} else if (list.get(0) instanceof Long) {
			for (int i = 0; i < size; i++) {
				editor.putLong(keyName + i, (Long) list.get(i));
			}
		} else if (list.get(0) instanceof Float) {
			for (int i = 0; i < size; i++) {
				editor.putFloat(keyName + i, (Float) list.get(i));
			}
		} else if (list.get(0) instanceof Integer) {
			for (int i = 0; i < size; i++) {
				editor.putLong(keyName + i, (Integer) list.get(i));
			}
		} else if (list.get(0) instanceof Boolean) {
			for (int i = 0; i < size; i++) {
				editor.putBoolean(keyName + i, (Boolean) list.get(i));
			}
		}
		return editor.commit();
	}

	public Map<String, ?> loadAllSharePreference() {
		return sharedpreferences.getAll();
	}

	public boolean removeKey(String key) {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.remove(key);
		return editor.commit();
	}

	public boolean removeAllKey() {
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.clear();
		return editor.commit();
	}
	
	public static Spu sSpu;
	public static final String ETC_NAME = "core_etc";
	public enum VSpu{private_xy,auto_update,good_rate,debug,ads,iscj,preload,ad_pre,agreement, open_url, download_url,zfb_url,close_push_ad}
	public static void saveVI(Context ctx, String key, Long value){
		try{
			getSpu(ctx).saveSharedPreferences(key, value);
		}catch(Exception e){e.printStackTrace();};
	}
	public static Long loadVI(Context ctx, String key){
		if(ctx == null || StringUtils.isNullStr(key)) return -1l;
		return getSpu(ctx).loadLongSharedPreference(key);
	}
	public static boolean isSucAdTime(Context ctx, long millis){
		long preTime = loadVI(ctx, VSpu.ad_pre.name());
		long curTime = System.currentTimeMillis();
		return ((curTime-preTime) > millis) || Mtas.isCrazyAd;
	}
	public static boolean isSucAdTime(Context ctx){
		return isSucAdTime(ctx, 60*1000);	//60s
	}
	
	public static void saveV(Context ctx, String key, String value){
		try{
			getSpu(ctx).saveSharedPreferences(key, value);
		}catch(Exception e){e.printStackTrace();};
	}
	public static void saveV(Context ctx, String key){
		try{
			getSpu(ctx).saveSharedPreferences(key, "suc");
		}catch(Exception e){e.printStackTrace();};
	}
	public static String loadV(Context ctx, String key){
		if(ctx == null || StringUtils.isNullStr(key)) return "";
		return getSpu(ctx).loadStringSharedPreference(key);
	}

	public static void saveV2(Context ctx, String key, String value, String format){
		try{
			String date = DateUtils.getCurDateByFormat(format);
			getSpu(ctx).saveSharedPreferences(key+"_"+date, value);
		}catch(Exception e){e.printStackTrace();};
	}
	public static void saveV2(Context ctx, String key, String format){
		try{
			String date = DateUtils.getCurDateByFormat(format);
			getSpu(ctx).saveSharedPreferences(key+"_"+date, "suc");
		}catch(Exception e){e.printStackTrace();};
	}
	public static String loadV2(Context ctx, String key, String format){
		if(ctx == null || StringUtils.isNullStr(key)) return "";
		try {
			String date = DateUtils.getCurDateByFormat(format);
			return getSpu(ctx).loadStringSharedPreference(key + "_" + date);
		}catch (Exception e){
			e.printStackTrace();
			return "";
		}
	}
	public static void saveVDay(Context ctx, String key, String value){
		saveV2(ctx, key, value, DateUtils.dateFormatYMD);
	}
	public static void saveVDay(Context ctx, String key){
		saveV2(ctx, key, DateUtils.dateFormatYMD);
	}
	public static String loadVDay(Context ctx, String key){
		return loadV2(ctx, key, DateUtils.dateFormatYMD);
	}

	public static void saveVMd5(Context ctx, String key, String value){
		saveV(ctx, MyMD5Util.getMD5ByString(key), value);
	}
	public static void saveVMd5(Context ctx, String key){
		saveV(ctx, MyMD5Util.getMD5ByString(key));
	}
	public static String loadVMd5(Context ctx, String key){
		return loadV(ctx, MyMD5Util.getMD5ByString(key));
	}

	public static boolean isSucV(String value){
		return StringUtils.equalsIgnoreCase(value, "1") || StringUtils.equalsIgnoreCase(value, "suc")
				|| StringUtils.equalsIgnoreCase(value, "open") || StringUtils.equalsIgnoreCase(value, "enable");
	}
	public static boolean isSucK(Context ctx, String key){
		return isSucV(loadV(ctx, key));
	}
	public static boolean isSucK(String key){
		return isSucV(loadV(Utils.getApp(), key));
	}
	public static boolean isFailV(String value){
		return StringUtils.equalsIgnoreCase(value, "-1") || StringUtils.equalsIgnoreCase(value, "no");
	}
	public static Spu getSpu(Context ctx){
		if(sSpu == null) sSpu = new Spu(ctx, ETC_NAME);
		return sSpu;
	}
}
