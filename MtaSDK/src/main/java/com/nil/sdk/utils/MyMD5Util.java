package com.nil.sdk.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MyMD5Util {
	/**
	 * 将字符串转成MD5值
	 * @param string
	 * @return
	 */
	public static String getMD5ByString(String string) {
		byte[] hash;
		try {
			string = (string == null) ? "" : string;
			hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}

		StringBuilder hex = new StringBuilder(hash.length * 2);
		for (byte b : hash) {
			if ((b & 0xFF) < 0x10)
				hex.append("0");
			hex.append(Integer.toHexString(b & 0xFF));
		}
		return hex.toString();
	}
	
	public static String getMD5Zw(String string) {
		byte[] hash;
		try {
			string = (string == null) ? "" : string;
			hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
		StringBuffer hex = new StringBuffer();
		int len = hash.length;
		for(int i = 0; i < len; i++){
			byte b = hash[i];
			if ((b & 0xFF) < 0x10)
				hex.append("0");
			hex.append(Integer.toHexString(b & 0xFF));
			if(i != len-1) hex.append(":");
		}
		
		return hex.toString().toUpperCase();
	}
	
	public static String getMD5Zw(byte[] ary) {
		return getMD5Zw(ary, null);
	}
	public static String getMD5Zw(byte[] ary, String split) {
		if(ary == null || ary.length < 1) return "MD5Zw is null...";
		byte[] hash;
		try {
			hash = MessageDigest.getInstance("MD5").digest(ary);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		StringBuffer hex = new StringBuffer();
		int len = hash.length;
		for(int i = 0; i < len; i++){
			byte b = hash[i];
			if ((b & 0xFF) < 0x10)
				hex.append("0");
			hex.append(Integer.toHexString(b & 0xFF));
			if(i != len-1 && split != null) hex.append(split);
		}
		
		return hex.toString().toUpperCase();
	}
}
