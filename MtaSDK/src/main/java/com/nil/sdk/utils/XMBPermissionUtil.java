package com.nil.sdk.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;

import androidx.appcompat.app.AlertDialog;

/**
 * 权限工具类<br>
 */
@Deprecated
public class XMBPermissionUtil {
    @SuppressLint("WrongConstant")
    public static void doRequestPermissions(final PermissionCallback permissionCallback, final Activity act, String... permissions) {
        if (!PermissionUtils.isGranted(permissions)) {//未授权
            PermissionUtils.permission(permissions).callback(new PermissionUtils.SimpleCallback() {
                @Override
                public void onGranted() {
                    permissionCallback.whenGrant();
                }

                @Override
                public void onDenied() {
                    gotoSetting2GivePermission(act);
                }
            }).request();

        } else {//已授权
            permissionCallback.whenGrant();
        }
    }

    private static void gotoSetting2GivePermission(final Activity act) {
        new AlertDialog.Builder(act).setTitle("该功能需要权限").setMessage("请前往设置中心给予权限")
                .setNegativeButton("取消", null)
                .setPositiveButton("去设置", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", Utils.getApp().getPackageName(), null);
                        intent.setData(uri);
                        BaseUtils.startActivity(act,intent);
                    }
                }).show();
    }

    public interface PermissionCallback {
        void whenGrant();
    }

}
