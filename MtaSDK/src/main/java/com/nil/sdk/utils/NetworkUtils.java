package com.nil.sdk.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;

/**
 * @declaration 网络?测工具类
 * @author ksA2y511xh@163.com
 *2012-10-14 下午10:13:06
 */
public class NetworkUtils {
	private final static String TAG = NetworkUtils.class.getSimpleName();
	public final static String NetworkStateKey = "NetworkStateKey";
	public final static int NONE = 0;// 无网?
	public final static int WIFI = 1;// Wi-Fi
	public final static int MOBILE = 2;// 3G,GPRS

	/**
	 * 
	 * 获取当前网络状??
	 * 
	 * @param context
	 * @return
	 */
	@SuppressLint("MissingPermission")
	public static int getNetworkState(Context context) {
		if (context != null) {
			ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connManager == null){
				return NONE;
			}

			// Wifi网络判断
			NetworkInfo networkInfo= connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (networkInfo != null){
				State state = networkInfo.getState();
				if (state == State.CONNECTED || state == State.CONNECTING) {
					return WIFI;
				}
			}

			// 手机网络判断
			networkInfo= connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (networkInfo != null){
				State state = networkInfo.getState();
				if (state == State.CONNECTED || state == State.CONNECTING) {
					return MOBILE;
				}
			}
		}
		return NONE;
	}
	
	/**
	 * 是否有可用网?<br>
	 * @param context
	 * @return
	 */
	//@RequiresPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
	@SuppressLint("MissingPermission")
	public static boolean isNetworkAvailable(Context context) {
		boolean isOK = false;
		if(!AppUtils.hasPermission(context, Manifest.permission.ACCESS_NETWORK_STATE)){
			//Android中判断连接的网络是否可用以及是否连接网络 https://blog.csdn.net/lxd_love_lgc/article/details/110540060
			isOK = StringUtils.noNullStr(ACacheUtils.getCacheValue(NetworkStateKey));	//com.nil.sdk.ui.BaseUtils.onResume进行检测更新状态
		}else {
			if (context != null) {
				ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
				if (connManager != null) {
					NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
					if (networkInfo != null) {
						isOK = networkInfo.isAvailable();
					}
				}
			}
		}
		if (Spu.isSucK(TAG)) LogUtils.dTag(TAG, "isNetworkAvailable==" + isOK);
		return isOK;
	}

	/**
	 * 跳转到网络设置界?(wifi or 3g)
	 * @param context
	 */
	public static void gotoNetworkSetUI(Context context, int types){
		try {
			String action = android.provider.Settings.ACTION_WIFI_SETTINGS;
			if(types == MOBILE){
				 // 跳转到无线网络设置界?
				action = android.provider.Settings.ACTION_WIRELESS_SETTINGS;
			}else if(types == WIFI){
				  // 跳转到无限wifi网络设置界面
				action = android.provider.Settings.ACTION_WIFI_SETTINGS;
			}
			Intent intent = new Intent(action);
			BaseUtils.startActivity(context, intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void gotoNetworkSetUI(Context context){
		gotoNetworkSetUI(context, WIFI);
	}
	public static void gotoNetworkSetUI(){
		gotoNetworkSetUI(Utils.getApp());
	}
}

