package com.nil.sdk.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * 日期工具类<br>
 */
//@SuppressLint("SimpleDateFormat")
public class DateUtils {
	public static final String dateFormatYMDHMSS = "yyyy/MM/dd HH:mm:ss.SSS";
	public static final String dateFormatYMDHMS = "yyyy/MM/dd HH:mm:ss";
	public static final String dateFormatYMDHM = "yyyy/MM/dd HH:mm";
	public static final String dateFormatYMD = "yyyy/MM/dd";
	public static final String dateFormatYM = "yyyy/MM";
	public static final String dateFormatY = "yyyy";
	public static final String dateFormatMD = "MM/dd";
	public static final String dateFormatHMS = "HH:mm:ss";
	public static final String dateFormatHM = "HH:mm";
	public static final String AM = "AM";
	public static final String PM = "PM";


	/**
	 * 指定日期是否到期（date大于当前日期，表示已到期true）<br>
	 */
	public static boolean isExpire(String date){
		return isExpire(date, 0);
	}
	public static boolean isExpire(String date, int days){
		boolean isOK = false;
		try {
			String curDate = getCurDate();
			isOK = !isSafetyDay(curDate, date, days);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return isOK;
	}

	/**
	 * 安全为true：releaseDate + days <= curDate<br>
	 */
	public static boolean isSafetyDay(String curDate, String releaseDate, int days){
		boolean isOK = false;
		try{
			Date cDate = getDateByFormat(curDate, dateFormatYMD);
			Date rDate = getDateByFormat(releaseDate, dateFormatYMD);
			int offset = getOffectDay(cDate, rDate);
			isOK = offset >= days;
		}catch(Exception e){
			e.printStackTrace();
		}
		return isOK;
	}
	public static boolean isSafetyDay(Date rDate, int days){
		boolean isOK = false;
		try{
			Date cDate = new Date();
			int offset = getOffectDay(cDate, rDate);
			isOK = offset >= days;
		}catch(Exception e){
			e.printStackTrace();
		}
		return isOK;
	}
	public static int getOffectDay(String date) {
		return getOffectDay(getCurDate(), date);
	}
	public static int getOffectDay(String endDate, String beginDate) {
		return (int) getOffectDayByLong(getDateByFormat(endDate, dateFormatYMDHMSS), getDateByFormat(beginDate, dateFormatYMDHMSS));
	}
	public static int getOffectDay(Date endDate, Date beginDate) {
		return (int) getOffectDayByLong(endDate,beginDate);
	}
	public static long getOffectDayByLong(Date endDate, Date beginDate) {
		long days = 0;
		try {
			Calendar bCal = Calendar.getInstance();
			Calendar dCal = Calendar.getInstance();
			bCal.setTime(beginDate);
			dCal.setTime(endDate);

			//设置时间为0时，忽略时分秒
			bCal.set(Calendar.HOUR_OF_DAY, 0);
			bCal.set(Calendar.MINUTE, 0);
			bCal.set(Calendar.SECOND, 0);

			dCal.set(Calendar.HOUR_OF_DAY, 0);
			dCal.set(Calendar.MINUTE, 0);
			dCal.set(Calendar.SECOND, 0);

			//得到两个日期相差的天数（9万年会溢出）
			days = ((dCal.getTime().getTime()-bCal.getTime().getTime())/1000/3600/24);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return days;
	}
	public static Date getDateByFormat(String strDate, String format) {
		Date date = null;
		try {
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			date = mSimpleDateFormat.parse(strDate);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return date;
	}
	public static String getStringByFormat(Date date, String format) {
		String strDate = null;
		try {
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			strDate = mSimpleDateFormat.format(date);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return strDate;
	}
	public static String getStringByFormat(String date, String format) {
		String strDate = null;
		try {
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			strDate = mSimpleDateFormat.format(getDateByFormat(date, format));
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return strDate;
	}
	public static String getStringByFormat(long millis, String format) {
		return getStringByFormat(new Date(millis), format);
	}
	public static String getCurDateByFormat(String format){
		return getStringByFormat(new Date(), format);
	}
	public static String getCurDate(){
		return getStringByFormat(new Date(), dateFormatYMDHMSS);
	}
	public static int getCurYear(){
		return Calendar.getInstance().get(Calendar.YEAR); // 获取当前年份
	}
	public static int getCurMonth(){
		return Calendar.getInstance().get(Calendar.MONTH); // 获取当前月份
	}
	public static int getCurDay(){
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH); // 获取当前日
	}

	public static int compareX(String beginTime, String endTime){	//日期比较，前者大则返回大于0
		int ii = -100;
		try {
			@SuppressLint("SimpleDateFormat")
			SimpleDateFormat sdf=new SimpleDateFormat(dateFormatYMD);
			Date bt = sdf.parse(beginTime);
			Date et = sdf.parse(endTime);
			if (bt.after(et)) {	//be>et
				ii = 1;
			}else if(bt.equals(et)){
				ii = 0;
			}else {
				ii = -1;
			}
		}catch (Exception e){
			e.printStackTrace();
			ii = -300;
		}
		return ii;
	}
}
