package com.nil.sdk.utils;

import android.content.Context;
import android.os.Vibrator;

/**
 * @Description 震动工具类<br>
 */
public class VibratorUtil {
	private final static long SHORT_TIME = 50;

	public static void vibrate(Context ctx) {
		vibrate(ctx, SHORT_TIME);
	}

	public static void vibrate(Context ctx, long time) {
		if(ctx == null) return;
		try{
			Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(time);
		}catch(Exception e){
			e.printStackTrace();
		};
	}
}
