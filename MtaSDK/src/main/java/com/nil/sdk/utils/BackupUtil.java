package com.nil.sdk.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;

import com.blankj.utilcode.util.ToastUtils;

import java.io.File;
import java.io.IOException;

/**
 * @author Geek_Soledad (66704238@51uc.com)
 */
@SuppressLint("SdCardPath")
public class BackupUtil {
	private final String SHARED_PREFS;
	private final String DATABASES;
	private final String APP_PATH;
	private final Context mContext;
	private String BACKUP_PATH;
	private final String BACKUP_DATABASES;
	private final String BACKUP_SHARED_PREFS;

	public BackupUtil(Context context) {
		mContext = context;
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		APP_PATH = new StringBuilder("/data/data/").append(info.packageName).toString();
		SHARED_PREFS = APP_PATH + "/shared_prefs";
		DATABASES = APP_PATH + "/databases";

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			BACKUP_PATH = AppConstantInfo.RootDirPath;
		} else {
			BACKUP_PATH = info.packageName + "/backup";
			ToastUtils.showShort("没有检测到SD卡，可能无法备份成功");
		}

		BACKUP_PATH += "/"+info.packageName;
		BACKUP_DATABASES = BACKUP_PATH + "/database";
		BACKUP_SHARED_PREFS = BACKUP_PATH + "/shared_prefs";
	}

	/**
	 * 备份文件
	 * 
	 * @return 当且仅当数据库及配置文件都备份成功时返回true<br>
	 */
	public boolean doBackup() {
		boolean isOK = backupDB() && backupSharePrefs();
		if (isOK){
			showToast("备份数据成功:" + BACKUP_PATH+".");
		}else {
			showToast("备份数据失败.");
		}
		return true;
	}

	private boolean backupDB() {
		return copyDir(DATABASES, BACKUP_DATABASES, "备份数据库文件成功:"
				+ BACKUP_DATABASES, "备份数据库文件失败");
	}

	private boolean backupSharePrefs() {
		return copyDir(SHARED_PREFS, BACKUP_SHARED_PREFS, "备份配置文件成功:"
				+ BACKUP_SHARED_PREFS, "备份配置文件失败");
	}

	/**
	 * 恢复
	 * 
	 * @return 当且仅当数据库及配置文件都恢复成功时返回true<br>
	 */
	public boolean doRestore() {
		boolean isOK = restoreDB() && restoreSharePrefs();
		if (isOK) {
			showToast("恢复数据成功."/* + BACKUP_PATH + "."*/);
		} else {
			showToast("恢复数据失败.");
		}
		return true;
	}

	private boolean restoreDB() {
		return copyDir(BACKUP_DATABASES, DATABASES, "恢复数据库文件成功", "恢复数据库文件失败");
	}

	private boolean restoreSharePrefs() {
		return copyDir(BACKUP_SHARED_PREFS, SHARED_PREFS, "恢复配置文件成功",
				"恢复配置文件失败");
	}

	private final void showToast(String msg) {
		ToastUtils.showShort(msg);
	}

	/**
	 * 复制目录
	 * 
	 * @param srcDir
	 *            源目录
	 * @param destDir
	 *            目标目录
	 * @param successMsg
	 *            复制成功的提示语
	 * @param failedMsg
	 *            复制失败的提示语
	 * @return 当复制成功时返回true, 否则返回false<br>
	 */
	private final boolean copyDir(String srcDir, String destDir,
			String successMsg, String failedMsg) {
		try {
			FileUtils.copyDirectory(new File(srcDir), new File(destDir));
		} catch (IOException e) {
			e.printStackTrace();
			//showToast(failedMsg);
			return false;
		}
		//showToast(successMsg);
		return true;
	}
}