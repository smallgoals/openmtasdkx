package com.nil.sdk.ui;


import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.utils.Spu;

import java.lang.ref.WeakReference;

/**
 * Author: liaohaiping
 * Time: 2018-09-04
 * Description:
 */
public class ActivitySafeHandler extends Handler {
    private final static String TAG = ActivitySafeHandler.class.getSimpleName();
    public enum ActivityState {CREATED, RESUMED, PAUSED, STOPED, DESTORYED}

    private WeakReference<Activity> weakReferenceHandler;
    private ActivityState mState;

    public ActivitySafeHandler(Activity handler) {
        super();
        mState = ActivityState.CREATED;
        this.weakReferenceHandler = new WeakReference<>(handler);
    }

    @Override
    public void handleMessage(Message msg) {
        try {
            Activity activity = this.weakReferenceHandler.get();
            if(activity != null && mState != ActivityState.DESTORYED) {
                if(activity instanceof BaseHandlerActivity){
                    ((BaseHandlerActivity)activity).processUIMessage(msg);
                }else if(activity instanceof BaseHandlerAppCompatActivity){
                    ((BaseHandlerAppCompatActivity)activity).processUIMessage(msg);
                }else  if(activity instanceof BaseHandlerFragmentActivity) {
                    ((BaseHandlerFragmentActivity) activity).processUIMessage(msg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onResume() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "onResume...");
        mState = ActivityState.RESUMED;
    }
    public void onPause() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "onPause...");
        mState = ActivityState.PAUSED;
    }
    public void onStop() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "onStop...");
        mState = ActivityState.STOPED;
    }
    public void onDestory() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "onDestory...");
        mState = ActivityState.DESTORYED;
    }
}
