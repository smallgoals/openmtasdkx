package com.nil.sdk.ui;

import android.os.Bundle;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

public class BaseHandlerActivity extends BaseActivity {
    protected ActivitySafeHandler uiHandler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        uiHandler = new ActivitySafeHandler(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        uiHandler.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        uiHandler.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        uiHandler.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHandler.removeCallbacksAndMessages(null);
        uiHandler.onDestory();
    }

    public void processUIMessage(Message message) {

    }

    public void postUIMessage(int what) {
        Message msg = Message.obtain(uiHandler);
        msg.what = what;
        msg.sendToTarget();
    }
    public void postUIMessage(int what, Object obj) {
        Message msg = Message.obtain(uiHandler);
        msg.what = what;
        msg.obj = obj;
        msg.sendToTarget();
    }
    public void postUIMessage(int what, int arg1, int arg2, Object obj) {
        Message msg = Message.obtain(uiHandler);
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;
        msg.sendToTarget();
    }

    public void postUIMessage(int what, Bundle data) {
        Message msg = Message.obtain(uiHandler);
        msg.what = what;
        msg.setData(data);
        msg.sendToTarget();
    }
}
