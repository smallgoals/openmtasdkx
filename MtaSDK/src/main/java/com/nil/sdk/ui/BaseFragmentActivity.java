package com.nil.sdk.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.LayoutRes;
import androidx.fragment.app.FragmentActivity;

public class BaseFragmentActivity extends FragmentActivity {
	public Activity getActivity(){return this;}
	protected Context context;
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		context = this;
		BaseUtils.onCreate(this);
	}
	public void setContentView(@LayoutRes int layoutResID) {
		super.setContentView(layoutResID);
		BaseUtils.initView(this);
	}
	public void setContentView(View view) {
		super.setContentView(view);
		BaseUtils.initView(this);
	}
	public void setContentView(View view, ViewGroup.LayoutParams params) {
		super.setContentView(view, params);
		BaseUtils.initView(this);
	}
	public void onBackPressed() {
		BaseUtils.onBackPressed(this);
	}
	protected void onResume() {
		super.onResume();
		BaseUtils.onResume(this);
	}
	protected void onPause() {
		super.onPause();
		BaseUtils.onPause(this);
	}

	public void setFullScreen(){
		// 全屏设置必须放setContentView(R.layout.xxy)方法之前
		try {
			//去除标题栏
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			//去除状态栏
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public void setNoScreenshot(){
		try{
			// 禁止截屏
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}