package com.nil.sdk.ui.aid;

import android.content.Context;
import android.content.DialogInterface;
import android.util.TypedValue;
import android.widget.TextView;

import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;

import androidx.appcompat.app.AlertDialog;

/**
 * @declaration 自定义弹出对话框<br>
 * @author ksA2y511xh@163.com<br>
 * 2012-10-17 下午10:27:42
 */
@Deprecated
public class MyTipDialog {
	public interface IDialogMethod{
		public void sure();
		public void neutral();
		public void cancel();
		public void sure(Object obj);
		public void neutral(Object obj);
		public void cancel(Object obj);
		public void sure(DialogInterface dialog,int which);
		public void neutral(DialogInterface dialog,int which);
		public void cancel(DialogInterface dialog,int which);
	}
	public static abstract class DialogMethod implements IDialogMethod{
		public abstract void sure();
		public void neutral(){}
		public void cancel(){}
		public void sure(Object obj){}
		public void neutral(Object obj){}
		public void cancel(Object obj){}
		public void sure(DialogInterface dialog,int which){}
		public void neutral(DialogInterface dialog,int which){}
		public void cancel(DialogInterface dialog,int which){}
	}
	
	/**
	 * 获取自定义对话框<br>
	 * @param title 标题<br>
	 * @param msg 消息内容<br>
	 * @param ok 确认按钮<br>
	 * @param neutral 中立按钮<br>
	 * @param cancel 取消按钮<br>
	 * @param md 回调接口（确认、中立、取消）<br>
	 * @param isDismiss 回调函数是否自动调用隐藏对话框<br>
	 * @param isCancelable 是否接收返回键和对话框外点时的关闭窗口，默认为true接收/false不接收<br>
	 */
	public static AlertDialog getMyDialog(Context context, Object title, Object msg, Object ok, Object neutral,
										  Object cancel, final IDialogMethod md, final boolean isDismiss, final boolean isCancelable){
		if(context == null) context = Utils.getApp();
		String _title = getMyResString(context, title);
		String _msg = getMyResString(context, msg);
		String _ok = getMyResString(context, ok);
		String _neutral = getMyResString(context, neutral);
		String _cancel = getMyResString(context, cancel);
		AlertDialog.Builder bd = new AlertDialog.Builder(context);
		if(_ok != null && bd != null){
			bd.setPositiveButton(_ok,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					if(md != null) {
						md.sure();
						md.sure(dialog, which);
					}
					if(isDismiss) dialog.dismiss();
				}
			});
		}
		if(_neutral != null && bd != null){//中立
			bd.setNeutralButton(_neutral,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					if(md != null) {
						md.neutral();
						md.neutral(dialog, which);
					}
					if(isDismiss) dialog.dismiss();
				}
			});
		}
		if(_cancel != null && bd != null){
			bd.setNegativeButton(_cancel,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					if(md != null) {
						md.cancel();
						md.cancel(dialog, which);
					}
					if(isDismiss) dialog.dismiss();
				}
			});
		}
		
		AlertDialog dlg = bd.create();
		dlg.setCancelable(isCancelable);	//default is true
		if(_title != null) dlg.setTitle(_title);
		if(_msg != null) dlg.setMessage(_msg);
		return dlg;
	}
	public static AlertDialog getMyDialog(Context context, Object title, Object msg, 
			Object ok, Object neutral, Object cancel, final IDialogMethod md){
		return getMyDialog(context, title, msg, ok, neutral, cancel, md, true, true);
	}
	public static AlertDialog getMyDialog(Context context, Object title, Object msg, 
			Object ok, Object cancel, final IDialogMethod md){
		return getMyDialog(context, title, msg, ok, null, cancel, md);
	}
	public static AlertDialog getMyDialog(Context context, Object msg, Object ok, 
			Object cancel, final IDialogMethod md){
		return getMyDialog(context, null, msg, ok, cancel, md);
	}
	
	public static void popDialog(Context context, Object msg, Object ok, Object cancel, final IDialogMethod md){
		popDialog(context, null, msg, ok, cancel, md);
	}
	public static void popDialog(Context context, Object title, Object msg, Object ok, Object cancel, 
			final IDialogMethod md){
		popDialog(context, title, msg, ok, cancel, md, -1);
	}
	public static void popDialog(Context context, Object title, Object msg, Object ok, Object cancel, 
			final IDialogMethod md, float size){
		popDialog(context, title, msg, ok, null, cancel, md, size);
	}
	public static void popDialog(Context context, Object title, Object msg, Object ok, Object neutral, 
			Object cancel, final IDialogMethod md, float size){
		popDialog(context, title, msg, ok, neutral, cancel, md, size, true);
	}
	public static void popDialog(Context context, Object title, Object msg, Object ok, Object neutral, 
			Object cancel, final IDialogMethod md, float size, final boolean isDismiss){
		popDialog(context, title, msg, ok, neutral, cancel, md, size, isDismiss, true);
	}
	public static void popDialog(Context context, Object title, Object msg, Object ok, Object neutral, 
			Object cancel, final IDialogMethod md, float size, final boolean isDismiss, final boolean isCancelable){
		AlertDialog dlg = getMyDialog(context, title, msg, ok, neutral, cancel, md, isDismiss, isCancelable);
		if(dlg != null){
			dlg.show();
			if(size >= 12){
				try{
					TextView textView = (TextView) dlg.findViewById(android.R.id.message);
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (size < 12) ? 12 : size);
				}catch(Exception e){e.printStackTrace();}
			}
		}else{
			System.err.println(MyTipDialog.class.getName()+"::popDialog is error...");
		}
	}
	private static final String sEndStr = "";//"\n\n";
	public static void popDialog(Context context, Object msg){
		popDialog(context, "提示", msg+sEndStr, "关闭", -1);
	}
	public static void popNilDialog(final Context context, final Object content){
		if(context == null || content == null) return;
		final String title = "欢迎进入调试模式";
		final String msg = getMyResString(context, content);
		MyTipDialog.popDialog(context,title, msg+sEndStr, "分享", "复制","关闭", new MyTipDialog.DialogMethod() {
			public void sure() {
				BaseUtils.openShare(context, msg);
			}
			public void neutral() {
				BaseUtils.openCopy(context, msg);
			}
		}, 12);
	}
	public static void popDialog(Context context, Object title, Object msg, Object cancel){
		popDialog(context, title, msg, cancel, -1);
	}
	public static void popDialog(Context context, Object title, Object msg, Object cancel, float size){
		popDialog(context, title, msg, null, cancel, null, size);
	}
	
	public static void popItemDialog(Context context, Object title, String[] items, 
			Object cancel, DialogInterface.OnClickListener click){
		if(context != null){
			String _title = getMyResString(context, title);
			String[] _arys = items;
			String _cancel = getMyResString(context, cancel);
			AlertDialog.Builder bd = new AlertDialog.Builder(context);
			if(bd != null){
				if(_title != null) bd.setTitle(_title);
				if(_arys != null) bd.setItems(_arys, click);
				if(_cancel != null) bd.setPositiveButton(_cancel, null);
				bd.create().show();
			}
		}
	}
	
	public static String getMyResString(Context context, Object resID){
		if(context  == null || resID == null) return null;
		String msg = null;
		if(resID instanceof Integer){
			msg =  context.getString((Integer)resID);
		}else if(resID instanceof String){
			msg = (String) resID;
		}else if(resID != null){
			msg = resID.toString();
		}
		return msg;
	}
}
