package com.nil.sdk.ui;

import androidx.fragment.app.Fragment;

public class BaseFragmentV4 extends Fragment{
	public void onResume() {
		super.onResume();
		BaseUtils.onResume(getActivity());
	}
	public void onPause() {
		super.onPause();
		BaseUtils.onPause(getActivity());
	}
	public boolean onBackPressed() {
		return false;
	}

	public void runOnUiThread(Runnable run) {
		getActivity().runOnUiThread(run);
	}
}
