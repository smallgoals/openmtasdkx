package com.nil.sdk.ui.aid;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 适配器专用实体类<br>
 */
public class BNItem{
    public int id;
    public String title;
    public Fragment fragment;
    public Object parameter;
    public Object reserve;
    public BNItem(){}
    public BNItem(int id, String title){
        this();
        this.id = id;
        this.title = title;
    }
    public BNItem(String title, Fragment fragment){
        this();
        this.title = title;
        this.fragment = fragment;
    }
    public BNItem(int id, String title, Fragment fragment){
        this(title,fragment);
        this.id = id;
    }
    public BNItem(int id, String title, Fragment fragment, Object parameter, Object reserve){
        this(id,title,fragment);
        this.parameter = parameter;
        this.reserve = reserve;
    }
    public static List<Object> getValues(List<BNItem> items, int index){
        List<Object> arys = new ArrayList<>();
        if(items != null) {
            for (BNItem item : items) {
                if (item != null) {
                    Object[] ary = {item.id, item.title, item.fragment, item.parameter, item.reserve};
                    arys.add(ary[index%ary.length]);
                }
            }
        }
        return arys;
    }
}
