package com.nil.sdk.ui.aid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.ui.aid.MyMessageDialog.IDialogMethod;

import androidx.appcompat.app.AlertDialog;

/**
 * @declaration 自定义一个全屏的AlertDialog<br>
 * 自定义一个全屏的AlertDialog。 - 李培能 - 博客园  http://www.cnblogs.com/lipeineng/p/6478025.html<br>
 * @author ksA2y511xh@163.com<br>
 *2017-8-3 下午5:29:23<br>
 */
public class MyAlertDialog extends AlertDialog {
    Context mContext;
    TextView mTvTip;
    final boolean DEF_IS_TOP = false;	//默认不顶层显示（如：Gg顶层/Qq不顶层） 
    final long DEF_SHOW_TIME = 800;		//800ms
    public MyAlertDialog(Context context) {
        super(context, R.style.MyAlertDialog); // 自定义全屏style
        this.mContext=context;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public void show() {
    	show(DEF_IS_TOP);
    }
    public void show(boolean isTop) {
        try{
        	if(isTop){
	        	/**设置为全局AlertDialog，可以跨Activity显示 */
	        	//getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
	        	getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        	}
        	super.show();
            /**
             * 设置宽度全屏，要设置在show的后面
             */
            WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
            layoutParams.gravity=Gravity.BOTTOM;
            layoutParams.width= LayoutParams.MATCH_PARENT;
            layoutParams.height= LayoutParams.MATCH_PARENT;
            getWindow().getDecorView().setPadding(0, 0, 0, 0);
            getWindow().setAttributes(layoutParams);
            
	        View v = LayoutInflater.from(mContext).inflate(R.layout.xmta_mpd_loading_full, null);
	        setContentView(v);
	        mTvTip = (TextView) v.findViewById(R.id.adTip);
        }catch(Exception e){}
    }
    public void show(MyMessageDialog.IDialogMethod md){
    	show(DEF_IS_TOP, DEF_SHOW_TIME, md);
    }
    public void show(long millis, MyMessageDialog.IDialogMethod md){
    	show(DEF_IS_TOP, millis, md);
    }
    public void show(boolean isTop, MyMessageDialog.IDialogMethod md){
    	show(isTop, DEF_SHOW_TIME, md);
    }
    public void show(boolean isTop, long millis, MyMessageDialog.IDialogMethod md){
    	try{
    		//mHandler.removeMessages(VMsg.End.ordinal());
	    	show(isTop);
	    	setCancelable(false);
	    	Message msg = mHandler.obtainMessage(VMsg.End.ordinal());
	    	msg.obj = md;
	    	mHandler.sendMessageDelayed(msg, (millis < 0)? 0 : millis);
    	}catch(Exception e){}
    }
    
    public void setTip(String tip){
    	if(mTvTip != null){
    		mTvTip.setText(tip);
    	}
    }
    
    public enum VMsg{Begin,End,Fail,Sucess};
    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler(){
    	public void handleMessage(Message msg) {
    		if(msg.what == VMsg.Begin.ordinal()){
    			
    		}else if(msg.what == VMsg.End.ordinal()){
    			dismiss();
    			if(msg.obj instanceof MyMessageDialog.IDialogMethod){
    				MyMessageDialog.IDialogMethod obj = (IDialogMethod) msg.obj;
    				obj.sure();
    			}
    		}else if(msg.what == VMsg.Fail.ordinal()){
    			
    		}else if(msg.what == VMsg.Sucess.ordinal()){
    			
    		}
    	};
    };
    
}
