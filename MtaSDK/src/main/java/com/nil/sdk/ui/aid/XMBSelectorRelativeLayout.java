package com.nil.sdk.ui.aid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * 不用写Selector，用JAVA代码实现点击效果
 * by Dengzer
 */
public class XMBSelectorRelativeLayout extends RelativeLayout {
    public XMBSelectorRelativeLayout(Context context) {
        super(context);
    }

    public XMBSelectorRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public XMBSelectorRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private Drawable normalDrawable;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            //按钮按下逻辑
            case MotionEvent.ACTION_DOWN:
                normalDrawable = getBackground();
                setBackground(XMBSelectorViewUtil.createPressImage(normalDrawable));
                break;

            //按钮弹起逻辑
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                setBackground(normalDrawable);
                break;
        }
        return super.onTouchEvent(event);
    }

    private XMBRoundViewDelegate dl;
    private float rect_adius = 8;  //单位为dp
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int w = getWidth();
        int h = getHeight();
        if (dl == null) {
            dl = new XMBRoundViewDelegate(this, getContext(), rect_adius);
        }
        dl.roundRectSet(w, h);
    }
    public void draw(Canvas canvas) {
        if (dl == null) {
            dl = new XMBRoundViewDelegate(this, getContext(), rect_adius);
        }
        dl.canvasSetLayer(canvas);
        super.draw(canvas);
        canvas.restore();
    }
}
