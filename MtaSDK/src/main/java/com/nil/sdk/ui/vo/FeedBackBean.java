package com.nil.sdk.ui.vo;

import android.app.ui.FeedbackUI;
import androidx.annotation.Keep;

import com.google.gson.annotations.Expose;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@Keep
public class FeedBackBean implements Serializable {
	public final static String CACHE_ARY_KEY = "cache_ary_key";
	private final static String TAG = FeedBackBean.class.getSimpleName();

	@Expose
	private int ID;// 自增ID
	@Expose
	private String app_id;
	@Expose
	private Date time;
	@Expose
	private String content;
	@Expose
	private String type;	//type：user-用户方 or developer-我方
	@Expose
	private String combined_id;

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCombined_id() {
		return combined_id;
	}

	public void setCombined_id(String combined_id) {
		this.combined_id = combined_id;
	}

	@Override
	public String toString() {
		return "FeedBackBean{" +
				"ID=" + ID +
				", app_id='" + app_id + '\'' +
				", time=" + time +
				", content='" + content + '\'' +
				", type='" + type + '\'' +
				", combined_id='" + combined_id + '\'' +
				'}';
	}
	public FeedBackBean() {}
	public FeedBackBean(String content) {
		this.content = content;
		this.app_id = AdSwitchUtils.Vs.app_id.value;
		this.time = new Date();
		this.type = FeedbackUI.SenderType.user.name();	// 默认为用户发送的反馈
		this.combined_id = ACacheUtils.getCombinedID(
				StringUtils.getValue(AdSwitchUtils.Vs.xmb_app_id.value, AdSwitchUtils.Vs.app_id.value));
	}
	public FeedBackBean(String app_id, Date time, String content, String type, String combined_id) {
		this.app_id = app_id;
		this.time = time;
		this.content = content;
		this.type = type;
		this.combined_id = combined_id;
	}

	public static int getMaxID(ArrayList<FeedBackBean> ary){
		return getMaxID(ary, FeedbackUI.SenderType.developer.name());
	}
	public static int getMaxID(ArrayList<FeedBackBean> ary, String type){
		int id = 0;
		FeedBackBean fb = getMaxFB(ary, type);
		if(fb != null) {
			id = fb.getID();
		}
		return id;
	}
	public static FeedBackBean getMaxFB(ArrayList<FeedBackBean> ary){
		return getMaxFB(ary, FeedbackUI.SenderType.developer.name());
	}
	public static FeedBackBean getMaxFB(ArrayList<FeedBackBean> ary, String type){
		FeedBackBean fb = new FeedBackBean("空消息");
		fb.setType(type);
		int id = 0;
		if(ary != null && !ary.isEmpty() && StringUtils.noNullStr(type)) {
			for (FeedBackBean f : ary) {
				String tt = f.getType();
				int ii = f.getID();
				if(type.equals(tt) && ii > id){
					id = ii;
					fb = f;
				}
			}
		}
		return fb;
	}
}
