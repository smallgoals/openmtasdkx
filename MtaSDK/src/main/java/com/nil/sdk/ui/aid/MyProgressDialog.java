package com.nil.sdk.ui.aid;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;

/**
 * @declaration 进度提示框<br>
 * @author denger/qinglei.yin@192.168.88.9<br>
 *2014-1-8 上午11:38:13<br>
 */
@SuppressLint("InflateParams") 
public class MyProgressDialog {
	private Context act;
	private String msg;
	private LayoutInflater inflater;
	private AlertDialog dialog;
	private View view;

	private boolean cancelable;
	private TextView loading_text;
	
	private String gStr(Context act,Object msgText){
		String str = "";
		if(act != null && msgText != null){
			if(msgText instanceof Integer){
				str = act.getResources().getString((Integer)msgText);
			}else if(msgText instanceof String){
				str = (String)msgText;
			}
		}
		return str;
	}

	public MyProgressDialog(Context act) {
		this.act = act;
		buildView();
	}

	public MyProgressDialog(Context act, Object msg) {
		this.act = act;
		this.msg = gStr(act, msg);
		buildView();
	}

	public MyProgressDialog setCancelable(boolean flag) {
		cancelable = flag;
		return this;
	}

	public void setTitle(Object text){
		changeText(text);
	}
	
	public void changeText(Object text) {
		msg = gStr(act, text);
		loading_text.setText(msg);
	}

	public boolean isShowing() {
		return (dialog == null)? false : dialog.isShowing();
	}

	private void buildView() {
		inflater = LayoutInflater.from(act);
		view = inflater.inflate(R.layout.xmta_mpd_loading, null);
		if (msg != null) {
			loading_text = (TextView) view.findViewById(R.id.loading_text);
			loading_text.setText(msg);
		}
		dialog = new AlertDialog.Builder(act, R.style.MyAlertDialogSmall).setCancelable(cancelable).create();
	}

	public MyProgressDialog show() {
//		buildView();		//"Error:The specified child already has a parent." modify by ksA2y511xhs
//		dialog = new AlertDialog.Builder(act).setCancelable(cancelable).create();
		dialog.show();
		dialog.setContentView(view);
		return this;
	}

	public void cancel() {
		if(isShowing()) dialog.cancel();
	}
	
	public void dismiss(){
		if(isShowing()) dialog.dismiss();
	}
}
