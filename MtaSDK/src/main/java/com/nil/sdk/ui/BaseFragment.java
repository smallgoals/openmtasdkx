package com.nil.sdk.ui;

import android.app.Fragment;

public class BaseFragment extends Fragment{
	public void onResume() {
		super.onResume();
		BaseUtils.onResume(getActivity());
	}
	public void onPause() {
		super.onPause();
		BaseUtils.onPause(getActivity());
	}

	public void runOnUiThread(Runnable run) {
		getActivity().runOnUiThread(run);
	}
}
