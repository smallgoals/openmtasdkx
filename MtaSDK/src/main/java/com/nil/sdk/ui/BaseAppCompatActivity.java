package com.nil.sdk.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class BaseAppCompatActivity extends AppCompatActivity {
	public Activity getActivity(){return this;}
	protected Context context;
	private boolean isDisplayHomeAsUpEnabled;

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		context = this;
		BaseUtils.onCreate(this);
	}
	public void setContentView(@LayoutRes int layoutResID) {
		super.setContentView(layoutResID);
		BaseUtils.initView(this);
	}
	public void onBackPressed() {
		BaseUtils.onBackPressed(this);
	}
	protected void onResume() {
		super.onResume();
		BaseUtils.onResume(this);

		// 显示返回按钮
		if (isDisplayHomeAsUpEnabled) {
			ActionBar actionBar = getSupportActionBar();
			if(actionBar != null) {
				actionBar.setDisplayHomeAsUpEnabled(true);
			}
		}
	}
	protected void onPause() {
		super.onPause();
		BaseUtils.onPause(this);
	}
	public void setContentView(View view) {
		super.setContentView(view);
		BaseUtils.initView(this);
	}
	public void setContentView(View view, ViewGroup.LayoutParams params) {
		super.setContentView(view, params);
		BaseUtils.initView(this);
	}

	public void setDisplayHomeAsUpEnabled(boolean displayHomeAsUpEnabled) {
		isDisplayHomeAsUpEnabled = displayHomeAsUpEnabled;
	}

	public void setElevation() {
		try{
			// 去掉ActionBar顶部的阴影
			ActionBar actionBar = getSupportActionBar();
			if(actionBar != null) {
				actionBar.setElevation(0);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void hideActionBar(){
		try{
			// 隐藏ActionBar
			ActionBar actionBar = getSupportActionBar();
			if(actionBar != null) {
				actionBar.hide();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public void setFullScreen(){
		// 全屏设置必须放setContentView(R.layout.xxy)方法之前
		try {
			//去除标题栏
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			//去除状态栏
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public void setNoScreenshot(){
		try{
			// 禁止截屏
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {   //返回键的id
			this.finish();
			return false;
		}
		return super.onOptionsItemSelected(item);
	}

/*	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(curFrag instanceof NewsFragment){
			boolean isOK = ((NewsFragment)curFrag).onKeyDown(keyCode, event);
			return isOK ? isOK : super.onKeyDown(keyCode, event);
		}
		return super.onKeyDown(keyCode, event);
	}*/

}