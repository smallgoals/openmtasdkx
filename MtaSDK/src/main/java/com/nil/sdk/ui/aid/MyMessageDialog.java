package com.nil.sdk.ui.aid;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.Utils;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.interfaces.BaseDialog;
import com.kongzue.dialogx.interfaces.DialogXStyle;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.style.IOSStyle;
import com.kongzue.dialogx.style.KongzueStyle;
import com.kongzue.dialogx.style.MIUIStyle;
import com.kongzue.dialogx.style.MaterialStyle;
import com.kongzue.dialogxmaterialyou.style.MaterialYouStyle;
import com.nil.sdk.ui.BaseUtils;

/**
 * @declaration 自定义弹出对话框（可以完全替换MyTipDialog）<br>
 * @author ksA2y511xh@163.com<br>
 * 2023-03-31 下午17:27:42
 */
public class MyMessageDialog {
	public interface IDialogMethod{
		public void sure();
		public void neutral();
		public void cancel();
		public void sure(Object obj);
		public void neutral(Object obj);
		public void cancel(Object obj);
		public void sure(BaseDialog dialog, View v);
		public void neutral(BaseDialog dialog, View v);
		public void cancel(BaseDialog dialog, View v);
	}
	public static abstract class DialogMethod implements IDialogMethod{
		public abstract void sure();
		public void neutral(){}
		public void cancel(){}
		public void sure(Object obj){}
		public void neutral(Object obj){}
		public void cancel(Object obj){}
		public void sure(BaseDialog dialog, View v){}
		public void neutral(BaseDialog dialog, View v){}
		public void cancel(BaseDialog dialog, View v){}
	}
	
	/**
	 * 获取自定义对话框<br>
	 * @param title 标题<br>
	 * @param msg 消息内容<br>
	 * @param ok 确认按钮<br>
	 * @param neutral 中立按钮<br>
	 * @param cancel 取消按钮<br>
	 * @param md 回调接口（确认、中立、取消）<br>
	 * @param isDismiss 回调函数是否自动调用隐藏对话框<br>
	 * @param isCancelable 是否接收返回键和对话框外点时的关闭窗口，默认为true接收/false不接收<br>
	 */
	public static MessageDialog getMyDialog(Context context, Object title, Object msg, Object ok, Object neutral,
										  Object cancel, final IDialogMethod md, final boolean isDismiss, final boolean isCancelable){
		if(context == null) context = Utils.getApp();
		String _title = getMyResString(context, title);
		String _msg = getMyResString(context, msg);
		String _ok = getMyResString(context, ok);
		String _neutral = getMyResString(context, neutral);
		String _cancel = getMyResString(context, cancel);

		MessageDialog bd = MessageDialog.build();
		if(_ok != null){
			bd.setOkButton(_ok, new OnDialogButtonClickListener<MessageDialog>() {
				@Override
				public boolean onClick(MessageDialog dialog, View v) {
					if(md != null) {
						md.sure();
						md.sure(dialog, v);
					}
					return !isDismiss;
				}
			});
		}
		if(_neutral != null){//中立
			bd.setOtherButton(_neutral, new OnDialogButtonClickListener<MessageDialog>() {
				@Override
				public boolean onClick(MessageDialog dialog, View v) {
					if(md != null) {
						md.neutral();
						md.neutral(dialog, v);
					}
					return !isDismiss;
				}
			});
		}
		if(_cancel != null){
			bd.setCancelButton(_cancel, new OnDialogButtonClickListener<MessageDialog>() {
				@Override
				public boolean onClick(MessageDialog dialog, View v) {
					if(md != null) {
						md.cancel();
						md.cancel(dialog, v);
					}
					return !isDismiss;
				}
			});
		}
		
		bd.setCancelable(isCancelable);	//default is true
		if(_title != null) bd.setTitle(_title);
		if(_msg != null) bd.setMessage(_msg);
		return bd;
	}
	public static MessageDialog getMyDialog(Context context, Object title, Object msg, 
			Object ok, Object neutral, Object cancel, final IDialogMethod md){
		return getMyDialog(context, title, msg, ok, neutral, cancel, md, true, true);
	}
	public static MessageDialog getMyDialog(Context context, Object title, Object msg, 
			Object ok, Object cancel, final IDialogMethod md){
		return getMyDialog(context, title, msg, ok, null, cancel, md);
	}
	public static MessageDialog getMyDialog(Context context, Object msg, Object ok, 
			Object cancel, final IDialogMethod md){
		return getMyDialog(context, null, msg, ok, cancel, md);
	}
	
	public static MessageDialog popDialog(Context context, Object msg, Object ok, Object cancel, final IDialogMethod md){
		return popDialog(context, null, msg, ok, cancel, md);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object ok, Object cancel,
			final IDialogMethod md){
		return popDialog(context, title, msg, ok, cancel, md, -1);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object ok, Object cancel,
			final IDialogMethod md, float size){
		return popDialog(context, title, msg, ok, null, cancel, md, size);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object ok, Object neutral,
			Object cancel, final IDialogMethod md, float size){
		return popDialog(context, title, msg, ok, neutral, cancel, md, size, true);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object ok, Object neutral,
			Object cancel, final IDialogMethod md, float size, final boolean isDismiss){
		return popDialog(context, title, msg, ok, neutral, cancel, md, size, isDismiss, true);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object ok, Object neutral,
			Object cancel, final IDialogMethod md, float size, final boolean isDismiss, final boolean isCancelable){
		MessageDialog dlg = getMyDialog(context, title, msg, ok, neutral, cancel, md, isDismiss, isCancelable);
		if(dlg != null){
			dlg.show();
			if(size >= 12){
				try{
					TextView textView = dlg.getDialogView().findViewById(R.id.txt_dialog_tip);
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (size < 12) ? 12 : size);
				}catch(Exception e){e.printStackTrace();}
			}
		}else{
			System.err.println(MyMessageDialog.class.getName()+"::popDialog is error...");
		}
		return dlg;
	}
	private static final String sEndStr = "";//"\n\n";
	public static MessageDialog popDialog(Context context, Object msg){
		return popDialog(context, "提示", msg+sEndStr, "关闭", -1);
	}
	public static MessageDialog popNilDialog(final Context context, final Object content){
		if(context == null || content == null) return null;
		final String title = "欢迎进入调试模式";
		final String msg = getMyResString(context, content);
		return MyMessageDialog.popDialog(context,title, msg+sEndStr, "分享", "复制","关闭", new MyMessageDialog.DialogMethod() {
			public void sure() {
				BaseUtils.openShare(context, msg);
			}
			public void neutral() {
				BaseUtils.openCopy(context, msg);
			}
		}, 12);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object cancel){
		return popDialog(context, title, msg, cancel, -1);
	}
	public static MessageDialog popDialog(Context context, Object title, Object msg, Object cancel, float size){
		return popDialog(context, title, msg, null, cancel, null, size);
	}

	public static String getMyResString(Context context, Object resID){
		if(context  == null || resID == null) return null;
		String msg = null;
		if(resID instanceof Integer){
			msg =  context.getString((Integer)resID);
		}else if(resID instanceof String){
			msg = (String) resID;
		}else if(resID != null){
			msg = resID.toString();
		}
		return msg;
	}

	//获取主题类型：
	public static DialogXStyle getDialogxStyle(){
		int type = 0;
		try {
			type = Integer.parseInt(Utils.getApp().getString(R.string.xdialogx_dialog_style));
		}catch (Exception e){
			e.printStackTrace();
		}
		return getDialogxStyle(type);
	}
	public static DialogXStyle getDialogxStyle(int type){
		DialogXStyle style = MaterialStyle.style();
		final DialogXStyle[] styleAry = {MaterialStyle.style(), KongzueStyle.style(),
				IOSStyle.style(), MIUIStyle.style(), MaterialYouStyle.style()};
		if(type >0 && type < styleAry.length){
			style = styleAry[type];
		}
		return style;
	}
}
