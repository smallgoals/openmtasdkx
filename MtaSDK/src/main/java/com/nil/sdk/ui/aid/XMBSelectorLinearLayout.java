package com.nil.sdk.ui.aid;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.LogUtils;

/**
 * 不用写Selector，用JAVA代码实现点击效果
 * by Dengzer
 */
public class XMBSelectorLinearLayout extends LinearLayout {

    public XMBSelectorLinearLayout(Context context) {
        super(context);
    }

    public XMBSelectorLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public XMBSelectorLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private Drawable normalDrawable;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogUtils.i(event.toString());
        switch (event.getAction()) {
            //按钮按下逻辑
            case MotionEvent.ACTION_DOWN:
                normalDrawable = getBackground();
                setBackground(XMBSelectorViewUtil.createPressImage(normalDrawable));
                break;

            //按钮弹起逻辑
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                setBackground(normalDrawable);
                break;
        }
        return super.onTouchEvent(event);
    }


}
