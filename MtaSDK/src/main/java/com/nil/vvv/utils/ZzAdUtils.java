package com.nil.vvv.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.NetworkUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils.Sws;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @declaration 广告工具类<br>
 */
public class ZzAdUtils{
	private final static String TAG = ZzAdUtils.class.getSimpleName();
	public static void addBannerAd(final Activity act){
		ZFactory.getMyVVV().addBannerAd(act);
	}
	public static void openCpAd(final Activity act){
		ZFactory.getMyVVV().openCpAd(act);
	}
	public static int openExitAd(final Activity act){
		return ZFactory.getMyVVV().openExitAd(act);
	}

	public static void closePushAd(final Activity act){
		Spu.saveV(act, Spu.VSpu.close_push_ad.name(), "suc");
		//刷新横幅状态：关闭横幅
		ZFactory.getMyVVV().addBannerAd(act);
	}
	public static void openPushAd(final Activity act){
		Spu.saveV(act, Spu.VSpu.close_push_ad.name(), "no");
		//刷新横幅状态：开启横幅
		ZFactory.getMyVVV().addBannerAd(act);
	}

	/** 存储需要排除显示启动插屏广告的Activity，主要解决Gdt插屏的故障*/
	private static Map<String, Activity> sActMap;
	public static void addActivity(Activity act){
		if(sActMap == null) sActMap = new HashMap<String, Activity>();
		if(act != null){
			sActMap.put(act.getClass().getName(), act);
		}
	}
	public static boolean hasActivity(Activity act){
		boolean isOK = false;
		if(act != null && sActMap != null){
			isOK = sActMap.get(act.getClass().getName()) != null;
		}
		return isOK;
	}
	public static boolean isRandom(){
//		return new Random().nextInt(10)%3 == 0;		//0-3-6-9=4/10=2/5
		return getProb(180) || Mtas.isCrazyAd;	//18%
	}
	public static boolean getProb(int num){
		return new Random().nextInt(999) < num;
	}
	public static enum VType{hf,cp,boot_tab,boot,tab,out,mix,other}
	public static int openVVV(Activity act, VType type){
		int flag = -1;
		if(hasActivity(act)) return flag;
		if(!AppUtils.hasAgreePrivacyPolicy()) return flag;	//防止未同意双协议开启广告
		if(AppUtils.isClosePushAd()) return flag;	//未关闭推送广告时才开启广告
		if(type == VType.hf){
			addBannerAd(act);
		}else if(type == VType.cp){
			if(Sws.Cp.flag){
				openCpAd(act);
			}
		}else if(type == VType.boot_tab){
			if((BaseUtils.isMainUI(act) && Sws.Boot.flag)
					|| ((!BaseUtils.isLaunchUI(act)) && (!BaseUtils.isNoCpUI(act))&& Sws.Tab.flag && isRandom())){
				openCpAd(act);
			}
		}else if(type == VType.boot){
			if(BaseUtils.isMainUI(act) && Sws.Boot.flag){
				openCpAd(act);
			}
		}else if(type == VType.tab){
			if(Sws.Tab.flag && isRandom()){
				openCpAd(act);
			}
		}else if(type == VType.out){
			if(Sws.Out.flag){
				flag = openExitAd(act);
			}
		}else if(type == VType.mix){
			if(Sws.Mix.flag){
				addBannerAd(act);
				openCpAd(act);
				flag = openExitAd(act);
			}
		}else if(type == VType.other){
			if(Sws.Other.flag){
				openCpAd(act);
			}
		}else{
			
		}
		return flag;
	}
	
	public static abstract class AbVVV{
		protected String appID;
		protected AbVVV res;	// 用于当前广告加载失败时启用
		public void addBannerAd(final Activity act){
			if(act == null) return;
			LinearLayout mLayout = act.findViewById(R.id.llMainV);
			if(mLayout == null){
				mLayout = act.findViewById(R.id.llSubVV);
				if(mLayout == null){
					mLayout = act.findViewById(R.id.llMain);
				}
			}
			if(mLayout != null){
				mLayout.removeAllViews();
				if(!AppUtils.isClosePushAd() && NetworkUtils.isNetworkAvailable(act) && Sws.Hf.flag){
					mLayout.setVisibility(View.VISIBLE);
					addBarBnAgent(act,mLayout,appID);
				}else{
					mLayout.setVisibility(View.GONE);
				}
			}
		}
		public String toString() {
			String r = "AbVVV.toString";
			try{
				r = StringUtils.getObjectSimpleName(this)+"/"+StringUtils.getObjectSimpleName(res);
				r += "#"+this.getClass().getSuperclass().getSimpleName();
			}catch(Exception e){
				LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
				e.printStackTrace();
			}
			return r;
		}
		public void openCpAd(final Activity act){
			if(act == null) return; 
			if(!AppUtils.isClosePushAd() && NetworkUtils.isNetworkAvailable(act) && Sws.Cp.flag){
				showCPAdAgent(act,appID);
			}
		}
		public int openExitAd(final Activity act){
			int flag = -1;
			if(act == null) return flag; 
			if(!AppUtils.isClosePushAd() && NetworkUtils.isNetworkAvailable(act) && Sws.Cp.flag){
				if(Sws.ECp.flag){
					flag = showExitAdAgent(act,appID);
				}else{
					showCPAdAgent(act,appID);
				}
			}
			return flag;
		}
		private void addBarBnAgent(final Activity act, ViewGroup vg, String appid){
			if(act != null && vg != null && !TextUtils.isEmpty(appid)){
				try{
					addBarBn(act, vg, appid);
				}catch(Exception e){
					if(res != null) res.addBannerAd(act);
				};
			}
		}
		private void showCPAdAgent(final Activity act, String appid){
			if(act != null  && !TextUtils.isEmpty(appid)){
				try{
					showCPAd(act, appid);
				}catch(Exception e){
					if(res != null) res.openCpAd(act);
				};
			}
		}
		private int showExitAdAgent(final Activity act, String appid){
			int flag = -1;
			if(act != null  && !TextUtils.isEmpty(appid)){
				try{
					flag = showExitAd(act, appid);
				}catch(Exception e){
					if(res != null) res.openExitAd(act);
				};
			}
			return flag;
		}
		
		protected abstract void addBarBn(final Activity act, ViewGroup vg, String appid) throws Exception;
		protected abstract void showCPAd(final Activity act, String appid) throws Exception;
		protected int showExitAd(final Activity act, String appid) throws Exception{
			showCPAd(act, appid);
			return -1;
		}
	}
	public static class DefAbVVV extends AbVVV{
		protected void addBarBn(Activity act, ViewGroup vg, String appid)  throws Exception{}
		protected void showCPAd(Activity act, String appid)  throws Exception{}
	}
}
