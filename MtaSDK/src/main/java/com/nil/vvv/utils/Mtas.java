package com.nil.vvv.utils;

import android.app.Activity;
import android.app.ui.FeedbackService;
import android.content.Context;

import com.android.core.XSEUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACache;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.AutoUpdateUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils.Ads;
import com.nil.vvv.utils.AdSwitchUtils.Vs;
import com.umeng.cconfig.RemoteConfigSettings;
import com.umeng.cconfig.UMRemoteConfig;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XmbOnlineConfigAgent;

/**
 * @declaration 综合统计工具类(友盟统计、腾讯统计、Xmb在线参数)<br>
 * 友盟统计分析 http://dev.umeng.com/analytics/android-doc/integration<br>
 * 讯飞开放平台 http://www.xfyun.cn/index.php/services/analysis/mobileappDoc?itemTitle=YW5kcm9pZGJiamNzbQ==<br>
 *2017-5-14 下午10:29:40<br>
 */
public class Mtas {
    private final static String TAG = Mtas.class.getSimpleName();
	/** 开启疯狂模式，所有开关都打开，可用于广告测试 */
	public static final String Version = "2312121";	 //Mta的版本号
	public static boolean isCrazyAd = false; //疯狂模式：各种开关全部打开（仅用于测试）
	public static final int defDays = 15;	//默认15天后，在线参数获取失败时广告开关进行默认配置
	public static final String defReleaseDate = "2066/06/06";	//默认发布时间
	public static final String defChannel = "channel_mix";	//Nil综合应用
	public static final String defUmKey = "5a2cff57f43e485c4a000123##ID@@Um$Yj$OpenMtaSDK";	//OpenMtaSDK
	public static final String defXfKey = "5a2d00cf##ID@@Xf$Yj$OpenMtaSDK";	//OpenMtaSDK
	public static final String defTaKey = "AVE2T6X52FBM##ID@@Tx$Nil$nilzhyy";
	
	public static final String defYkKey = "3c84d9a25d24d80d";
	public static final String defZcUrls = "youku.com|le.com|mgtv.com|sohu.com|tudou.com|pptv.com|wasu.cn|iqiyi.com|qq.com";
	
	public static final String defGgID = "ca-app-pub-3940256099942544/6300978111##ca-app-pub-3940256099942544/1033173712##HF##CP@@Gg$Test$admob";
	public static final String defQqID = "1101152570##9079537218417626401##8575134060152130849##5000709048439488$2050206699818455$7030020348049331##8863364436303842593##ID##HF##CP##JFQ##KP@@Qq$Test$gdt";
	public static final String defJzID = "jz_id";
	public static final String defDydID = "dyd_id";
	public static final String defFoID = "fo_id";
	public static final String defOpID = "op_id";
	public static final String defAppDate = "2019年03月16日";

	public static final String defAgencyKey = "a3NMBUDkuMUrnQ7bLr2UzLAzokkrw%3D%3D";
	public static final String defMtaUrl = "gBAR5OiLXzceiKSv4Ciqz1DGZTyoZiWo9D%2B2zr%2Bro3Cd4yGCUc%3D";
	public static final String defDateUrl = "jiOmElaqRWoZoSJW26KEZ99J3dgRTmstiB1xrx2UaPI0%2FEiPLo%3D"; //【带S网站】https://www.baidu.com 百度
	public static final String defReserveDateUrl = "Yhph7XeQYH7KG2Mu4oC3dBkK48m7UN0fdLz9aCUuOFd7nxklf8%3D";	//【不带S网站】http://www.ntsc.ac.cn 中国科学院国家授时中心
	public static final int defKpGapSecond = 2*60; //默认间隔120秒（即2分）;
	public static final int defFbPeriod = 10*60; //反馈接收周期，默认为10分钟（600秒）
	public static final int defRatePercent = 20; //默认好评机率20，表示20%的概率弹出好评

	public static final int defXmbCacheTime = ACache.TIME_MINUTE * 30;  //默认缓存半小时

	public static final String defSplit = "##";

	public static final boolean defVerSp = true;
	public static String getDefValue(String key, String result){
		String defValue = null;
		if(!StringUtils.isNullStr(key) && StringUtils.isNullStr(result)){ //key no null&&result null
			//Vs参数相关默认配置：
			if(StringUtils.equalsIgnoreCase(key, Vs.um_key.getKey())){
				defValue = Mtas.defUmKey;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.ta_key.getKey())){
				defValue = Mtas.defTaKey;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.xf_key.getKey())){
				defValue = Mtas.defXfKey;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.yk_key.getKey())){
				defValue = Mtas.defYkKey;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.release_date.getKey())){
				defValue = Mtas.defReleaseDate;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.gap_days.getKey())){
				defValue = Mtas.defDays+"";
			}else if(StringUtils.equalsIgnoreCase(key, Vs.zc_urls.getKey())){
				defValue = Mtas.defZcUrls;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.app_date.getKey())){
				defValue = Mtas.defAppDate;
			}else if(StringUtils.equalsIgnoreCase(key, Vs.agency_key.getKey())){
				defValue = XSEUtils.decode(Mtas.defAgencyKey);
			}else if(StringUtils.equalsIgnoreCase(key, Vs.mta_url.getKey())){
				defValue = XSEUtils.decode(Mtas.defMtaUrl);
			}else if(StringUtils.equalsIgnoreCase(key, Vs.date_url.getKey())){
				defValue = XSEUtils.decode(Mtas.defDateUrl);
			}else if(StringUtils.equalsIgnoreCase(key, Vs.kp_gap_second.getKey())){
				defValue = Mtas.defKpGapSecond+"";
			}else if(StringUtils.equalsIgnoreCase(key, Vs.crazy_gap_second.getKey())){
				defValue = Mtas.defKpGapSecond+"";
			}else if(StringUtils.equalsIgnoreCase(key, Vs.fb_period.getKey())){
				defValue = Mtas.defFbPeriod+"";
			}else if(StringUtils.equalsIgnoreCase(key, Vs.xmb_cache_time.getKey())){
				defValue = Mtas.defXmbCacheTime+"";
			}else if(StringUtils.equalsIgnoreCase(key, Vs.rate_percent.getKey())){
				defValue = Mtas.defRatePercent +"";
			}

			//Ads广告相关默认配置：
			if(StringUtils.equalsIgnoreCase(key, Ads.Gg.getKey())){
				defValue = Mtas.defGgID;
			}else if(StringUtils.equalsIgnoreCase(key, Ads.Qq.getKey())){
				//defValue = Mtas.defQqID;
			}else if(StringUtils.equalsIgnoreCase(key, Ads.Jz.getKey())){
				defValue = Mtas.defJzID;
			}else if(StringUtils.equalsIgnoreCase(key, Ads.Dyd.getKey())){
				defValue = Mtas.defDydID;
			}else if(StringUtils.equalsIgnoreCase(key, Ads.Fo.getKey())){
				defValue = Mtas.defFoID;
			}else if(StringUtils.equalsIgnoreCase(key, Ads.Op.getKey())){
				//defValue = Mtas.defOpID;
			}

			//Sws开关相关默认配置：
			/*if(StringUtils.equalsIgnoreCase(key, Sws.Ver_Sp.getKey())){
				defValue = Mtas.defVerSp;
			}*/
		}
		return defValue;
	}

	/**
	 * 状态开关，用于记录各初始化的状态<br>
	 */
	public enum IIB {
		Um,QQ,Config;
		IIB() {
			flag = false;
		}
		public boolean flag;
		public static boolean isInit(){
			boolean isOK = false;
			IIB[] ary = values();
			for (IIB anAry : ary) {
				isOK = anAry.flag;
				if(!isOK) break;//为false时跳出循环
			}
			return isOK;
		}
		public static void configState(boolean state){
			IIB[] ary = values();
			for (IIB anAry : ary) {
				anAry.flag = state;
			}
		}
	}

	/**
	 * 数据仅初始化一次<br>
	 */
	public static void init(Context ctx){
		if(ctx == null) ctx = Utils.getApp();
		if(ctx == null || IIB.isInit()) return;

		Mta2Um.init(ctx);//初始友盟统计

		initOnelineConfig(ctx);//手动更新在线参数
	}
	public static void initOnelineConfig(Context ctx){
		// 初始化前添加权限判断（添加隐私政策同意标识）
		if(IIB.Config.flag || !AppUtils.hasXmbMTAPermission(ctx)
				|| !AppUtils.hasAgreePrivacyPolicy()) {
			return;
		}
		updateOnelineConfig(ctx);
		IIB.Config.flag = true;
	}

	public static void updateOnelineConfig(Context ctx){
		// 添加隐私政策同意标识：
		if(!AppUtils.hasXmbMTAPermission(ctx)
				|| !AppUtils.hasAgreePrivacyPolicy()) {
			return;
		}
		try {
			//友盟在线参数配置：（xmta_umeng-onlineconfig_v1.0.0.jar-->common:9.3.0）
			UMRemoteConfig.getInstance().setConfigSettings(new RemoteConfigSettings.Builder().setAutoUpdateModeEnabled(true).build());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Xmb在线参数，手动更新一次：
		XmbOnlineConfigAgent.updateOnlineConfig();
	}
	
	public static void onResume(final Activity act) {
		if(act == null) return;
		try {
			Mta2Um.onResume(act);
			onPageStart(act);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void onPause(final Activity act) {
		if(act == null) return;
		try {
			onPageEnd(act);
			Mta2Um.onPause(act);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void onPageStart(final Activity act) {
		if(act == null) return;
		try {
			Mta2Um.onPageStart(act.getClass().getSimpleName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void onPageEnd(final Activity act) {
		if(act == null) return;
		try {
			Mta2Um.onPageEnd(act.getClass().getSimpleName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void reportError(Context ctx, String msg){
		if(ctx == null || StringUtils.isNullStr(msg)) return;
		try{
		    Mta2Um.reportError(ctx, msg);
		}catch(Exception e){
		    LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		    e.printStackTrace();
		}

		try{
			if(BaseUtils.IsCj(ctx)) XMBApi.reportBigData(msg);
		}catch(Exception e){
		    LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		    e.printStackTrace();
		}
	}
	
	public static void reportEvent(Context ctx, String key, String value){
		if(ctx == null || StringUtils.isNullStr(key)) return;
		try{
			Mta2Um.onEvent(ctx, key, value);
		}catch(Exception e){
		    LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		    e.printStackTrace();
		}

		try{
			if(BaseUtils.IsCj(ctx)) XMBApi.sendEvent(key, value);
		}catch(Exception e){
		    LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		    e.printStackTrace();
		}
	}
	
	public static void onKillProcess(Context ctx){
		if(ctx == null) ctx = Utils.getApp();
		if(ctx == null) return;
		try{
			//清除全部通知，并暂停后台服务
			FeedbackService.cleanNotification();

			Mta2Um.onKillProcess(ctx);
			IIB.configState(false);	 //开关状态清除

			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onKillProcess is success....");
		}catch(Exception e){
		    LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		    e.printStackTrace();
		}
	}
	
	public static String getOnlineValue(Context ctx, String key){
		if(ctx == null) ctx = Utils.getApp();
		String r = null;
		//[online(xmb/um/xf/qq)-->local-->def]
		if(ctx != null && !StringUtils.isNullStr(key)){
			// 优先获取xmb上的在线参数：
			r = getAllConfigParams2Xmb(ctx, key);

			// 获取友盟上的在线参数：
			if(StringUtils.isNullStr(r)) {
				r = getAllConfigParams2Um(ctx, key);
			}
		}
		return r;
	}

	public static String getAllConfigParams2Xmb(Context ctx, String key){
		/**
		 * 获取参数的优先级如下：<br>
		 * 包名_版本名_渠道_Key/版本名_渠道_Key/版本名_Key/渠道_Key/包名_Key/Key<br>
		 */
		String v = null;
		if(ctx == null) ctx = Utils.getApp();
		if(ctx != null && !StringUtils.isNullStr(key)
				&& (XmbOnlineConfigAgent.isPreloadXmbConfig() || AppUtils.hasAgreePrivacyPolicy())
		){
			if(AdSwitchUtils.Sws.Cs_whole.flag) {
				String pkg = ctx.getPackageName();
				String version = AppUtils.getVersionName(ctx);
				String channel = AppUtils.getMetaChannel(ctx);	//发到哪个渠道

				v = XmbOnlineConfigAgent.getConfigParams(StringUtils.concat("_", pkg, version, channel, key));
				if (StringUtils.isNullStr(v)) {
					v = XmbOnlineConfigAgent.getConfigParams(StringUtils.concat("_", version, channel, key));
					if (StringUtils.isNullStr(v)) {
						v = XmbOnlineConfigAgent.getConfigParams(StringUtils.concat("_", version, key));

						if (StringUtils.isNullStr(v)) {
							v = XmbOnlineConfigAgent.getConfigParams(StringUtils.concat("_", channel, key));

							if (StringUtils.isNullStr(v)) {
								v = XmbOnlineConfigAgent.getConfigParams(StringUtils.concat("_", pkg, key));

								if (StringUtils.isNullStr(v)) {
									v = XmbOnlineConfigAgent.getConfigParams(key);
								}
							}
						}
					}
				}
			} else {
				v = XmbOnlineConfigAgent.getConfigParams(key);
			}
		}
		return v;
	}
	public static String getAllConfigParams2Um(Context ctx, String key){
		/**
		 * 获取参数的优先级如下：<br>
		 * 包名_版本名_渠道_Key/版本名_渠道_Key/版本名_Key/渠道_Key/包名_Key/Key<br>
		 */
		String v = null;
		if(ctx == null) ctx = Utils.getApp();
		if(ctx != null && !StringUtils.isNullStr(key)
				&& AppUtils.hasAgreePrivacyPolicy()	//已同意隐私协议
		){
			if(AdSwitchUtils.Sws.Cs_whole.flag) {
				String pkg = ctx.getPackageName();
				String version = AppUtils.getVersionName(ctx);
				String channel = AppUtils.getMetaChannel(ctx);	//发到哪个渠道

				v = UMRemoteConfig.getInstance().getConfigValue(StringUtils.concat("_", version, channel, key));
				if (StringUtils.isNullStr(v)) {
					v = UMRemoteConfig.getInstance().getConfigValue(StringUtils.concat("_", version, key));

					if (StringUtils.isNullStr(v)) {
						v = UMRemoteConfig.getInstance().getConfigValue(StringUtils.concat("_", channel, key));

						if (StringUtils.isNullStr(v)) {
							v = UMRemoteConfig.getInstance().getConfigValue(StringUtils.concat("_", pkg, key));

							if (StringUtils.isNullStr(v)) {
								v = UMRemoteConfig.getInstance().getConfigValue(key);
							}
						}
					}
				}
			}else{
				v = UMRemoteConfig.getInstance().getConfigValue(key);
			}
		}
		return v;
	}
	
	public static void checkUpdate(final Activity act, final boolean isTip){
		if(act == null) {
			LogUtils.eTag(TAG, "Mtas.checkUpdate is fail, act is null");
			return;
		}

		//true:后台自动检测，无"当前已经是最新版本"提示；false:用户手动点击检测，有"当前已经是最新版本"提示
		AutoUpdateUtil.checkUpdate(act, !isTip);
	}
}
