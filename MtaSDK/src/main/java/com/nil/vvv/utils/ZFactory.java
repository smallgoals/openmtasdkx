package com.nil.vvv.utils;

import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.utils.Fs;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils.Ads;
import com.nil.vvv.utils.ZzAdUtils.AbVVV;
import com.nil.vvv.utils.ZzAdUtils.DefAbVVV;

import java.util.HashMap;
import java.util.Map;

/**
 * @declaration 广告工厂类<br>
 *2017-4-26 下午2:34:59<br>
 */
public class ZFactory {
	private final static String TAG = ZFactory.class.getSimpleName();
	public static final String sPN = "com.android.vy";
	public static final String sSp = "com.android.vy.SplashUI";
	public static final String sZz = "android.app.ui.TransferUI";
	public static Map<String, AbVVV> sAbVVVAry;
	private static String[] sAdName = Ads.getPN(sPN);
	public static void init(){
		if(sAbVVVAry == null || sAbVVVAry.isEmpty()){
			sAbVVVAry = new HashMap<String, ZzAdUtils.AbVVV>();
			for(String clsName : sAdName){
				if(AdSwitchUtils.isNullStr(clsName)) continue;
				Object obj = Fs.getObjectByClassName(clsName);
				if(obj != null && obj instanceof AbVVV){
					sAbVVVAry.put(clsName, (AbVVV) obj);
				}
			}
		}
	}
	
	/** isTop首层标识用于防止陷入无限循环*/
	public static AbVVV configVVV(AbVVV ab, Ads ad, boolean isTop){
		AbVVV r = ab;
		if(r != null){
			if(StringUtils.isNullStr(ad.appID)){//T1--T5 OR appID is null
				try{
					String abPN = r.getClass().getSuperclass().getName();
					Ads abAd = Ads.getAds(abPN);
					if(abAd != null){
						r.appID = abAd.appID;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				r.appID = ad.appID;	
			}
			if(isTop) r.res =  getReserveInfo(ad);
		}
		return r;
	}
	public static AbVVV configVVV(AbVVV ab, Ads ad){
		return configVVV(ab, ad, true);
	}
	/** 得到备用广告实体类，用于主广告加载失败时的填充*/
	public static AbVVV getReserveInfo(Ads ad){
		AbVVV r = null;
		if(sAbVVVAry == null || sAbVVVAry.size() < 2){
			r = new DefAbVVV();
		}else{
			try{
				Ads[] ary = Ads.values();
				int offset = ad.ordinal();
				int len = ary.length;
				for(int i = offset+1; i < len+offset; i++){
					Ads aa = ary[i%len];
					if(AdSwitchUtils.isNullStr(aa.pn) || !aa.flag) continue;
					AbVVV ab = getMyVVV(aa);
					if(ab != null){
						r = ab;
						break;
					}
				}
			}catch(Exception e){
				LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
				e.printStackTrace();
			}
		}
		return r;
	}
	
	public static AbVVV getMyVVV(Ads ad){
		return getMyVVV(ad, false);
	}
	public static AbVVV getMyVVV(Ads ad, boolean isTop){
		init();
		AbVVV r = null;
		try{
			r = sAbVVVAry.get(ad.pn);
			configVVV(r, ad, isTop);
		}catch(Exception e){
		    LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		    e.printStackTrace();
		}
		if(r == null) r = new DefAbVVV();
		return r;
	}
	private static AbVVV sCurAbVVV = null;
	public static AbVVV getMyVVV(){
		if(sCurAbVVV != null && !(sCurAbVVV instanceof DefAbVVV)) return sCurAbVVV;
		init();
		AbVVV r = null;
		for(Ads t : Ads.values()){
			try{
				r = sAbVVVAry.get(t.pn);
				if(t.flag && r != null){
					configVVV(r, t);
					sCurAbVVV = r;
					break;
				}
			}catch(Exception e){
				LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
				e.printStackTrace();
			}
		}
		if(r == null) r = new DefAbVVV();
		return r;
	}
}