package com.nil.vvv.utils;

import android.content.Context;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.Fs;
import com.nil.sdk.utils.Gid;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.Spu.VSpu;
import com.nil.sdk.utils.StringUtils;
import com.xmb.mta.util.XmbOnlineConfigAgent;

import java.util.Arrays;

/**
 * @declaration 广告开关工具类<br>
 * Ads:广告平台/Sws:各类开关/Vs:在线参数键值<br>
 * @author ksA2y511xh@163.com<br>
 *2017-4-25 上午11:51:21<br>
 */
public class AdSwitchUtils {
	private static final String TAG = AdSwitchUtils.class.getSimpleName();
	private static final String ID_SUFFIX = "_id";
	private static final String CLOSE_SUFFIX = "_close";
	private static final String OPEN_SUFFIX = "_open";
	private static final String VALUE_SUFFIX = "_value";
	private static final String VERSION_HEAD = "V_";
	public static final String UMENG_CHANNEL = "UMENG_CHANNEL";

	/**
	 * 广告类，用于管理广告ID和广告总开关<br>
	 * 如:qq_id=xx|qq_close<br>
	 */
	public enum Ads{
		/**
		 * T1,T2,T3,T4,T5<br>
		 * Admob,BaiDu,GuangDianTong,XiaoMi,JuXiao,OPPO,VIVO,HuaWei,ALi,YouMi,FeiWo,XunFei<br>
		 * WanPu,DianLe<br>
		 * DUAdPlatform,Facebook,Chartboost,Vungle,Inmobi,Twitter<br>
		 * JingZhong,DaoYouDao,KuGu,QuMi<br>
		 * Mix,ohter<br>
		 */
		T1,T2,T3,T4,T5,//榜单广告Top
		Csj,Ks,Jd,Dy,Wb,Zf,
		Qq,Fo,Op,Qh,Xm,Vo,Hw,Al,Ym,Xf,Wp,Dl,Az,Bd,//国内广告
		Gg,Du,Fb,Cb,Vg,Im,//国外广告
		Jz,Dyd,Qm,Kg,//强推平台
		Mix,Other,
		E1,E2,E3,E4,E5,//榜底广告End
		End;
		public boolean flag;
		public String appID;
		public String pn;
		Ads() {
			flag = false;
		}
		public String toString(){
			String s = "";
			try {
				s = ordinal()+"."+name()+":"+flag+"/"+appID+"/"+pn;
				s = s.replace("/null", "");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return s;
		}
		public String getKey(){
			return (name()+ID_SUFFIX).toLowerCase();
		}
		public String getCloseKey(){
			return (name()+CLOSE_SUFFIX).toLowerCase();
		}
		public String getResID(){
			return ("R.string."+getKey());
		}
		public static String toAllString(){
			return Arrays.toString(values());
		}

		public static void configState(boolean state){
			Ads[] ary = values();
			for (Ads anAry : ary) {
				anAry.flag = state;
			}
		}
		public static void config(String onlineStr){
			Ads[] ary = values();
			for (Ads anAry : ary) {
				// 包含qq_id/qq_close==false
				anAry.flag = myContains(onlineStr, anAry.getKey()) && myContains(onlineStr, anAry.getCloseKey());
			}
		}
		public static void configID(){
			if(sAct == null) sAct = Utils.getApp();
			Ads[] ary = values();
			for (Ads anAry : ary) {
				anAry.appID = getInstance(sAct).getOnlineValue(anAry.getKey(), Gid.getID(sAct, anAry.getResID()));

				anAry.pn = ZFactory.sPN + ".Z" + anAry.name();
				if (!Fs.findMyClass(anAry.pn)) {
					anAry.flag = false;
					anAry.pn = null;
				}
			}
		}
		public static String[] getPN(String pn){
			String[] r = null;
			if(StringUtils.noNullStr(pn)) {
				Ads[] ary = values();
				int len = ary.length;
				r = new String[len];
				for (int i = 0; i < len; i++) {
					r[i] = pn + ".Z" + ary[i].name();
				}
			}
			return r;
		}
		public static Ads getAds(String pn){
			Ads r = null;
			if(!isNullStr(pn)){
				for(Ads ad : values()){
					if(StringUtils.containsIgnoreCase(ad.pn, pn)){
						r = ad;
						break;
					}
				}
			}
			return r;
		}

		public String close(){
			return name().toLowerCase()+CLOSE_SUFFIX;
		}
		public String open(){
			return name().toLowerCase()+OPEN_SUFFIX;
		}
	}

	/**
	 * 开关类，用于管理广告类型开关和辅助开关<br>
	 * 如：kp_close|all_open|ads_close|sp_close<br>
	 */
	public enum Sws{
		/**
		 * ECp：退出插屏开关，主要用于控制退出原生广告<br>
		 * Out：控制退出时的普通插屏和原生插屏<br>
		 * Mix：综合开关，目前分配了原生退屏和非WiFi下载广告的操作<br>
		 * ------<br>
		 * Collect：采集隐私数据的开关，在线开关为开时才会打开（默认为关闭）<br>
		 * Update：应用自动更新开关（默认为关，讯飞更新没配置容易产生假死现象）<br>
		 * MyWeb：调用内嵌浏览器进行播放Web视频（默认为关闭）<br>
		 * ERate：退出时的正规好评&反馈弹屏<br>
		 * Sp：用于本地缓存开关，默认为开启；仅当在线参数为关闭时才关<br>
		 * Exit：应用完全退出的控制开关<br>
		 */
		Rate,Push,Boot,Kp,Sp,Hf,Cp,Ys,Ys1,Ys2,Ys3,Ys4,Ys5,Jlsp,Out,ECp,Mix,Other,		//close，默认开启（含xx_close才会关闭）
		Online,Jx,Kw,Wifi,Wps,Video,Agreement,Tbk,Hb,Xsp,Zfb,Wx,Qq,Pay,Launch,
		Ver_Sp,Offline,
		noReview,//未审核
		Gdt2,
		CompleteExit,//彻底退出标识，默认随开关为true[防止应用宝隐私检测报应用退出后出现关联启动或自启]
		C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,
		All,Notice,Jfq,Tab,Collect,Update,Myweb,ERate,Exit,	//open，默认关闭（含xx_open才会开启）
		O1,O2,O3,O4,O5,O6,O7,O8,O9,O10,O11,O12,O13,O14,O15,
		Open_url,Download_url,Zfb_url,Zfb_key,Debug,
		Weal_url,Crazy_url,//福利链接（onCreate时弹出）、疯狂链接（onResume时弹出）
		privacy_policy_url,user_agreement_url,//隐私政策、用户协议链接
		Fb_Period,//反馈列表间隔拉取开关
		Cs_whole,//包名|版本名|渠道参数开关（默认关闭）
		cj_tel,cj_apps,cj_mac,cj_imei,cj_web,cj_text,cj_grxx,//采集安装列表、Mac开关（默认关闭，含xx_open才会开启）
		End;
		public boolean flag;
		Sws() {
			flag = false;
		}
		public String getKey(){
			int index = ordinal();
			if(index < All.ordinal()){
				return (name()+CLOSE_SUFFIX).toLowerCase();
			}else{
				return (name()+OPEN_SUFFIX).toLowerCase();
			}
		}
		public String toString(){
			return ordinal()+"."+name()+":"+flag;
		}
		public static String toAllString(){
			return Arrays.toString(values());
		}

		public static void configState(boolean state){
			for(Sws tmp : values()){
				tmp.flag = state;
			}
		}
		public static void config(String onlineStr){
			Sws[] ary = values();
			for (Sws anAry : ary) {
				anAry.flag = myContains(onlineStr, anAry.getKey());
			}
		}
		public static void configDef(){
			Sws[] ary = values();
			for (Sws anAry : ary) {
				int index = anAry.ordinal();
				anAry.flag = index < All.ordinal();
			}
		}

		public String close(){
			return name().toLowerCase()+CLOSE_SUFFIX;
		}
		public String open(){
			return name().toLowerCase()+OPEN_SUFFIX;
		}
	}

	/**
	 * 参数类，用于参数存储参数具体的值，值来于在线、xml、默认<br>
	 * rate_text=xx|agency_key=xvx|app_id=mn<br>
	 */
	public enum Vs{
		/**
		 * rate_text：好评弹屏的文本信息<br>
		 * rate_percent：好评弹出的百分比概率[0,100]br>
		 * dialog_private：隐私声明弹屏的文本信息<br>
		 * gap_days：用于达至指定天数后开关自动打开（默认发布后的15天）<br>
		 * release_date：发布日期，用于计算间隔天数<br>
		 * nb_web：Nil专用的所有Web视频的解析接口<br>
		 * all_web：所有Web视频的解析接口<br>
		 * exit_rate：退出时的好评&反馈文本信息<br>
		 */
		rate_text,rate_percent,notice_text,min_points,point_text,tieba_url,
		dialog_private,site_list,kw_list,update_url,zc_urls,
		um_key,ta_key,xf_key,yk_key,zfb_key,wx_key,qq_key,pay_key,xmb_key,xmb_cache_time,
		nb_web,all_web,iqiyi_web,youku_web,
		gap_days,release_date,
		version_limit,	//低于此版本号的，直接启动时跳转到版本受限界面
		ver_sp,offline,fb_period,
		c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,
		all,nb_m3u8,all_m3u8,iqiyi_m3u8,youku_m3u8,exit_rate,
		o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,
		open_url, download_url,zfb_url,mta_url,ser_url,data_url,api_url,date_url,
		weal_url,crazy_url,	//福利链接（onCreate时弹出）、疯狂链接（onResume时弹出）
		privacy_policy_url,user_agreement_url,//隐私政策、用户协议链接
		app_email,app_firm,app_name,app_date,
		agency_key,xmb_app_id,app_id,alipay_app_id, weixin_appid,weixinpay_sys_key,pay_mode,		//xmb配置信息
		kf_tel,kf_qq,kf_wx,kf_email,kf_name,kf_dy,	//客服信息
		update_config,share_config,app_config,vip_config,sys_config,welfare_config,guide_config/*发型首页展示配置*/,	//json配置信息
		kp_gap_second,cp_gap_second,hf_gap_second,crazy_gap_second,
		sms_code_switch,//短信验证码开关（1或open表示开，其它的为关）
		End;
		public boolean flag;
		public String value;
		Vs() {
			flag = false;
		}
		public String toString(){
			String s = "";
			try {
				s = ordinal()+"."+name()+":"/*+flag+"/"*/+value;
				s = s.replace(":null", "");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return s;
		}
		public String getKey(){
			return name().toLowerCase();
		}
		public String getResID(){
			return ("R.string."+getKey());
		}
		public static String toAllString(){
			return Arrays.toString(values());
		}

		public static void configState(boolean state){
			for(Vs tmp : values()){
				tmp.flag = state;
			}
		}
		public static void config(String onlineStr){
			Vs[] ary = values();
			for (Vs anAry : ary) {
				anAry.flag = myContains(onlineStr, anAry.getKey());
			}
		}
		public static void configValue(){
			Vs[] ary = values();
			for (Vs anAry : ary) {
				anAry.value = getValueByVs(anAry);
			}
		}
		public static String getValueByVs(Vs vs){
			String result = null;
			if(vs != null){
				if(sAct == null) sAct = Utils.getApp();
				result = getInstance(sAct).getOnlineValue(vs.getKey(), Gid.getID(sAct, vs.getResID()));
			}
			return result;
		}
	}

	public static boolean isNullStr(String str){
		return (str == null) || ("".equals(str.trim()));
	}
	public static String[] getAry(String str, String split, String[] def){
		return getAry(str, split, def, 1);
	}
	public static String[] getAry(String str, String split, String[] def, int minLength){
		String[] r = null;
		if(!isNullStr(str) && str.contains(split)){
			r = str.split(split);
		}
		if(r == null || r.length < minLength) r = def;
		return r;
	}
	public static boolean myContains(String onlineStr, String sw){
		boolean isOK = false;
		if(!isNullStr(onlineStr) && !isNullStr(sw)){
			if(StringUtils.containsIgnoreCase(sw, ID_SUFFIX) || StringUtils.containsIgnoreCase(sw, CLOSE_SUFFIX)){
				//包含qq_id/qq_clse:qq==false
				isOK = !StringUtils.containsIgnoreCase(onlineStr,sw);
			}else if(StringUtils.containsIgnoreCase(sw, VALUE_SUFFIX) || StringUtils.containsIgnoreCase(sw, OPEN_SUFFIX)){
				//包含qq_value/qq_open:qq==true
				isOK = StringUtils.containsIgnoreCase(onlineStr,sw);
			}else{
				isOK = StringUtils.containsIgnoreCase(onlineStr,sw);
			}
		}
		return isOK;
	}
	public void configAdSwV(Object obj){
		if(obj == null){
			System.err.println(getClass().getName()+":参数异常，请优化代码...");
		}else if(obj instanceof Boolean){
			Boolean tmp = (Boolean) obj;
			Ads.configState(tmp);
			Sws.configState(tmp);
			Vs.configState(tmp);
		}else if(obj instanceof String){
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "Adss.obj="+obj+"...");
			String tmp = ((String) obj).trim();
			if(StringUtils.containsIgnoreCase(tmp, Sws.All.name()+OPEN_SUFFIX)){//channel@@all_open##
				configAdSwV(true);
			}else if(StringUtils.containsIgnoreCase(tmp, Sws.All.name()+CLOSE_SUFFIX)){
				configAdSwV(false);
			}else{
				Ads.config(tmp);
				Sws.config(tmp);
				Vs.config(tmp);
			}
		}
	}
	public void configAdType(int type){
		int index = 0;
		String channel = StringUtils.getValue(AppUtils.getMetaChannel(sAct), Mtas.defChannel);
		if(type == index++){//0:no data
			configAdSwV(false);

			Sws.Ver_Sp.flag = Mtas.defVerSp; //默认开启Sp参数优先于在线参数，只有当在线参数包含ver_sp_close时才会关闭

			if(StringUtils.containsIgnoreCase(channel, "_kphf") || StringUtils.equalsIgnoreCase(channel, "kphf")){
				Sws.Kp.flag= Sws.Hf.flag=true;
				Ads.T1.flag= Ads.Qq.flag= Ads.Gg.flag= Ads.Fo.flag=true;
			}else if(StringUtils.containsIgnoreCase(channel, "_hf") || StringUtils.equalsIgnoreCase(channel, "hengfu")){
				Sws.Hf.flag=true;
				Ads.T1.flag= Ads.Op.flag= Ads.Qq.flag= Ads.Gg.flag=true;
			}else if(StringUtils.containsIgnoreCase(channel, CLOSE_SUFFIX) || StringUtils.equalsIgnoreCase(channel, "close")){
				configAdSwV(false);
			}else if(StringUtils.containsIgnoreCase(channel, OPEN_SUFFIX) || StringUtils.equalsIgnoreCase(channel, "open")){
				configAdSwV(true);
			}else if(StringUtils.containsIgnoreCase(channel, "_gg") || StringUtils.equalsIgnoreCase(channel, "google")){
				Sws.Hf.flag=true;
				Ads.T1.flag= Ads.Gg.flag= Ads.Qq.flag= Ads.Fo.flag=true;
			}
		}else if(type == index++){//1:debug or crazy
			configAdSwV(true);
		}else if(type == index++){//2:auto mode(after 15 days)
			boolean ok=true,bar=false;
			//开关打开模式：
			Ads.configState(ok);
			Vs.configState(ok);
			Sws.configDef();

			//开关关闭模式：
			if(StringUtils.containsIgnoreCase(channel, "_gg") || StringUtils.equalsIgnoreCase(channel, "google")){
				Sws.Out.flag= Sws.Rate.flag= Sws.Video.flag=bar;
				Sws.ERate.flag=ok;
			}else if(StringUtils.containsIgnoreCase(channel, "_op") || StringUtils.equalsIgnoreCase(channel, "oppo")){
				Sws.Cp.flag= Sws.Tab.flag=bar;
			}else if(StringUtils.containsIgnoreCase(channel, "_azOld") || StringUtils.equalsIgnoreCase(channel, "anzhiOld")){
				Sws.Cp.flag= Sws.Tab.flag=bar;
			}else if(StringUtils.containsIgnoreCase(channel, "_hw") || StringUtils.equalsIgnoreCase(channel, "huawei")){
				Sws.Cp.flag= Sws.Tab.flag=bar;
			}else if(StringUtils.containsIgnoreCase(channel, CLOSE_SUFFIX) || StringUtils.equalsIgnoreCase(channel, "close")){
				configAdSwV(false);
			}else if(StringUtils.containsIgnoreCase(channel, OPEN_SUFFIX) || StringUtils.equalsIgnoreCase(channel, "open")){
				configAdSwV(true);
			}
		}else{
			//type>=3,handle with...
		}
	}
	public void configOnlineValue(){
		//配置在线的广告ID和辅助参数
		Ads.configID();
		Vs.configValue();
	}
	public String getAdSwString(){
		String split = "::\n";
		StringBuffer buf = new StringBuffer();
		buf.append("AdsName").append(split).append(Ads.toAllString()).append("\n\n")
				.append("SwsName").append(split).append(Sws.toAllString()).append("\n\n")
				.append("VsName").append(split).append(Vs.toAllString());
		return buf.toString();
	}

	/**###########################AdSwitchUtils...begin#######################*/
	private static Context sAct;
	private static AdSwitchUtils sAdSwitchUtils;
	private AdSwitchUtils() {}
	public static AdSwitchUtils getInstance(){
		return getInstance(Utils.getApp());
	}
	public static AdSwitchUtils getInstance(Context act){
		if(act != null) {
			sAct = act;
		}else {
			sAct = Utils.getApp();
		}

		if(sAdSwitchUtils == null){;
			sAdSwitchUtils = new AdSwitchUtils();
		}

		Mtas.init(act);	//初始化各平台移动统计SDK
		return sAdSwitchUtils;
	}

	public String getOnlineValue(String key, int resId){
		if(sAct == null) sAct = Utils.getApp();
		String result = null;
		try{
			if(sAct != null && !isNullStr(key)){
				result = Mtas.getOnlineValue(sAct, key);
				if((resId > 0) && (isNullStr(result))) result = Gid.getS(sAct, resId);
				if(isNullStr(result)) result = Mtas.getDefValue(key, result);
			}
		}catch(Exception e){
			System.gc();
			e.printStackTrace();
		}
		return result;
	}
	public String getOnlineValue(String key){
		//return "mix@@hf_close##";
		return getOnlineValue(key, -1);
	}
	public String getOnlineValue(Vs vs){
		return getOnlineValue(vs.getKey(), -1);
	}

	public String getVersionKey(){
		return VERSION_HEAD+ AppUtils.getVersionName(sAct);
	}
	public String getVersionOnlineValue(String version){
		String key = (StringUtils.containsIgnoreCaseHead(version, VERSION_HEAD)) ? version : VERSION_HEAD + version;
		return getOnlineValue(key);
	}
	public String getVersionOnlineValue(){
		//支持xml配置：V_1.0=mix@@rate_close$$hf_close$$cp_close##
		return getVersionOnlineValue(AppUtils.getVersionName(sAct));
	}
	public String getVersionSpValue(){
		return Spu.loadV(sAct, getVersionKey());
	}
	public String getVersionValue(){
		if(Sws.Ver_Sp.flag){//ver_sp开关打开时，优先获取sp中的版本开关参数
			return StringUtils.getValue(getVersionSpValue(), getVersionOnlineValue());
		}else{
			return getVersionOnlineValue();
		}
	}
	public String getVersionValueInfo(){
		return (StringUtils.isNullStr(getVersionSpValue()))? "zx:"+getVersionOnlineValue(): "zx:"+getVersionOnlineValue()+"|"+"sp:"+getVersionSpValue();
	}
	public void setVersionSpValue(String sws){//channel@@hf_close$$cp_close##
		if(StringUtils.noNullStr(sws)){
			Spu.saveV(sAct, getVersionKey(), sws);
		}else {
			Spu.saveV(sAct, getVersionKey(), "");
		}
		initAdInfo();	//重新初始化数据
	}
	public void setChannelValue(String channel, String... sws){
		if(StringUtils.noNullStr(channel) && sws != null){
			StringBuilder buf = new StringBuilder();
			for(String s : sws){
				if(StringUtils.isNullStr(s)) continue;
				buf.append(s).append("$$");
			}
			buf.append("##");
			String subSws = buf.toString().replace("$$##","##");
			String mainSws = (channel+"@@"+subSws).replace("@@##","##");
			setVersionSpValue(mainSws);
		}
	}

	/**
	 * setCurChannelValue(Sws.Kp.open(), Sws.Hf.open(),Sws.Cp.close());<br>
	 */
	public void setCurChannelValue(String... sws){
		String channel = AppUtils.getMetaData(sAct, UMENG_CHANNEL);// 发到哪个市场
		setChannelValue(channel, sws);
	}

	public static void main2(String[] args) {
		String onlineStr = "tab_open$$boot_close$$gg_id$$jx_close";
		Ads.config(onlineStr);
		Sws.config(onlineStr);

		String r = Ads.toAllString();
		System.out.println(r);
		r = Sws.toAllString();
		System.out.println(r);

		System.out.println("----------");
		Ads.configState(false);
		Sws.configState(true);
		r = Ads.toAllString();
		System.out.println(r);
		r = Sws.toAllString();
		System.out.println(r);

		AdSwitchUtils.getInstance(null).setCurChannelValue(Sws.Hf.close(), Sws.Cp.close(), Sws.Kp.open());
	}

	public boolean isMyBug(String msg){
		boolean isOK = false;
		String myHead = "NiN";
		if(!isNullStr(msg) && msg.length() > myHead.length()){
			isOK = StringUtils.containsIgnoreCase(msg.substring(0, myHead.length()), myHead);
		}
		return isOK;
	}
	public void reportBugs(String msg){
		if(sAct == null || isNullStr(msg)) return;
		if(isMyBug(msg) || BaseUtils.IsCj(sAct)){
			Mtas.reportError(sAct, msg);
		}
	}
	public static void reportError(Context ctx, String msg){
		getInstance(ctx).reportBugs(msg);
	}
	public static void reportEvent(Context ctx, String key, String value){
		if(sAct == null || isNullStr(key)) return;
		Mtas.reportEvent(ctx, key, value);
	}
	public enum SjType{rate_ok,rate_no,app_begin,app_end,update_ok,update_no,mix,other}
	public static void reportEvent(Context ctx, SjType et){
		reportEvent(ctx, et.name(), DateUtils.getCurDate()+"#"+ ACacheUtils.getMyIMEI());
	}
	public void initAdInfo() {    //初始化广告信息
		initAdInfo(true);
	}
	public void initAdInfo(final boolean isUpdateOnelineConfig) {    //初始化广告信息
		//未同意隐私政策 且 禁用提前加载xmb在线参数，直接返回（不拉取在线参数）
		if(!AppUtils.hasAgreePrivacyPolicy() && XmbOnlineConfigAgent.noPreloadXmbConfig()) return;

		CrashApp.diskThread(new Runnable() {
			@Override
			public void run() {
				// 配置参数可能耗时（1-15秒），所以放到线程中执行
				initAdInfoData(isUpdateOnelineConfig);
			}
		});
	}
	public void initAdInfoData(boolean isUpdateOnelineConfig){
		if(sAct == null) sAct = Utils.getApp();
		if(sAct == null) return;
		if(isUpdateOnelineConfig) Mtas.updateOnelineConfig(sAct);	//手动更新在线参数

		try {
			String channel = AppUtils.getMetaData(sAct, UMENG_CHANNEL);// 发到哪个市场
			String strSwitch = getVersionValue();
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "Adss.strSwitch="+strSwitch+"...");
			if(StringUtils.equalsIgnoreCase(channel, "close")
					|| StringUtils.equalsIgnoreCase(strSwitch, "close")){
				// 开启debug模式
				Spu.saveV(sAct, VSpu.debug.name());
				configAdSwV(false);
			}else if(StringUtils.equalsIgnoreCase(channel, "open")
					|| StringUtils.equalsIgnoreCase(strSwitch, "open")){
				// 开启debug模式
				Spu.saveV(sAct, VSpu.debug.name());
				configAdSwV(true);
			}else if(StringUtils.equalsIgnoreCase(channel, "debug")
					|| StringUtils.equalsIgnoreCase(strSwitch, "debug")){
				// 开启debug模式
				Spu.saveV(sAct, VSpu.debug.name());
				configAdType(2);
			}else {//处理其它渠道：
				if (StringUtils.isNullStr(strSwitch)) {//null || xm##baidu (channel=google)
					configAdType(0);
					if (Mtas.isCrazyAd) {//疯狂测试模式
						configAdType(1);
					} else if (AppUtils.isSafetyDay(sAct)) {//间隔时间已到了
						configAdType(2);
					}
				} else if (StringUtils.containsIgnoreCase(strSwitch, "crazes")) {
					configAdSwV(true); //为true时，所有开关都打开
				} else if (StringUtils.containsIgnoreCase(strSwitch, "alls_close")
						|| StringUtils.equalsIgnoreCase(strSwitch, "closes")
						|| StringUtils.equalsIgnoreCase(strSwitch, "close")) {
					//在线参数中含有"alls_close"即为所有开关全关
					configAdSwV(false);
				} else if (StringUtils.containsIgnoreCase(strSwitch, "alls_open")
						|| StringUtils.equalsIgnoreCase(strSwitch, "opens")
						|| StringUtils.equalsIgnoreCase(strSwitch, "open")) {
					//在线参数中含有"alls_open"即为所有主流开关打开
					configAdType(2);
				} else if (StringUtils.containsIgnoreCase(strSwitch, channel)) {
					//渠道尽量以##结尾，@@尽量以$$结尾
					if (StringUtils.equalsIgnoreCase(strSwitch, channel)
							|| StringUtils.containsIgnoreCase(strSwitch, channel + "##")) {
						//google || google##xm## (channel=google)
						configAdSwV(channel);
					} else if (StringUtils.containsIgnoreCase(strSwitch, channel + "@@")) {
						//google@@tab_open$$cp_close## || google@@ (channel=google)
						String[] tdStrAry = strSwitch.split(channel + "@@");
						//gg_id$$bd_id$$wp_id$$am_id,含有*_id表示关
						String subSwitchs = (tdStrAry.length <= 1) ? "$$" : tdStrAry[1].split("##")[0];
						configAdSwV(subSwitchs);
					} else {
						//googlek2##googlek3@@none (channel=google)
						configAdSwV(false);
					}
				} else {
					//googlek2##googlek3@@none (channel=google)
					configAdSwV(false);
				}
			}

			//配置在线的广告ID和辅助参数
			configOnlineValue();

			//手动全关和全开处理：（Spu.saveV(ctx, VSpu.ads.name(), "suc|no");）
			String adsV = Spu.loadV(sAct, VSpu.ads.name());
			if(Spu.isSucV(adsV)){//ads_open
				configAdSwV(true);
				Mtas.isCrazyAd = true;
			}else if(Spu.isFailV(adsV)){//ads_close
				configAdSwV(false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**###########################AdSwitchUtils...end#########################*/

}
