package com.nil.vvv.utils;

import android.content.Context;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.cconfig.RemoteConfigSettings;
import com.umeng.cconfig.UMRemoteConfig;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.umcrash.UMCrash;

/**
 * 友盟数据统计工具类<br>
 *     9.X.X版本与之前8.X.X版本相比较，增加了如下新特性：<br>
 *     1、基础组件库和统计SDK两个jar包合二为一。包名及API方法名及类名不变。会话埋点(MobclickAgent.onResume/onPause)、页面埋点(MobclickAgent.onPageStart/onPageEnd)、自定义事件埋点(MobclickAgent.onEvent/onEventObject)等代码无需进行改动。<br>
 *     2、开发者不显式指定页面采集模式时，SDK默认工作在AUTO页面采集模式下。对于之前已经集成了友盟统计SDK的开发者，如果您之前的埋点代码中并没有显式调用MobclickAgent.setPageCollectionMode()方法，则需要您在升级到9.0.0版本之后，手动调用MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.LEGACY_AUTO)以保持和老版本SDK的统计行为一致。<br>
 *     3、增加一个新的SDK初始化函数UMConfigure.preInit(Context context, String appkey, String channel)，对于有延迟初始化SDK需求的开发者(不能在Application.onCreate函数中调用UMConfigure.init初始化函数），必须在Application.onCreate函数中调用UMConfigure.preInit预初始化函数(preInit耗时极少，不会影响冷启动体验)，而后UMConfigure.init函数可以按需延迟调用(可以放到后台线程中延时调用，可以延迟，但还是必须调用)。如果您的App已经是在Application.onCreate函数中调用UMConfigure.init进行初始化，则无需额外调用UMConfigure.preInit预初始化函数。<br>
 */
public class Mta2Um {
    private final static String TAG = Mta2Um.class.getSimpleName();

    /**
     * 友盟统计的预加载方法，不用权限也不进行采集，耗时极少<br>
     */
    public static void preinit(Context ctx) {
        //预初始化，不能保证在Appcalition.onCreate函数中调用UMConfigure.init初始化函数的App，必须在Appcalition.onCreate函数中调用此预初始化函数。
        String strSplit = Mtas.defSplit;
        String appKey = StringUtils.getValue(AppUtils.getMetaData(ctx, "UMENG_APPKEY"), Mtas.defUmKey);
        String channel = StringUtils.getValue(AppUtils.getMetaData(ctx, "UMENG_CHANNEL"), Mtas.defChannel);
        UMConfigure.preInit(ctx, appKey.split(strSplit)[0], channel);
    }
    public static void init(Context ctx) {
        if(ctx == null) ctx = Utils.getApp();
        // 初始化前添加权限判断（标识为true、没权限、没同意隐私政策均不进行初始化）
        if(ctx == null || Mtas.IIB.Um.flag
                || !AppUtils.hasUmMTAPermission(ctx)
                || !Spu.isSucK(ctx, Spu.VSpu.agreement.name())) {
            return;
        }
        String strSplit = Mtas.defSplit;
        try{
            //友盟在线参数配置：（xmta_umeng-onlineconfig_v1.0.0.jar-->common:9.3.0-->abtest:1.0.0）
            UMRemoteConfig.getInstance().setConfigSettings(new RemoteConfigSettings.Builder().setAutoUpdateModeEnabled(true).build());

            //新友盟统计8.0.0+（https://developer.umeng.com/docs/66632/detail/101848）：
            String appKey = StringUtils.getValue(AppUtils.getMetaData(ctx, "UMENG_APPKEY"), Mtas.defUmKey);
            String channel = StringUtils.getValue(AppUtils.getMetaData(ctx, "UMENG_CHANNEL"), Mtas.defChannel);
            UMConfigure.init(ctx, appKey.split(strSplit)[0], channel, UMConfigure.DEVICE_TYPE_PHONE, "");
            MobclickAgent.setScenarioType(ctx, MobclickAgent.EScenarioType.E_UM_NORMAL);
            MobclickAgent.setCatchUncaughtExceptions(true);

            //选用AUTO页面采集模式：支持Activity和Fragment、CustomView及用户自定义的页面场景自动采集统计
            MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);

            //友盟崩溃捕获库初始化：(apm:1.1.1)
            UMCrash.init(Utils.getApp(), appKey.split(strSplit)[0], channel);
            /*
            //开发者自己捕获了错误，需要手动上传到【友盟+】服务器可以调用下面两种方法：
            UMCrash.generateCustomLog("自定义异常代码", "UmengException");
            UMCrash.generateCustomLog(new NullPointerException("null"), "NullPointerException");
            //当崩溃发生时，您可以通过此回调回调到您的业务逻辑，该接口返回string类型数据，该返回的数据会写入到崩溃文件中并上传到服务器展示。
            UMCrash.registerUMCrashCallback(new UMCrashCallback() {
                @Override
                public String onCallback() {
                    return "崩溃时register的自定义内容字符串";
                }
            });
            */

            Mtas.IIB.Um.flag = true;
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "init is success....");
        }catch (Exception e) {
            Mtas.IIB.Um.flag = false;
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }
    public static void onKillProcess(Context ctx) {
        if(ctx != null  && Mtas.IIB.Um.flag){
            MobclickAgent.onKillProcess(ctx);
        }
    }

    public static void onResume(Context ctx) {
        if(ctx != null  && Mtas.IIB.Um.flag){
            MobclickAgent.onResume(ctx);
        }
    }
    public static void onPause(Context ctx) {
        if(ctx != null  && Mtas.IIB.Um.flag){
            MobclickAgent.onPause(ctx);
        }
    }

    public static void onPageStart(String name) {
        if(Mtas.IIB.Um.flag){
            MobclickAgent.onPageStart(name);
        }
    }
    public static void onPageEnd(String name) {
        if(Mtas.IIB.Um.flag){
            MobclickAgent.onPageEnd(name);
        }
    }

    public static void reportError(Context ctx, String key) {
        if(ctx != null  && Mtas.IIB.Um.flag){
            MobclickAgent.reportError(ctx, key);
        }
    }
    public static void onEvent(Context ctx, String key, String value) {
        if(ctx != null  && Mtas.IIB.Um.flag){
            MobclickAgent.onEvent(ctx, key, value);
        }
    }
}
