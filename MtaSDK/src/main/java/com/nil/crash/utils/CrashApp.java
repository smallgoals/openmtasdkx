package com.nil.crash.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import com.android.core.XSEUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.kongzue.dialogx.DialogX;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.Mta2Um;

/**
 * 异常捕获Application<br>
 */
@SuppressLint("StaticFieldLeak")
public class CrashApp extends Application {
    public static Context CONTEXT;
    private final static String TAG = CrashApp.class.getSimpleName();
    public void onCreate() {
        super.onCreate();

        init(this);
    }

	// 异常工具类的初始化
	public static void init(Application app){
        CONTEXT = app;

        // 加密库初始化：
        XSEUtils.init(CONTEXT);

        //关闭FileUriExposure检测：
        closeFileUriExposure();

        //AndroidUtil initOthers:
        Utils.init(app);

        //LOG日志开关：
        initDebugLog(app);

        //初始化异常捕获类：
        CustomOnCrash.install(app);

        //线程池：
        appExecutors = new AppExecutors();

        //友盟预加载：
        Mta2Um.preinit(app);

        //DialogX初始化：
        DialogX.init(app);
        //开启调试模式，在部分情况下会使用 Log 输出日志信息
        DialogX.DEBUGMODE = BaseUtils.getIsDebug();

        //设置主题样式：MaterialStyle、KongzueStyle、IOSStyle、MIUIStyle、MaterialYouStyle
        DialogX.globalStyle = MyMessageDialog.getDialogxStyle();

        //设置亮色/暗色（在启动下一个对话框时生效）
        DialogX.globalTheme = DialogX.THEME.LIGHT;
    }
    /**
     * 初始化日志LOG
     */
    public static void initDebugLog(Context ctx) {
        String app_id = StringUtils.getValue(AdSwitchUtils.Vs.xmb_app_id.value, AdSwitchUtils.Vs.app_id.value);
        initDebugLog(ctx, StringUtils.getValue(app_id, TAG));
    }
    public static void initDebugLog(Context ctx, String tag) {
        initDebugLog(tag, BaseUtils.getIsDebug(ctx));
    }
    public static void initDebugLog(String tag, boolean isLogOpen) {
        LogUtils.Config config = LogUtils.getConfig();
        config.setGlobalTag(tag);
        config.setLogSwitch(isLogOpen);
        config.setConsoleSwitch(isLogOpen);

        //调试代码：
        config.setLog2FileSwitch(isLogOpen);
    }

    @SuppressLint("ObsoleteSdkInt")
    public static void closeFileUriExposure(){
        try {
            //关闭FileUriExposure检测：
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {  //18--4.3
                builder.detectFileUriExposure();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static AppExecutors appExecutors;
    public static void mainThread(Runnable runnable) {
        if(appExecutors == null) appExecutors = new AppExecutors();
        appExecutors.mainThread().execute(runnable);
    }

    public static void networkThread(Runnable runnable) {
        if(appExecutors == null) appExecutors = new AppExecutors();
        appExecutors.networkIO().execute(runnable);
    }

    public static void diskThread(Runnable runnable) {
        if(appExecutors == null) appExecutors = new AppExecutors();
        appExecutors.diskIO().execute(runnable);
    }
}
