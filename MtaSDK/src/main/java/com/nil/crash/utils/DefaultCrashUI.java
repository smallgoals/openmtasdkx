package com.nil.crash.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.ui.BaseActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.AppUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.umeng.umcrash.UMCrash;

/**#########################################################################################
DefaultCrashUI使用说明如下：
1.1<application标签处进行声明；
	<application android:name="com.nil.crash.utils.CrashApp"
1.2或者Application在继承类的onCreate()方法中添加如下代码;
	CustomOnCrash.install(this); 	//捕捉崩溃异常的初始化
2.<activity标签处进行声明；
	<activity
	   android:name="com.nil.crash.utils.DefaultCrashUI"
	   android:configChanges="keyboard|keyboardHidden|orientation|navigation|screenSize"
	   android:label="@string/app_name"
	   android:theme="@android:style/Theme.Light.NoTitleBar"
	   android:process=":error_activity"/>
3.资源文件：
	res\drawable-hdpi\nc_oncrash_error_image.png
	res\layout\nc_oncrash_ui.xml
	res\values\nc_strings.xml
#########################################################################################**/

/**
 * @declaration 默认异常捕获界面<br>
 */
public final class DefaultCrashUI extends BaseActivity {
    private final static String TAG = DefaultCrashUI.class.getSimpleName();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_nc_oncrash_ui);

        //Close/restart button logic:
        //If a class if set, use restart.
        //Else, use close and just finish the app.
        //It is recommended that you follow this logic if implementing a custom error activity.
        Button restartBtn = (Button) findViewById(R.id.nc_crash_restart_button);

        final Class<? extends Activity> restartActivityClass = CustomOnCrash.getRestartActivityClassFromIntent(getIntent());
        final CustomOnCrash.EventListener eventListener = CustomOnCrash.getEventListenerFromIntent(getIntent());

        if (restartActivityClass != null) {
            restartBtn.setText(R.string.nc_crash_restart_app);
            restartBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(DefaultCrashUI.this, restartActivityClass);
                    CustomOnCrash.restartApplicationWithIntent(DefaultCrashUI.this, intent, eventListener);
                }
            });
        } else {
            restartBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CustomOnCrash.closeApplication(DefaultCrashUI.this, eventListener);
                }
            });
        }
        
        findViewById(R.id.nc_crash_close_button).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onBackPressed();
			}
		});

        Button bugBtn = (Button) findViewById(R.id.nc_crash_more_info_button);
        if (CustomOnCrash.isShowErrorDetailsFromIntent(getIntent())) {
        	final String msg = CustomOnCrash.getAllErrorDetailsFromIntent(DefaultCrashUI.this, getIntent());
        	AdSwitchUtils.reportError(DefaultCrashUI.this, "NiN异常日志："+msg);
        	BaseUtils.reportEvent("Crash异常日志",msg);
        	LogUtils.eTag(TAG, msg);

            UMCrash.generateCustomLog(msg, "Crash异常日志");
        	
            bugBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    final String title = "异常日志详情：";
            		MyMessageDialog.popDialog(context,title, msg, "分享", "复制","关闭", new MyMessageDialog.DialogMethod() {
            			public void sure() {
            				BaseUtils.openShare(context, msg);
            			}
            			public void neutral() {
            				BaseUtils.openCopy(context, msg);
            			}
            		}, 12);
                }
            });
            bugBtn.setOnLongClickListener(new OnLongClickListener() {
    			public boolean onLongClick(View v) {
    				if(BaseUtils.getIsDebug(DefaultCrashUI.this)) {
    					BaseUtils.popAppInfoDlg(DefaultCrashUI.this);
    					return true;
    				}
    				return false;
    			}
    		});
        } else {
            bugBtn.setVisibility(View.GONE);
        }
        

        int defaultErrorActivityDrawableId = CustomOnCrash.getDefaultErrorActivityDrawableIdFromIntent(getIntent());
        ImageView errImg = ((ImageView) findViewById(R.id.nc_crash_image));
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            errorImageView.setImageDrawable(getResources().getDrawable(defaultErrorActivityDrawableId, getTheme()));
        } else {
            //noinspection deprecation
            errorImageView.setImageDrawable(getResources().getDrawable(defaultErrorActivityDrawableId));
        }*/
        errImg.setImageDrawable(getResources().getDrawable(defaultErrorActivityDrawableId));

        TextView tv_appInfo = findViewById(R.id.tv_appInfo);
        tv_appInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                BaseUtils.showInnerLog(getActivity());
                return true;
            }
        });
        tv_appInfo.setText(String.format("%s | Version %s", AppUtils.getAppName(getActivity()), AppUtils.getVersionName(getActivity())));
    }

    @Override
    public void onBackPressed() {
    	BaseUtils.onExit(this);
    }
}
