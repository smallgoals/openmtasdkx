package com.weixin.sdk.pay;

import android.app.Activity;

import com.alipay.sdk.pay.PayResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyProgressDialog;
import com.nil.sdk.utils.Spu;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.XMBOkHttp;
import com.xmb.mta.util.XMBSign;
import com.xvx.sdk.payment.utils.PayBusUtils;
import com.xvx.sdk.payment.utils.Urls;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 微信支付入口<br>
 */
public class WeixinMainPay {
    public static final String TAG = WeixinMainPay.class.getSimpleName();
    private Activity act;
    private PayListener lsn;

    //付费标签:“BSQ”表示是变声器类的付费，“VEdit”表示是视频编辑神器类的
    private String pay4whatTag;
    //服务器pay_app_id_list.json文件里的ID，分别对应不同的支付宝媒体，如：82是BZ的手机变声器，1是XMB的手机万能变声器
    private String alipayAppID;

    private MyProgressDialog mProgressDialog;

    /**
     * @param pay4whatTag 付费标签:“BSQ”表示是变声器类的付费，“VEdit”表示是视频编辑神器类的
     * @param alipayAppID 服务器pay_app_id_list.json文件里的ID，分别对应不同的支付宝媒体，如：82是BZ的手机变声器，1是XMB的手机万能变声器
     * @param act
     * @param lsn
     */
    public WeixinMainPay(String pay4whatTag, String alipayAppID, Activity act, PayListener lsn) {
        this.act = act;
        this.lsn = lsn;
        this.pay4whatTag = pay4whatTag;
        this.alipayAppID = alipayAppID;
    }

    /**
     * @param combined_id Android手机唯一串号
     * @param subject     显示给用户看的支付文字， 要官方一点哟
     * @param body        我们后台自己看的，可以用本字段做一些拓展，如：标志用户是开了VIP1还是VIP2还是SVIP等等
     * @param price       价格，如："10.9"
     * @param dead_time   VIP订单失效时间:2020-11-13 10:38:02
     */
    public void newPay(String combined_id, String
            subject, String body, String price, String dead_time, String contact, String user_id,
                       final String weixin_appid, String weixinpay_sys_key) {
        // TODO: 2022/4/29 test...
        BaseUtils.openLogOut(act, TAG);
        //【1】.先从服务器生成订单：
        String url = Urls.URL_BUILD_WEIXIN_ORDER + "?data=";
        HashMap<String, String> map = new HashMap<>();
        map.put("title", subject);
        map.put("pay_for_what", pay4whatTag);
        map.put("pay_for_what_des", body);
        map.put("money", price);
        map.put("id", alipayAppID + "");//服务器pay_app_id_list.json文件里的ID，分别对应不同的支付宝媒体
        map.put("pay_count", "1");
        map.put("dead_time", dead_time);
        map.put("combined_id", combined_id);
        map.put("diy1", contact);
        map.put("user_id", user_id);

        map.put("weixin_appid", weixin_appid);
        map.put("weixinpay_sys_key", weixinpay_sys_key);

        PayBusUtils.post(PayBusUtils.Type.MSG_TIP_SHOW, "开始生成支付订单...");
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "0.开始生成支付订单-->"+map);
        url += XMBSign.buildUrlData(map);
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "1.服务器生成订单:开始请求链接-->"+url);
        XMBOkHttp.doGet(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                PayBusUtils.post(PayBusUtils.Type.MSG_NET_FAILED, e.toString());
            }
            public void onResponse(Call call, final Response response) throws IOException {
                //【2】.拿到订单信息后，去支付：
                ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                final String orderInfo = (resultBean == null)? null: resultBean.getResult_data();
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "2.0.拿到订单信息-->"+orderInfo);

                CrashApp.networkThread(new Runnable() {
                    @Override
                    public void run() {
                        if(orderInfo == null) {
                            PayBusUtils.post(PayBusUtils.Type.MSG_GET_ORDER_FAILED, null);
                            return;
                        }

                        PayBusUtils.post(PayBusUtils.Type.MSG_TIP_SHOW, "开始启动微信支付...");
                        WeiXinPrepayBean wp;
                        try {
                            wp = GsonUtils.fromJson(orderInfo, WeiXinPrepayBean.class);
                        }catch (Exception e){
                            if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, "2.1.订单信息Json解析出错-->"+orderInfo);
                            PayBusUtils.post(PayBusUtils.Type.MSG_GET_ORDER_PARSE_FAILED, orderInfo);
                            return;
                        }
                        if(wp != null) {
                            if (Spu.isSucK(TAG)) LogUtils.dTag(TAG, "2.2.拿到订单信息-->" + wp);
                            PayReq request = new PayReq();
                            request.appId = wp.getAppid();
                            request.partnerId = wp.getMch_id();
                            request.prepayId = wp.getPrepayid();
                            request.packageValue = "Sign=WXPay";//暂填写固定值Sign=WXPay
                            request.nonceStr = wp.getNonceStr();
                            request.timeStamp = wp.getTimeStamp();
                            request.sign = wp.getSign();

                            //缓存xmb微信支付订单号：
                            WeixinUtils.setPayXmbTradeNo(wp.getPay_xmb_trade_no());

                            boolean issendReqOK = WeixinUtils.getIWXAPI().sendReq(request);
                            if (Spu.isSucK(TAG)) LogUtils.dTag(TAG, "2.3.开始拉起微信支付-->" + issendReqOK);

                            PayBusUtils.post(PayBusUtils.Type.MSG_TIP_CLOSE, null);
                        }
                    }
                });
            }
        });
    }

    public interface PayListener {
        /**
         * 已经处于UI线程，可以做View的操作
         *
         * @param payResultState
         * @param payResult
         */
        void onPayResult(int payResultState, PayResult payResult);
    }
}
