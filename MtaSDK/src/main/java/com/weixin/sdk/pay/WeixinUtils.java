package com.weixin.sdk.pay;

import com.blankj.utilcode.util.Utils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * 微信支付工具类<br>
 */
public class WeixinUtils {
    private static IWXAPI sWxAPI;
    public static IWXAPI getIWXAPI(){
        if(sWxAPI == null) {
            String appid = AdSwitchUtils.Vs.weixin_appid.value;
            sWxAPI = WXAPIFactory.createWXAPI(Utils.getApp(), appid);
            sWxAPI.registerApp(appid);
        }
        return sWxAPI;
    }

    //是否支付微信支付：
    public static boolean isSupportWxPay() {
        IWXAPI iwxapi = getIWXAPI();
        return iwxapi.isWXAppInstalled() && iwxapi.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
    }
    //微信appid和商户信息同时有效配置返回true
    public static boolean hasWeixinPayConfig(){
        return isValid(AdSwitchUtils.Vs.weixin_appid.value)
                && isValid(AdSwitchUtils.Vs.weixinpay_sys_key.value);
    }
    //判断值是否有效：（不为空且长度大于5）
    public static boolean isValid(String value){
        //wx0cea9d8c12580fcc-weixinpay_config_mayun
        return StringUtils.noNullStr(value) && value.length() > 5;
    }

    //保存和读取Xmb订单号：
    public static void setPayXmbTradeNo(String xmbTradeNo){
        if(xmbTradeNo != null) {
            //缓存xmb微信支付订单号：
            ACacheUtils.setAppFileCacheValue(WeiXinPrepayBean.PAY_XMB_TRADE_NO_KEY, xmbTradeNo);
        }
    }
    public static String getPayXmbTradeNo(){
        //读取xmb微信支付订单号：
        return ACacheUtils.getAppFileCacheValue(WeiXinPrepayBean.PAY_XMB_TRADE_NO_KEY);
    }
}
