package com.weixin.sdk.pay;

import java.io.Serializable;

/**
 * 该Bean用于传递给手机的预支付信息。<br/>
 * 手机端请求XMB Server创建订单->XMB<br>
 * Server通过微信服务器拿到PrepayID后，再生成签名信息->把信息下发给手机端，手机就可以直接拉起支付<br>
 *
 * 微信支付-开发者文档  https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_2_4.shtml<br>
 *
 * @author dengz 2022年4月20日 15:45:39<br>
 *
 */
public class WeiXinPrepayBean implements Serializable {
	private static final long serialVersionUID = -7993587980608097096L;

	private String appid;
	private String mch_id;// 商户号，如：1624690765
	private String timeStamp;// 10位数字的时间戳
	private String nonceStr;// 随机字符串，不长于32位
	private String prepayid;// 微信返回的支付交易会话ID，该值有效期为2小时。
	private String sign;// 使用字段appId、timeStamp、nonceStr、prepayid计算得出的签名值

	private String packageValue = "Sign=WXPay";	//订单详情扩展字符串-暂填写固定值Sign=WXPay

	private String pay_xmb_trade_no;// 我们服务器生成的订单号，即支付宝或者微信的out_trade_no
	public static final String PAY_XMB_TRADE_NO_KEY = "pay_xmb_trade_no_key";	//缓存用的key

	@Override
	public String toString() {
		return "WeiXinPrepayBean{" +
				"appid='" + appid + '\'' +
				", mch_id='" + mch_id + '\'' +
				", timeStamp='" + timeStamp + '\'' +
				", nonceStr='" + nonceStr + '\'' +
				", prepayid='" + prepayid + '\'' +
				", sign='" + sign + '\'' +
				", packageValue='" + packageValue + '\'' +
				", pay_xmb_trade_no='" + pay_xmb_trade_no + '\'' +
				'}';
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getPrepayid() {
		return prepayid;
	}

	public void setPrepayid(String prepayid) {
		this.prepayid = prepayid;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getPackageValue() {
		return packageValue;
	}

	public void setPackageValue(String packageValue) {
		this.packageValue = packageValue;
	}

	public String getPay_xmb_trade_no() {
		return pay_xmb_trade_no;
	}

	public void setPay_xmb_trade_no(String pay_xmb_trade_no) {
		this.pay_xmb_trade_no = pay_xmb_trade_no;
	}
}
