package com.chad.library.adapter.base.viewholder

import android.view.View

open class BaseViewHolderEx (view: View) : BaseViewHolder(view) {
    open fun  itemView() : View {
        return itemView
    }
    open fun  itemViewType() : Int {
        return itemViewType
    }
    open fun  adapterPosition() : Int {
        return adapterPosition
    }
}