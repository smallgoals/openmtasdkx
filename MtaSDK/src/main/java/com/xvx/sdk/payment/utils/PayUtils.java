package com.xvx.sdk.payment.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.style.KongzueStyle;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xvx.sdk.payment.PayVipActivity;
import com.xvx.sdk.payment.UserLoginActivity;
import com.xvx.sdk.payment.UserPersonalDataActivity;
import com.xvx.sdk.payment.UserRegisterActivity;
import com.xvx.sdk.payment.db.UserDb;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.util.Date;

/**
 * 支付和用户相关工具类<br>
 */
@SuppressLint("StaticFieldLeak")
public class PayUtils {
    private final static String TAG = PayUtils.class.getSimpleName();
    public final static String XPAY_NET_TIME_KEY = "xpay_net_time";

    private static MessageDialog sBuyViPTipDlg;
    private static MessageDialog sLoginDlg;
    private static MessageDialog sBindViPDlg;
    private static MessageDialog sLoginStateDlg;

    public static void removeViPConfigVOCache(){
        PayVipActivity.removeViPConfigVOCache();
    }

    /**
     * 进入用户购买会员界面<br>
     */
    public static void gotoBuyViPUIV1(Activity act){
        //进入老ViP界面逻辑，不含用户个人中心
        if(OrderBeanV2.hasViP()){
            PayVipActivity.popViPInfoDlg(act);
        }else {
            PayVipActivity.start(act, act.getString(R.string.xpay_buy_vip_def_title));
        }
    }
    public static void gotoBuyViPUI(final Activity act){
        gotoBuyViPUI(act, new MyMessageDialog.DialogMethod() {
            @Override
            public void sure() {
                if(sBuyViPTipDlg != null) {
                    sBuyViPTipDlg.dismiss();
                }

                sLoginDlg = MyMessageDialog.popDialog(act, act.getString(R.string.xuser_buy_vip_tip_title),
                                act.getString(R.string.xuser_buy_vip_tip_msg),
                                act.getString(R.string.xuser_buy_vip_tip_ok),
                                act.getString(R.string.xuser_buy_vip_tip_cannel),
                                new MyMessageDialog.DialogMethod() {
                                    @Override
                                    public void sure() {
                                        PayVipActivity.start(act);
                                    }
                                })
                        .setTitleIcon(R.drawable.xpay_pay_icon_vip)
                        .setCancelable(false);
            }
        });
    }
    public static void gotoBuyViPUI(final Activity act, final MyMessageDialog.DialogMethod callback){
        //判断是否已经登录
        if(UserLoginDb.isLogin()) {
            //判断登录状态是否正常
            if(UserLoginDb.isLoginSuccess()){
                if(callback != null){
                    callback.sure();
                }
            }else{
                checkLoginState(act);
            }
        }else{
            //进行提醒用户进行登录
            popLoginTip(act);
        }
    }

    /**
     * 进入用户个人中心界面<br>
     */
    public static void gotoPersonalDataUI(final Activity act){
        //是否已登录
        if(UserLoginDb.isLogin()) {
            //判断登录状态是否正常
            if(UserLoginDb.isLoginSuccess()
                    || XSEUtils.decode("XWpscKr1dzWME1YoI0ym6ntAT3%2FLg%3D%3D").equals(UserLoginDb.getUserName())){
                //已登录直接进入个人中心
                UserPersonalDataActivity.start(act);
            }else{
                checkLoginState(act);
            }
        }else{
            //进行提醒用户进行登录
            popLoginTip(act);
        }
    }

    /**
     * 用户未登录，进行引导登录提示（由于大多为新用户，默认前往用户注册）<br>
     */
    public static void popLoginTip(final Activity act){
        popLoginTip(act, new MyMessageDialog.DialogMethod() {
            @Override
            public void sure() {
                //有用户信息或未开支付则跳转到登录界面，没有则跳转到注册界面
                if(UserDb.hasUserInfo() || !OrderBeanV2.hasPay()) {
                    UserLoginActivity.start(act);
                }else {
                    UserRegisterActivity.start(act);
                }
            }
        });
    }
    public static void popLoginTip(final Activity act, final MyMessageDialog.DialogMethod callback){
        CrashApp.mainThread(new Runnable() {
            public void run() {
                if(sLoginDlg != null) {
                    sLoginDlg.dismiss();
                }

                sLoginDlg = MyMessageDialog.popDialog(act, act.getString(R.string.xuser_mine_no_login_tip_title),
                                act.getString(R.string.xuser_mine_no_login_tip_msg),
                                act.getString(R.string.xuser_mine_no_login_tip_ok),
                                act.getString(R.string.xuser_mine_no_login_tip_cannel),
                                new MyMessageDialog.DialogMethod() {
                                    @Override
                                    public void sure() {
                                        if (callback != null) {
                                            callback.sure();
                                        }
                                    }
                                })
                        //.setTitleIcon(R.drawable.xpay_pay_icon_vip)
                        .setCancelable(false).setStyle(KongzueStyle.style());
            }
        });
    }

    /**
     * 与网络时间进行比较，防止用户修改手机系统时间<br>
     */
    public static Date getCurNetDate(){
        Date curDate = new Date();
        if(isTamperDate()){
            curDate = (Date) ACacheUtils.getAppFileCacheObject(XPAY_NET_TIME_KEY);
        }
        return curDate;
    }

    /**
     * 判断是否篡改日期，true为日期被篡改<br>
     */
    public static boolean isTamperDate(){
        boolean isOK = false;
        Date curDate = new Date();
        try {
            Date netDate = (Date) ACacheUtils.getAppFileCacheObject(XPAY_NET_TIME_KEY);
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, String.format("netDate=%s,curDate=%s",netDate, curDate));
            // 网络时间与当前时间的间隔大于或小于5（用户可能更改了系统时间），则赋值网络时间
            if (netDate != null && Math.abs(DateUtils.getOffectDay(curDate, netDate)) > 5) {
                isOK = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return isOK;
    }

    public static void onResume(final Activity act){
        /*if(OrderBeanV2.hasPay() && isOldApp()) {
            //检查老会员是否绑定用户，没绑定时弹出对话框进行提醒
            PayUtils.checkIsBindViP(act);
        }*/

        if(!UserLoginDb.isLoginSuccess()) {
            //检查用户登录状态，进行异常提醒
            checkLoginState(act, UserLoginDb.getLoginState());
        }
    }

    public static boolean isOldApp(){
        boolean isOK = false;
        String app_id = StringUtils.getValue(AdSwitchUtils.Vs.xmb_app_id.value, AdSwitchUtils.Vs.app_id.value);
        // 老APP需要支持Cid或用户查询ViP
        if (StringUtils.equalsIgnoreCase("BSQ", app_id)
                || StringUtils.equalsIgnoreCase("VEdit", app_id)
                || StringUtils.equalsIgnoreCase("LOVE", app_id)
                || StringUtils.equalsIgnoreCase("ScreenShot", app_id)
                || StringUtils.equalsIgnoreCase("Ring", app_id)
                //部分测试APP
                || StringUtils.equalsIgnoreCase("MtaDemo", app_id)
                || StringUtils.equalsIgnoreCase("PayDemo", app_id)
                || StringUtils.equalsIgnoreCase("DEBUG", app_id)
        ){
            isOK = true;
        }
        return isOK;
    }

    /**
     * 检查老会员是否绑定用户，没绑定时弹出对话框进行提醒<br>
     */
    public static void checkIsBindViP(final Activity act){
        CrashApp.mainThread(new Runnable() {
            public void run() {
                // 有ViP但没登录，提示绑定账号
                if(OrderBeanV2.hasViPNoLogin()) {
                    if(sBindViPDlg != null) {
                        sBindViPDlg.dismiss();
                    }

                    sBindViPDlg = new MessageDialog(act.getString(R.string.xuser_bind_tip_title),
                            act.getString(R.string.xuser_bind_tip_msg),
                            act.getString(R.string.xuser_bind_tip_ok),
                            act.getString(R.string.xuser_bind_tip_cannel))
                            .setTitleIcon(R.drawable.xpay_pay_icon_vip)
                            .setOkButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                                @Override
                                public boolean onClick(MessageDialog dialog, View v) {
                                    UserRegisterActivity.start(act, act.getString(R.string.xuser_bind_tip_def_title), true);
                                    return false;
                                }
                            });
                    sBindViPDlg.show();
                }
            }
        });
    }

    /**
     * 检查用户登录状态，进行异常提醒<br>
     */
    public static void checkLoginState(final Activity act){
        checkLoginState(act, UserLoginDb.getLoginState());
    }
    public static void checkLoginState(final Activity act, final String state){
        //admin测试账号不检查用户登录状态：
        if(act == null || StringUtils.isNullStr(state)
                || XSEUtils.decode("XWpscKr1dzWME1YoI0ym6ntAT3%2FLg%3D%3D").equals(UserLoginDb.getUserName())) return;

        if(StringUtils.equalsIgnoreCase(Codes.LoginState.TK_STATE_FAIL, state)){
            popDialog(act, act.getString(R.string.xuser_mine_login_fail_title),
                    act.getString(R.string.xuser_mine_login_fail_msg),
                    act.getString(R.string.xuser_ok_btn_text), act.getString(R.string.xuser_cancel_btn_text),
                    new MyMessageDialog.DialogMethod(){
                        public void sure() {
                            //清除缓存并退出APP
                            BaseUtils.clearXmbCache();
                            BaseUtils.onAutoExit(act, true);
                        }
                    });
        }else if(StringUtils.equalsIgnoreCase(Codes.LoginState.TK_STATE_TTL_TIMEOUT, state)){
            popDialog(act, act.getString(R.string.xuser_mine_login_fail_ttl_timeout_title),
                    act.getString(R.string.xuser_mine_login_fail_ttl_timeout_msg),
                    act.getString(R.string.xuser_ok_btn_text), act.getString(R.string.xuser_cancel_btn_text),
                    new MyMessageDialog.DialogMethod(){
                        public void sure() {
                            UserLoginActivity.logoutToLoginUI();
                        }
                    });
        }else if(StringUtils.equalsIgnoreCase(Codes.LoginState.TK_STATE_OTHER_ERROR, state)){
            popDialog(act, act.getString(R.string.xuser_mine_login_fail_vindicate_title),
                    act.getString(R.string.xuser_mine_login_fail_vindicate_msg),
                    act.getString(R.string.xuser_ok_btn_text), act.getString(R.string.xuser_cancel_btn_text),
                    new MyMessageDialog.DialogMethod(){
                        public void sure() {
                            BaseUtils.onAutoExit(act, true);
                        }
                    });
        }else if(StringUtils.equalsIgnoreCase(Codes.LoginState.TK_STATE_TTL_OFFLINE, state)){
            //未审核时，才弹出“被挤下线通知”（默认开关未开时为false，表示审核中）
            if(AdSwitchUtils.Sws.noReview.flag) {
                popDialog(act, act.getString(R.string.xuser_mine_login_fail_crowded_offline_title),
                        act.getString(R.string.xuser_mine_login_fail_crowded_offline_msg),
                        act.getString(R.string.xuser_ok_btn_text), act.getString(R.string.xuser_cancel_btn_text),
                        new MyMessageDialog.DialogMethod() {
                            public void sure() {
                                UserLoginActivity.logoutToLoginUI();
                            }
                        });
            }
        }
    }

    private static void popDialog(final Activity act, final String title, final String msg, final String ok, final String cancel,
                                 final MyMessageDialog.IDialogMethod callback){
        if(sLoginStateDlg != null) {
            sLoginStateDlg.dismiss();
        }
        sLoginStateDlg = new MessageDialog(title, msg, ok, cancel)
                //.setTitleIcon(R.drawable.xpay_pay_icon_vip)
                .setOkButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                    @Override
                    public boolean onClick(MessageDialog dialog, View v) {
                        if(callback != null){
                            callback.sure();
                        }
                        return false;
                    }
                }).setCancelButtonClickListener(new OnDialogButtonClickListener<MessageDialog>() {
                    @Override
                    public boolean onClick(MessageDialog dialog, View v) {
                        if(callback != null){
                            callback.cancel();
                        }
                        return false;
                    }
                });
        sLoginStateDlg.show();
    }

    /**
     * 重启到启动界面<br>
     */
    public static void reStartLauncherUI(final Activity act){
        // 清除栈内全部Activity：[可修复退出程序，点桌面图标老是启动ViP重启界面的bug]
        ActivityUtils.finishToActivity(act, true);

        // 重新进入启动界面：
        BaseUtils.gotoActivity(act, ActivityUtils.getLauncherActivity());
    }
}
