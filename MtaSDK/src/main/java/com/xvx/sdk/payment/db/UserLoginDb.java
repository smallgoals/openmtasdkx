package com.xvx.sdk.payment.db;

import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.StringUtils;
import com.xmb.mta.util.XMBSign;
import com.xvx.sdk.payment.utils.Codes;
import com.xvx.sdk.payment.utils.UserIDUtils;
import com.xvx.sdk.payment.vo.UserLoginVO;

/**
 * 用户登录返回信息的存储或读取<br>
 */
public class UserLoginDb {
    public static final String LOGIN_INFO_KEY = "xuser_login_info";
    public static final String LOGIN_STATE_KEY = "xuser_login_state";

    public static void delete() {
        ACacheUtils.removeObject(LOGIN_INFO_KEY);
        ACacheUtils.removeObject(LOGIN_STATE_KEY);

        ACacheUtils.removeObject(XMBSign.Cjs.user_id.name());
        ACacheUtils.removeObject(XMBSign.Cjs.token.name());
    }

    public static void save(UserLoginVO data) {
        ACacheUtils.setAppFileCacheObject(LOGIN_INFO_KEY, data);
        if(data != null) {
            ACacheUtils.setAppFileCacheValue(XMBSign.Cjs.user_id.name(), data.getId() + "");
            ACacheUtils.setAppFileCacheValue(XMBSign.Cjs.token.name(), data.getToken());
        }
    }
    public static UserLoginVO get() {
        return (UserLoginVO) ACacheUtils.getAppFileCacheObject(LOGIN_INFO_KEY);
    }

    public static String getLoginState(){
        return ACacheUtils.getAppFileCacheValue(LOGIN_STATE_KEY);
    }
    public static void saveLoginState(String data){
        ACacheUtils.setAppFileCacheValue(LOGIN_STATE_KEY, data);
        if(StringUtils.noNullStr(data)){
            ACacheUtils.setCacheValue(XMBSign.Cjs.login_state.name(), data);
        }
    }

    public static String getUserID(){
        String r = null;
        UserLoginVO uu = get();
        if(uu != null){
            r = uu.getId()+"";
        }
        return r;
    }

    //手机号码进行隐藏中间四位数（防止隐私泄露）：131****2345
    public static String getHideUserName(){
        return UserIDUtils.getStarTel(UserLoginDb.getUserName());
    }
    public static String getUserName(){
        return getUserName("未登录");
    }
    public static String getUserName(String defValue){
        String r = defValue;
        UserLoginVO uu = get();
        if(uu != null){
            r = uu.getUser_name();
        }
        return r;
    }
    public static String getToken(){
        String r = null;
        UserLoginVO uu = get();
        if(uu != null){
            r = uu.getToken();
        }
        return r;
    }
    public static boolean isLogin(){
        return StringUtils.noNullStr(getUserID());
    }
    public static boolean isLoginSuccess(){
        boolean isOK = true;
        if(isLogin()){
            String state = getLoginState();
            //switch字符串时报错
            if (Codes.LoginState.TK_STATE_FAIL.equals(state)
                    || Codes.LoginState.TK_STATE_TTL_OFFLINE.equals(state)
                    || Codes.LoginState.TK_STATE_TTL_TIMEOUT.equals(state)
                    || Codes.LoginState.TK_STATE_OTHER_ERROR.equals(state)) {
                isOK = false;
            } else {
                isOK = true;
            }
        }
        return isOK;
    }
}
