package com.xvx.sdk.payment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.ClipboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.kongzue.dialogx.DialogX;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.dialogs.PopMenu;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.interfaces.DialogXStyle;
import com.kongzue.dialogx.interfaces.OnMenuItemClickListener;
import com.kongzue.dialogx.style.IOSStyle;
import com.kongzue.dialogx.style.KongzueStyle;
import com.kongzue.dialogx.style.MIUIStyle;
import com.kongzue.dialogx.style.MaterialStyle;
import com.kongzue.dialogxmaterialyou.style.MaterialYouStyle;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.NetworkUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.scwang.smart.refresh.header.BezierRadarHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.xmb.mta.util.ResultBean;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.PayUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import java.util.ArrayList;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 个人中心界面<br>
 */
public class UserPersonalDataActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private final static String TAG = UserPersonalDataActivity.class.getSimpleName();
    ImageView ivUserHelp;
    ImageView ivUserViPIcon;
    TextView tvUserName;
    TextView tvUserTip;

    ImageView ivLogoffIcon;
    TextView tvLogoffTitle;
    RelativeLayout rlLogoff;
    ImageView ivModfiyPwdIcon;
    TextView tvModfiyPwdTitle;
    RelativeLayout rlModfiyPwd;
    LinearLayout llUserInfo;
    LinearLayout llOrderDetails;
    RelativeLayout rlLogout;
    TextView tvLogout;
    TextView tvOrderTitle;
    RecyclerView rvOrderInfo;
    SmartRefreshLayout refreshLayout;

    RelativeLayout rlBuyViP;

    private ArrayList<OrderBeanV2> sucOrderBeans;
    OrdersAdapter ordersAdapter;

    private static boolean mIsShowOrder = false;
    private MessageDialog sLogoffDlg;

    public static void start(Activity act) {
        start(act, null);
    }
    public static void start(Activity act, String title) {
        start(act, title, false);
    }
    public static void start(final Activity act, String title, boolean isShowOrder) {
        Intent it = new Intent(act, UserPersonalDataActivity.class);
        it.putExtra("title", title);
        it.putExtra("is_show_order", isShowOrder);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.xuser_activity_personal_data);
        setDisplayHomeAsUpEnabled(true);

        ivUserHelp = findViewById(R.id.ivUserHelp);
        ivUserViPIcon = findViewById(R.id.ivUserViPIcon);
        tvUserName = findViewById(R.id.tvUserName);
        BaseUtils.addShowInnerLogV2(tvUserName);
        tvUserTip = findViewById(R.id.tvUserTip);
        ivLogoffIcon = findViewById(R.id.ivLogoffIcon);
        tvLogoffTitle = findViewById(R.id.tvLogoffTitle);
        rlLogout = findViewById(R.id.rlLogout);

        tvLogout = findViewById(R.id.tv_logout);
        tvLogout.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvLogout.getPaint().setAntiAlias(true);//抗锯齿

        rlLogoff = findViewById(R.id.rlLogoff);
        ivModfiyPwdIcon = findViewById(R.id.ivModfiyPwdIcon);
        tvModfiyPwdTitle = findViewById(R.id.tvModfiyPwdTitle);
        rlModfiyPwd = findViewById(R.id.rlModfiyPwd);
        llUserInfo = findViewById(R.id.llUserInfo);
        tvOrderTitle = findViewById(R.id.tvOrderTitle);
        rvOrderInfo = findViewById(R.id.rvOrderInfo);
        refreshLayout = findViewById(R.id.refreshLayout);
        //设置 Header 为 贝塞尔雷达 样式
        refreshLayout.setRefreshHeader(new BezierRadarHeader(this));

        llOrderDetails = findViewById(R.id.llOrderDetails);
        rlBuyViP = findViewById(R.id.rlBuyViP);

        rlModfiyPwd.setOnClickListener(this);
        rlLogoff.setOnClickListener(this);
        rlLogout.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        rlBuyViP.setOnClickListener(this);
        ivUserHelp.setOnClickListener(this);

        String title = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(title)) {
            setTitle(title);
        }else {
            setTitle(R.string.xuser_mine_def_title);
        }
        mIsShowOrder = getIntent().getBooleanExtra("is_show_order", false);

        if(mIsShowOrder || BaseUtils.getIsDebug(this)) {
            llOrderDetails.setVisibility(View.VISIBLE);
            sucOrderBeans = OrderBeanV2.getOrderBeans();
            ordersAdapter = new OrdersAdapter();
            rvOrderInfo.setAdapter(ordersAdapter);
            rvOrderInfo.setLayoutManager(new LinearLayoutManager(getActivity()));

            buildRefresh();
        }else{
            llOrderDetails.setVisibility(View.GONE);
        }

        tvUserTip.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                llOrderDetails.setVisibility(View.VISIBLE);
                sucOrderBeans = OrderBeanV2.getOrderBeans();
                ordersAdapter = new OrdersAdapter();
                rvOrderInfo.setAdapter(ordersAdapter);
                rvOrderInfo.setLayoutManager(new LinearLayoutManager(getActivity()));

                buildRefresh();
                refreshMimeInfo();
                return true;
            }
        });

        //长按退出登录，返回登录界面
        rlLogoff.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(sLogoffDlg != null) {
                    sLogoffDlg.dismiss();
                }

                sLogoffDlg =  MyMessageDialog.popDialog(getActivity(),
                        getString(R.string.xuser_mine_psk_logoff_title),
                        getString(R.string.xuser_mine_psk_logoff_msg),
                        getString(R.string.xuser_ok_btn_text),
                        getString(R.string.xuser_cancel_btn_text),
                        new MyMessageDialog.DialogMethod(){
                            public void sure() {
                                UserLoginActivity.logoutToLoginUI();
                            }
                        });
                return true;
            }
        });

        refreshMimeInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //便于支付成功后刷新会员到期时间
        refreshMimeInfo();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.rlLogoff) {
            if(sLogoffDlg != null) {
                sLogoffDlg.dismiss();
            }

            sLogoffDlg =  MyMessageDialog.popDialog(getActivity(),
                    getString(R.string.xuser_mine_psk_logoff_title),
                    getString(R.string.xuser_mine_psk_logoff_msg),
                    getString(R.string.xuser_ok_btn_text),
                    getString(R.string.xuser_cancel_btn_text),
                    new MyMessageDialog.DialogMethod(){
                        public void sure() {
                            //退出登录并返回启动界面
                            UserLoginActivity.logoutToLauncherUI(getActivity());
                        }
                    });
        } else if (id == R.id.rlModfiyPwd) {
            BaseUtils.startActivity(UserUpdatePskActivity.class);
        } else if (id == R.id.rlBuyViP) {
            PayUtils.gotoBuyViPUIV1(this);
        } else if(id == R.id.rlLogout || id == R.id.tv_logout){
            //sendZhuxiao();
            UserDestroyActivity.start(this);
        } else if (id == R.id.ivUserHelp) {
            String[] ary = {"查看机器码", "复制客服QQ", "查看系统版本"};
            if(BaseUtils.getIsDebug()) ary = new String[]{"查看机器码", "复制客服QQ", "查看系统版本", "切换对话框风格（debug可见）"};
            PopMenu.show(ary).setOnMenuItemClickListener(new OnMenuItemClickListener<PopMenu>() {
                @Override
                public boolean onClick(PopMenu dialog, CharSequence text, int which) {
                    int index = 0;
                    if(which == index++){
                        BaseUtils.doCode();
                    }else if(which == index++){
                        String defQQ = "1283705915";
                        ClipboardUtils.copyText(StringUtils.getValue(AdSwitchUtils.Vs.kf_qq.value, defQQ));
                        ToastUtils.showLong("QQ号已经复制成功，请到手机QQ添加好友咨询");
                    }else if(which == index++){
                        BaseUtils.doSyetemVersion();
                    }else if(which == index++){
                        //MaterialStyle、KongzueStyle、IOSStyle、MIUIStyle、MaterialYouStyle
                        final String[] key = {"Material风格：0","Kongzue风格：1","IOS风格：2","MIUI风格：3","MaterialYou风格：4"};
                        final DialogXStyle[] vv = {MaterialStyle.style(), KongzueStyle.style(), IOSStyle.style(), MIUIStyle.style(), MaterialYouStyle.style()};
                        final int [] cc = {R.color.colorRed, R.color.colorBlue, R.color.colorBlack, R.color.orange, R.color.colorGreen};
                        dialog.setMenuList(key).setOnMenuItemClickListener(new OnMenuItemClickListener<PopMenu>() {
                            @Override
                            public boolean onClick(PopMenu dialog, CharSequence text, int index) {
                                TipDialog.show("已切换为"+key[index], TipDialog.TYPE.SUCCESS);
                                DialogX.globalStyle = vv[index];
                                return false;
                            }
                        })/*.setOnIconChangeCallBack(new OnIconChangeCallBack<PopMenu>() {
                            @Override
                            public int getIcon(PopMenu dialog, int index, String menuText) {
                                return cc[index];
                            }
                        })*/;
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    public Bitmap generateTextImage(String text) {
        return generateTextImage(text, 12, R.color.black, Typeface.DEFAULT_BOLD);
    }
    public Bitmap generateTextImage(String text, int textSize, int textColor, Typeface typeface) {
        Paint paint = new Paint();
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTypeface(typeface);
        paint.setAntiAlias(true);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawText(text, 0, baseline, paint);
        return bitmap;
    }

    private void fillData() {
        try {
            refreshLayout.finishRefresh();
            ordersAdapter.notifyDataSetChanged();

            refreshMimeInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void refreshMimeInfo(){
        //手机号码进行隐藏中间四位数（防止隐私泄露）：
        tvUserName.setText(UserLoginDb.getHideUserName());
        String des="";
        if(OrderBeanV2.hasViP()) {
            Date endDate = OrderBeanV2.getTotalDeadtime();
            Date curDate = PayUtils.getCurNetDate();
            long ml = endDate.getTime()-curDate.getTime();
            long day = ml/86400000;
            //如果会员有效期大于当前日期2000天，就显示为终身会员：
            if (day >2000){
                des = "终身会员";
            }else {//不是终身会员，则显示具体到期时间：
                des = String.format(getString(R.string.xuser_mine_vip_expiry_date),
                        TimeUtils.date2String(endDate)); //OrderBeanV2.getOrderBean().getDead_time()
            }
            tvUserTip.setText(des);
            ivUserViPIcon.setVisibility(View.VISIBLE);
        }else{
            tvUserTip.setText(R.string.xuser_mine_normal_user);
            ivUserViPIcon.setVisibility(View.GONE);
        }

        String orderInfo = getString(R.string.xuser_mine_order_info_title_normal);
        if(sucOrderBeans != null && !sucOrderBeans.isEmpty()) {
            orderInfo = String.format(getString(R.string.xuser_mine_order_info_title_vip), sucOrderBeans.size());
        }
        tvOrderTitle.setText(orderInfo);

        //有支付且没会员才显示“升级VIP，尊享特权”按钮：
        rlBuyViP.setVisibility(OrderBeanV2.hasPayNoViP() ? View.VISIBLE : View.GONE);

        //admin测试账号时隐藏“修改密码”：
        if (XSEUtils.decode("XWpscKr1dzWME1YoI0ym6ntAT3%2FLg%3D%3D").equals(UserLoginDb.getUserName())) {
            rlModfiyPwd.setVisibility(View.GONE);
        } else {
            rlModfiyPwd.setVisibility(View.VISIBLE);
        }
    }

    private void buildRefresh() {
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//下拉刷新
                loadData();
            }
        });
    }

    private void loadData() {
        PayWebAPI.queryOrder(new PayWebApiCallback<ResultBean>() {
            @Override
            public void onFailure(Call call, Exception e) {
                onLoadFailure();
            }
            public void onResponse(final ResultBean obj, Call call, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "拉取：ResultBean="+obj);
                        //ToastUtils.showLong("拉取：ResultBean="+obj);
                        sucOrderBeans = null;
                        if(obj != null && ResultBean.CODE_SUC == obj.getResult_code()){
                            sucOrderBeans = OrderBeanV2.getOrderBeans();
                        }else if(obj != null && ResultBean.CODE_FAIL == obj.getResult_code()){
                            PayUtils.checkLoginState(getActivity(), obj.getResult_data());
                        }else{
                            //Toast.makeText(getActivity(), R.string.xuser_mine_network_fail_tip, Toast.LENGTH_SHORT).show();

                            MyMessageDialog.popDialog(getActivity(),
                                    getString(R.string.xuser_mine_query_vip_fail_title),
                                    getString(R.string.xuser_mine_network_fail_tip),
                                    getString(R.string.xuser_know_btn_text));
                        }

                        fillData();
                    }
                });
            }
        });
    }

    private void onLoadFailure() {
        refreshLayout.finishRefresh();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!NetworkUtils.isNetworkAvailable(getActivity())) {
                    MyMessageDialog.popDialog(getActivity(),
                            getString(R.string.xuser_error_network_title),
                            getString(R.string.xuser_error_network_msg),
                            getString(R.string.xuser_retry_btn_text),
                            getString(R.string.xuser_close_btn_text),
                            new MyMessageDialog.DialogMethod(){
                                public void sure() {
                                    loadData();
                                }
                            });
                }
            }
        });
    }

    public int getOrderDays(OrderBeanV2 ob) {
        int day = 0;
        if(ob != null) {
            day = DateUtils.getOffectDay(ob.getDead_time(), ob.getOrder_time());
        }
        return day;
    }
    public String getViPType(OrderBeanV2 vo) {
        int days = getOrderDays(vo);
        String r;
        if (days == 30 || days == 31) {
            r = getString(R.string.xuser_mine_month);
        } else if (days == 90) {
            r = getString(R.string.xuser_mine_quarter);
        } else if (days == 365 || days == 366) {
            r = getString(R.string.xuser_mine_year);
        }else if(days > 36500){
            r = getString(R.string.xuser_mine_lifelong);
        }else if(days < 1){
            r = "普通";
        }else{
            r = days+"天";
        }
        r += getString(R.string.xuser_vip);
        if (vo != null && vo.getDead_time().getTime() < PayUtils.getCurNetDate().getTime()) {
            r = String.format(getString(R.string.xuser_mine_expired), r);
        }
        return r;
    }

    public int getPayColor(OrderBeanV2 vo) {
        int payTimeType = getOrderDays(vo);
        int r;
        if (payTimeType <= 30) {
            r = R.color.colorBlue;
        } else if (payTimeType <= 90) {
            r = R.color.colorPurple;
        } else if (payTimeType <= 366) {
            r = R.color.colorGold;
        }else {
            r = R.color.colorRed;
        }
        if (vo != null && vo.getDead_time().getTime() < PayUtils.getCurNetDate().getTime()) {
            r = R.color.colorGray;
        }
        return r;
    }

    private class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersViewHold> {
        @Override
        public OrdersAdapter.OrdersViewHold onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout
                    .xuser_person_list_item, parent, false);
            return new OrdersAdapter.OrdersViewHold(view);
        }

        @Override
        public void onBindViewHolder(OrdersAdapter.OrdersViewHold holder, int position) {
            final OrderBeanV2 vo = sucOrderBeans.get(position);
            if (vo != null) {
                String payType = getViPType(vo);
                SpannableString title = new SpannableString(String.format(" %s ", payType));
                title.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorWhite)),
                        0, payType.length() + 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                title.setSpan(new BackgroundColorSpan(getResources().getColor(getPayColor(vo))),
                        0, payType.length() + 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tv_title.setText(title);

                String des = String.format(getString(R.string.xuser_mine_expiry_date), TimeUtils.date2String(vo.getDead_time()));
                holder.tv_des.setText(des);
                holder.tv_time.setText(DateUtils.getStringByFormat(vo.getOrder_time(), DateUtils.dateFormatYMDHM));

                holder.iv.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        MyMessageDialog.popDialog(getActivity(),
                                getString(R.string.xuser_mine_check_order_info_title_normal),
                                vo.toString(),
                                getString(R.string.xuser_close_btn_text));
                        return true;
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            if (sucOrderBeans == null)
                return 0;
            return sucOrderBeans.size();
        }

        public class OrdersViewHold extends RecyclerView.ViewHolder {
            public View rootView;
            public TextView tv_title, tv_des, tv_time;
            public ImageView iv;

            public OrdersViewHold(View view) {
                super(view);
                rootView = view;
                tv_title = view.findViewById(R.id.tv_title);
                tv_des = view.findViewById(R.id.tv_tel);
                tv_time = view.findViewById(R.id.tv_time);
                iv = view.findViewById(R.id.iv_icon);
            }
        }
    }
}
