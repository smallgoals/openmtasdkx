package com.xvx.sdk.payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.xmb.mta.util.ResultBean;
import com.xvx.sdk.payment.db.UserDb;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.DataCheckUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.vo.UserVO;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import okhttp3.Call;
import okhttp3.Response;

/**
 *  修改密码界面<br>
 */
public class UserUpdatePskActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private final static String TAG = UserUpdatePskActivity.class.getSimpleName();
    ImageView ivOldPsk;
    EditText etOldPsk;
    RelativeLayout vg1;
    ImageView ivPsk;
    EditText etPsk;
    ImageView ivPsk2;
    EditText etPsk2;
    Button btnUpdatePsk;
    SpinKitView skv;
    RelativeLayout progress;

    public static void start(Activity act) {
        start(act, null);
    }
    public static void start(Activity act, String title) {
        Intent it = new Intent(act, UserUpdatePskActivity.class);
        it.putExtra("title", title);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xuser_activity_update_psk);
        setDisplayHomeAsUpEnabled(true);

        ivOldPsk = findViewById(R.id.ivOldPsk);
        etOldPsk = findViewById(R.id.etOldPsk);
        vg1 = findViewById(R.id.vg_1);
        ivPsk = findViewById(R.id.iv_psk);
        etPsk = findViewById(R.id.tv_psk);
        ivPsk2 = findViewById(R.id.iv_psk2);
        etPsk2 = findViewById(R.id.tv_psk2);
        btnUpdatePsk = findViewById(R.id.btnUpdatePsk);
        skv = findViewById(R.id.skv);
        progress = findViewById(R.id.progress);

        btnUpdatePsk.setOnClickListener(this);

        String title = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(title)) {
            setTitle(title);
        }else {
            setTitle(getString(R.string.xuser_update_psk_def_title));
        }
    }

    public void onBtnUpdatePskClicked() {
        final String oldPsk = etOldPsk.getText().toString();
        final String psk = etPsk.getText().toString();
        String psk2 = etPsk2.getText().toString();

        if (!DataCheckUtils.isValidPsk(oldPsk)) {
            etOldPsk.setError(getString(R.string.xuser_error_old_psk));
            DataCheckUtils.requestFocus(etOldPsk);
            return;
        }
        if (!DataCheckUtils.isValidPsk(psk)) {
            etPsk.setError(getString(R.string.xuser_error_psk));
            DataCheckUtils.requestFocus(etPsk);
            return;
        }
        if (!psk2.equals(psk)) {
            etPsk2.setError(getString(R.string.xuser_error_psk2));
            DataCheckUtils.requestFocus(etPsk2);
            return;
        }

        progress.setVisibility(View.VISIBLE);
        String user_id = UserLoginDb.getUserID();
        PayWebAPI.userUpdatePsk(new PayWebApiCallback<ResultBean>() {
            @Override
            public void onFailure(Call call, Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(R.string.xuser_connect_fail);
                        progress.setVisibility(View.INVISIBLE);
                    }
                });
            }
            public void onResponse(final ResultBean obj, Call call, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "改密：ResultBean="+obj+",OrderBeanV2="+ OrderBeanV2.getOrderBean());
                        progress.setVisibility(View.INVISIBLE);
                        KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘

                        if (obj.getResult_code() == ResultBean.CODE_SUC) {
                            UserVO uu = UserDb.get();
                            if(uu != null){
                                uu.setUser_psk(XSEUtils.mtaEncode(psk));
                                UserDb.save(uu);
                            }

                            MyMessageDialog.popDialog(getActivity(),
                                    getString(R.string.xuser_update_psk_suc_title),
                                    getString(R.string.xuser_update_psk_success),
                                    getString(R.string.xuser_update_psk_suc_okbtn_text),
                                    new MyMessageDialog.DialogMethod(){
                                        public void sure() {
                                            UserLoginActivity.logoutToLoginUI();
                                        }
                                    }).setCancelable(false);
                        } else {
                            //ToastUtils.showLong(obj.getResult_data());
                            MyMessageDialog.popDialog(getActivity(),
                                    getString(R.string.xuser_update_psk_fail),
                                    obj.getResult_data(),
                                    getString(R.string.xuser_know_btn_text));
                        }
                    }
                });

            }
        }, user_id, oldPsk, psk);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnUpdatePsk){
            onBtnUpdatePskClicked();
        }
    }
}
