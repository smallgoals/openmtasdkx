package com.xvx.sdk.payment.vo;

import java.io.Serializable;
import java.util.Date;

import androidx.annotation.Keep;

/**
 * 注册用户的信息
 * 
 * @author dengz
 *
 */
@Keep
public class UserVO implements Serializable {
	private static final long serialVersionUID = 4487553200666744191L;
	public static final int USER_STATE_NORMAL = 0;// 正常状态
	public static final int USER_STATE_BIND_VIP = 1;// 老VIP绑定过来的用户
	public static final int USER_STATE_GRAY_LIST = -1;// 处于灰名单
	public static final int USER_STATE_BLACK_LIST = -2;// 处于黑名单
	public static final int USER_STATE_CANCEL = -3;// 注销

	private int id;// 自增ID
	private String app_pkg;
	private String user_name;
	private String user_nick_name;
	private String user_psk;
	private String user_tel;
	private Date register_time;
	private String ip;
	private String combined_id;
	private int user_state;
	private String so_channel;
	private String app_channel;
	private String app_name;
	private String app_version;
	private String app_build_time;
	private String app_sign;
	private String device_version;
	private String device_imei;
	private String device_mac;
	private String device_manufacturer;
	private String remark;

	public UserVO(){}
	public UserVO(String user_name, String user_psk) {
		this.user_name = user_name;
		this.user_psk = user_psk;
	}

	@Override
	public String toString() {
		return "UserVO [id=" + id + ", app_pkg=" + app_pkg + ", user_name=" + user_name + ", user_nick_name="
				+ user_nick_name + ", user_psk=" + user_psk + ", user_tel=" + user_tel + ", register_time="
				+ register_time + ", ip=" + ip + ", combined_id=" + combined_id + ", user_state=" + user_state
				+ ", so_channel=" + so_channel + ", app_channel=" + app_channel + ", app_name=" + app_name
				+ ", app_version=" + app_version + ", app_build_time=" + app_build_time + ", app_sign=" + app_sign
				+ ", device_version=" + device_version + ", device_imei=" + device_imei + ", device_mac=" + device_mac
				+ ", device_manufacturer=" + device_manufacturer + ", remark=" + remark + "]";
	}

	public String getUser_nick_name() {
		return user_nick_name;
	}

	public void setUser_nick_name(String user_nick_name) {
		this.user_nick_name = user_nick_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApp_pkg() {
		return app_pkg;
	}

	public void setApp_pkg(String app_pkg) {
		this.app_pkg = app_pkg;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_psk() {
		return user_psk;
	}

	public void setUser_psk(String user_psk) {
		this.user_psk = user_psk;
	}

	public String getUser_tel() {
		return user_tel;
	}

	public void setUser_tel(String user_tel) {
		this.user_tel = user_tel;
	}

	public Date getRegister_time() {
		return register_time;
	}

	public void setRegister_time(Date register_time) {
		this.register_time = register_time;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCombined_id() {
		return combined_id;
	}

	public void setCombined_id(String combined_id) {
		this.combined_id = combined_id;
	}

	public int getUser_state() {
		return user_state;
	}

	public void setUser_state(int user_state) {
		this.user_state = user_state;
	}

	public String getSo_channel() {
		return so_channel;
	}

	public void setSo_channel(String so_channel) {
		this.so_channel = so_channel;
	}

	public String getApp_channel() {
		return app_channel;
	}

	public void setApp_channel(String app_channel) {
		this.app_channel = app_channel;
	}

	public String getApp_name() {
		return app_name;
	}

	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}

	public String getApp_version() {
		return app_version;
	}

	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}

	public String getApp_build_time() {
		return app_build_time;
	}

	public void setApp_build_time(String app_build_time) {
		this.app_build_time = app_build_time;
	}

	public String getApp_sign() {
		return app_sign;
	}

	public void setApp_sign(String app_sign) {
		this.app_sign = app_sign;
	}

	public String getDevice_version() {
		return device_version;
	}

	public void setDevice_version(String device_version) {
		this.device_version = device_version;
	}

	public String getDevice_imei() {
		return device_imei;
	}

	public void setDevice_imei(String device_imei) {
		this.device_imei = device_imei;
	}

	public String getDevice_mac() {
		return device_mac;
	}

	public void setDevice_mac(String device_mac) {
		this.device_mac = device_mac;
	}

	public String getDevice_manufacturer() {
		return device_manufacturer;
	}

	public void setDevice_manufacturer(String device_manufacturer) {
		this.device_manufacturer = device_manufacturer;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
