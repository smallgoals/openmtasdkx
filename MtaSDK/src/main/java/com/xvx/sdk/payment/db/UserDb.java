package com.xvx.sdk.payment.db;

import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.StringUtils;
import com.xvx.sdk.payment.vo.UserVO;

/**
 * 用户登录返回信息的存储或读取<br>
 */
public class UserDb {
    public static final String USER_INFO_KEY = "xuser_user_info";

    public static void delete() {
        ACacheUtils.removeObject(USER_INFO_KEY);
    }

    public static void save(UserVO data) {
        ACacheUtils.setAppFileCacheObject(USER_INFO_KEY, data);
    }
    public static UserVO get() {
        return (UserVO) ACacheUtils.getAppFileCacheObject(USER_INFO_KEY);
    }

    public static String getUserID(){
        String r = null;
        UserVO uu = get();
        if(uu != null){
            r = uu.getId()+"";
        }
        return r;
    }
    public static String getUserName(){
        String r = null;
        UserVO uu = get();
        if(uu != null){
            r = uu.getUser_name();
        }
        return r;
    }
    public static String getUserPsk(){
        String r = null;
        UserVO uu = get();
        if(uu != null){
            r = uu.getUser_psk();
        }
        return r;
    }

    /**
     * 用于记住密码标识：有用户信息则跳转到登录界面，没有则跳转到注册界面<br>
     */
    public static boolean hasUserInfo(){
        return StringUtils.noNullStr(getUserName());
    }
}
