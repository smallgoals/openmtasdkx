package com.xvx.sdk.payment.vo;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.Keep;

/**
 * ViP配置实体类<br>
 */
@SuppressWarnings("serial")
@Keep
public class ViPConfigVO implements Serializable {
    private static final long serialVersionUID = 5677315327063818522L;
    public static final String Cache_KEY = "ViPConfigVO";

    @Keep
    public static class ViPItem implements Serializable{
        private static final long serialVersionUID = 8053867899066010643L;
        private String name;
        private String zfbRemark;   //支付宝备注
        private String tip = ""; //促销、推荐、打折
        private double oldPrice;
        private double curPirce;
        private int vipDays;
        private String description = ""; //详细描述
        private String userContact = ""; //用户联系方式

        public ViPItem(){}
        public ViPItem(String name, double oldPrice, double curPirce){
            this();
            this.name = name;
            this.oldPrice = oldPrice;
            this.curPirce = curPirce;
        }
        public ViPItem(String name, double oldPrice, double curPirce, int vipDays) {
            this(name,oldPrice,curPirce);
            this.vipDays = vipDays;
        }
        public ViPItem(String name, double oldPrice, double curPirce, int vipDays, String description) {
            this(name,oldPrice,curPirce,vipDays);
            this.description = description;
        }
        public ViPItem(String name, String tip, double oldPrice, double curPirce, int vipDays, String description) {
            this(name,oldPrice,curPirce,vipDays,description);
            this.tip = tip;
        }
        public ViPItem(String name, String zfbRemark, String tip, double oldPrice, double curPirce, int vipDays, String description) {
            this(name,tip,oldPrice,curPirce,vipDays,description);
            this.zfbRemark = zfbRemark;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getZfbRemark() {
            return zfbRemark;
        }

        public void setZfbRemark(String zfbRemark) {
            this.zfbRemark = zfbRemark;
        }

        public String getTip() {
            return tip;
        }

        public void setTip(String tip) {
            this.tip = tip;
        }

        public double getOldPrice() {
            return oldPrice;
        }

        public void setOldPrice(double oldPrice) {
            this.oldPrice = oldPrice;
        }

        public double getCurPirce() {
            return curPirce;
        }

        public void setCurPirce(double curPirce) {
            this.curPirce = curPirce;
        }

        public int getVipDays() {
            return vipDays;
        }

        public void setVipDays(int vipDays) {
            this.vipDays = vipDays;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUserContact() {
            return userContact;
        }

        public void setUserContact(String userContact) {
            this.userContact = userContact;
        }

        @Override
        public String toString() {
            return "ViPItem{" +
                    "name='" + name + '\'' +
                    ", tip='" + tip + '\'' +
                    ", oldPrice=" + oldPrice +
                    ", curPirce=" + curPirce +
                    ", vipDays=" + vipDays +
                    ", description='" + description + '\'' +
                    ", userContact='" + userContact + '\'' +
                    '}';
        }
    }

    private boolean paySwitch = true;   //支付开关（默认打开支付开关）
    private boolean noticeSwitch;   //公告开关
    private String noticeContents = "";   //消息内容，用##进行隔开
    private String announcements = "";   //注意事项
    private String buy_agreement_url = "";   //购买协议
    private int def_vip_index;   //默认选中的ViP序号
    private List<ViPItem> vips;     //ViP列表（月、季、年会员信息）

    //以下为应用其它配置参数（脱离于支付逻辑相关体系）
    private Object sysConfig;   //存储系统或平台的配置参数（对象自定义）
    private Object appConfig;   //存储app的配置参数（对象自定义）
    private Object reserve; //备用对象参数（对象自定义）
    private List<Object> reserveAry;    //备用数组参数（对象自定义）
    private Object remark;    //备用参数（对象自定义）

    public boolean isPaySwitch() {
        return paySwitch;
    }
    public void setPaySwitch(boolean paySwitch) {
        this.paySwitch = paySwitch;
    }
    public boolean isNoticeSwitch() {
        return noticeSwitch;
    }
    public void setNoticeSwitch(boolean noticeSwitch) {
        this.noticeSwitch = noticeSwitch;
    }

    public String getNoticeContents() {
        return noticeContents;
    }
    public void setNoticeContents(String noticeContents) {
        this.noticeContents = noticeContents;
    }
    public String getAnnouncements() {
        return announcements;
    }
    public void setAnnouncements(String announcements) {
        this.announcements = announcements;
    }
    public List<ViPItem> getVips() {
        return vips;
    }
    public void setVips(List<ViPItem> vips) {
        this.vips = vips;
    }
    public String getBuy_agreement_url() {
        return buy_agreement_url;
    }
    public void setBuy_agreement_url(String buy_agreement_url) {
        this.buy_agreement_url = buy_agreement_url;
    }
    public int getDef_vip_index() {
        return def_vip_index;
    }
    public void setDef_vip_index(int def_vip_index) {
        this.def_vip_index = def_vip_index;
    }

    public Object getSysConfig() {
        return sysConfig;
    }
    public void setSysConfig(Object sysConfig) {
        this.sysConfig = sysConfig;
    }
    public Object getAppConfig() {
        return appConfig;
    }
    public void setAppConfig(Object appConfig) {
        this.appConfig = appConfig;
    }
    public Object getReserve() {
        return reserve;
    }
    public void setReserve(Object reserve) {
        this.reserve = reserve;
    }
    public List<Object> getReserveAry() {
        return reserveAry;
    }
    public void setReserveAry(List<Object> reserveAry) {
        this.reserveAry = reserveAry;
    }
    public Object getRemark() {
        return remark;
    }
    public void setRemark(Object remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "ViPConfigVO{" +
                "paySwitch=" + paySwitch +
                ", noticeSwitch=" + noticeSwitch +
                ", noticeContents='" + noticeContents + '\'' +
                ", announcements='" + announcements + '\'' +
                ", buy_agreement_url='" + buy_agreement_url + '\'' +
                ", def_vip_index=" + def_vip_index +
                ", vips=" + vips +
                ", sysConfig=" + sysConfig +
                ", appConfig=" + appConfig +
                ", reserve=" + reserve +
                ", reserveAry=" + reserveAry +
                ", remark=" + remark +
                '}';
    }

    public static ViPConfigVO getRawViPConfig(){
        return getRawViPConfig(R.raw.vip_config, "UTF-8");
    }
    public static ViPConfigVO getRawViPConfig(final int resId, final String charsetName){
        ViPConfigVO vo = null;
        try {
            //加载Raw文件中的数据
            String json = ResourceUtils.readRaw2String(resId, charsetName);
            vo = new Gson().fromJson(json, new TypeToken<ViPConfigVO>(){}.getType());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return vo;
    }
    public static ViPConfigVO getAllViPConfig(){
        ViPConfigVO obj = null;
        //在线参数#vip_config>服务器#vip_config.json>raw#vip_config.json
        String mta_json = AdSwitchUtils.Vs.vip_config.value;
        if(StringUtils.noNullStr(mta_json)){
            //在线参数#vip_config
            obj = GsonUtils.fromJson(mta_json, ViPConfigVO.class);
        }

        if(obj == null) {
            //服务器#vip_config.json
            obj = (ViPConfigVO) ACacheUtils.getAppCacheObject(ViPConfigVO.Cache_KEY);
            if (obj == null) {
                //raw#vip_config.json
                obj = getRawViPConfig();
            }
        }
        return obj;
    }
}
