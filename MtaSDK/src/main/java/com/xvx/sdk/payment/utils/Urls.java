package com.xvx.sdk.payment.utils;

public class Urls {
    //服务器地址：
    public static final String baseUrl = com.xmb.mta.util.Urls.URL_TEST;

//    //花生壳或本地服务器地址：
//    public static final String baseUrl_hsk = "http://283hh38790.zicp.vip";
//    public static final String baseUrl = baseUrl_hsk;

    //通过Cid查询VIP信息：
    public static final String URL_ORDER_QUERY = baseUrl + "/mta/query_order_by_combined_id";

    /*****【支付宝支付】*****/
    //支付宝支付-同步查询支付验签：
    public static final String URL_QUERY_ALIPAY_ORDER = baseUrl + "/mta/query_alipay_order";
    //支付宝支付-生成订单：
    public static final String URL_BUILD_ALIAY_ORDER = baseUrl + "/mta/build_alipay_order";

    /*****【微信支付】*****/
    //微信支付-同步查询支付验签：
    public static final String URL_QUERY_WEIXIN_ORDER = Urls.baseUrl + "/mta/query_weixinpay_order";
    //微信支付-生成订单：
    public static final String URL_BUILD_WEIXIN_ORDER = Urls.baseUrl + "/mta/build_weixin_order";

    //用户注册、登录、修改密码：
    public static final String URL_USER_REGISTER = baseUrl + "/mta/user_register";
    public static final String URL_USER_LOGIN = baseUrl + "/mta/user_login";
    public static final String URL_USER_UPDATE_PASSWORD = baseUrl + "/mta/user_update_psk";
    public static final String URL_USER_DESTROY = baseUrl + "/mta/user_destroy";

    //通过用户ID查询VIP信息：
    public static final String URL_QUERY_VIP_BY_USER_ID = baseUrl + "/mta/query_order_by_user_id";
    //用户注册并绑定支付好的订单：
    public static final String URL_USER_REGISTER_AND_BIND_VIP = baseUrl + "/mta/user_register_and_bind_vip";

    //存储/更新用户数据到云端
    public static final String URL_USER_DATA = baseUrl + "/mta/UserDataServlet";

    //获取手机验证码：
    public static final String URL_SMS_CODE = baseUrl + "/mta/sms_code";
}
