package com.xvx.sdk.payment;


import android.os.Handler;
import android.os.Message;

import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.utils.Spu;

import java.lang.ref.WeakReference;

/**
 * Author: liaohaiping
 * Time: 2018-09-04
 * Description:
 */
public class ActivitySafeHandler extends Handler {
    private final static String TAG = ActivitySafeHandler.class.getSimpleName();
    public enum ActivityState {CREATED, RESUMED, PAUSED, STOPED, DESTORYED}

    private WeakReference<BaseHandlerACActivity> weakReferenceHandler;
    private ActivityState mState;

    public ActivitySafeHandler(BaseHandlerACActivity handler) {
        super();
        mState = ActivityState.CREATED;
        this.weakReferenceHandler = new WeakReference<>(handler);
    }

    @Override
    public void handleMessage(Message msg) {
        BaseHandlerACActivity activity = this.weakReferenceHandler.get();
        if(activity != null && mState != ActivityState.DESTORYED) {
            activity.processUIMessage(msg);
        }
    }

    public void onResume() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onResume...");
        mState = ActivityState.RESUMED;
    }
    public void onPause() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onPause...");
        mState = ActivityState.PAUSED;
    }
    public void onStop() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onStop...");
        mState = ActivityState.STOPED;
    }
    public void onDestory() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onDestory...");
        mState = ActivityState.DESTORYED;
    }
}
