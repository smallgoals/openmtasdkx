package com.xvx.sdk.payment.vo;

import androidx.annotation.Keep;

import com.xvx.sdk.payment.db.UserLoginDb;

import java.io.Serializable;

@Keep
public class UserLoginVO implements Serializable {
    private static final long serialVersionUID = 2534332379854896797L;
    private int id;
    private String user_name;
    private String token;
    public UserLoginVO(){}
    public UserLoginVO(String user_name, String token) {
        this();
        this.user_name = user_name;
        this.token = token;
    }
    public UserLoginVO(int id, String user_name, String token) {
        this(user_name,token);
        this.id = id;
    }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setUser_name(String user_name){
        this.user_name = user_name;
    }
    public String getUser_name(){
        return this.user_name;
    }
    public void setToken(String token){
        this.token = token;
    }
    public String getToken(){
        //return "V1.100.1568793237683";
        return this.token;
    }
    @Override
    public String toString() {
        return "UserLoginVO [id=" + id + ", user_name=" + user_name + ", token="
                + token+", login_state=" + UserLoginDb.getLoginState() + "]";
    }
}
