package com.xvx.sdk.payment.utils;

import com.nil.sdk.utils.StringUtils;
import com.xvx.sdk.payment.vo.ViPConfigVO;
import com.xvx.sdk.payment.vo.ViPConfigVO.ViPItem;

import java.util.List;
import java.util.Random;

public class UserIDUtils {
	public static final String sDefNotice = "开通APP会员";
	public static final String sDefPrefixViP = "开通";
	public static void main(String[] args) {
		for(int i = 0; i < 30; i++){
			String id = getUserID()+"\t"+getStarDate();
			System.out.println(id);
		}
	}
	public static String getViPNotice(ViPConfigVO vo){
		return getViPNotice(vo, "  ", sDefPrefixViP);
	}
	public static String getViPNotice(ViPConfigVO vo, String split, String prefixViP){
		StringBuffer buf = new StringBuffer();
		buf.append(getUserID());
		buf.append(split);
		buf.append(getStarViPName(vo, prefixViP));
		buf.append(split);
		buf.append(getStarDate());
		return buf.toString();
	}

    public static String getViPNoticeV2(ViPConfigVO vo){
        return getViPNoticeV2(vo, "  ");
    }
    public static String getViPNoticeV2(ViPConfigVO vo, String split){
        StringBuffer buf = new StringBuffer();
        buf.append(getUserID());
        buf.append(split);
        buf.append(getStarNotice(vo));
        buf.append(split);
        buf.append(getStarDate());
        return buf.toString();
    }

	public static String getUserID(){
		String id = "";
		int i = new Random().nextInt(100);
		if(i%4 == 0){
			id = getStarTel();
		}else if(i%4 ==1){
			id = getStarEmail(2, 3);
		}else if(i%4 ==2){
			id = getStarQQ(8, 12);
		}else if(i%4 ==3){
			id = getStarWeixin(6, 10);
		}
		return id;
	}

	public static String base = "abcdefghijklmnopqrstuvwxyz0123456789";
    //private static final String[] email_suffix="@gmail.com,@yahoo.com,@msn.com,@hotmail.com,@aol.com,@ask.com,@live.com,@qq.com,@0355.net,@163.com,@163.net,@263.net,@3721.net,@yeah.net,@googlemail.com,@126.com,@sina.com,@sohu.com,@yahoo.com.cn".split(",");  
    private static final String[] email_suffix="@qq.com,@163.com,@126.com".split(",");
    public static int getNum(int start,int end) {  
        return (int)(Math.random()*(end-start+1)+start);  
    }  
    /** 
     * 返回Email 
     * @param min 最小长度 
     * @param max 最大长度 
     * @return 
     */ 
	public static String getEmail(int min, int max) {
		int length = getNum(min, max);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = (int) (Math.random() * base.length());
			sb.append(base.charAt(number));
		}
		sb.append(email_suffix[(int) (Math.random() * email_suffix.length)]);
		return sb.toString();
	}
	public static String getStarEmail(int min, int max) {
		int length = getNum(min, max);
		StringBuffer sb = new StringBuffer();
		String end = email_suffix[(int) (Math.random() * email_suffix.length)];
		
		if("@qq.com".equals(end)){
			String tel = getTel();
			int len = min+new Random().nextInt(max-min+1);
			sb.append(tel.substring(0, len));
		}else{
			for (int i = 0; i < length; i++) {
				int number = (int) (Math.random() * base.length());
				sb.append(base.charAt(number));
			}
		}
		sb.append("****");
		sb.append(end);
		return sb.toString();
	}
   
    /** 
     * 返回手机号码 
     */ 
    private static String[] telFirst="130,131,132,145,155,156,185,186,176,175,134,135,136,137,138,139,147,150,151,152,157,158,159,182,183,187,188,178,133,153,180,181,189,177,173,149,170,171,199".split(",");  
	public static String getTel() {
		int index = getNum(0, telFirst.length - 1);
		String first = telFirst[index];
		String second = String.valueOf(getNum(1,888)+10000).substring(1);
		String thrid = String.valueOf(getNum(1, 9100) + 10000).substring(1);
		return first + second + thrid;
	}
	public static String getStarTel() {
		int index = getNum(0, telFirst.length - 1);
		String first = telFirst[index];
		String second = "****"; // String.valueOf(getNum(1,888)+10000).substring(1);
		String thrid = String.valueOf(getNum(1, 9100) + 10000).substring(1);
		return first + second + thrid;
	}

	/**
	 * 获取中间四位隐藏的手机号<br>
	 * @param phone 11位手机号（小于11位原样返回）
	 */
	public static String getStarTel(String phone) {
		if(StringUtils.isNullStr(phone) || phone.length() < 11) return phone;
		String first = phone.substring(0, 3);
		String second = "****";
		String thrid = phone.substring(phone.length()-4);
		return first + second + thrid;
	}

	enum CType {XChar,XNum,XAll}
	public static char getChar(CType type) {
		char c = 0;
		if(type == CType.XChar){//char
			int number = (int)(Math.random()*26);
			c = base.charAt(number);
		}else if (type == CType.XNum){//num
			c = (char) getNum(0, 9);
		}else{//char || num
			int number = (int)(Math.random()*base.length());
			c = base.charAt(number);
		}
		return c;
	}
	public static String getQQ(int min,int max) {
		int length = getNum(min, max) - 1;
		StringBuffer sb = new StringBuffer();
		sb.append(getNum(1, 9));
		for (int i = 0; i < length; i++) {
			sb.append(getNum(0, 9));
		}
		return sb.toString();
	}
	public static String getStarQQ(int min,int max) {
		String s = getQQ(min, max);
		int starLen = 4;
		int index = (s.length()-starLen)/2;

		String r = s;
		if(index > 0){
			char[] ary = s.toCharArray();
			for(int i = index; i < index+starLen; i++){
				ary[i] = '*';
			}
			r = new String(ary);
		}
		return r;
	}
	public static String getWeixin(int min,int max) {
		int length=getNum(min,max)-1;
		StringBuffer sb = new StringBuffer();
		sb.append(getChar(CType.XChar));
		for (int i = 0; i < length; i++) {
			sb.append(getChar(CType.XAll));
		}
		return sb.toString();
	}
	public static String getStarWeixin(int min,int max) {
		String s = getWeixin(min, max);
		int starLen = 4;
		int index = (s.length()-starLen)/2;

		String r = s;
		if(index > 0){
			char[] ary = s.toCharArray();
			for(int i = index; i < index+starLen; i++){
				ary[i] = '*';
			}
			r = new String(ary);
		}
		return r;
	}

	private static final String[] date_suffix="分钟前,分钟前,分钟前,分钟前,分钟前,小时前,小时前,小时前,天前,天前".split(",");
	public static String getStarDate() {
		int pos = getNum(0, date_suffix.length - 1);
		String end = date_suffix[pos];
		StringBuffer buf = new StringBuffer();
		
		if(end.contains("分钟")){
			buf.append(getNum(1, 59));
		}else if(end.contains("小时")){
			buf.append(getNum(1, 23));
		}else if(end.contains("天")){
			buf.append(getNum(1, 2));
		}
		
		buf.append(end);
		return buf.toString();
	}
	
	public static String getStarViPName(ViPConfigVO vo) {
		return getStarViPName(vo, sDefPrefixViP);
	}
	public static String getStarViPName(ViPConfigVO vo, String prefixViP) {
		String item = sDefNotice;
		if(vo != null){
			List<ViPItem> vips = vo.getVips();
			if(vips != null && !vips.isEmpty()){
				int pos = getNum(0, vips.size() - 1);
				String name = vips.get(pos).getName();
				if(name != null && !"".equals(name)){
					item = prefixViP + name;
				}
			}
		}
		return item;
	}
    public static String getStarNotice(ViPConfigVO vo) {
        return getStarNotice(vo, "##");
    }
    public static String getStarNotice(ViPConfigVO vo, String split) {
        String item = sDefNotice;
        if(vo != null){
            String contents = vo.getNoticeContents();
            if(contents != null && !"".equals(contents.trim())){
                String [] ary = contents.split(split);
                int pos = getNum(0, ary.length - 1);
                String name = ary[pos];
                if(name != null && !"".equals(name)){
                    item = name;
                }
            }else{
				item = getStarViPName(vo);
			}
        }
        return item;
    }

	/**
	 * 得到N位的数字验证码<br>
	 * @param num 验证码的个数<br>
	 */
	public static String getNumCode(int num) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < num; i++) {
			buf.append(getNum(0, 9));
		}
		return buf.toString();
	}
	public static String getNumCode() {
		return getNumCode(6);
	}
}
