package com.xvx.sdk.payment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ui.WebHtmlActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Keep;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alipay.sdk.pay.ZhifubaoMainPay;
import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.BusUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.ui.aid.MyProgressDialog;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.Gid;
import com.nil.sdk.utils.MobileInfoUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.sdk.utils.XMBEventUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.AdSwitchUtils.Vs;
import com.weixin.sdk.pay.WeixinMainPay;
import com.weixin.sdk.pay.WeixinUtils;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.PayBusUtils;
import com.xvx.sdk.payment.utils.PayUtils;
import com.xvx.sdk.payment.utils.UserIDUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.vo.PayDlgVO;
import com.xvx.sdk.payment.vo.UserLoginVO;
import com.xvx.sdk.payment.vo.ViPConfigVO;
import com.xvx.sdk.payment.vo.ViPConfigVO.ViPItem;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 购买会员界面<br>
*/
public class PayVipActivity extends BaseHandlerACActivity implements View.OnClickListener {
    private final static String TAG = PayVipActivity.class.getSimpleName();
    TextView tvNotice;
    RecyclerView rvVipList;
    EditText etContactPhone;
    TextView tvPrice;
    TextView tvbtnPay;
    LinearLayout pBottom;
    LinearLayout loading;
    LinearLayout llRoot;
    LinearLayout llNotice;
    TextView tvBuyVipPs;
    TextView tvbuyAgreement;

    LinearLayout llZfbPay, llWxPay,llPayModeTag;
    ImageView imgZfbPay, imgWxPay;
    int payMode = 0; //0:支付宝支付 1:微信支付

    TextView tvUserID; //用户ID

    ViPAdapter mAdapter;
    int mCurPos = 0;
    ViPItem mCurViPItem;
    ViPConfigVO mViPConfigVO;
    static IPayCallback sPayCallback = null;
    String mUrl;

    int mNoticeGapSecond = 5;
    private MyProgressDialog mProgressDialog;

    public static void start(Context ctx) {
        start(ctx, null);
    }
    public static void start(Context ctx, String title) {
        start(ctx, title, null);
    }
    public static void start(Context ctx, String title, IPayCallback payCallback) {
        Intent it = new Intent(ctx, PayVipActivity.class);
        it.putExtra("title", title);
        sPayCallback = payCallback;
        BaseUtils.startActivity(ctx,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xpay_activity_buy_vip);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setDisplayHomeAsUpEnabled(true);

        tvNotice = findViewById(R.id.tvNotice);
        rvVipList = findViewById(R.id.rvVipList);
        etContactPhone = findViewById(R.id.etContactPhone);
        tvPrice = findViewById(R.id.tvPrice);
        tvbtnPay = findViewById(R.id.tvbtnPay);
        pBottom = findViewById(R.id.pBottom);
        loading = findViewById(R.id.ll_load);

        llRoot = findViewById(R.id.ll_root);
        llNotice = findViewById(R.id.ll_notice);
        tvBuyVipPs = findViewById(R.id.tv_buy_vip_ps);
        tvbuyAgreement = findViewById(R.id.tv_buy_agreement);

        tvbtnPay.setOnClickListener(this);
        tvbuyAgreement.setOnClickListener(this);

        String title = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(title)) {
            setTitle(title);
        }else {
            setTitle(getString(R.string.xpay_buy_vip_def_title));
        }
        mUrl = getResources().getString(R.string.vip_config_url);
        try {
            mNoticeGapSecond = Integer.parseInt(StringUtils.getValue(Gid.getS(getActivity(), R.string.notice_gap_second), "5"));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        if(mNoticeGapSecond <= 0 || mNoticeGapSecond > 36000){
            mNoticeGapSecond = 5;
        }

        /*if(sPayCallback == null){
            sPayCallback = getDefPayCallback(getActivity());
        }*/

        loading.setVisibility(View.VISIBLE);
        llRoot.setVisibility(View.VISIBLE);
        llNotice.setVisibility(View.GONE);

        mAdapter = new ViPAdapter(R.layout.xpay_item_buy_vip, null);
        //设置布局管理器
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        rvVipList.setLayoutManager(layoutManager);
        rvVipList.setAdapter(mAdapter);

        llZfbPay = findViewById(R.id.ll_zfb_pay);
        llWxPay = findViewById(R.id.ll_wx_pay);
        imgZfbPay = findViewById(R.id.img_zfb_pay);
        imgWxPay = findViewById(R.id.img_wx_pay);
        llZfbPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payMode = 0;
                imgZfbPay.setImageResource(R.drawable.xpay_pay_ic_checked);
                imgWxPay.setImageResource(R.drawable.xpay_pay_ic_unchecked);
            }
        });
        llWxPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payMode = 1;
                imgZfbPay.setImageResource(R.drawable.xpay_pay_ic_unchecked);
                imgWxPay.setImageResource(R.drawable.xpay_pay_ic_checked);
            }
        });
        /*****动态显示支付宝或微信支付：（微信不支持H5支付、支付宝支持H5支付）*****/
        //已安装微信且微信版本支持微信支付、且微信支付参数已配置、且wx开关已打开，才显示微信支付
        if(WeixinUtils.isSupportWxPay() && WeixinUtils.hasWeixinPayConfig() && AdSwitchUtils.Sws.Wx.flag){
            llWxPay.setVisibility(View.VISIBLE);
        }else{
            llWxPay.setVisibility(View.GONE);
        }
        //zfb开关已打开或微信支付隐藏，显示支付宝支付
        if(AdSwitchUtils.Sws.Zfb.flag || llWxPay.getVisibility() == View.GONE){
            llZfbPay.setVisibility(View.VISIBLE);
        }else{
            llZfbPay.setVisibility(View.GONE);
        }

        /*配置默认支付方式*/
        try{
            payMode = Integer.parseInt(Vs.pay_mode.value);
        }catch(Exception e){
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, e.toString());
        }
        //支付方式为微信支付且布局显示
        if(payMode == 1 && llWxPay.getVisibility() == View.VISIBLE){
            payMode = 1;
            imgZfbPay.setImageResource(R.drawable.xpay_pay_ic_unchecked);
            imgWxPay.setImageResource(R.drawable.xpay_pay_ic_checked);
        }else{
            payMode = 0;
            imgZfbPay.setImageResource(R.drawable.xpay_pay_ic_checked);
            imgWxPay.setImageResource(R.drawable.xpay_pay_ic_unchecked);
        }

        //老年模式，关闭支付方式文字
        llPayModeTag = findViewById(R.id.ll_pay_mode_tag);
        llPayModeTag.setVisibility(BaseUtils.isOldMode(this) ? View.GONE : View.VISIBLE);

        initData();

        //注册Bus事件：
        BusUtils.register(this);

        //显示用户ID：未登录显示当前日期:[20220509] | 登录则显示用户ID：[4838820]
        String userID = UserLoginDb.getUserID();
        if(StringUtils.isNullStr(userID)){
            userID = DateUtils.getCurDateByFormat("yyyyMMdd");
        }
        tvUserID = findViewById(R.id.tvUserID);
        tvUserID.setText(String.format("[%s]", userID));
        tvUserID.setVisibility(View.VISIBLE);

        //用户登录后，配置联系方式为用户名称，这样用户支付不需要输入联系方式
        etContactPhone.setText(UserIDUtils.getStarTel(UserLoginDb.getUserName("")));
    }

    public void initData() {
        //1.先读取在线参数
        ViPConfigVO vo = loadVIPConfigFromMTA();
        //2.如果没有配置，才去读取URL的：
        if (vo != null) {
            loadData(vo);
        } else {
            loadVIPConfigFromJsonURL();
        }
    }

    /**
     * 从MTA的后台在线参数读取
     */
    private ViPConfigVO loadVIPConfigFromMTA() {
        try {
            String json = AdSwitchUtils.getInstance(Utils.getApp()).getOnlineValue("vip_config");//该行无网络访问
            if (!com.blankj.utilcode.util.StringUtils.isEmpty(json)) {
                ViPConfigVO vipVO = new Gson().fromJson(json, new TypeToken<ViPConfigVO>() {
                }.getType());
                return vipVO;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    /**
     * 从URL的JSON文件读取
     */
    private void loadVIPConfigFromJsonURL() {
        PayWebAPI.loadViPConfig(mUrl, new PayWebApiCallback<ViPConfigVO>() {
            @Override
            public void onFailure(Call call, Exception e) {
                ViPConfigVO vo = ViPConfigVO.getRawViPConfig();

                if(vo != null && vo.getVips() != null && vo.getVips().size() > 0){
                    loadData(vo);
                }else{
                    popErrorTip();
                }
            }
            public void onResponse(final ViPConfigVO vo, final Call call, Response response) {
                ViPConfigVO vc = vo;
                if(vc == null){
                    vc = ViPConfigVO.getRawViPConfig();
                }
                loadData(vc);
            }
        });
    }

    public void loadData(final ViPConfigVO vo){
        if (getActivity() != null && vo != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mViPConfigVO = vo;
                    List<ViPItem> ary = mViPConfigVO.getVips();
                    mCurPos = vo.getDef_vip_index();

                    String des = vo.getAnnouncements();
                    if(StringUtils.noNullStr(des)) {
                        tvBuyVipPs.setText(des);
                    }

                    boolean noticeSwitch = mViPConfigVO.isNoticeSwitch();

                    mAdapter.getData().clear();
                    if(ary != null && !ary.isEmpty()){
                        mAdapter.getData().addAll(ary);

                        mCurPos = (mCurPos >= 0 && mCurPos < ary.size()) ? mCurPos : 0;
                        mCurViPItem = ary.get(mCurPos);

                        mAdapter.notifyDataSetChanged();
                        loading.setVisibility(View.GONE);
                        rvVipList.setVisibility(View.VISIBLE);
                        llRoot.setVisibility(View.VISIBLE);

                        if(noticeSwitch){
                            postUIMessage(PayBusUtils.Type.MSG_START_NOTICE);
                            llNotice.setVisibility(View.VISIBLE);
                        }else{
                            llNotice.setVisibility(View.GONE);
                        }
                    }else {
                        mCurPos = 0;
                        popErrorTip();
                    }
                }
            });
        }else{
            mCurPos = 0;
            popErrorTip();
        }
    }
    public void popErrorTip(){
        if(getActivity() != null){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.setVisibility(View.GONE);
                    llRoot.setVisibility(View.GONE);
                    llNotice.setVisibility(View.GONE);

                    errorTipDlg(getActivity());
                }
            });
        }
    }
    public static void popViPInfoDlg(Activity act, OrderBeanV2 obj){
        if(act != null && obj != null) {
            vipInfoDlg(act, obj);
        }else {
            // TO-DO:为非会员
        }
    }
    public static void popViPInfoDlg(Activity act){
        popViPInfoDlg(act, OrderBeanV2.getOrderBean());
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tvbtnPay) {
            String contact = etContactPhone.getText().toString().trim();
            if (StringUtils.noNullStr(contact)) {
                mCurViPItem.setUserContact(contact);
                String subject = StringUtils.getValue(mCurViPItem.getZfbRemark(), mCurViPItem.getName());
                String price = mCurViPItem.getCurPirce() + "";
                String dead_time = buildDeadTime(mCurViPItem.getVipDays());
                if (sPayCallback != null) {
                    sPayCallback.onPay(subject, price, dead_time, mCurViPItem, UserLoginDb.get(), this);
                } else {
                    //BaseUtils.popMyToast(getActivity(), "请初始化支付回调接口：" + IPayCallback.class.getName());
                    doPay(getActivity(), subject, price, dead_time, getContactInfo(mCurViPItem), UserLoginDb.getUserID());
                }
            } else {
                BaseUtils.popMyToast(getActivity(), "联系方式不能为空");
            }
        }else if(i == R.id.tv_buy_agreement){
            WebHtmlActivity.startWithURL(getActivity(), "购买协议", mViPConfigVO.getBuy_agreement_url());
        }
    }

    public class ViPAdapter extends BaseQuickAdapter<ViPItem, BaseViewHolder> {
        ViPAdapter(int layoutResId, List data) {
            super(layoutResId, data);
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void convert(final BaseViewHolder helper, final ViPItem item) {
            final int pos = getItemPosition(item);

            TextView vName = helper.getView(R.id.tv_vip_type_name);
            TextView vOldPrice = helper.getView(R.id.tv_vip_type_old_price);
            TextView vPrice = helper.getView(R.id.tv_vip_type_price);
            TextView vDesc = helper.getView(R.id.tv_vip_group_desc);
            RadioButton rb_vip_type_item = helper.getView(R.id.rb_vip_type_item);
            RadioButton rb_vip_type_item_press = helper.getView(R.id.rb_vip_type_item_press);

            View ll_vip_item = helper.getView(R.id.ll_vip_item);
            View group_desc_divider = helper.getView(R.id.group_desc_divider);

            final String moneyFlag = Html.fromHtml(" &yen ").toString(); //￥
            vName.setText(item.getName());

            vOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //添加删除线
            vOldPrice.setText(moneyFlag + item.getOldPrice());

            vPrice.setText(moneyFlag + item.getCurPirce());
            vDesc.setText(item.getDescription());

            ColorStateList color = getResources().getColorStateList(R.color.pay_color_vip_text);
            if(pos == mCurPos){
                color = getResources().getColorStateList(R.color.pay_color_vip_text_press);

                ll_vip_item.setBackgroundResource(R.drawable.xpay_pay_bg_vip_group_press);
                group_desc_divider.setBackgroundResource(R.drawable.xpay_pay_dash_line_press);

                tvPrice.setText(moneyFlag + item.getCurPirce());
                //rb_vip_type_item.setChecked(true);
                rb_vip_type_item.setVisibility(View.GONE);
                rb_vip_type_item_press.setVisibility(View.VISIBLE);
            }else{
                ll_vip_item.setBackgroundResource(R.drawable.xpay_pay_bg_vip_group);
                group_desc_divider.setBackgroundResource(R.drawable.xpay_pay_dash_line_normal);

                //rb_vip_type_item.setChecked(false);
                rb_vip_type_item.setVisibility(View.VISIBLE);
                rb_vip_type_item_press.setVisibility(View.GONE);
            }
            vName.setTextColor(color);
            vOldPrice.setTextColor(color);
            vPrice.setTextColor(color);
            vDesc.setTextColor(color);

            //View ll_vip_item_container = helper.getView(R.id.ll_vip_item_container"));
            ll_vip_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCurPos = pos;
                    mCurViPItem = item;
                    tvPrice.setText(moneyFlag + item.getCurPirce());
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Keep
    @BusUtils.Bus(tag = PayBusUtils.TAG_RECEIVE_PAY_MSG)
    public void receivePayMsg(PayBusUtils.Msg msg) {
        if(msg == null) return;
        postUIMessage(msg.what, msg.arg1, msg.arg2, msg.obj);
    }

    @Override
    public void processUIMessage(Message m) {
        super.processUIMessage(m);
        final Activity act = this;
        if (m.what == PayBusUtils.Type.MSG_START_NOTICE) {
            startNotice();
        }else if(m.what == PayBusUtils.Type.MSG_NET_FAILED){
            postUIMessage(PayBusUtils.Type.MSG_TIP_CLOSE);

            ToastUtils.showLong("网络异常，检查网络状态并重试！");
        }else if(m.what == PayBusUtils.Type.MSG_GET_ORDER_FAILED){
            postUIMessage(PayBusUtils.Type.MSG_TIP_CLOSE);
            
            errorOrderDlg(act);
        }else if(m.what == PayBusUtils.Type.MSG_GET_ORDER_PARSE_FAILED){
            postUIMessage(PayBusUtils.Type.MSG_TIP_CLOSE);

            errorOrderDlg(act);
        }else if(m.what == PayBusUtils.Type.MSG_PAY_SUCCESS){
            postUIMessage(PayBusUtils.Type.MSG_TIP_CLOSE);
            XMBEventUtils.send(XMBEventUtils.EventType.pay_success);

            //更新会员订单信息：
            OrderBeanV2.updateOrderBean();

            //支付成功，提示重启应用
            //paySuccessDlg(act);

            //支付成功，关闭购买会员界面，回到原界面
            paySuccessBackDlg(act);
            finish();
        }else if(m.what == PayBusUtils.Type.MSG_PAY_FAILED){
            postUIMessage(PayBusUtils.Type.MSG_TIP_CLOSE);

            final String cause = (String) m.obj; //失败原因
            checkOrderDlg(act, cause);
        }else if(m.what == PayBusUtils.Type.MSG_PAY_CANCEL){
            postUIMessage(PayBusUtils.Type.MSG_TIP_CLOSE);

            ToastUtils.showLong("支付取消！");
        }else if (m.what == PayBusUtils.Type.MSG_TIP_SHOW) {
            try {
                if(mProgressDialog == null){
                    mProgressDialog = new MyProgressDialog(act, "正在努力加载中...");
                }
                mProgressDialog.changeText(m.obj);
                if(!mProgressDialog.isShowing()) mProgressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (m.what == PayBusUtils.Type.MSG_TIP_CLOSE) {
            if(mProgressDialog != null) mProgressDialog.cancel();
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "processUIMessage-->"+m);
    }
    public void startNotice(){
        String notice = UserIDUtils.getViPNoticeV2(mViPConfigVO);
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "startNotice-->"+notice);
        tvNotice.setText(notice);
        uiHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startNotice();
            }
        }, mNoticeGapSecond*1000);
    }

    public void doZfbPay(final Activity act, String subject, String price, String dead_time, String contact, final String user_id) {
        if(act != null) {
            //调用支付宝支付：
            new ZhifubaoMainPay(Gid.getS(act, R.string.xmb_app_id), Gid.getS(act, R.string.ALIPAY_APP_ID),
                    act, null).newPay(getCombinedIDV2(), subject, subject, price, dead_time, contact, user_id);
        }else{
            ToastUtils.showLong("系统初始化异常，请检查网络后重启APP再试");
            LogUtils.eTag(TAG, "系统初始化异常，请检查网络后重启APP再试");
        }
    }
    public void doWxPay(final Activity act, String subject, String price, String dead_time, String contact, final String user_id) {
        if(act != null) {
            String weixin_appid = Vs.weixin_appid.value;
            String weixinpay_sys_key = Vs.weixinpay_sys_key.value;;

            //调用微信支付：
            new WeixinMainPay(Gid.getS(act, R.string.xmb_app_id), Gid.getS(act, R.string.ALIPAY_APP_ID),
                    act, null).newPay(getCombinedIDV2(), subject, subject, price, dead_time, contact, user_id,
                    weixin_appid, weixinpay_sys_key);
        }else{
            ToastUtils.showLong("系统初始化异常，请检查网络后重启APP再试");
            LogUtils.eTag(TAG, "系统初始化异常，请检查网络后重启APP再试");
        }
    }

    public void doPay(final Activity act, String subject, String price, String dead_time, String contact, final String user_id) {
        if(payMode == 0){
            doZfbPay(act, subject, price, dead_time, contact, user_id);
        }else{
            doWxPay(act, subject, price, dead_time, contact, user_id);
        }
    }

    /**
     * @param days 有效期，天
     * @return 2020-11-13 10:38:02
     */
    @SuppressLint("SimpleDateFormat")
    public String buildDeadTime(int days) {
        Date date = PayUtils.getCurNetDate();//取时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date); //需要将date数据转移到Calender对象中操作
        calendar.add(Calendar.DATE, days);//把日期往后增加n天.正数往后推,负数往前移动
        Date dead_date = calendar.getTime();   //这个时间就是日期往后推一天的结果
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(dead_date);
    }

    public static String getCombinedIDV2(){
        // 优先使用缓存中的cid，为空时才自动生成cid
        return StringUtils.getValue(ACacheUtils.getCacheCombinedID(),
                ACacheUtils.getCombinedID(StringUtils.getValue(Vs.xmb_app_id.value, Vs.app_id.value)));
    }
    public IPayCallback getDefPayCallback(final Activity act){
        if(act == null) return null;
        IPayCallback ic = new IPayCallback() {
            @Override
            public void onPay(String subject, String price, String dead_time, ViPItem item, UserLoginVO userLoginBean, Object obj) {
                String user_id = (userLoginBean != null) ? userLoginBean.getId()+"" : null;
                doPay(act, subject, price, dead_time, getContactInfo(item), user_id);
            }
        };
        return ic;
    }
    public String getContactInfo(ViPItem item){
        String contact = (item != null) ? item.getUserContact() : null;
        try {
            String curTel = MobileInfoUtil.getInstance(this).getDeviceCode();
            contact = contact + "/" + curTel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contact;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sPayCallback = null;

        //注销Bus事件：
        BusUtils.unregister(this);
    }

    public void checkOrderDlg(final Activity act, String cause){
        MyMessageDialog.popDialog(act, "检查订单",
                "检查订单异常，服务器开小差啦~如果你已经付款成功，重进APP看看VIP是否生效，" +
                        "如果未开通成功，请联系我们的客服。失败原因:" + ((StringUtils.isNullStr(cause)) ? "未知原因" : cause),
                "联系客服", "立即重进APP",
                new MyMessageDialog.DialogMethod() {
                    public void sure() {
                        BaseUtils.gotoFeedbackUI(act);
                    }
                    public void cancel() {
                        PayUtils.reStartLauncherUI(act);
                    }
                });
    }
    public void paySuccessDlg(final Activity act){
        PayDlgVO payDlgVO = new PayDlgVO(Gid.getS(act, R.string.pay_suc_dlg_title),
                Gid.getS(act, R.string.pay_suc_dlg_msg), Gid.getS(act, R.string.pay_suc_dlg_confirm));
        payDlgVO.setCancel(false);
        PayDlgActivity.start(act, payDlgVO, new IPayDlgDefCallback() {
            @Override
            public void sure() {
                PayUtils.reStartLauncherUI(act);
            }
        });
    }
    public void paySuccessBackDlg(final Activity act){
        PayDlgVO payDlgVO = new PayDlgVO(Gid.getS(act, R.string.pay_suc_dlg_back_title),
                Gid.getS(act, R.string.pay_suc_dlg_back_msg), Gid.getS(act, R.string.pay_suc_dlg_back));
        payDlgVO.setCancel(false);
        PayDlgActivity.start(act, payDlgVO, null);
    }
    public static void vipInfoDlg(final Activity act, OrderBeanV2 obj){
        PayDlgVO payDlgVO = new PayDlgVO("欢迎您，尊贵的VIP会员",
                "您当前可以尽情享受VIP专属特权，会员有效期至："+ TimeUtils.date2String(OrderBeanV2.getTotalDeadtime()),
                "返回上级");
        payDlgVO.setCancel(false);
        PayDlgActivity.start(act, payDlgVO);
    }
    public void errorTipDlg(final Activity act){
        MyMessageDialog.popDialog(act, "数据初始化失败", "请确认网络畅通后重启应用再试", "关闭");
    }
    public void errorOrderDlg(final Activity act) {
        MyMessageDialog.popDialog(act, "生成订单",
                "生成支付订单异常，请检查网络后再试或咨询客服",
                "咨询客服", "前往检查网络",
                new MyMessageDialog.DialogMethod() {
                    public void sure() {
                        BaseUtils.gotoFeedbackUI(act);
                    }
                    public void cancel() {
                        NetworkUtils.openWirelessSettings();
                    }
                });
    }


    //手动移除ViPConfigVO缓存
    public static void removeViPConfigVOCache(){
        try {
            ACacheUtils.removeObject(ViPConfigVO.Cache_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
