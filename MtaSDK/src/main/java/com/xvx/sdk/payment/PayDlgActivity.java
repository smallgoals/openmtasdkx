package com.xvx.sdk.payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.hacknife.immersive.Immersive;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.xvx.sdk.payment.vo.PayDlgVO;

/**
 * 开通会员返回的对话框界面<br>
*/
public class PayDlgActivity extends BaseHandlerACActivity implements View.OnClickListener {
    private final static String TAG = PayDlgActivity.class.getSimpleName();
    ImageView ivLogo;
    TextView tvTitle;
    TextView tvContent;
    TextView tvOK;
    TextView tvCancel;
    TextView tvNeutral;

    int logo_id = -1;   // 顶部图标的resID
    String title,content,ok,cancel,neutral; //标题、内容、确认、返回、中立
    boolean isCancel = true; //默认支持返回退出

    static IPayDlgCallback sPayCallback;

    public static void start(Activity act, PayDlgVO payDlgVO) {
        start(act, payDlgVO, null);
    }
    public static void start(Activity act, PayDlgVO payDlgVO, IPayDlgCallback payCallback) {
        Intent it = new Intent(act, PayDlgActivity.class);
        it.putExtra("paydlgvo", payDlgVO);
        sPayCallback = payCallback;
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //兼容低版本手机，不然会出现状态栏系统时间看不清：
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//23-6.0
            Immersive.setContentView(this, R.layout.xpay_activity_buy_vip_sucess,
                    R.color.xpayHeadRed, R.color.white,
                    false, false);
            //BarUtils.setStatusBarLightMode(this, true);
        }else {
            setContentView(R.layout.xpay_activity_buy_vip_sucess);
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        ivLogo = findViewById(R.id.iv_logo);
        tvTitle = findViewById(R.id.tv_title);
        tvContent = findViewById(R.id.tv_content);
        tvOK = findViewById(R.id.tv_ok);
        tvCancel = findViewById(R.id.tv_cancel);
        tvNeutral = findViewById(R.id.tv_neutral);

        PayDlgVO payDlgVO = (PayDlgVO) getIntent().getSerializableExtra("paydlgvo");
        if(payDlgVO != null) {
            logo_id = payDlgVO.getLogo_id();
            title = payDlgVO.getTitle();
            content = payDlgVO.getContent();
            ok = payDlgVO.getOk();
            cancel = payDlgVO.getCancel();
            neutral = payDlgVO.getNeutral();
            isCancel = payDlgVO.isCancel();
        }

        if(logo_id > 0) ivLogo.setImageResource(logo_id);
        setMyText(tvTitle, title);
        setMyText(tvContent, content);
        setMyText(tvOK, ok);
        setMyText(tvCancel, cancel);
        setMyText(tvNeutral, neutral);

        setMyVisibility(tvCancel, cancel);
        setMyVisibility(tvNeutral, neutral);

        tvOK.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvNeutral.setOnClickListener(this);

        if(isCancel){//true
            setDisplayHomeAsUpEnabled(true);
        }else{
            // 不支持返回取消
            setDisplayHomeAsUpEnabled(false);
            hideActionBar();
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onCreate is call");
    }

    public void setMyText(TextView tv, String value){
        if(StringUtils.noNullStr(value) && tv != null){
            tv.setText(value);
        }
    }
    public void setMyVisibility(TextView tv, String value){
        if(tv != null){
            tv.setVisibility((StringUtils.noNullStr(value)) ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(sPayCallback == null){
            finish();
        }else{
            if(v.getId() == R.id.tv_ok){
                sPayCallback.sure();
            }else if(v.getId() == R.id.tv_cancel){
                sPayCallback.cancel();
            }else if(v.getId() == R.id.tv_neutral){
                sPayCallback.neutral();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sPayCallback = null;
    }
    public void onBackPressed() {
        if(isCancel) super.onBackPressed();
    }
}
