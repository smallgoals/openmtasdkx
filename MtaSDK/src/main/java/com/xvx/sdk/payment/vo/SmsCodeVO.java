package com.xvx.sdk.payment.vo;

import androidx.annotation.Keep;

import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.StringUtils;
import com.xmb.mta.util.XMBGson;

import java.io.Serializable;

/**
 * 短信验证码实体类<br>
 */
@Keep
public class SmsCodeVO implements Serializable {
   private static final long serialVersionUID = -8426336092232248161L;

   String app_pkg;
   String user_tel;
   String code;
   String code_time;
   String sms_config_key;
   boolean is_real_name; //是否实名验证：true为实名

   public SmsCodeVO(){}
   public SmsCodeVO(String app_pkg, String user_tel, String code, String sms_config_key) {
      this.app_pkg = app_pkg;
      this.user_tel = user_tel;
      this.code = code;
      this.code_time = DateUtils.getCurDateByFormat(DateUtils.dateFormatYMDHMS);
      this.sms_config_key = sms_config_key;
   }

   @Override
   public String toString() {
      return "SmsCodeVO{" +
              "app_pkg='" + app_pkg + '\'' +
              ", user_tel='" + user_tel + '\'' +
              ", code='" + code + '\'' +
              ", code_time='" + code_time + '\'' +
              ", sms_config_key='" + sms_config_key + '\'' +
              ", is_real_name=" + is_real_name +
              '}';
   }

   public String getApp_pkg() {
      return app_pkg;
   }

   public void setApp_pkg(String app_pkg) {
      this.app_pkg = app_pkg;
   }

   public String getUser_tel() {
      return user_tel;
   }

   public void setUser_tel(String user_tel) {
      this.user_tel = user_tel;
   }

   public String getCode() {
      return code;
   }

   public void setCode(String code) {
      this.code = code;
   }

   public String getCode_time() {
      return code_time;
   }

   public void setCode_time(String code_time) {
      this.code_time = code_time;
   }

   public String getSms_config_key() {
      return sms_config_key;
   }

   public void setSms_config_key(String sms_config_key) {
      this.sms_config_key = sms_config_key;
   }

   public boolean isIs_real_name() {
      return is_real_name;
   }
   public void setIs_real_name(boolean is_real_name) {
      this.is_real_name = is_real_name;
   }

   //验证码合法性检测
   public static boolean isLegalCode(String tel, String code) {
      boolean isOK = false;
      try {
         SmsCodeVO src = (SmsCodeVO) ACacheUtils.getAppCacheObject(code);
         if (src != null && src.is_real_name) {
            //进行手机号、验证码同时匹配：
            isOK = StringUtils.equalsIgnoreCase(src.getUser_tel(), tel)
                    && StringUtils.equalsIgnoreCase(src.getCode(), code);
         }
      }catch (Exception e){
         e.printStackTrace();
      }
      return isOK;
   }

   //获取上报的数据：
   public static String getRemark(String code){
      String s = null;
      try {
         SmsCodeVO src = (SmsCodeVO) ACacheUtils.getAppCacheObject(code);
         if(src != null) {
            src.setUser_tel(null);  //移除敏感的手机号
            s = XMBGson.getGson().toJson(src);
         }
      }catch (Exception e){
         e.printStackTrace();
      }
      return s;
   }

   //服务收到验证码发送成功回调时，设置此手机和验证码的标识为已实名：
   public static void updateRemark(String code, boolean is_real_name){
      if(StringUtils.noNullStr(code)){
         try {
            SmsCodeVO sc = (SmsCodeVO) ACacheUtils.getAppCacheObject(code);
            if (sc != null) {
               sc.setIs_real_name(is_real_name); //更新配置实名状态
               ACacheUtils.setAppCacheObject(code, sc);

               setLastSendTime();  //记录最近发送验证码成功的时间
            }
         }catch (Exception e){
            e.printStackTrace();
         }
      }
   }

   public static void setLastSendTime(){
      ACacheUtils.setAppFileCacheValue("code_last_send_ok_time_key", System.currentTimeMillis()+"");
   }
   public static String getLastSendTime(){
      return ACacheUtils.getAppFileCacheValue("code_last_send_ok_time_key");
   }

   /**
    * 判断是否发送频繁<br>
    */
   public static boolean isSendFrequently(){
      boolean isOK = false;
      try {
         String preTime = getLastSendTime();
         if (StringUtils.noNullStr(preTime)) {
            long pTime = Long.parseLong(preTime);
            long cTime = System.currentTimeMillis();
            isOK = (cTime - pTime) < 60*1000;   //小于60秒，表示获取频繁
         }
      }catch (Exception e){
         e.printStackTrace();
      }
      return isOK;
   }
}
