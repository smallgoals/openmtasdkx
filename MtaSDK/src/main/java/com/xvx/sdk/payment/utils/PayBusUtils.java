package com.xvx.sdk.payment.utils;

import com.blankj.utilcode.util.BusUtils;
import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.utils.Spu;

import java.io.Serializable;

import androidx.annotation.Keep;

/**
 * 支付Bus工具类<br>
 */
@Keep
public class PayBusUtils {
    private final static String TAG = PayBusUtils.class.getSimpleName();

    //接收支付回调事件：
    public static final String TAG_RECEIVE_PAY_MSG = "tag_receive_pay_msg";

    @Keep
    public static class Msg implements Serializable {
        private static final long serialVersionUID = 4586101451787739573L;
        public int arg1;
        public int arg2;
        public Object obj;
        public int what;
        public Msg(){}
        public Msg(int what, Object obj){
            this();
            this.what = what;
            this.obj = obj;
        }
        public Msg(int what, Object obj, int arg1, int arg2){
            this(what, obj);
            this.arg1 = arg1;
            this.arg2 = arg2;
        }
    }

    /**
     * 消息类型<br>
     */
    public static class Type{
        public static final int MSG_START_NOTICE = 1024; //显示用户支付成功消息
        public static final int MSG_STOP_NOTICE = 1025; //关闭用户支付成功消息

        public static final int MSG_GET_ORDER_SUCCESS = 1101; //获取订单成功
        public static final int MSG_GET_ORDER_FAILED = 1102; //获取订单异常

        public static final int MSG_GET_ORDER_PARSE_SUCCESS = 1201; //获取订单Json解析成功
        public static final int MSG_GET_ORDER_PARSE_FAILED = 1202; //获取订单Json解析失败

        public static final int MSG_VERIFY_SIGN_SUCCESS = 1301; //验证签名成功
        public static final int MSG_VERIFY_SIGN_FAILED = 1302; //验证签名失败

        public static final int MSG_PAY_SUCCESS = 1401; //支付成功
        public static final int MSG_PAY_FAILED = 1402; //支付失败
        public static final int MSG_PAY_CANCEL = 1403; //支付取消

        public static final int MSG_TIP_SHOW = 2001; //提示框显示
        public static final int MSG_TIP_CLOSE = 2002; //提示框关闭
        public static final int MSG_NET_FAILED = 2044; //网络异常：手机断网或服务器无法访问
    }

    public static void post(int what, Object obj) {
        post(TAG_RECEIVE_PAY_MSG, what, obj);
    }
    public static void post(String tag, int what, Object obj) {
        BusUtils.post(tag, new Msg(what, obj));
        if (Spu.isSucK(TAG)) LogUtils.iTag(TAG, "发送普通事件-->tag=" + tag + ",what=" + what + ",obj=" + obj);
    }
    public static void postSticky(int what, Object obj) {
        postSticky(TAG_RECEIVE_PAY_MSG, what, obj);
    }
    public static void postSticky(String tag, int what, Object obj) {
        BusUtils.postSticky(tag, new Msg(what, obj));
        if (Spu.isSucK(TAG)) LogUtils.iTag(TAG, "发送粘性事件-->tag=" + tag + ",what=" + what + ",obj=" + obj);
    }
}
