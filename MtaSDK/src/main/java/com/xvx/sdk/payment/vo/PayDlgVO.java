package com.xvx.sdk.payment.vo;

import androidx.annotation.Keep;

import java.io.Serializable;

@Keep
public class PayDlgVO implements Serializable {
    private static final long serialVersionUID = 665616902472117393L;
    int logo_id = -1;   // 顶部图标的resID
    String title,content,ok,cancel,neutral; //标题、内容、确认、返回、中立
    boolean isCancel = true; //默认支持返回退出

    public PayDlgVO(){}

    public PayDlgVO(String title, String content, String ok) {
        this();
        this.title = title;
        this.content = content;
        this.ok = ok;
    }
    public PayDlgVO(String title, String content, String ok, String cancel) {
        this(title, content, ok);
        this.cancel = cancel;
    }
    public PayDlgVO(String title, String content, String ok, String cancel, String neutral) {
        this(title, content, ok, cancel);
        this.neutral = neutral;
    }
    public PayDlgVO(int logo_id, String title, String content, String ok, String cancel, String neutral) {
        this(title, content, ok, cancel, neutral);
        this.logo_id = logo_id;
    }

    @Override
    public String toString() {
        return "PayDlgVO{" +
                "logo_id=" + logo_id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", ok='" + ok + '\'' +
                ", cancel='" + cancel + '\'' +
                ", neutral='" + neutral + '\'' +
                ", isCancel=" + isCancel +
                '}';
    }

    public int getLogo_id() {
        return logo_id;
    }

    public void setLogo_id(int logo_id) {
        this.logo_id = logo_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getNeutral() {
        return neutral;
    }

    public void setNeutral(String neutral) {
        this.neutral = neutral;
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }
}
