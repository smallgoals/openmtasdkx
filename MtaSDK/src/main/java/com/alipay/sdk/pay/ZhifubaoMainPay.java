package com.alipay.sdk.pay;

import android.app.Activity;
import android.text.TextUtils;

import com.alipay.sdk.app.PayTask;
import com.blankj.utilcode.util.LogUtils;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyProgressDialog;
import com.nil.sdk.utils.Spu;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.XMBOkHttp;
import com.xmb.mta.util.XMBSign;
import com.xvx.sdk.payment.utils.PayBusUtils;
import com.xvx.sdk.payment.utils.Urls;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 支付宝支付入口<br>
 */
public class ZhifubaoMainPay {
    public static final String TAG = ZhifubaoMainPay.class.getSimpleName();
    private Activity act;
    private PayListener lsn;

    //付费标签:“BSQ”表示是变声器类的付费，“VEdit”表示是视频编辑神器类的
    private String pay4whatTag;
    //服务器pay_app_id_list.json文件里的ID，分别对应不同的支付宝媒体，如：82是BZ的手机变声器，1是XMB的手机万能变声器
    private String alipayAppID;

    private MyProgressDialog mProgressDialog;
    /**
     * @param pay4whatTag 付费标签:“BSQ”表示是变声器类的付费，“VEdit”表示是视频编辑神器类的
     * @param alipayAppID 服务器pay_app_id_list.json文件里的ID，分别对应不同的支付宝媒体，如：82是BZ的手机变声器，1是XMB的手机万能变声器
     * @param act
     * @param lsn
     */
    public ZhifubaoMainPay(String pay4whatTag, String alipayAppID, Activity act, PayListener lsn) {
        this.act = act;
        this.lsn = lsn;
        this.pay4whatTag = pay4whatTag;
        this.alipayAppID = alipayAppID;
    }

    /**
     * @param combined_id Android手机唯一串号
     * @param subject     显示给用户看的支付文字， 要官方一点哟
     * @param body        我们后台自己看的，可以用本字段做一些拓展，如：标志用户是开了VIP1还是VIP2还是SVIP等等
     * @param price       价格，如："10.9"
     * @param dead_time   VIP订单失效时间:2020-11-13 10:38:02
     */
    public void newPay(String combined_id, String
            subject, String body, String price, String dead_time) {
        newPay(combined_id,subject,body,price,dead_time,null);
    }
    public void newPay(String combined_id, String
            subject, String body, String price, String dead_time, String contact) {
        newPay(combined_id,subject,body,price,dead_time,contact,null);
    }
    public void newPay(String combined_id, String
            subject, String body, String price, String dead_time, String contact, String user_id) {
        //【1】.先从服务器生成订单：
        String url = Urls.URL_BUILD_ALIAY_ORDER + "?data=";
        HashMap<String, String> map = new HashMap<>();
        map.put("title", subject);
        map.put("pay_for_what", pay4whatTag);
        map.put("pay_for_what_des", body);
        map.put("money", price);
        map.put("id", alipayAppID + "");//服务器pay_app_id_list.json文件里的ID，分别对应不同的支付宝媒体
        map.put("pay_count", "1");
        map.put("dead_time", dead_time);
        map.put("combined_id", combined_id);
        map.put("diy1", contact);
        map.put("user_id", user_id);

        PayBusUtils.post(PayBusUtils.Type.MSG_TIP_SHOW, "开始生成支付订单...");
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "0.开始生成支付订单-->"+map);
        url += XMBSign.buildUrlData(map);
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "1.服务器生成订单:开始请求链接-->"+url);
        XMBOkHttp.doGet(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                PayBusUtils.post(PayBusUtils.Type.MSG_NET_FAILED, e.toString());
            }
            public void onResponse(Call call, final Response response) throws IOException {
                //【2】.拿到订单信息后，去支付：
                ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                final String orderInfo = (resultBean == null)? null: resultBean.getResult_data();

                CrashApp.networkThread(new Runnable() {
                    @Override
                    public void run() {
                        if(orderInfo == null) {
                            PayBusUtils.post(PayBusUtils.Type.MSG_GET_ORDER_FAILED, null);
                            return;
                        }

                        PayBusUtils.post(PayBusUtils.Type.MSG_TIP_SHOW, "开始启动支付宝支付...");
                        PayTask alipay = new PayTask(act);
                        final Map<String, String> result = alipay.payV2(orderInfo, true);
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "2.手机支付返回信息-->"+result);
                        PayBusUtils.post(PayBusUtils.Type.MSG_TIP_CLOSE, null);

                        //【3】.为了确认是真实的支付成功，需要去验签（调用了以下API验证后，服务器数据库才会标记付款成功。一般情况下调用即可，不用管调用返回的结果）：
                        PayResult payResult = new PayResult(result);
                        String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                        String resultStatus = payResult.getResultStatus();
                        // 判断resultStatus 为9000则代表支付成功
                        if (TextUtils.equals(resultStatus, "9000")) {
                            PayBusUtils.post(PayBusUtils.Type.MSG_TIP_SHOW, "开始同步验签...");

                            String url = Urls.URL_QUERY_ALIPAY_ORDER + "?data=";
                            // 组装一个alipay_response的键值对：
                            Map<String, String> map = new HashMap<>();
                            map.put("alipay_response", result.get("result").toString());
                            url += XMBSign.buildUrlData(map);
                            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "3.0.同步验签:开始请求链接-->"+url);
                            XMBOkHttp.doGet(url, new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, "3.1.同步验签:手机支付成功-网页请求失败-->"+e.toString());
                                    PayBusUtils.post(PayBusUtils.Type.MSG_PAY_FAILED, result);
                                }
                                public void onResponse(Call call, Response response) throws IOException {
                                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                                    if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, "3.1.同步验签:手机支付成功-网页请求成功-->"+resultBean);
                                    if(resultBean != null && resultBean.getResult_code() == ResultBean.CODE_SUC){
                                        //更新订单信息：
                                        OrderBeanV2.addOrderBeans(resultBean.getResult_data());

                                        //支付成功，把服务器返回的订单列表发送出去
                                        BaseUtils.popDebugToast("支付宝同步验签成功");
                                        PayBusUtils.post(PayBusUtils.Type.MSG_PAY_SUCCESS, resultBean.getResult_data());
                                    }else {
                                        BaseUtils.popDebugToast("支付宝同步验签失败");
                                        PayBusUtils.post(PayBusUtils.Type.MSG_PAY_FAILED, (resultBean == null) ? null: resultBean.getResult_data());
                                    }
                                }
                            });
                        } else {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "3.1.支付取消-->"+result);
                            PayBusUtils.post(PayBusUtils.Type.MSG_PAY_CANCEL, result);
                        }
                    }
                });
            }
        });
    }

    public interface PayListener {
        /**
         * 已经处于UI线程，可以做View的操作
         *
         * @param payResultState
         * @param payResult
         */
        void onPayResult(int payResultState, PayResult payResult);
    }
}
