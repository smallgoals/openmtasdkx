package com.xmb.mta.vo;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

import androidx.annotation.Keep;

/**
 * 在线参数实体类<br>
 */
@Keep
public class OnlineConfigBean implements Serializable {
	private static final long serialVersionUID = 6280722233351980981L;

	@Expose
	private int id;// 自增ID
	private int app_id;// 属于哪个User的APP
	// @Column
	// private String xmb_key;// 用户可自己设置，建议使用Umeng的AppKey
	private String config_key;// 参数名称
	private String config_value;// 参数值
	private Date config_create_time;// 创建时间
	private String remark;

	@Override
	public String toString() {
		//return String.format("%s=%s time=%s remark=%s\n",config_key,config_value,config_create_time,remark);
		return String.format("%s=%s\n",config_key,config_value);
	}

	public Date getConfig_create_time() {
		return config_create_time;
	}

	public void setConfig_create_time(Date config_create_time) {
		this.config_create_time = config_create_time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getApp_id() {
		return app_id;
	}

	public void setApp_id(int app_id) {
		this.app_id = app_id;
	}

	public String getConfig_key() {
		return config_key;
	}

	public void setConfig_key(String config_key) {
		this.config_key = config_key;
	}

	public String getConfig_value() {
		return config_value;
	}

	public void setConfig_value(String config_value) {
		this.config_value = config_value;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
