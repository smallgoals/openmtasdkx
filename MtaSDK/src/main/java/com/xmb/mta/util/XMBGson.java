package com.xmb.mta.util;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class XMBGson {

    public static Gson getGson() {
        //服务器日期格式：May 11, 2022 5:38:32 PM
        return new GsonBuilder().setDateFormat("MMM dd, yyyy hh:mm:ss a").create();
    }

    /**
     * 只导出@Expose的字段
     *
     * @return
     */
    public static Gson getGsonUseAnnotation() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson;
    }

}
