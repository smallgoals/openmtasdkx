package com.xmb.mta.util;

import android.content.Context;
import android.os.Build;

import com.android.core.XSEUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.MobileInfoUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.AdSwitchUtils.Vs;
import com.nil.vvv.utils.Mtas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class XMBSign {
    private final static String TAG = XMBSign.class.getSimpleName();

    // 数据解析（解密、序列化）
    public static ResultBean deSignByResult4Obj(Response response) throws IOException {
        return (ResultBean) deSign2Obj(response, ResultBean.class);
    }
    public static Object deSign2Obj(Response response, Class<?> clazz) throws IOException {
        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, response.toString());
        Object resultBean = null;
        try {
            String data = deSign(response.body().string());
            resultBean = XMBGson.getGson().fromJson(data, clazz);
        }catch (Exception e){
            e.printStackTrace();
            if(ResultBean.class.isAssignableFrom(clazz)){
                resultBean = new ResultBean(ResultBean.CODE_FAIL, e.toString());
            }
        }
        return resultBean;
    }

    public static <T> ArrayList<T> deSignByResult4List(Class<T> clazz, Response response) throws IOException {
        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, response.toString());
        String data = null;
        try {
            assert response.body() != null;
            data = deSign(response.body().string());
        }catch (Exception e){
            e.printStackTrace();
        }
        return deSignByResult4List(clazz, data);
    }
    public static <T> ArrayList<T> deSignByResult4List(Class<T> clazz, String result) {
        ArrayList<T> list = null;
        try {
            ResultBean resultBean = XMBGson.getGson().fromJson(result, ResultBean.class);
            String s = resultBean.getResult_data();

            list = new ArrayList<T>();
            JsonArray array = new JsonParser().parse(s).getAsJsonArray();
            for (JsonElement elem : array) {
                list.add(new Gson().fromJson(elem, clazz));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    public static String deSign(String str) {
        String result = null;
        if(StringUtils.noNullStr(str)){
            result = XSEUtils.mtaDecode(str);
            if(Spu.isSucK(TAG)) LogUtils.wTag(TAG+"deSign", str+"\n"+result);
        }
        return result;
    }

    // 数据封装（序列化、加密）
    public static String enSign(String str) {
        String result = null;
        if(StringUtils.noNullStr(str)){
            result = XSEUtils.mtaEncode(str);
            if(Spu.isSucK(TAG)) LogUtils.wTag(TAG+"enSign", str+"\n"+result);
        }
        return result;
    }

    public static String buildUrlData(Map<String, String> map){
        map = addDataTrail(map);
        return XMBSign.enSign(XMBGson.getGson().toJson(map));
    }
    public enum Cjs {
        so_channel,app_channel,app_pkg,app_name,app_version,app_build_time,app_sign,
        device_version,device_imei,device_mac,device_manufacturer,
        //app_id=app_type：存储应用类型：BSQ、Vedit、LOVE
        app_type,app_id,
        type,diy1,diy2,diy3,diy4,diy5,
        combined_id,size,page,
        c1,c2,c3,c4,c5,
        app_key,
        //恋爱话术、字典&成语搜索接口专用
        cid,pkg,
        //cache后面的值来自sd|app的缓存值（这样方便跨SDK存取）
        cache,token,user_id,login_state,
        xmb_imei,xmb_uuid,
        //首次启动时间、刚买会员时间（存储System.currentTimeMillis）
        first_boot_time,bug_vip_time,
        end;
        public String v;
    }

    public static Map<String, String> addDataTrail(Map<String, String> map){
        if(map == null){
            map = new HashMap<>();
        }

        Context ctx = Utils.getApp();
        //代理商名称：
        Cjs.so_channel.v = Vs.agency_key.value;
        Cjs.app_channel.v = AppUtils.getMetaChannel(ctx);
        Cjs.app_pkg.v = AppUtils.getPackageName(ctx);
        Cjs.pkg.v = AppUtils.getPackageName(ctx);
        Cjs.app_name.v = AppUtils.getAppName(ctx);
        Cjs.app_version.v = AppUtils.getVersionName(ctx);

        try {
            Cjs.device_version.v = Build.VERSION.RELEASE;
        }catch (Exception e){e.printStackTrace();}

        Cjs.device_imei.v = ACacheUtils.getMyIMEI()/*MobileInfoUtil.getInstance(ctx).getDeviceIMEI()*/;

        Cjs.device_manufacturer.v = MobileInfoUtil.getInstance(ctx).getDeviceModelName();
        Cjs.app_build_time.v = AppUtils.getAppBuildTime(ctx);
        Cjs.app_sign.v = AppUtils.getSign(ctx, Cjs.app_pkg.v);
        Cjs.device_mac.v = MobileInfoUtil.getInstance(ctx).getMacAddress();

        try{
            //xmb_app_id优先使用，为空时取app_id
            Cjs.app_type.v = StringUtils.getValue(Vs.xmb_app_id.value, Vs.app_id.value);
            Cjs.app_id.v = Cjs.app_type.v;  //存储应用类型：BSQ、Vedit、LOVE
            String tel = MobileInfoUtil.getInstance(ctx).getDeviceCode();
            //记录CombinedID、Series、Code
            Cjs.diy3.v = ACacheUtils.getCode()+"##TEL:"+tel+";";
            Cjs.diy4.v = AdSwitchUtils.getInstance(ctx).getVersionValueInfo();
            //mta的版本+app版本+手机版本
            Cjs.diy5.v = "mta:"+Mtas.Version+"|"+ Vs.gap_days.value
                    +"##app:"+AppUtils.getVersionName(ctx)+"|"+AppUtils.getVersionCode(ctx)
                    +"##sj:"+MobileInfoUtil.getInstance(ctx).getDeviceVersionName()+"|"+Build.HARDWARE+" "+Build.CPU_ABI;
        }catch (Exception e){e.printStackTrace();}

        // 新增的Cid
        Cjs.combined_id.v = ACacheUtils.getCombinedID(Cjs.app_type.v);
        Cjs.cid.v = ACacheUtils.getCombinedID(Cjs.app_type.v);
        Cjs.size.v = 10+""; //默认消息条数：10
        Cjs.page.v = 0+""; //默认页数：0

        Cjs.app_key.v = XmbOnlineConfigAgent.getAppKey();
        Cjs.user_id.v = ACacheUtils.getCacheValue(Cjs.user_id.name());

        // 循环进行赋值操作：
        for(Cjs cc : Cjs.values()){
            //cache后面的参数都来自sd|app的缓存值（这样方便跨SDK存取）：
            if(cc.ordinal() >= Cjs.cache.ordinal()){
                cc.v = ACacheUtils.getCacheValue(cc.name());
            }

            // 当前赋值不为空，且MAP里面的元素没被之前赋过值
            if(StringUtils.noNullStr(cc.v)
                    && StringUtils.isNullStr(map.get(cc.name()))){
                map.put(cc.name(), cc.v);
            }
        }
        return map;
    }
}
