package com.xmb.mta.util;

import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

public class Urls {
    //是否测试模式：
    public static final boolean isTest = false;
    //服务器地址：
    public static String URL_TEST = isTest ? "http://192.168.31.61:8080" : StringUtils.getValue(AdSwitchUtils.Vs.mta_url.value, "https://mtaapi.com");
    public static String URL_BASE_WEB = "http://api.gupiaoxianji.com/";
    public static String URL_BASE_WEB_RESERVE = "http://api.newxld.com/";

    //MTA:
    //public static final String URL_MTA_HEAD = "http://192.168.31.50:8080/mta";    //本地测试地址
    public static final String URL_MTA_HEAD = URL_TEST + "/mta";
    public static final String URL_MTA_FEEDBACK = URL_MTA_HEAD + "/feedback";
    public static final String URL_MTA_EVENT = URL_MTA_HEAD + "/event";

    // 查询反馈列表：
    public static final String URL_MTA_QUERY_FEEDBACK_NEWS = URL_MTA_HEAD + "/query_feedback_by_user";

    // 在线参数列表：
    public static final String URL_MTA_QUERY_CONFIG_BY_APP_KEY = URL_MTA_HEAD + "/query_config_by_app_key";
}
