package com.xmb.mta.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.nil.sdk.nb.utils.NbUriUtils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressLint("MissingPermission")
public class XMBOkHttp {
    private final static String TAG = XMBOkHttp.class.getSimpleName();
    private static OkHttpClient okHttpClient = null;

    //查询一般采用get方式，增删改采用post方式：
    public final static String REQ_MODE_GET = "get";
    public final static String REQ_MODE_POST = "post";
    public final static String REQ_MODE_DEF = REQ_MODE_GET;

    private XMBOkHttp() {
    }

    public static OkHttpClient getInstance() {
        if (okHttpClient == null) {
            try {
                //加同步安全
                synchronized (XMBOkHttp.class) {
                    if (okHttpClient == null) {
                        int cacheSize = 10 * 1024 * 1024;
                        OkHttpClient.Builder builder = new OkHttpClient.Builder();
                        SSLSocketFactoryCompat.trustSSLCertificate(builder); //信任所有证书
                        builder.connectTimeout(15, TimeUnit.SECONDS);
                        builder.writeTimeout(20, TimeUnit.SECONDS).readTimeout(20, TimeUnit.SECONDS);
                        builder.addNetworkInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request();
                                Response response = chain.proceed(request);
                                if (NetworkUtils.isConnected()) {
                                    int maxAge = 0;                     // 有网络时 设置缓存超时时间0个小时
                                    response.newBuilder()
                                            .header("Cache-Control", "public, max-age=" +
                                                    maxAge)
                                            .removeHeader("Pragma")     // 清除头信息
                                            .build();
                                } else {
                                    int maxStale = 60 * 60 * 24;        // 无网络时，设置超时为1天
                                    response.newBuilder()
                                            .header("Cache-Control", "public, only-if-cached," +
                                                    " max-stale=" + maxStale)
                                            .removeHeader("Pragma")
                                            .build();
                                }
                                return response;
                            }
                        });
                        okHttpClient = builder.build();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return okHttpClient;
    }

    /**
     * 【请求不到，返回失败】
     * 如果没有缓存，进行get请求获取服务器数
     * 据并缓存起来
     * 如果缓存已经存在：不超过maxAge---->不进行请求,直接返回缓存数据
     * 超出了maxAge--->返回失败
     */
    public static void doGetAndCacheInAgeTime(String url, int cache_maxAge_inSeconds,
                                              Callback callback) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建Request
        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().maxAge(cache_maxAge_inSeconds,
                        TimeUnit.SECONDS)
                        .build())
                .url(url).build();
        //得到Call对象
        Call call = okHttpClient.newCall(request);
        //执行异步请求
        call.enqueue(callback);
    }

    /**
     * 【请求不到，返回缓存】
     * 如果没有缓存，进行get请求获取服务器数据并缓存起来
     * 如果缓存已经存在：不超过maxStale---->不进行请求,直接返回缓存数据
     * 超出了maxStale--->可以使用过期缓存
     */
    public static void doGetAndCacheInStaleTime(String url, int cache_maxStale_inSeconds,
                                                Callback callback) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建Request
        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().maxStale(cache_maxStale_inSeconds,
                        TimeUnit.SECONDS)
                        .build())
                .url(url).build();
        //得到Call对象
        Call call = okHttpClient.newCall(request);
        //执行异步请求
        call.enqueue(callback);
    }

    /**
     * get请求
     * 参数1 url
     * 参数2 回调Callback
     */
    public static void doGet(String url, Callback callback) {
        try {
            String _url = url;
            if(BaseUtils.getIsDebug()){
                String dUrl = ACacheUtils.getAppFileCacheValue("debug_xmb_server_base_url");
                if(StringUtils.noNullStr(dUrl)){
                    //替换为本地配置的Url
                    _url = _url.replace(Urls.URL_TEST, dUrl);
                }
            }

            //创建OkHttpClient请求对象
            OkHttpClient okHttpClient = getInstance();
            //创建Request
            Request request = new Request.Builder().url(_url).build();
            //得到Call对象
            Call call = okHttpClient.newCall(request);
            //执行异步请求
            call.enqueue(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * post请求
     * 参数1 url
     * 参数2 回调Callback
     */
    public static void doPost(String url, Map<String, String> params, Callback callback) {
        try {
            String _url = url;
            if(BaseUtils.getIsDebug()){
                String dUrl = ACacheUtils.getAppFileCacheValue("debug_xmb_server_base_url");
                if(StringUtils.noNullStr(dUrl)){
                    //替换为本地配置的Url
                    _url = _url.replace(Urls.URL_TEST, dUrl);
                }
            }

            //创建OkHttpClient请求对象
            OkHttpClient okHttpClient = getInstance();

            //3.x版本post请求换成FormBody 封装键值对参数
            FormBody.Builder builder = new FormBody.Builder();
            //遍历集合
            for (String key : params.keySet()) {
                builder.add(key, params.get(key));
            }
            //创建Request
            Request request = new Request.Builder().url(_url).post(builder.build()).build();
            Call call = okHttpClient.newCall(request);
            call.enqueue(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * post请求上传文件
     * 参数1 url
     * 参数2 回调Callback
     */
    public static void uploadPic(String url, File file, String fileName) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = getInstance();
        //创建RequestBody 封装file参数
        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"),
                file);
        //创建RequestBody 设置类型等
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", fileName, fileBody).build();
        //创建Request
        Request request = new Request.Builder().url(url).post(requestBody).build();

        //得到Call
        Call call = okHttpClient.newCall(request);
        //执行请求
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //上传成功回调 目前不需要处理
            }
        });

    }

    /**
     * Post请求发送JSON数据
     * 参数一：请求Url
     * 参数二：请求的JSON
     * 参数三：请求回调
     */
    public static void doPostJson(String url, String jsonParams, Callback callback) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; " +
                "charset=utf-8"), jsonParams);
        Request request = new Request.Builder().url(url).post(requestBody).build();
        Call call = getInstance().newCall(request);
        call.enqueue(callback);
    }

    /**
     * 下载文件 以流的形式把apk写入的指定文件 得到file后进行安装
     * 参数一：请求Url
     * 参数二：保存文件的路径名
     * 参数三：保存文件的文件名
     */
    public static void download(final Activity context, final String url, final String saveDir) {
        Request request = new Request.Builder().url(url).build();
        Call call = getInstance().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                try {
                    is = response.body().byteStream();
                    //apk保存路径
                    final String fileDir = isExistDir(saveDir);
                    //文件
                    File file = new File(fileDir, getNameFromUrl(url));
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "下载成功:" + fileDir + "," + getNameFromUrl(url)
                                    , Toast.LENGTH_SHORT).show();
                        }
                    });
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }

                    fos.flush();
                    //apk下载完成后 调用系统的安装方法
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(NbUriUtils.fromFile(file, true), "application/vnd.android" +
                            ".package-archive");
                    BaseUtils.startActivity(context, intent);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (is != null) is.close();
                    if (fos != null) fos.close();
                }
            }
        });
    }

    /**
     * @param saveDir
     * @return
     * @throws IOException 判断下载目录是否存在
     */
    public static String isExistDir(String saveDir) throws IOException {
        // 下载位置
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File downloadFile = new File(Environment.getExternalStorageDirectory(), saveDir);
            if (!downloadFile.mkdirs()) {
                downloadFile.createNewFile();
            }
            String savePath = downloadFile.getAbsolutePath();
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "savePath-->"+savePath);
            return savePath;
        }
        return null;
    }

    /**
     * @param url
     * @return 从下载连接中解析出文件名
     */
    private static String getNameFromUrl(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }
}
