package com.xmb.mta.util;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACache;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.Gid;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.Mtas;
import com.xmb.mta.vo.OnlineConfigBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 在线参数代理类，含【手动更新在线参数值】、【获取指定Key的在线参数值】<br>
 */
public class XmbOnlineConfigAgent {
    private static final String TAG = XmbOnlineConfigAgent.class.getSimpleName();
    private final static String CACHE_KEY = "xmb_online_config";    //默认的缓存Key
    public final static String CACHE_KEY_MAP_END = "_map";    //默认的缓存Key
    public final static String PRE_CACHE_KEY = "pre_xmb_online_config";    //备用的缓存Key（成功请求后的数据）
    public final static String LAST_UPDATE_KEY = "xmb_last_update";    //最后更新时间（单位秒）
    public final static String LAST_UPDATE_ERROR_KEY = "xmb_last_update_error";    //最后更新时间（单位秒）
    public final static String APP_KEY_KEY = "xmb_app_key";    //存储App_key的值

    public final static String CODE_APP_KEY_ERROR = "40001";
    public final static String CODE_UNKNOW_ERROR = "40002";
    public final static int XMB_ERROR_CACHE_TIME = ACache.TIME_MINUTE * 3;  //请求错误时缓存3分钟

    /**
     * 将在线参数的对象数组转换为Map对象<br>
     */
    public static HashMap<String, String> ary2Map(){
        return ary2Map(getAppKey());
    }
    public static HashMap<String, String> ary2Map(String app_key){
//        return ary2Map(getCacheDatasByAppKey(app_key));
        HashMap<String, String> maps = new HashMap<>();
        try {
            String mapKey = getCacheMapKey(app_key);
            maps = (HashMap<String, String>) ACacheUtils.getAppCacheObject(mapKey);
            if(maps == null){
                maps = ary2Map(getCacheDatasByAppKey(app_key));
                // 缓存Map对象，避免多次转换
                ACacheUtils.setAppCacheObject(mapKey, maps);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "ary2Map.e="+StringUtils.getExcetionInfo(e));
        }
        return maps;
    }
    public static HashMap<String, String> ary2Map(List<OnlineConfigBean> ary){
        HashMap<String, String> map = new HashMap<>();
        /*if(ary == null){
            // 缓存数据失效时，使用备用的缓存数据
            ary = getPreCacheDatas();
        }*/

        if(ary != null){
            for(OnlineConfigBean oc : ary){
                if(oc == null || StringUtils.isNullStr(oc.getConfig_key())) continue;
                String key = oc.getConfig_key();
                String value = oc.getConfig_value();
                map.put(key, value);
            }
        }
        return map;
    }

    /**
     * 获取在线参数的缓存数据（未同意隐私政策前，获取的仅为xmb在线参数）<br>
     */
    public static ArrayList<OnlineConfigBean> getCacheDatas(){
        return getCacheDatas(getCacheKey());
    }
    public static ArrayList<OnlineConfigBean> getCacheDatas(String cacheKey){
        //LogUtils.eTag(TAG, "cacheKey.read="+cacheKey);
        ArrayList<OnlineConfigBean> data = null;
        try {
            data = (ArrayList<OnlineConfigBean>) ACacheUtils.getAppCacheObject(cacheKey);
            //data = (ArrayList<OnlineConfigBean>) ACacheUtils.getCacheObject(cacheKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig.data="+data);
        return data;
    }
    public static ArrayList<OnlineConfigBean> getCacheDatasByAppKey(String appKey){
        return getCacheDatas(getCacheKey(appKey));
    }

    /**
     * 备用的缓存Key（成功请求后的数据），防止定时缓存失效时数据丢失的问题<br>
     */
    public static ArrayList<OnlineConfigBean> getPreCacheDatas(){
        ArrayList<OnlineConfigBean> data = null;
        try {
            data = (ArrayList<OnlineConfigBean>) ACacheUtils.getCacheObject(PRE_CACHE_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig.data="+data);
        return data;
    }

    /**
     * 通过app_key生成MD5，作为在线参数的缓存Key<br>
     */
    public static String getCacheKey(){
        return getCacheKey(getAppKey());
    }
    public static String getCacheKey(String app_key){
        if(StringUtils.isNullStr(app_key)){
            return EncryptUtils.encryptMD5ToString(CACHE_KEY);
        }
        return EncryptUtils.encryptMD5ToString(app_key);
    }
    public static String getCacheMapKey(String app_key){
        return getCacheKey(app_key) + XmbOnlineConfigAgent.CACHE_KEY_MAP_END;
    }

    /**
     * 获取在线参数的app_key<br>
     */
    public static String getAppKey(){
        // 优先缓存的app_key，再获取xmb_key的值，没有配置xmb_key则使用友盟的Key
        String cache_app_key = ACacheUtils.getAppCacheValue(APP_KEY_KEY);
        if(StringUtils.isNullStr(cache_app_key)){
            cache_app_key = StringUtils.getValue((String) Gid.getValue(Utils.getApp(), AdSwitchUtils.Vs.xmb_key.getResID()),
                    (String) Gid.getValue(Utils.getApp(), AdSwitchUtils.Vs.um_key.getResID()));
            setAppKey(cache_app_key);
        }
        //if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "app_key="+cache_app_key);
        return cache_app_key;
    }
    public static void setAppKey(String app_key){
        // 缓存app_key，方便定制更改
        ACacheUtils.setAppCacheValue(APP_KEY_KEY, app_key);
        ACacheUtils.setCacheValue(XMBSign.Cjs.app_key.name(), app_key);
    }

    /**
     * 手动更新当前APP在线参数<br>
     */
    public static void updateOnlineConfig(){
        updateOnlineConfig(getAppKey());
    }
    /**
     * 手动更新指定App_Key的在线参数<br>
     *     1.缓存时间在线更改时，上次的缓存未失效时继续有效，此情况忽略新缓存时间<br>
     */
    public static void updateOnlineConfig(final String app_key){
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, app_key+"-->updateOnlineConfig is begin..."+ AdSwitchUtils.Vs.xmb_cache_time);

        //2019/08/20 缓存为空时有两种情况（一网络异常、二服务器无参数）
        if(isExpire(app_key)) {//缓存是否已到期、错误请求是否已到期
            CrashApp.networkThread(new Runnable() {
                public void run() {
                    XMBApi.updateOnlineConfig(null, app_key);
                    if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, app_key+"-->updateOnlineConfig is call success..."+ AdSwitchUtils.Vs.xmb_cache_time);
                }
            });
        }
    }

    /**
     * 通过key获取在线参数<br>
     */
    public static String getConfigParams(String key){
        String value = null;
        if(StringUtils.noNullStr(key)){
            value = getConfigParams(getAppKey(), key);
        }
        return value;
    }
    public static String getConfigParams(String app_key, String key){
        String value = null;
        try {
            value = ary2Map(app_key).get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
    public static String getConfigParams(ArrayList<OnlineConfigBean> list, String key){
        String value = null;
        try {
            value = ary2Map(list).get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取在线参数的缓存时间<br>
     *     调试模式下可配置3分钟以下，否则最小配置3分钟以上才有效<br>
     *     默认缓存时间为2小时<br>
     */
    public static int getXmbCacheTime(ArrayList<OnlineConfigBean> list){
        int time = Mtas.defXmbCacheTime;
        try {
            // 获取缓存时间
            time = Integer.parseInt(getConfigParams(list, AdSwitchUtils.Vs.xmb_cache_time.getKey()));
            time = getRightCacheTime(time);  //检测缓存时间的合法性
        } catch (Exception e) {
            //e.printStackTrace();
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "xmb_cache_time="+time);
        return time;
    }
    public static int getXmbCacheTime(String app_key){
        int time = Mtas.defXmbCacheTime;
        try {
            // 获取缓存时间
            time = Integer.parseInt(getConfigParams(app_key, AdSwitchUtils.Vs.xmb_cache_time.getKey()));
            time = getRightCacheTime(time);  //检测缓存时间的合法性
        } catch (Exception e) {
            //e.printStackTrace();
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "xmb_cache_time="+time);
        return time;
    }
    private static int getRightCacheTime(int time){
        int r = time;
        if(!BaseUtils.getIsDebug(Utils.getApp()) && r < XMB_ERROR_CACHE_TIME){ //非调试模式下，小于3分钟默认设置为半小时
            r = Mtas.defXmbCacheTime;
        }else if(r < 0){
            r = Mtas.defXmbCacheTime;
        }

        // 更新缓存时间
        AdSwitchUtils.Vs.xmb_cache_time.value = r+"";
        return r;
    }

    /**
     * 判断缓存是否到期，距离最近更新时间的间隔秒数大于缓存时间<br>
     */
    public static boolean isCacheExpire(String app_key){
        // 间隔大于缓存时间，判断为已到期
        long gap = (System.currentTimeMillis() - getLastUpdateTime(app_key, LAST_UPDATE_KEY));   //间隔时间的毫秒数
        try {
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "gap="+(gap/1000)+"/"+getXmbCacheTime(app_key)+","+ StringUtils.concat("-",
                    DateUtils.getStringByFormat(new Date(System.currentTimeMillis()), DateUtils.dateFormatYMDHMSS),
                    DateUtils.getStringByFormat(new Date(getLastUpdateTime(app_key, LAST_UPDATE_KEY)), DateUtils.dateFormatYMDHMSS)
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (gap/1000) > getXmbCacheTime(app_key);
    }
    public static boolean isErrorExpire(String app_key){
        // 间隔大于缓存时间，判断为已到期
        long gap = (System.currentTimeMillis() - getLastUpdateTime(app_key, LAST_UPDATE_ERROR_KEY));//间隔时间的毫秒数
        try {
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "gap="+(gap/1000)+"/"+XMB_ERROR_CACHE_TIME+","+ StringUtils.concat("-",
                    DateUtils.getStringByFormat(new Date(System.currentTimeMillis()), DateUtils.dateFormatYMDHMSS),
                    DateUtils.getStringByFormat(new Date(getLastUpdateTime(app_key, LAST_UPDATE_ERROR_KEY)), DateUtils.dateFormatYMDHMSS)
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (gap/1000) > XMB_ERROR_CACHE_TIME;
    }
    public static boolean isExpire(String app_key){
        return isCacheExpire(app_key) && isErrorExpire(app_key); //缓存是否已到期、错误请求是否已到期
    }
    public static long getLastUpdateTime(String app_key, String key){
        long currentTimeMillis = 0;
        try {
            String tKey = getLastUpdateTimeKey(app_key, key);
            currentTimeMillis = Long.parseLong(ACacheUtils.getAppCacheValue(tKey));
        } catch (NumberFormatException e) {
            //e.printStackTrace();
        }
        return currentTimeMillis;
    }
    public static String getLastUpdateTimeKey(String app_key, String key){
        //时间key为：app_key-key-->MD5
        return EncryptUtils.encryptMD5ToString(StringUtils.concat("-",app_key,key));
    }

    /**
     *  判断是否提前加载xmb在线参数<br>
     * @return false：不提前加载xmb在线参数、true：提前加载xmb在线参数（包括没同意隐私政策前）<br>
     */
    public static boolean isPreloadXmbConfig(){
        boolean isOK = false;
        try {
            // 0或false：不提前加载xmb在线参数、其它：提前加载xmb在线参数（包括没同意隐私政策前）
            String v = Utils.getApp().getString(R.string.preload_xmb_config);
            isOK = !("0".equalsIgnoreCase(v) || "false".equalsIgnoreCase(v));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isOK;
    }
    public static boolean noPreloadXmbConfig(){
        return !isPreloadXmbConfig();
    }
}