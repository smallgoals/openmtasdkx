package android.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.nb.utils.NbWebviewUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;

import java.io.InputStream;

/**
 * 声明
 */
@Deprecated
public class AgreementUI extends BaseAppCompatActivity {
    public static final int REQUEST_CODE = 1010;
    protected WebView wv;
    protected String url = "privacy_policy.html";
    protected String defEncoding = "UTF-8";

    public static void startForResult(Activity act, String url) {
        Intent it = new Intent(act, AgreementUI.class);
        it.putExtra("url", url);
        act.startActivityForResult(it, REQUEST_CODE);
    }

    public static void start(Activity act, String url) {
        Intent it = new Intent(act, AgreementUI.class);
        it.putExtra("url", url);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_agreement_ui);
        hideActionBar();

        if (AppUtils.hasAgreePrivacyPolicy()) {
            setFullScreen();
            next();
            return;
        }

        String value = getIntent().getStringExtra("url");
        if (!StringUtils.isNullStr(value)) {
            url = value;
        }

        wv = findViewById(R.id.wv);
//        wv.loadUrl(getString(R.string.url_agreement));
        wv.getSettings().setDefaultTextEncodingName(defEncoding);
        NbWebviewUtils.closeWebviewBug(wv.getSettings());
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        /*wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view,
                                                              WebResourceRequest request) {
                return super.shouldInterceptRequest(view, request);
            }
        });*/
        wv.loadDataWithBaseURL(null, readAssetsHTML(url), "text/html", defEncoding, null);
    }

    public void onClickExit(View v) {
        setResult(Activity.RESULT_CANCELED);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        BaseUtils.onExit(this);
    }

    public void onClickAgree(View v) {
        Spu.saveV(getActivity(), Spu.VSpu.agreement.name(), "suc");
        next();
    }

    /**
     * 读取assets下的txt文件，返回utf-8 String
     *
     * @param fileName 不包括后缀
     * @return
     */
    public String readAssetsHTML(String fileName) {
        try {
            String[] arrayELKeys = getResources().getStringArray(R.array.agreement_el_key);
            String[] arrayELValues = getResources().getStringArray(R.array.agreement_el_value);

            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer, "utf-8");
            for (int i = 0; i < arrayELKeys.length; i++) {
                text = text.replace(arrayELKeys[i], arrayELValues[i]);
            }
            return text;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error:10086";
    }

    public void next() {
//        BaseUtils.gotoMainUI(this, 0,false);
        setResult(Activity.RESULT_OK);
        finish();
    }
}
