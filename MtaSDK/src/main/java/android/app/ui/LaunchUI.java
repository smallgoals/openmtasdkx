package android.app.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.AppUtils;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * @declaration 启动界面<br>
 */
public class LaunchUI extends BaseAppCompatActivity {
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setFullScreen();
		setContentView(R.layout.launch_ui);
		hideActionBar();

		try {
			String versionStr = "V" + AppUtils.getVersionName(this);
			((TextView) findViewById(R.id.tvVersionInfo)).setText(versionStr);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try{
			TextView copyright = (TextView) findViewById(R.id.tvRightInfo);
			String cr = BaseUtils.getRightInfo(getActivity());
			if(cr != null && !"".equals(cr.trim())) copyright.setText(cr);
		}catch(Exception e){e.printStackTrace();};

		boolean isTip = BaseUtils.popPrivateDlgGg(LaunchUI.this, new MyMessageDialog.DialogMethod(){
			public void sure() {
				next();
			}
		});
		if(!isTip) next();
	}

	public void next(){
		if (AppUtils.hasAgreePrivacyPolicy()) {//已同意隐私协议
			BaseUtils.gotoMainUI(this);
		}else {
			BaseUtils.gotoActivity(this, AgreementUI.class.getName(),
					(AdSwitchUtils.Sws.Launch.flag) ? 0 : 500);
		}

	}
}