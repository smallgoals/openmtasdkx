package android.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.Utils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.just.agentweb.AgentWebView;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.ui.vo.FeedBackBean;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.DensityUtils;
import com.nil.sdk.utils.Gid;
import com.nil.sdk.utils.ImgUtil;
import com.nil.sdk.utils.MobileInfoUtil;
import com.nil.sdk.utils.SoftKeyBoardListener;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.scwang.smart.refresh.header.BezierRadarHeader;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XMBApiCallback;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.DataCheckUtils;
import com.xvx.sdk.payment.utils.UserIDUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * @declaration 反馈界面，带反馈列表，支持实时回复<br>
 * 反馈类型：<br>
 */
public class FeedbackUIV2 extends BaseAppCompatActivity implements View.OnClickListener {
    protected final static String TAG = FeedbackUIV2.class.getSimpleName();
    public static final String NEWS_MAX_ID_KEY = "news_max_id_key";
    public static final String NEWS_CONTACT_KEY = "news_contact_key";

    public static void start(Activity act) {
        Intent it = new Intent(act, FeedbackUIV2.class);
        it.putExtra("title", StringUtils.getValue(Gid.getS(act, R.string.fb_title), "意见反馈"));
        BaseUtils.startActivity(act,it);
    }
    public static void start(Activity act, String title) {
        Intent it = new Intent(act, FeedbackUIV2.class);
        it.putExtra("title", title);
        BaseUtils.startActivity(act,it);
    }

    TextView tvSend;
    EditText etContent;
    RecyclerView rv;
    FeedbackV2Adapter mAdapter;
    LayoutInflater mInflater;
    final String newsHaed = "[web_news]";
    RefreshLayout refreshLayout;
    RelativeLayout rlContact;
    TextView tvContact;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_feedback_ui_v2);
        setDisplayHomeAsUpEnabled(true);

        String t = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(t)) setTitle(t);

        mInflater = getLayoutInflater();
        tvSend = findViewById(R.id.tvSend);
        etContent = findViewById(R.id.etContent);
        tvSend.setOnClickListener(this);

        refreshLayout = findViewById(R.id.refreshLayout);
        //设置 Header 为 贝塞尔雷达 样式
        refreshLayout.setRefreshHeader(new BezierRadarHeader(this));

        rv = findViewById(R.id.rv);
        mAdapter = new FeedbackV2Adapter(R.layout.xmta_feedback_news_item, null);
        //设置布局管理器
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(mAdapter);

        buildRefresh();

        SoftKeyBoardListener.setListener(this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                LogUtils.dTag(TAG, "keyBoardShow-->"+height);
                scrollToBottom();
            }

            @Override
            public void keyBoardHide(int height) {
                LogUtils.dTag(TAG, "keyBoardHide-->"+height);
            }
        });

        rlContact = findViewById(R.id.item_contact);
        rlContact.setOnClickListener(this);
        tvContact = findViewById(R.id.tvContact);
        /*if(StringUtils.noNullStr(getContact())){
            tvContact.setText(String.format("联系方式：%s",getContact()));
        }*/
    }

    protected void refresh() {
        reConfigList();
        XMBApi.queryFeedback(new XMBApiCallback<ArrayList<FeedBackBean>>() {
            @Override
            public void onFailure(Call call, Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.finishRefresh();
                        showLoadErrorDialog(getActivity());
                    }
                });
            }
            public void onResponse(final ArrayList<FeedBackBean> ary, Call call, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.finishRefresh();
                        if(mAdapter != null && ary != null) {
                            //添加默认的提示消息：
                            FeedBackBean fb = new FeedBackBean(getResources().getString(R.string.fb_default_prompt_message));
                            fb.setType(FeedbackUI.SenderType.developer.name());
                            ary.add(0, fb);

                            mAdapter.getData().clear();
                            mAdapter.getData().addAll(ary);
                            mAdapter.notifyDataSetChanged();
                            scrollToBottom();
                            cacheLatesNews(ary);

                            FeedbackService.cancelNotification();   //取消通知
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        String contact = getContact();
        if(v.getId() == R.id.tvSend){
            String c = etContent.getText().toString();
            //先检查反馈内容，再检查联系方式
            if(StringUtils.isNullStr(c)){
                BaseUtils.popMyToast(getActivity(), "输入的内容不能为空");
            }else if(DataCheckUtils.isIllegalUTF8(c)) {
                BaseUtils.popMyToast(getActivity(), R.string.fb_illegal_utf8_tip);
            }else if(StringUtils.isNullStr(contact)) {
                BaseUtils.popEditDlg(this, "填写联系信息", "",
                        "电话、邮箱、QQ、微信等","保存","取消",
                        50, new MyMessageDialog.DialogMethod() {
                    @Override
                    public void sure() {}
                    public void sure(Object obj) {
                        if(DataCheckUtils.isIllegalUTF8(obj.toString())){
                            BaseUtils.popMyToast(getActivity(),  R.string.fb_illegal_utf8_tip);
                        }else {
                            ACacheUtils.setCacheValue(NEWS_CONTACT_KEY, (String) obj);
                        }
                    }
                });
            } else {
                etContent.setText("");
                String uContact = String.format("%s/%s",contact,MobileInfoUtil.getInstance(getActivity()).getDeviceCode());
                XMBApi.sendFeedback(uContact, c);

                FeedBackBean fb = new FeedBackBean(c);
                mAdapter.getData().add(fb);
                mAdapter.notifyDataSetChanged();
                scrollToBottom();
            }
        }else if(v.getId() == R.id.item_contact){
            BaseUtils.popEditDlg(this, StringUtils.isNullStr(contact) ? "填写联系信息" : "修改联系信息", UserIDUtils.getStarTel(contact),
                    "电话、邮箱、QQ或微信","保存","取消",
                    50, new MyMessageDialog.DialogMethod() {
                        @Override
                        public void sure() {}
                        public void sure(Object obj) {
                            if(DataCheckUtils.isIllegalUTF8(obj.toString())){
                                BaseUtils.popMyToast(getActivity(),  R.string.fb_illegal_utf8_tip);
                            }else {
                                ACacheUtils.setCacheValue(NEWS_CONTACT_KEY, (String) obj);
                            }
                        }
                    });
        }
    }
    public String getContact(){
        //优先拿用户主动填写的联系方式，若未填写则拿取登录的用户名
        return StringUtils.getValue(ACacheUtils.getCacheValue(NEWS_CONTACT_KEY), UserLoginDb.getUserName(""));
    }

    /**
     * 滚动条自动滚到底部<br>
     */
    public void scrollToBottom(){
        if(mAdapter == null || rv == null) return;
        try {
            int size = mAdapter.getData().size();
            size = (size-1 < 0) ? 0 : size-1;
            rv.smoothScrollToPosition(size);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class FeedbackV2Adapter extends BaseQuickAdapter<FeedBackBean, BaseViewHolder> {
        public FeedbackV2Adapter(int layoutResId, List data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(final BaseViewHolder helper, final FeedBackBean item) {
            ImageView imgUser = helper.getView(R.id.img_user);
            ImageView imgKf = helper.getView(R.id.img_kf);
            TextView tvContent = helper.getView(R.id.tvContent);

            int w = ScreenUtils.getScreenWidth();
            int gap = DensityUtils.dp2px(getActivity(),(38*2+10)+20); // 头像大小38dp，左右间隔5dp，多加间隔20dp
            tvContent.setMaxWidth(w-gap);

            int hide = View.GONE;
            String type = item.getType();
            if(FeedbackUI.SenderType.developer.name().equals(type)){
                imgKf.setVisibility(View.VISIBLE);
                imgUser.setVisibility(hide);
                tvContent.setBackgroundResource(R.drawable.xmta_fb_news_white);
            }else{
                imgKf.setVisibility(hide);
                imgUser.setVisibility(View.VISIBLE);
                tvContent.setBackgroundResource(R.drawable.xmta_fb_news_green);
            }

            String srcContent = item.getContent();
            if(StringUtils.noNullStr(srcContent)) {
                AgentWebView agentWebView = helper.getView(R.id.agent_webview);
                if(srcContent.startsWith(newsHaed)){//网页消息，方便发送图文消息
                    agentWebView.setVisibility(View.VISIBLE);
                    tvContent.setVisibility(hide);

                    String data = srcContent.replace(newsHaed, "");
                    if (StringUtils.noNullStr(data)) {
                        data = data.replace("<img", "<img style=\"max-width:100%;height:auto\"");
                        //agentWebView.loadData(data,  "text/html", "utf-8");
                        agentWebView.loadDataWithBaseURL(null, data, "text/html", "utf-8", null);
                        agentWebView.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                final WebView.HitTestResult result = ((WebView) v).getHitTestResult();
                                int type = result.getType();
                                if (type == WebView.HitTestResult.IMAGE_TYPE) {
                                    MyMessageDialog.popDialog(getActivity(), "保存图片",
                                            "亲，您确定要保存本张图片至相册目录？",
                                            "确定保存", "取消",
                                            new MyMessageDialog.DialogMethod() {
                                                public void sure() {
                                                    String imgurl = result.getExtra();
                                                    ImgUtil.donwloadImg(getActivity(), imgurl);
                                                }
                                            });
                                    return true;
                                }
                                return false;
                            }
                        });
                    } else {
                        BaseUtils.popMyToast(getActivity(), "加载失败，请检查网络后重试！");
                    }
                }else {
                    agentWebView.setVisibility(hide);
                    tvContent.setVisibility(View.VISIBLE);
                    tvContent.setText(item.getContent());
                    /*try {
                        tvContent.setText(Html.fromHtml(item.getContent()));
                        //设置超链接可点击
                        tvContent.setMovementMethod(LinkMovementMethod.getInstance());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    tvContent.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            MyMessageDialog.popDialog(getActivity(), "复制",
                                    "亲，您确定要复制本条消息？",
                                    "确定", "取消",
                                    new MyMessageDialog.DialogMethod() {
                                        public void sure() {
                                            BaseUtils.openCopy(getActivity(), item.getContent(), false);
                                            BaseUtils.popMyToast(getActivity(), "消息复制成功");
                                        }
                                    });
                            return true;
                        }
                    });
                }

                //去掉消息头的内容
                final String copyContent = srcContent.replace(newsHaed,"");
                imgKf.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        etContent.setText(copyContent);
                        return true;
                    }
                });
                imgUser.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        etContent.setText(copyContent);
                        return true;
                    }
                });
            }
        }
    }

    /**
     * 初始化异常的提示对话框<br>
     */
    public void showLoadErrorDialog(Context context) {
        if(context == null) context = Utils.getApp();
        MyMessageDialog.popDialog(context, "初始化失败", "请确认网络畅通后重启应用再试", "关闭");
    }

    protected void buildRefresh() {
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshLayout.autoRefresh();
    }
    public void reConfigList(){
        try {
            KeyboardUtils.hideSoftInput(getActivity());
            mAdapter.getData().clear();
            mAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 判断是否有来自客服（developer）的最新消息
    public static boolean hasLatestNews(ArrayList<FeedBackBean> ary){
        boolean flag = false;
        if(ary != null && !ary.isEmpty()){
            try {
                String v = ACacheUtils.getCacheValue(NEWS_MAX_ID_KEY);
                int cacheID = Integer.parseInt(StringUtils.isNullStr(v) ? "0" : v);
                int curID = FeedBackBean.getMaxID(ary);

                flag = curID > cacheID; //最新获取的ID大于缓存的就表示有最新消息
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "hasLatestNews....(curID/cacheID:"+curID+"/"+cacheID+"-"+flag+")");

                /*if(flag) {
                    ACacheUtils.setCacheValue(NEWS_MAX_ID_KEY, curID+"");
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return flag;
    }
    // 缓存来自客服（developer）最新消息的ID
    public static void cacheLatesNews(ArrayList<FeedBackBean> ary){
        if(ary != null && !ary.isEmpty()){
            try {
                int curID = FeedBackBean.getMaxID(ary);
                ACacheUtils.setCacheValue(NEWS_MAX_ID_KEY, curID+"");
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "cacheLatesNews...."+curID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static void cacheLatesNews(FeedBackBean ary){
        if(ary != null){
            try {
                String v = ACacheUtils.getCacheValue(NEWS_MAX_ID_KEY);
                int cacheID = Integer.parseInt(StringUtils.isNullStr(v) ? "0" : v);
                int curID = Math.max(ary.getID(), cacheID);
                ACacheUtils.setCacheValue(NEWS_MAX_ID_KEY, curID+"");
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "cacheLatesNews...."+curID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}