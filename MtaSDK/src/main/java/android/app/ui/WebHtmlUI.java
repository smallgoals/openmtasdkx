package android.app.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.nb.utils.NbWebviewUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.StringUtils;

import java.io.InputStream;


/**
 * Created by dengz on 2017/11/3.
 */
@Deprecated
public class WebHtmlUI extends BaseAppCompatActivity {
    protected WebView mWebView;
    protected String mTitle;
    protected String mUrl;
    protected String mAssetsFileName = "privacy_policy.html";
    protected String mCharsetName = "UTF-8";

    public static void startWithURL(Context ctx, String title, String url, String charsetName) {
        Intent it = new Intent(ctx, WebHtmlUI.class);
        it.putExtra("mTitle", title);
        it.putExtra("mUrl", url);
        it.putExtra("mCharsetName", charsetName);
        BaseUtils.startActivity(ctx, it);
    }
    public static void startWithURL(Context ctx, String title, String url) {
        startWithURL(ctx,title,url,null);
    }

    public static void startWithAssets(Context ctx, String title, String assetsFileName, String charsetName) {
        Intent it = new Intent(ctx, WebHtmlUI.class);
        it.putExtra("mTitle", title);
        it.putExtra("mAssetsFileName", assetsFileName);
        it.putExtra("mCharsetName", charsetName);
        BaseUtils.startActivity(ctx, it);
    }
    public static void startWithAssets(Context ctx, String title, String assetsFileName) {
        startWithAssets(ctx,title,assetsFileName,null);
    }
    public static void startWithAssets(Context ctx, String title) {
        startWithAssets(ctx,title,null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_web_html_ui);

        try {
            setDisplayHomeAsUpEnabled(true);
            String vv = getIntent().getStringExtra("mTitle");
            if(StringUtils.noNullStr(vv)) {
                mTitle = vv;
                setTitle(mTitle);
            }

            vv = getIntent().getStringExtra("mUrl");
            if(StringUtils.noNullStr(vv)) {
                mUrl = vv;
            }

            vv = getIntent().getStringExtra("mAssetsFileName");
            if(StringUtils.noNullStr(vv)) {
                mAssetsFileName = vv;
            }

            vv = getIntent().getStringExtra("mCharsetName");
            if(StringUtils.noNullStr(vv)) {
                mCharsetName = vv;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        mWebView = findViewById(R.id.webView);
        mWebView.getSettings().setDefaultTextEncodingName(mCharsetName);
        NbWebviewUtils.closeWebviewBug(mWebView.getSettings());
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        /*mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest
                    request) {
                return super.shouldInterceptRequest(view, request);
            }
        });*/

        if (StringUtils.noNullStr(mUrl)) {
            mWebView.loadUrl(mUrl);
        } else if (StringUtils.noNullStr(mAssetsFileName)) {
            mWebView.loadDataWithBaseURL(null, readAssetsHTML(mAssetsFileName), "text/html",
                    mCharsetName, null);
        }
    }


    /**
     * 读取assets下的txt文件，返回utf-8 String
     *
     * @param fileName 不包括后缀
     * @return
     */
    public String readAssetsHTML(String fileName) {
        try {
            String[] arrayELKeys = getResources().getStringArray(R.array.agreement_el_key);
            String[] arrayELValues = getResources().getStringArray(R.array.agreement_el_value);

            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer, "utf-8");
            for (int i = 0; i < arrayELKeys.length; i++) {
                text = text.replace(arrayELKeys[i], arrayELValues[i]);
            }
            return text;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error:10086";
    }

}
