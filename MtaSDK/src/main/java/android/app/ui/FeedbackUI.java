package android.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Keep;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xmb.mta.util.XMBApi;

import java.io.Serializable;

/**
 * @declaration 反馈界面<br>
 * 反馈类型：<br>
 * type:彩票、股票、WiFi、音乐、视频、自拍、铃声、变声器、优惠券、手机管家、综合<br>
 * diy1：反馈、咨询、举报、评论<br>
 */
public class FeedbackUI extends BaseAppCompatActivity {
    private final static String TAG = FeedbackUI.class.getSimpleName();
    public enum FeedbackType {
        // 彩票、股票、WiFi、音乐、视频、自拍、铃声、变声器、优惠券、手机管家、综合、默认、其它
        Lottery,Stock,WiFi,Music,Video,Selfie,Ringtone,VoiceChanger,Coupon,MobilePhoneManager,Integrated,Default,Others;
    }
    public enum FeedbackDiy1 {
        // 反馈、咨询、举报、评论、综合、默认、其它
        Feedback,Consultation,Report,Comment,Integrated,Default,Others;
    }

    public enum SenderType{
        // type：用户方 or 我方
        user,developer;
    }

    @Keep
    public static class FeedbackVo implements Serializable{
        String type;
        String diy1;

        String activityTitle;
        String contentTitle;
        String contentHint;
        String contentValue;
        String contactTitle;
        String contactHint;
        String contactValue;
        String buttonName;

        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
        public String getDiy1() {
            return diy1;
        }
        public void setDiy1(String diy1) {
            this.diy1 = diy1;
        }
        public String getActivityTitle() {
            return activityTitle;
        }
        public void setActivityTitle(String activityTitle) {
            this.activityTitle = activityTitle;
        }
        public String getContentTitle() {
            return contentTitle;
        }
        public void setContentTitle(String contentTitle) {
            this.contentTitle = contentTitle;
        }
        public String getContentHint() {
            return contentHint;
        }
        public void setContentHint(String contentHint) {
            this.contentHint = contentHint;
        }
        public String getContentValue() {
            return contentValue;
        }
        public void setContentValue(String contentValue) {
            this.contentValue = contentValue;
        }
        public String getContactTitle() {
            return contactTitle;
        }
        public void setContactTitle(String contactTitle) {
            this.contactTitle = contactTitle;
        }
        public String getContactHint() {
            return contactHint;
        }
        public void setContactHint(String contactHint) {
            this.contactHint = contactHint;
        }
        public String getContactValue() {
            return contactValue;
        }
        public void setContactValue(String contactValue) {
            this.contactValue = contactValue;
        }
        public String getButtonName() {
            return buttonName;
        }
        public void setButtonName(String buttonName) {
            this.buttonName = buttonName;
        }
        @Override
        public String toString() {
            return "FeedbackVo{" +
                    "activityTitle='" + activityTitle + '\'' +
                    ", contentTitle='" + contentTitle + '\'' +
                    ", contentHint='" + contentHint + '\'' +
                    ", contentValue='" + contentValue + '\'' +
                    ", contactTitle='" + contactTitle + '\'' +
                    ", contactHint='" + contactHint + '\'' +
                    ", contactValue='" + contactValue + '\'' +
                    ", buttonName='" + buttonName + '\'' +
                    '}';
        }
        public FeedbackVo() {
            type = SenderType.user.name(); //FeedbackType.Integrated.name();
            diy1 = FeedbackDiy1.Feedback.name();
            activityTitle = "问题反馈";
            contentTitle = "* 问题描述或建议";
            contentHint = "请在此描述您遇到的问题或建议（必填）";
            contentValue = "";
            contactTitle = "* 联系方式";
            contactHint = "输入手机号、QQ或邮箱（必填）";
            contactValue = "";
            buttonName = "提交";
        }
        public FeedbackVo(String activityTitle) {
            this();
            this.activityTitle = activityTitle;
        }

        public FeedbackVo(String type, String activityTitle) {
            this();
            this.type = type;
            this.activityTitle = activityTitle;
        }

        public FeedbackVo(String type, String diy1, String activityTitle) {
            this();
            this.type = type;
            this.diy1 = diy1;
            this.activityTitle = activityTitle;
        }

        public FeedbackVo(String type, String diy1, String activityTitle, String contentTitle, String contentHint, String contactTitle, String contactHint, String contactValue, String buttonName) {
            this.type = type;
            this.diy1 = diy1;
            this.activityTitle = activityTitle;
            this.contentTitle = contentTitle;
            this.contentHint = contentHint;
            this.contactTitle = contactTitle;
            this.contactHint = contactHint;
            this.contactValue = contactValue;
            this.buttonName = buttonName;
        }
        public FeedbackVo(String type, String diy1, String activityTitle, String contentTitle, String contentHint, String contentValue, String contactTitle, String contactHint, String contactValue, String buttonName) {
            this.type = type;
            this.diy1 = diy1;
            this.activityTitle = activityTitle;
            this.contentTitle = contentTitle;
            this.contentHint = contentHint;
            this.contentValue = contentValue;
            this.contactTitle = contactTitle;
            this.contactHint = contactHint;
            this.contactValue = contactValue;
            this.buttonName = buttonName;
        }
    }

    public static void start(Activity act, String title) {
        Intent it = new Intent(act, FeedbackUI.class);
        it.putExtra(Feedbackvo_Key, new FeedbackVo(title));
        BaseUtils.startActivity(act,it);
    }
    public static void start(Activity act, FeedbackVo feedbackVo) {
        Intent it = new Intent(act, FeedbackUI.class);
        it.putExtra(Feedbackvo_Key, feedbackVo);
        BaseUtils.startActivity(act,it);
    }
    public static void start(Activity act, FeedbackVo feedbackVo, IFeedbackCallback cb) {
        Intent it = new Intent(act, FeedbackUI.class);
        it.putExtra(Feedbackvo_Key, feedbackVo);
        addCallback(cb);
        BaseUtils.startActivity(act,it);
    }
    public static void start(Activity act, FeedbackVo feedbackVo, IFeedbackCallback cb, boolean isFinish) {
        Intent it = new Intent(act, FeedbackUI.class);
        it.putExtra(Feedbackvo_Key, feedbackVo);
        addCallback(cb, isFinish);
        BaseUtils.startActivity(act,it);
    }


    TextView tvFeedbackK, tvRelationK;
    EditText etFeedbackV;
    EditText etRelationV;
    TextView btnSubmit;
    private static IFeedbackCallback feedbackCallback;
    private static boolean sFinish = true;  //true：默认正常提交反馈信息后退出反馈界面
    private static final String split = "##";
    public static final String Callback_Key = "callback_key";
    public static final String Feedbackvo_Key = "feedbackvo_key";
    private FeedbackVo mFeedbackVo;

    public static void addCallback(IFeedbackCallback cb){
        feedbackCallback = cb;
    }
    public static void addCallback(IFeedbackCallback cb, boolean isFinish){
        feedbackCallback = cb;
        sFinish = isFinish;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        feedbackCallback = null;
        sFinish = true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_feedback_ui);

         mFeedbackVo = (FeedbackVo) getIntent().getSerializableExtra(Feedbackvo_Key);
        if(mFeedbackVo == null) mFeedbackVo = new FeedbackVo();

        setTitle(mFeedbackVo.getActivityTitle());
        setDisplayHomeAsUpEnabled(true);

        tvFeedbackK = findViewById(R.id.tvFeedbackK);
        tvRelationK = findViewById(R.id.tvRelationK);

        etFeedbackV = findViewById(R.id.etFeedbackV);
        etRelationV = findViewById(R.id.etRelationV);
        btnSubmit = findViewById(R.id.btnSubmit);

        etFeedbackV.setHint(mFeedbackVo.getContentHint());
        etRelationV.setHint(mFeedbackVo.getContactHint());
        btnSubmit.setText(mFeedbackVo.getButtonName());

        try {
            if(mFeedbackVo.getContentTitle().contains("*")) {
                SpannableString value = new SpannableString(mFeedbackVo.getContentTitle());
                value.setSpan(new ForegroundColorSpan(Color.parseColor("#FF0000")), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvFeedbackK.setText(value);
            }else{
                tvFeedbackK.setText(mFeedbackVo.getContentTitle());
            }

            if(mFeedbackVo.getContactTitle().contains("*")) {
                SpannableString value = new SpannableString(mFeedbackVo.getContactTitle());
                value.setSpan(new ForegroundColorSpan(Color.parseColor("#FF0000")), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvRelationK.setText(value);
            }else{
                tvRelationK.setText(mFeedbackVo.getContactTitle());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String feedback = etFeedbackV.getText().toString();
                    String relation = etRelationV.getText().toString();
                    if (StringUtils.isNullStr(feedback)) {
                        BaseUtils.popMyToast(getActivity(),mFeedbackVo.getContentTitle().replace("* ","")+"不能为空");
                    } else if (StringUtils.isNullStr(relation)) {
                        BaseUtils.popMyToast(getActivity(),mFeedbackVo.getContactTitle().replace("* ","")+"不能为空");
                    } else {
                        String r = relation +split+feedback;
                        if(feedbackCallback == null){
                            AdSwitchUtils.reportError(getActivity(), "NiN投诉建议："+r);
                            XMBApi.sendFeedback(mFeedbackVo.getType(), mFeedbackVo.getDiy1(), relation, feedback);
                            BaseUtils.popMyToast(getActivity(),"感谢您提交的有效信息，处理好后会第一时间通知您哦.");

                            XMBApi.queryFeedback(null);
                        }else {
                            feedbackCallback.handleFeedback(getActivity(), relation, feedback);
                            feedbackCallback.handleFeedback(getActivity(), r);
                        }
                        if(sFinish) finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    //BaseUtils.popMyToast(getActivity(),"信息提交异常，请检查网络后重试.");
                    BaseUtils.popMyToast(getActivity(),"Done");
                    if(sFinish) finish();
                }
            }
        });
    }

    private interface IFeedbackCallback{
        void handleFeedback(Activity ctx, String content);
        void handleFeedback(Activity ctx, String relation, String feedback);
    }
    public static abstract class DefFeedbackCallback implements IFeedbackCallback{
        public void handleFeedback(Activity ctx, String content){}
        public abstract void handleFeedback(Activity ctx, String relation, String feedback);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}