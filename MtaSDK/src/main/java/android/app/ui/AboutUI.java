package android.app.ui;

import android.os.Bundle;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.ui.BaseAppCompatActivity;

public class AboutUI extends BaseAppCompatActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_from_fragment);
		getSupportFragmentManager()
				.beginTransaction()
				.add(R.id.fragment_container, new AboutFgV4())
				.commit();
	}
}
