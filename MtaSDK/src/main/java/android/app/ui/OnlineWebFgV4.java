package android.app.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.OpenMtaSDK.R;
import com.bd.utils.BdSspWeb;
import com.blankj.utilcode.util.LogUtils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;
import com.nil.sdk.nb.utils.NbWebviewUtils;
import com.nil.sdk.ui.BaseFragmentV4;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;

/**
 * Created by dengz on 2017/11/8.
 */
public class OnlineWebFgV4 extends BaseFragmentV4 {
    protected static final String TAG = OnlineWebFg.class.getSimpleName();
    protected AgentWeb mAgentWeb;
    protected String url = BdSspWeb.sppd;

    public static OnlineWebFgV4 newInstance(String url) {
        OnlineWebFgV4 fragment = new OnlineWebFgV4();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.xmta_online_web_ui, container, false);
        if (getArguments() != null) {
            String value = getArguments().getString("url");
            if (!StringUtils.isNullStr(value)) {
                url = value;
            }
        }
        init(rootView);
        return rootView;
    }

    protected void init(View rootView) {
        FrameLayout frameLayout = rootView.findViewById(R.id.flwv);
        mAgentWeb = AgentWeb.with(getActivity())//传入Activity
                //传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams
                .setAgentWebParent(frameLayout, new LinearLayout.LayoutParams(-1, -1))
                .closeIndicator()
                .setWebChromeClient(mWebChromeClient)
                .setWebViewClient(mWebViewClient)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.ASK)   //打开其他应用时，弹窗咨询用户是否前往其他应用
                .interceptUnkownUrl()   //拦截找不到相关页面的Scheme
                .createAgentWeb()
                .ready()
                .go(url);
        NbWebviewUtils.closeWebviewBug(mAgentWeb);
    }

    protected WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //do you  work
        }
    };
    protected WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            //do you work
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, newProgress + ">>>");
            if (newProgress >= 100) {

            }
        }

        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb != null && mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return false;
    }

    public void onPause() {
        if (mAgentWeb != null) mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();
    }

    public void onResume() {
        if (mAgentWeb != null) mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }

    public void onDestroyView() {
        if (mAgentWeb != null) mAgentWeb.getWebLifeCycle().onDestroy();
        super.onDestroyView();
    }
}