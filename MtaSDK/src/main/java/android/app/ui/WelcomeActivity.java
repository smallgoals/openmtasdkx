package android.app.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.nil.crash.utils.CustomOnCrash;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.vvv.utils.Mtas;
import com.umeng.commonsdk.UMConfigure;
import com.xmb.mta.util.XmbOnlineConfigAgent;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

/**
 * 使用Blankj/AndroidUtilCode的权限申请框架，感觉更简单<br>
 */
public class WelcomeActivity extends AppCompatActivity {
    public Activity getActivity(){return this;}
    private static final String TAG = WelcomeActivity.class.getSimpleName();
    private AlertDialog permissionDialog;
    private String[] permissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFullScreen();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        hideActionBar();
        try {
            permissions = getResources().getStringArray(R.array.xmta_permission_array);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        //判断是否已经同意隐私政策：
        boolean isAgree = AppUtils.hasAgreePrivacyPolicy();
        if (!isAgree) {//未同意隐私政策
            // 使用对话框显示两个协议
            DoubleAgreementActivity.startForResult(this);
        } else {//已经同意隐私政策，不强制权限，去下一步：
//            doGrantPermission();
            next();
        }

        // Xmb在线参数（Xmb在线参数不需要任何权限）
        XmbOnlineConfigAgent.updateOnlineConfig();
    }
    public void hideActionBar(){
        try{
            // 隐藏ActionBar
            ActionBar actionBar = getSupportActionBar();
            if(actionBar != null) {
                actionBar.hide();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setFullScreen(){
        // 全屏设置必须放setContentView(R.layout.xxy)方法之前
        try {
            //去除标题栏
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            //去除状态栏
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DoubleAgreementActivity.REQUEST_CODE) {//双协议Act的返回
            if (resultCode == Activity.RESULT_OK) {
                doGrantPermission();
            } else if (resultCode == Activity.RESULT_CANCELED) {//不同意协议，则退出APP
                BaseUtils.onAutoExit(this); //不同意直接强退
            }
        }
    }

    private void doGrantPermission() {
        //判断是否已经授权：
        boolean isGranted = PermissionUtils.isGranted(permissions);
        if (!isGranted) {
            showWhyNeedPermission();
        } else {//已授权，就跳转：
            next();
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "doGrantPermission is sucess...");
        }
    }

    private void getPermission() {
        @SuppressLint("WrongConstant") PermissionUtils permissionUtils =
                PermissionUtils.permission(permissions);
        permissionUtils.callback(new PermissionUtils.FullCallback() {
            @Override
            public void onGranted(List<String> permissionsGranted) {
                next();
            }

            public void onDenied(List<String> permissionsDeniedForever,
                                 List<String> permissionsDenied) {
                next();
            }
        });
        permissionUtils.request();
    }

    /**
     * 提示为什么需要权限[权限说明]
     */
    private void showWhyNeedPermission() {
        if (permissionDialog != null && permissionDialog.isShowing()) return;

        String permission_describe = getString(R.string.xmta_permission_describe);
        permissionDialog =
                new AlertDialog.Builder(this).setTitle(getString(R.string.xmta_permission_title))
                        .setMessage(permission_describe).setPositiveButton(getString(R.string.xmta_permission_next),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getPermission();
                            }
                        }).setNegativeButton(getString(R.string.xmta_permission_exit),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                next();
                            }
                        }).setCancelable(false).create();
        permissionDialog.show();
    }

    /**
     * 隐私政策和权限申请处理后执行的操作<br>
     * 可以方便子类继承实现一些初始化操作；
     */
    public void next() {
        //隐私合规授权结果上传：https://developer.umeng.com/docs/119267/detail/118637
        UMConfigure.submitPolicyGrantResult(this.getApplicationContext(), true);

        //初始化异常捕获类：
        CustomOnCrash.install(this.getApplicationContext());

        long delayMillis = 0;
        if(XmbOnlineConfigAgent.isExpire(XmbOnlineConfigAgent.getAppKey())) {//已到期
            // Xmb在线参数（Xmb在线参数不需要任何权限）
            XmbOnlineConfigAgent.updateOnlineConfig();
            //初始化广告参数：
            BaseUtils.initAdInfo(getActivity());
            delayMillis = 1000;  //延时1000毫秒，让其获取在线参数
        }

        ThreadUtils.runOnUiThreadDelayed(new Runnable() {
            @Override
            public void run() {
                // Xmb在线参数（Xmb在线参数不需要任何权限）
                XmbOnlineConfigAgent.updateOnlineConfig();
                //初始化广告参数：
                BaseUtils.initAdInfo(getActivity());

                // 友盟正式初始化：
                Mtas.init(getActivity());

                //更新订单状态
                OrderBeanV2.updateOrderBean();

                loadData();
                BaseUtils.gotoMainUI(getActivity());
            }
        }, delayMillis);
    }

    public void loadData() {
        // 子类加载数据实现此接口
    }
}
