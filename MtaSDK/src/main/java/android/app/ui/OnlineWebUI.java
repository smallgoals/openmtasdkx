package android.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.OpenMtaSDK.R;
import com.bd.utils.BdSspWeb;
import com.blankj.utilcode.util.LogUtils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;
import com.nil.sdk.nb.utils.NbWebviewUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;

/**
 * Created by dengz on 2017/11/8.
 */
public class OnlineWebUI extends BaseAppCompatActivity {
    protected static final String TAG = OnlineWebUI.class.getSimpleName();
    protected AgentWeb mAgentWeb;
    protected String url = BdSspWeb.sppd;
    static boolean isAutoFinish = false;

    public static void start(Activity act, String url, boolean isClose) {
        Intent it = new Intent(act, OnlineWebUI.class);
        it.putExtra("url", url);
        BaseUtils.startActivity(act,it);
        isAutoFinish = isClose;
    }
    public static void start(Activity act, String url,String title, boolean isClose) {
        Intent it = new Intent(act, OnlineWebUI.class);
        it.putExtra("url", url);
        it.putExtra("title", title);
        BaseUtils.startActivity(act,it);
        isAutoFinish = isClose;
    }
    public static void start(Activity act, String url) {
        Intent it = new Intent(act, OnlineWebUI.class);
        it.putExtra("url", url);
        BaseUtils.startActivity(act,it);
    }
    public static void start(Activity act, String url,String title) {
        Intent it = new Intent(act, OnlineWebUI.class);
        it.putExtra("url", url);
        it.putExtra("title", title);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_online_web_ui);
        init();
    }

    protected  void init() {
        String value = getIntent().getStringExtra("url");
        if(!StringUtils.isNullStr(value)) {
            url = value;
        }
        String title = getIntent().getStringExtra("title");
        if(!StringUtils.isNullStr(title)) {
            setTitle(title);
            setDisplayHomeAsUpEnabled(true);
        }
        FrameLayout frameLayout = findViewById(R.id.flwv);
        mAgentWeb = AgentWeb.with(getActivity())//传入Activity
                //传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams
                .setAgentWebParent(frameLayout, new LinearLayout.LayoutParams(-1, -1))
                .closeIndicator()
                .setWebChromeClient(mWebChromeClient)
                .setWebViewClient(mWebViewClient)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.ASK)   //打开其他应用时，弹窗咨询用户是否前往其他应用
                .interceptUnkownUrl()   //拦截找不到相关页面的Scheme
                .createAgentWeb()
                .ready()
                .go(url);
        NbWebviewUtils.closeWebviewBug(mAgentWeb);
    }

    protected WebViewClient mWebViewClient = new WebViewClient(){
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //do you  work
        }
    };
    protected WebChromeClient mWebChromeClient = new WebChromeClient(){
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            //do you work
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, newProgress+">>>");
            if(newProgress >= 100){

            }
        }
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb != null && mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    public void onPause() {
        if(mAgentWeb != null) mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();
        if(isAutoFinish) {
            finish();
            isAutoFinish = false;
        }
    }
    public void onResume() {
        if(mAgentWeb != null) mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }
    public void onDestroy() {
        if(mAgentWeb != null) mAgentWeb.getWebLifeCycle().onDestroy();
        super.onDestroy();
        isAutoFinish = false;
    }
}
