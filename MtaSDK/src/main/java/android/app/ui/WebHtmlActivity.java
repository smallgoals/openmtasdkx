package android.app.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.PathUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.nb.utils.NbFileUtils;
import com.nil.sdk.nb.utils.NbWebviewUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyMessageDialog;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * 可以使用EL表达式加载本地和URL的网页 by Deng
 * 2021年12月1日 16:59:22
 */
public class WebHtmlActivity extends BaseAppCompatActivity {
    protected AgentWeb mAgentWeb;
    protected WebView mWebView;
    protected String mTitle;
    protected String mUrl;
    protected String mAssetsFileName = "privacy_policy.html";
    protected static String mCharsetName = "UTF-8";

    /**
     * @param ctx
     * @param haveELKeys 是否有EL表达式
     * @param title Activity的标题
     * @param url 网址
     * @param charsetName HTML的编码
     */
    public static void startWithURL(Context ctx, boolean haveELKeys, String title, String url, String charsetName) {
        Intent it = new Intent(ctx, WebHtmlActivity.class);
        it.putExtra("mTitle", title);
        it.putExtra("mUrl", url);
        it.putExtra("mCharsetName", charsetName);
        it.putExtra("haveELKeys", haveELKeys);
        BaseUtils.startActivity(ctx, it);
    }

    public static void startWithURL(Context ctx, String title, String url) {
        startWithURL(ctx, false, title, url, null);
    }

    /**
     * @param ctx
     * @param haveELKeys 是否有EL表达式
     * @param title Activity的标题
     * @param url 网址
     */
    public static void startWithURL(Context ctx, boolean haveELKeys, String title, String url) {
        startWithURL(ctx, haveELKeys, title, url, null);
    }

    public static void startWithAssets(Context ctx, String title, String assetsFileName, String charsetName) {
        Intent it = new Intent(ctx, WebHtmlActivity.class);
        it.putExtra("mTitle", title);
        it.putExtra("mAssetsFileName", assetsFileName);
        it.putExtra("mCharsetName", charsetName);
        BaseUtils.startActivity(ctx, it);
    }

    public static void startWithAssets(Context ctx, String title, String assetsFileName) {
        startWithAssets(ctx, title, assetsFileName, mCharsetName);
    }

    public static void startWithAssets(Context ctx, String title) {
        startWithAssets(ctx, title, mCharsetName);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_web_html_ui);

        boolean haveELKeys = getIntent().getBooleanExtra("haveELKeys", false);

        try {
            setDisplayHomeAsUpEnabled(true);
            String vv = getIntent().getStringExtra("mTitle");
            if (StringUtils.noNullStr(vv)) {
                mTitle = vv;
                setTitle(mTitle);
            }

            vv = getIntent().getStringExtra("mUrl");
            if (StringUtils.noNullStr(vv)) {
                mUrl = vv;
            } else {
                String u = AdSwitchUtils.getInstance(this).getOnlineValue(AdSwitchUtils.Vs.privacy_policy_url.name(), R.string.privacy_policy_url);
                if (getString(R.string.privacy_policy_title).equals(mTitle)
                        && StringUtils.noNullStr(u)) {
                    mUrl = u;
                    haveELKeys = StringUtils.containsIgnoreCase(u, "haveELKeys=true");
                }

                u = AdSwitchUtils.getInstance(this).getOnlineValue(AdSwitchUtils.Vs.user_agreement_url.name(), R.string.user_agreement_url);
                if (getString(R.string.user_agreement_title).equals(mTitle)
                        && StringUtils.noNullStr(u)) {
                    mUrl = u;
                    haveELKeys = StringUtils.containsIgnoreCase(u, "haveELKeys=true");
                }
            }

            vv = getIntent().getStringExtra("mAssetsFileName");
            if (StringUtils.noNullStr(vv)) {
                mAssetsFileName = vv;
            }

            vv = getIntent().getStringExtra("mCharsetName");
            if (StringUtils.noNullStr(vv)) {
                mCharsetName = vv;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mWebView = findViewById(R.id.webView);
        mWebView.getSettings().setDefaultTextEncodingName(mCharsetName);
        NbWebviewUtils.closeWebviewBug(mWebView.getSettings());

        if (StringUtils.noNullStr(mUrl)) {
            if (haveELKeys) {
                CrashApp.networkThread(new Runnable() {
                    @Override
                    public void run() {
                        String html = getHtmlByURL(mUrl);
                        final String finalHtml = replaceAllELKeys(html);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mWebView.loadDataWithBaseURL(null, finalHtml, "text/html",
                                        mCharsetName, null);
                            }
                        });
                    }
                });
            } else {
                FrameLayout frameLayout = findViewById(R.id.flwv);
                mAgentWeb = AgentWeb.with(getActivity())//传入Activity
                        //传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams
                        .setAgentWebParent(frameLayout, new LinearLayout.LayoutParams(-1, -1))
                        .closeIndicator()
                        .setWebChromeClient(mWebChromeClient)
                        .setWebViewClient(mWebViewClient)
                        .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.ASK)   //打开其他应用时，弹窗咨询用户是否前往其他应用
                        .interceptUnkownUrl()   //拦截找不到相关页面的Scheme
                        .createAgentWeb()
                        .ready()
                        .go(mUrl);
                NbWebviewUtils.closeWebviewBug(mAgentWeb);
            }
        } else if (StringUtils.noNullStr(mAssetsFileName)) {
            mWebView.loadDataWithBaseURL(null, readAssetsHTML(mAssetsFileName), "text/html",
                    mCharsetName, null);
        }

        //导出Html：
        findViewById(R.id.tv_export_html).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ThreadUtils.executeByIo(new ThreadUtils.Task<File>() {
                    @Override
                    public File doInBackground(){
                        File file;
                        try {
                            String afn = mAssetsFileName;
                            if(StringUtils.containsIgnoreCase(afn, "privacy_policy")){
                                afn = ".html";
                            } else {
                                afn = "_" + mAssetsFileName;
                            }
                            String filePath = PathUtils.getExternalAppFilesPath() + File.separator
                                    + AppUtils.getAppPackageName() + afn;
                            String data = readAssetsHTML(mAssetsFileName);
                            file = new File(filePath);
                            if (!file.exists()) {
                                file.getParentFile().mkdir();
                                file.createNewFile();
                            }

                            OutputStreamWriter otr = new OutputStreamWriter(new FileOutputStream(file, false), mCharsetName);
                            BufferedWriter bw = new BufferedWriter(otr);
                            bw.write(data);
                            bw.close();
                        }catch (Exception e){
                            file = null;
                            e.printStackTrace();
                        }
                        return file;
                    }
                    public void onSuccess(final File file) {
                        if(file == null){
                            ToastUtils.showLong("替换后的Html文件生成失败！");
                        } else {
                            final String filePath = file.getAbsolutePath();
                            MyMessageDialog.popDialog(Utils.getApp(), "Html文件导出成功", NbFileUtils.getFileAttribute(filePath),
                                    "分享文件", "查看文件", "关闭", new MyMessageDialog.DialogMethod() {
                                @Override
                                public void sure() {
                                    NbFileUtils.shareFile(file, "text/html");
                                }
                                public void neutral(){
                                    NbFileUtils.openFile(file, "text/html");
                                }
                            }, 16f).setCancelable(false);
                        }
                    }
                    public void onCancel() {}
                    public void onFail(Throwable t) {}
                });

                return true;
            }
        });
    }

    /**
     * 读取assets下的txt文件，返回utf-8 String<br>
     * @param fileName 不包括后缀<br>
     */
    public String readAssetsHTML(String fileName) {
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer, mCharsetName);
            text = replaceAllELKeys(text);
            return text;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error:10086";
    }

    private String replaceAllELKeys(String src) {
        try {
            String[] arrayELKeys = getResources().getStringArray(R.array.agreement_el_key);
            String[] arrayELValues = getResources().getStringArray(R.array.agreement_el_value);
            for (int i = 0; i < arrayELKeys.length; i++) {
                src = src.replace(arrayELKeys[i], arrayELValues[i]);
            }
            return src;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error:显示失败，请重试！";
    }

    public static String getHtmlByURL(String path) {
        try {
            URL url = null;
            url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(8000);
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() == 200) {
                InputStream inStream = conn.getInputStream();
                byte[] data = read(inStream);
                String html = new String(data, mCharsetName);
                return html;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error:网络错误，请重试！";
    }

    public static byte[] read(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[9999];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1)//读取流中的数据
        {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }

    protected WebViewClient mWebViewClient = new WebViewClient(){
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //do you  work
        }
    };
    protected WebChromeClient mWebChromeClient = new WebChromeClient(){
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            //do you work
        }
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb != null && mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    public void onPause() {
        if(mAgentWeb != null) mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();
    }
    public void onResume() {
        if(mAgentWeb != null) mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }
    public void onDestroy() {
        if(mAgentWeb != null) mAgentWeb.getWebLifeCycle().onDestroy();
        super.onDestroy();
    }
}
