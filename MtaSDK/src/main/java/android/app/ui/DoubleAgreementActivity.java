package android.app.ui;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Spu;

public class DoubleAgreementActivity extends AppCompatActivity {
    public Activity getActivity(){return this;}

    public static final int REQUEST_CODE = 1010;
    TextView tvConfirmTip;
    CheckBox cbConfirmTip;

    public static void startForResult(Activity act) {
        Intent it = new Intent(act, DoubleAgreementActivity.class);
        act.startActivityForResult(it, REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_double_agreement);
        hideActionBar();

        TextView tv_title = findViewById(R.id.tv_title);
        BaseUtils.addShowInnerLogV2(tv_title);
        TextView tv_link = findViewById(R.id.tv_link);

        cbConfirmTip = findViewById(R.id.cb_confirm_tip);
        tvConfirmTip = findViewById(R.id.tv_confirm_tip);
        /*-tvConfirmTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSign = cbConfirmTip.isChecked();
                cbConfirmTip.setChecked(!isSign);
            }
        });
        LinearLayout llConfirmTip;
        llConfirmTip = findViewById(R.id.ll_confirm_tip);
        llConfirmTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSign = cbConfirmTip.isChecked();
                cbConfirmTip.setChecked(!isSign);
            }
        });*/

        callService(getString(R.string.double_agreement_link), tv_link);
    }
    public void hideActionBar(){
        try{
            // 隐藏ActionBar
            ActionBar actionBar = getSupportActionBar();
            if(actionBar != null) {
                actionBar.hide();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setFullScreen(){
        // 全屏设置必须放setContentView(R.layout.xxy)方法之前
        try {
            //去除标题栏
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            //去除状态栏
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @param content  文字内容
     * @param textView 加载文字的textview
     */

    private void callService(String content, TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        int i = content.indexOf("《");//截取文字开始的下标
        int i2 = content.indexOf("》", i) + 1;

        int j = content.indexOf("《", i2);
        int j2 = content.indexOf("》", i2)+ 1;

        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.privacy_policy_title),
                        getString(R.string.xmta_assets_html_file_name_privacy_policy));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, i, i2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.user_agreement_title),
                        getString(R.string.xmta_assets_html_file_name_user_agreement));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, j, j2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setHighlightColor(Color.TRANSPARENT); //设置点击后的颜色为透明，否则会一直出现高亮
        textView.setText(builder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    // 双协议：同意
    public void onClickAgree(View v) {
        //勾选同意双协议复选框，才能进入主界面：
        boolean isSign = cbConfirmTip.isChecked();
        if(!isSign){
            // 创建水平缩放抖动动画
            ObjectAnimator animator1 = ObjectAnimator.ofFloat(cbConfirmTip, "scaleX", 0.8f, 0.2f, 1.2f, 0.8f);
            animator1.setInterpolator(new AccelerateInterpolator());
            animator1.setDuration(500);
            // 创建垂直缩放抖动动画
            ObjectAnimator animator2 = ObjectAnimator.ofFloat(cbConfirmTip, "scaleY", 0.8f, 0.2f, 1.2f, 0.8f);
            animator2.setInterpolator(new AccelerateInterpolator());
            animator2.setDuration(500);
            // 创建动画集合，并同时播放两个动画
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animator1, animator2);
            animatorSet.start();

            String msg = String.format("请勾选 %s", tvConfirmTip.getText().toString());
            BaseUtils.popMyToast(getActivity(), msg);
            return;
        }

        Spu.saveV(getActivity(), Spu.VSpu.agreement.name(), "suc");
        setResult(Activity.RESULT_OK);
        finish();
    }

    // 双协议：不同意
    public void onClickExit(View v) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    public void onBackPressed() {
        // 重写此方法，屏蔽按返回键退出
    }
}
