# [OpenMtaSDK/开放统计库](https://bitbucket.org/smallgoals/openmtasdk)

## 库使用步骤
### 引用说明：
To get a Git project into your build:<br>
Step 1. Add the JitPack repository to your build file
Add it in your root build.gradle at the end of repositories:
```
    allprojects {
        repositories {
            maven { url 'https://jitpack.io' }
        }
    }
```

Step 2. Add the dependency
```
    dependencies {
        compile 'org.bitbucket.smallgoals:openmtasdk:9.11.15'
    }
```

### 调用说明：
#### 1.平台ID配置：
.\app\src\main\res\values\ad_string.xml
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="app_name">MtaDemo</string>
    <string name="um_key">5a2cff57f43e485c4a000123</string>
    <string name="um_channel">mtademod_mix</string>

    <string name="xf_key">5a2d00cf</string>
    <string name="main_activity">com.android.MtaDemo.MainActivity</string>
<!--
    <string name="qq_id">1101152570##9079537218417626401##8575134060152130849##5000709048439488$2050206699818455$7030020348049331##8863364436303842593##ID##HF##CP##JFQ##KP@@Gdt$Qq$gdt</string>
    <string name="release_date">2018/07/07</string>
    <string name="gap_days">15</string>
-->
</resources>
```

#### 2.继承调用：
.\app\src\main\java\com\android\MtaDemo\MainActivity.java
```java
public class MainActivity extends BaseAppCompatActivity {}

```
#### 2.1手动调用：
```java
public class MainActivity extends AppCompatActivity {
    public Activity getActivity(){
        return this;
    }
    protected Context context;
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        context = this;
        BaseUtils.onCreate(this);
    }
    public void onBackPressed() {
        BaseUtils.onBackPressed(this);
    }
    protected void onResume() {
        super.onResume();
        BaseUtils.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        BaseUtils.onPause(this);
    }
}
```

#### 3.常用方法介绍：
```java
public class MainActivity extends BaseAppCompatActivity {
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_rate) {//市场好评
            if(!BaseUtils.hasGoodRate(getActivity())){
                BaseUtils.popMyToast(getActivity(), "已经好评了或者好评开关已关！");
            }
            return true;
        }else if(id == R.id.action_appinfo){//应用信息
            MyTipDialog.popNilDialog(getActivity(), BaseUtils.getAppInfo(getActivity()));
            return true;
        }else if(id == R.id.action_crash){//异常捕获测试
            Activity act = null;
            act.runOnUiThread(null);
            return true;
        }else if(id == R.id.action_feedback){//反馈建议
            BaseUtils.gotoFeedbackUI(getActivity());
            return true;
        }else if(id == R.id.action_update){//在线更新
            BaseUtils.checkUpdate(getActivity());
            return true;
        }else if(id == R.id.action_feedback_v2){//在线反馈
             // 启动反馈消息的接收，启动后台服务
             FeedbackService.addNotification();
             
             //清除全部通知，并暂停后台服务
             FeedbackService.cleanNotification();
             return true;
         }
        return super.onOptionsItemSelected(item);
    }
}
```
## 混淆说明：
### 友盟统计分析-混淆：
```
-keep class com.umeng.** {*;}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
```

### 腾讯统计分析-混淆：
```
-keep class com.tencent.stat.*{*;}
-keep class com.tencent.mid.*{*;}
```

### 讯飞统计分析-混淆：
```
-keep class com.iflytek.sunflower.**{*;}
```

### MTA混淆：
```
#【fastjson】
-dontwarn com.alibaba.fastjson.**
-keep class com.alibaba.fastjson.**{*; }
#【okhttp3】
-dontwarn com.squareup.**
-keep class com.squareup.** { *;}
-dontwarn okio.**
#【XMB需要Keep的】
-dontwarn org.apache.**
-keep class org.apache.** {*;}
-dontwarn android.net.**
-keep class android.net.**{*;}

-dontwarn org.conscrypt.**
-keep class org.conscrypt.**{*;}

-dontwarn com.cazaea.*
-keep class com.cazaea.**{*;}

-dontwarn com.umeng.*
-keep class com.umeng.** {*;}
-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class com.tencent.stat.*{*;}
-keep class com.tencent.mid.*{*;}

-dontwarn com.iflytek.sunflower.*
-keep class com.iflytek.sunflower.**{*;}

#【反馈相关实体类-MTA9.6.3】
-dontwarn com.nil.sdk.ui.vo.*
-keep class com.nil.sdk.ui.vo.**{*;}
-dontwarn android.app.ui.FeedbackUI$FeedbackVo
-keep class android.app.ui.FeedbackUI$FeedbackVo{*;}
#【AliPay】
-dontwarn com.xmb.mta.util.*
-keep class com.xmb.mta.util.**{*;}
#【GDT】
-dontwarn com.qq.*
-keep class com.qq.**{*;}
-dontwarn com.android.vy.*
-keep class com.android.vy.**{*;}
#【OpenPaySDK】
-dontwarn com.xvx.sdk.payment.*
-keep class com.xvx.sdk.payment.**{*;}
```

### MTA资源白名单：
```
"R.string.t1_id",
"R.string.t2_id",
"R.string.t3_id",
"R.string.t4_id",
"R.string.t5_id",
"R.string.qq_id",
"R.string.fo_id",
"R.string.op_id",
"R.string.qh_id",
"R.string.xm_id",
"R.string.vo_id",
"R.string.hw_id",
"R.string.al_id",
"R.string.ym_id",
"R.string.xf_id",
"R.string.wp_id",
"R.string.dl_id",
"R.string.az_id",
"R.string.gg_id",
"R.string.du_id",
"R.string.fb_id",
"R.string.cb_id",
"R.string.vg_id",
"R.string.im_id",
"R.string.jz_id",
"R.string.dyd_id",
"R.string.qm_id",
"R.string.kg_id",
"R.string.mix_id",
"R.string.other_id",
"R.string.e1_id",
"R.string.e2_id",
"R.string.e3_id",
"R.string.e4_id",
"R.string.e5_id",
"R.string.end_id",
"R.string.rate_text",
"R.string.notice_text",
"R.string.min_points",
"R.string.point_text",
"R.string.tieba_url",
"R.string.dialog_private",
"R.string.site_list",
"R.string.kw_list",
"R.string.update_url",
"R.string.zc_urls",
"R.string.um_key",
"R.string.ta_key",
"R.string.xf_key",
"R.string.yk_key",
"R.string.zfb_key",
"R.string.wx_key",
"R.string.qq_key",
"R.string.pay_key",
"R.string.xmb_key",
"R.string.nb_web",
"R.string.all_web",
"R.string.iqiyi_web",
"R.string.youku_web",
"R.string.gap_days",
"R.string.release_date",
"R.string.ver_sp",
"R.string.offline",
"R.string.fb_period",
"R.string.c1",
"R.string.c2",
"R.string.c3",
"R.string.c4",
"R.string.c5",
"R.string.c6",
"R.string.c7",
"R.string.c8",
"R.string.c9",
"R.string.c10",
"R.string.all",
"R.string.nb_m3u8",
"R.string.all_m3u8",
"R.string.iqiyi_m3u8",
"R.string.youku_m3u8",
"R.string.exit_rate",
"R.string.o1",
"R.string.o2",
"R.string.o3",
"R.string.o4",
"R.string.o5",
"R.string.o6",
"R.string.o7",
"R.string.o8",
"R.string.o9",
"R.string.o10",
"R.string.open_url",
"R.string.download_url",
"R.string.zfb_url",
"R.string.mta_url",
"R.string.ser_url",
"R.string.data_url",
"R.string.api_url",
"R.string.app_email",
"R.string.app_firm",
"R.string.app_name",
"R.string.app_date",
"R.string.agency_key",
"R.string.xmb_app_id",
"R.string.app_id",
"R.string.alipay_app_id",
"R.string.kf_tel",
"R.string.kf_qq",
"R.string.kf_wx",
"R.string.kf_email",
"R.string.kf_name",
"R.string.kp_gap_second",
"R.string.cp_gap_second",
"R.string.hf_gap_second",
"R.string.end",
```

## 仓库地址
```
https://jitpack.io/#org.bitbucket.smallgoals/openmtasdk/
```

## 更新日志：
### 2.1.5
- 集成友盟、讯飞官方统计SDK；<br>
- 调用案例见app<br>
- 去除库的android:icon="@android:drawable/sym_def_app_icon"配置<br>
- 尝试强制覆盖标签2.1.5<br>

### 2.1.6
- 编写说明文档，添加部分关键代码注释<br>
- Markdown文件首次编写<br>
- 添加图片分享工具方法<br>

### 8.1.29
- 添加账号信息正规验证工具类<br>
- 添加反馈界面支持回调（兼容老反馈弹出对话框方式）<br>

### 8.1.30
- AppCompatActivity支持一键添加返回按钮<br>
- 反馈界面部分调整<br>

### 8.5.10
- 去除部分手机信息获取代码<br>
- 优化反馈界面，支持自定义<br>
- 添加json、okhttp、utilcode工具库<br>

### 8.5.11
- 去除部分手机信息获取代码<br>
- 优化反馈界面，支持自定义<br>
- 添加json、okhttp、utilcode工具库<br>
- 修改反馈联系方式错误提示<br>

### 8.8.21
- 添加Web浏览器的工具类<br>
- 异常捕获时，添加Log.error日志<br>
- 添加启动时的隐私协议界面或手动隐私协议对话框<br>
- 华为渠道默认不开插屏<br>
- 优化XMB数据展示<br>

### 8.8.22
- 添加用户隐私政策工具类<br>
- 支持友盟渠道和包名在线参数，如：openmtasdk_qq_id和com.x.y_qq_id<br>
- 支持友盟在线参数开关alls_close和alls_open，多渠道全关和全开<br>

### 8.8.23
- 优化反馈主题无Actionbar时崩溃<br>
- AppCompatActivity添加去除阴影、全屏、隐藏Actionbar、禁止截屏方法<br>
- BaseUtils添加Log对话框的一键弹出<br>
- 版本号和版本名称配置至gradle.properties，方便多处共用<br>
- gradle.properties|Mtas.java|ad_string.xml存储版本信息<br>

### 8.8.24
- 修复部分bug<br>

### 8.9.11
- String工具类添加部分字符处理方法<br>
- 添加口令红包一键复制<br>

### 8.9.18
- 反馈界面支持定制标题<br>
- 发布日期和编译日期默认取大的<br>
- App采集方法优化<br>
- CrashApp异常捕获支持init(*)进行初始化<br>
- 添加打开网页接口<br>
- 添加下载文件接口<br>
- 添加每日领红包<br>
- 隐私政策界面默认随启动界面开启<br>

### 8.9.22
- Agentweb支持文件下载<br>
- 隐私界面支持应用名称、邮箱、日期、公司在线配置<br>
- 修复上版本下载地址单词拼错的bug<br>
- 优化启动界面的延时处理<br>
- Demo添加一键打包代码<br>

### 8.9.25
- 紧急修复反馈界面IP配置[9.11及以上版本异常]<br>
- 吱口令默认需要配置口令值才开启一键复制<br>

### 8.11.1
- 支持在Xml中配置（代理名称）和（app_id）<br>
- IP修改为https域名https://mtaapi.com<br>
- 添加大数据Get分段方式上报方式<br>
- 优化友盟在线参数获取逻辑<br>
- 添加ACache缓存工具类<br>

### 8.11.8
- 添加org.apache.http.legacy.jar，兼容Android P手机<br>
- 优化广告和参数开关，支持Sp缓存手动开关<br>
- 修复开屏初始化失败时的方法递归调用造成的内存溢出<br>
- 优化异常日志上报和事件发送机制<br>

### 8.11.30
- 修复Android 4.4访问Https异常<br>
- 优化启动界面逻辑<br>

### 9.1.4
- 添加App前后台切换监听，适用于重回App时的弹开屏广告<br>
- 优化日志管理，添加Open、Close、Debug渠道才有日志输出<br>
- 添加调试弹窗：BaseUtils.popDebugToast("消息内容");<br>
- 添加反馈时记录CombinedID;<br>

### 9.5.10
- OpenMtaSDK.build.gradle:compileSdkVersion从27更新至28<br>
- 添加在线实时反馈功能，添加后台服务定时获取反馈列表，便于实时沟通<br>
- 添加SoftKeyBoardListener软键盘监听工具类<br>
- 添加ImgUtil图片工具类<br>
- 优化MobileInfoUtil类中的IMEI和MAC信息的获取<br>

### 9.5.11
- 删除Android-SpinKit动画库，否则JitPack打包失败<br>

### 9.5.20
- 优化通知消息部分bug，兼容tag至28<br>

### 9.6.3
- 关闭FileUriExposure检测<br>
- 添加权限检测和获取工具类<br>
- 修复程序崩溃时的反馈消息循环获取时URL_TEST为空的异常<br>

### 9.7.5
- 更新友盟、讯飞、腾讯分析至最新SDK<br>
- 去除界面中的资源反射代码<br>
- md文件中添加混淆和资源白名单说明<br>
- 部分Gson实体类已添加Keep注解<br>
- 规范命名，添加前缀"xmta_"<br>

### 9.7.18
- 移除“ic_launcher.png”，不再出现应用图标覆盖的问题；
- 移除多余的无关TODO标签；
- 移除库清单中多余的振动权限；

### 9.7.19
- 更新APP隐私政策，可以使用string的array来自定义或者拓展EL表达式的内容，若要拓展：在main项目string.xml覆盖agreement_el_key和agreement_el_value即可;

### 9.7.29
- 启动流程变动，现在是：1.展示隐私政策；2.授权；3.开屏/进入MainActivity;
- WelComeActivity已经加入MTA库中，在APP清单里设置为启动Activity即可;
- 相关配置见：xmat_permission_string.xml;
- 使用方法：
  1. 升级MTA;
  2. 如果需要，修改覆盖assets里的“agreement.html”文件;
  3. 修改覆盖“xmat_permission_string.xml”文件;
  4. 在清单文件里把WelComeActivity设置为启动Activity;

### 9.8.2
- 去除fastjson库的引用;
- WelComeActivity添加next()接口，方便子类覆写；
- 添加@string/test_qq_id的GDT测试ID；

### 9.8.20【已删除】
- 移除讯飞统计分析相关代码和架包;
- 清单文件中只留下必要的权限申明，添加腾讯MTA和Gdt相关provider配置；
- demo中更新GDT至940，测试通过，开屏有部分API变动；
- 添加缓存清理接口：BaseUtils.clearAllCache();
- 获取设备IMEI为空时，生成一个UUID取代之；
- 友盟统计、腾讯统计、在线参数初始化前进行权限判断；
  1. AppUtils.hasUmMTAPermission(ctx);
  2. AppUtils.hasQQMTAPermission(ctx);
  3. AppUtils.hasXmbMTAPermission(ctx);
- 重大更新-XMB在线参数：
  1. string.xml中添加xmb_key作为app_key（xmb_key为空时取um_key）；
  2. 在线参数后台，xmb_cache_time键名存储缓存时间，不设置默认2小时；
  3. XmbOnlineConfigAgent.updateOnlineConfig(); 手动更新Xmb在线参数；
  4. getConfigParams(String key); 通过键名获取在线参数值；
  
### 9.8.28【无XMB加密库】
- 修复上版本权限问题导致GDT广告显示异常的bug；
- 优化XMB在线参数逻辑并添加测试Demo，基本稳定；
- 优化ACacheUtils逻辑，没存储权限只缓存在App内部缓存；
- 删除9.8.20问题标签；
  1. 删除本地标签：在版本Log中的commit记录右键[TAG '9.8.20']选择Delete；
  2. 删除远程标签：git push origin :refs/tags/9.8.20；
  
### 9.9.4【已删除】
- 开始添加XMB加密库，以拷贝so和源码方式，aar方式app引用成功而远程引用失败；
- Application中进行签包验证，并对部分敏感数据进行字符加密处理；
- MtaSDK中的大部分库引用方式由implementation更改为api方式；

### 9.9.5【已删除】
- 在WelcomeActivity的onCreate中进行在线参数的拉取，优化参数获取效率；
- 添加com.android.vy.XSEUtils，兼容以前aar方式的加密库调用；
- 更改app_file_path.xml，添加Gdt950的缓存目录；
- 删除9.9.4本地和远程标签；

### 9.9.11【已删除】
- Cjs中修改app_id为app_type，存储应用类型；
- ValidatorUtil中修复中文正规验证失效的bug；
- NbDataUtils中添加Type类型的序列化；
- 删除9.9.5本地和远程标签；

### 9.9.18
- Cjs中app_id和app_type同时保留，存储应用类型；
- Cjs添加部分登录和用户信息的键名，方便跨SDK存取；

### 9.9.20【卡顿】
- com.nil.sdk.utils.DateUtils.getOffectDay优化日期相减算法；

### 9.9.26【8.0崩溃】
- AdSwitchUtils.initAdInfo()有点耗时，放入线程中执行；
- 添加android:usesCleartextTraffic="true"，支持明文传输；

### 9.10.24【8.0崩溃】
- 根据安管部门需求，（用户协议）与（隐私政策）需分别显示提醒
- 用户协议：user_agreement.html；隐私政策：privacy_policy.html

### 9.10.31【8.0崩溃】
- 修改用户协议和隐私政策html内部标题；

### 9.11.4
- DoubleAgreementActivity清单中去除android:screenOrientation配置；

### 9.11.15 【强制申请权限】
- 更新友盟统计库至8.0.0，基础库至2.0.0；
- 更新com.blankj:utilcode库至1.25.9；
- 更新BaseRecyclerViewAdapterHelper库至2.9.45；
- 支持退出好评（默认关闭，使用erate_open开启）；

### 20.11.20 【Mta和Pay库合一】
- 腾讯移动统计已下线，则移除之；
- 友盟在线参数功能已移入common移动统计SDK，老Jar已删除；
- 双协议同意的BUG，取消强制申请权限；
- 拷贝PaySDK进行整合在一起；
- 库清单文件只保留网络，其它权限均删除；
- 关闭库多余的日志输出；

### 20.11.21
- GDT无权限时广告正常展示；
- 获取Mac、Phone方法进行权限+隐私确定判断；

### 20.12.12
- 友盟、GDT权限抽取到xml中，方便自定义配置；
- WelcomeActivity添加拒绝权限直接进入按钮；
- 优化网络状态检测NetworkUtils.isNetworkAvailable方法；

### 20.12.15
- 更新友盟至9.3.3；
- 更新privacy_policy.html文件；
- 整合OpenMtaSDKX和OpenPaySDKX库；

### 21.1.20
- 修复Webview明文存储密码风险和同源策略绕过漏洞；
- 添加权限申请工具类；

### 21.2.25/21.2.26
- 登录信息：APP内部缓存-->APP内部File；
- 订单信息：SD卡-->APP内部File; 
- ACacheUtils添加APP内部File存储操作方法；
- 查询会员状态，默认以用户方式判断ViP；
- 修复AppUtils.hasPermission报Context为空的bug；
- 清单文件中修改：FileProvider-->GDTFileProvider；
- 添加自动更新AutoUpdateUtil和分享ShareAPP2FriendUtil工具类；
- 登录界面，添加“忘记密码”功能；
- 添加在线参数rate_percent配置百分比概率进行好评功能；
- 添加友盟U-APM，重新集成崩溃捕获SDK；

### 21.3.25
- 修复未同意协议也弹开屏、插屏的bug；
- 关闭onResume中横幅频繁刷新加载；
- 移除Webview风险代码；
- 添加用户注销功能；
- 更新友盟至9.3.7；
- 忘记密码、账号注销添加下划线；
- 去除READ_PHONE_STATE权限；

### 21.5.7
- 更新UMSDK至9.3.8；
- 更新GDT至1203;
- 修复UMCrash: context not instanceof application；
- 更新OpenMtaSDK\build.gradle中远程同步仓库；

### 21.6.8
- 支付宝AAR方式引用有问题，改用解压拆分Jar形式(15.8.03.20210428)；
- GDT二次确认弹窗默认开启，需要关闭时添加vivo@@gdt2_close##(注：debug/open渠道始终有确认弹窗);
- 隐私政策同意之前，禁用GDT、XMB参数、友盟SDK的初始化；
- 修复分享和MyTipDialog在非AppCompat主题中崩溃；

### 21.6.11
- 更新GDT至1240，并更新测试开屏广告位；
- 添加推送广告开关接口ZzAdUtils.closePushAd/openPushAd(默认开启)；
- WelcomeActivity和DoubleAgreementActivity取消继承BaseAppCompatActivity，优化不同意隐私政策的初始化；

### 21.9.26
- 更新GDT至1280，优化SplashUI中广告ID的获取方式；
- 修复未同意隐私政策前，ActivityMonitor读取进程名的bug；
- AdSwitchUtils.Ads添加穿山甲Csj/快手Ks/京东Jd；
- 优化Mtas.checkUpdate调用AutoUpdateUtil.checkUpdate；
- 优化WelcomeActivity获取在线参数延时1秒后进行其它初始化操作；
- 添加Vs.version_limit，在线参数设置最小版本，低于此版本的进入版本限制界面；
- 添加Vs.weal_url/crazy_url，福利链接（onCreate时弹出）、疯狂链接（onResume时弹出）；
- 添加PropertiesUtil工具类，用于读取GDT错误码；

### 21.10.18【已删除】
- 更新GDT至1290；
- 更新友盟至9.4.4；

### 21.10.20
- 移除反馈10分钟拉取一次列表，改为进入反馈界面才进行拉取；

### 21.11.22【已删除】
- 更新GDT至1300/SpinKit至1.4.0/apm至1.5.2；
- 删除模糊的“下次再说”字样；
- 添加cj_apps,cj_mac,cj_imei采集APP、Mac、Imei开关，默认关闭；
- 添加XXPermissions权限工具类；
- 优化多个权限申请时，只要一个有权限成功onGranted就会调用；
- 修复权限永久拒绝后，提示跳转到权限列表手动解除禁止授权；
- 移除欢迎界面至开屏界面间的系统默认动画；

### 21.11.29
- 修复订单支付成功后不提示重启，导致会员没及时生效；

### 22.5.10
- 个人中心和付费界面兼容老年模式；
- 登录、注册、付费不再需要重启APP；
- 同时支持微信支付和支付宝支付，支持开关(zfb/wx)控制；
- 优化ViPConfigVO加载逻辑(在线参数#vip_config>服务器#vip_config.json>raw#vip_config.json)；
- 引入BusUtils进行支付事件的分发；

### 22.6.28
- 修复订单日期格式问题；
- Demo支持配置服务器地址；
- 删除旧广告类改集成AdSDK；
- 优化退出登录逻辑；
- 个人中心添加查看机器码/复制客服QQ/查看系统版本；
- 更新Umeng库至9.5.0、XXPermissions库至15.0；

### 22.8.18
- 更新AgentWeb库至5.0.0、Gson库至2.9.1；
- 更改退出程序为完全退出，并添加完全退出CompleteExit开关；
- 删除清单文件和xml目录中广告相关的信息；
- 添加WebHtmlActivit至清单文件；
- 添加权限中转界面PermissionTransferUI；
- 修改欢迎界面不同意隐私政策也完全退出；

### 22.9.2
- 删除支付宝Jar，使用在线引用；
- 优化PermissionTransferUI配置；
- 完善HookUtils捕获读取安装列表和Android ID；
- 修改ActivityUtils.startActivity调用方式；
- 给废弃的源码文件添加@Deprecated标识；
- 优化隐私政策中的第三方SDK详情；

### 22.9.16
- 替换Hook库为OpenToolSDK库，完善隐私合规检测；
- 优化CustomOnCrash初始化，防止未同意隐私政策前读取安装列表；

### 22.11.22
- 移除涉嫌“捆绑注销”的文字说明；
- OrderBeanV2添加有支付没登录hasPayNoLogin方法；
- UserLoginDb添加getToken方法；
- 个人中心：“升级VIP，尊享特权”随支付开关显示隐藏；
- 用户反馈：登录成功后不需要填联系方式；
- 支付开关未打开时，可以用测试账号admin、123456进行登录（供市场登录使用）；
- 修复深夜模式下，登录、注册的输入框背景和文字颜色都为白色，导致无法看清输入内容；
- 隐私政策/用户登录&注册&注销/购买会员/个人中心添加标题栏（主题为MtaAppTheme）；

### 22.12.14
- 更新友盟SDK至9.5.4；
- 优化隐私政策，添加传感感说明；

### 23.2.10
- 添加云端用户数据增删改查（PayWebAPI.getUserData/updateUserData/deleteUserData）；
- 更新友盟SDK至9.5.6；
- 添加XmbDataSign数据加密类；

### 23.2.23
- 隐私政策双协议弹窗，添加确认已阅读的复选框；
- 扩大用户注册界面中复选框的点击范围；
- 添加fix_sweet_dialog_bug.xml布局；

### 23.3.10
- 用户登录界面，添加我已阅读并同意隐私政策和用户协议的复选框；

### 23.3.18
- 优化用户云端数据接口（查询采用get，增删改采用post）；
- 修改注册、注销、改码使用post方式；
- 发送反馈、事件采用post方式；
- 更新AndroidUtilCode至1.31.1，修复ActivityUtils隐私政策问题；
- 更新XXPermissions至16.8，修复报15.0同步失败的问题；
- 修复blankj启动时未授权就在SD卡/Android/media/下创建包名文件夹；

### 23.4.3
- 添加NbUriUtils，用于兼容blankj的FileProvider被禁用，导致UriUtils.file2Uri失效的bug；
- 优化机器码，IMEI用随机值，MAC用固定值；
- 移除xmta_permission_array里面ACCESS_WIFI_STATE权限；
- 更新libxo.so文件，同时支持签名MD5或MD5冒号的密钥；
- 集成DialogX库，替换SweetAlertDialog为MessageDialog；
- 修改反馈界面的默认标题“在线客服”为“意见反馈”，并移除“在线客服”字样；
- 反馈内容和注销原因，添加屏蔽表情字符；
- 弱化alls_open开关（和渠道debug效果一致），之前的全部开关打开太猛了；
- 添加弹机器码对话框接口：BaseUtils.doCode()；
- 隐私政策和用户协议支持从网页加载；

### 23.8.25
- 双协议对话框、登录和注册界面修改为只点复选框才响应勾选；
- 隐私政策界面添加右上角长按导出替换后的html文件；
- 隐私政策中的${app_name}支持多名称替换（键名为multiple_app_name）；
- 修改反馈界面的首条默认提示（键名为fb_default_prompt_message）；
- 升级DialogX库至0.0.48；
- 修复自动更新，未授存储权限时，强制更新失效；
- 升级友盟SDK至9.6.3、alipaysdk至15.8.16、XXPermissions至18.3；

### 23.12.12
- 注册界面添加验证码，用于实名验证；
- 个人中心、反馈、支付界面的手机号进行脱敏展示；
- 更新友盟SDK至9.6.6，合规优化；
- 验证码添加发送频繁限制；


### 遗留问题：
- 线程策略（TreadPolicy）和VM策略（VmPolicy）全部开启时，程序不崩溃；
- 部分机型出现两次支付成功回调；
- 2023-11-28 15:48:22#AppUtils.hasSDCardPermission在targetSdkVersion>30调用会崩溃；